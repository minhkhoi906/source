
### NOTE: all msg use big endian

import socket
import sys
import time
import os
import json
import threading
import thread
import random

try:
    import thread
except ImportError:
    import _thread as thread



SERVER_ADDRESS = '/tmp/us_xfr'

IPC_MESH_SEND = 0xAA
IPC_MESH_HACK = 0xBB
IPC_MESH_PROV = 0xCC
IPC_MESH_SEQ  = 0xDD

GROUP_STATUS_CONTROL = 0xD000
GROUP_VNG_COMMAND = 0xC002

OPCODE_VNG_COMMAND_COMMON = 0x04FF
OPCODE_VNG_STATUS_CONTROL = 0x05FF
OPCODE_VNG_DOOR_SET = 0x8202
OPCODE_VNG_COMMAND_OTA = 0x06FF
OPCODE_VNG_COMMAND_RENEW = 0x08FF
OPCODE_VNG_COMMAND_RELAY = 0x09FF
OPCODE_VNG_COMMAND_TTL = 0x0AFF
OPCODE_LEVEL_LINE_SET = 0x8207
OPCODE_LEVELS_HUB_SET =  0x9000
OPCODE_LEVELS_GROUP_SET = 0x8207
OPCODE_LEVELS_GROUP_ENA = 0x9001
OPCODE_LEVELS_GROUP_ADD = 0x9002
OPCODE_LEVELS_GROUP_DEL = 0x9003
OPCODE_LEVELS_GROUP_CLR = 0x9004
OPCODE_CFG_RELAY_SET = 0x8027


## PSU
OPCODE_PSU_PORTS_SET = 0x8305
OPCODE_PSU_PORT_SET = 0x8202
OPCODE_PSU_GROUP_ADD = 0x9002
OPCODE_PSU_GROUP_DEL = 0x9003
OPCODE_PSU_GROUP_CLR = 0x9004
OPCODE_PSU_GROUP_SET = 0x8202

MAC_READER_0 = [0xF6, 0xF0, 0x84, 0x6B, 0xF7, 0xEB] #READER (F3)

MAC_READER_1 = [0xE7, 0x0F, 0x7A, 0xA7, 0xE8, 0x5D]
MAC_HUB_F3 = [0xD6, 0xEB, 0x7F, 0x46, 0xC1, 0x98]
MAC_GATEWAY_OR_F3 = [0xFF, 0x76, 0x3E, 0x34, 0xFC, 0x8F]
MAC_DEMO = [0xE9, 0xB9, 0x91, 0xE3, 0xC4, 0xB9]

MAC_DEMO_1 = [0xE1, 0x89, 0x3B, 0x86, 0x41, 0x72]

ADRR_HUB_F3 = 0x7FE7
ADDR_DEMO = 0x0001
ADDR_GATEWAY_OR_F3 = 0x0064
ADDR_PSU_DEMO = 0x0001

GROUP_UNUSED = 0xCFFF
GROUP_LINE_0_1 = 0xC352
GROUP_LINE_2_3 = 0xC353
GROUP_LINE_4_5 = 0xC354
GROUP_LINE_6_7 = 0xC355
GROUP_LINE_0_1_2_3 = 0xC356
GROUP_LINE_4_5_6_7 = 0xC357
GROUP_LINE_ALL = 0xC368
GROUP_LINE_0_1_6_7 = 0xC359
GROUP_LINE_2_3_4_5 = 0xC35A
GROUP_LINE_0_2_4_6 = 0xC35B
GROUP_LINE_1_3_5_7 = 0xC35C


GROUP_HUB_F3 = [GROUP_LINE_0_1, GROUP_LINE_2_3, GROUP_LINE_4_5, GROUP_LINE_6_7]


HUB_TARGET = ADDR_DEMO
# HUB_TARGET = ADRR_HUB_F3
# HUB_TARGET = ADDR_PSU_DEMO

def set_bit(byte, bit):
	byte |= 1 << bit
	return byte

def socket_send(msg):
	sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
	sock.connect(SERVER_ADDRESS)
	sock.sendall(msg)
	sock.close()

def mesh_hack(show):
	message = bytearray(2)
	message[0] = IPC_MESH_HACK
	message[1] = show
	socket_send(message)


def mesh_send(target, opcode, data):
	data_len = len(data)
	message = bytearray(1 + 2 + 2 + 2 + data_len)
	message[0] = IPC_MESH_SEND
	message[1] = (target & 0xFF00) >> 8
	message[2] = target & 0x00FF
	message[3] = (opcode & 0xFF00) >> 8
	message[4] = opcode & 0x00FF
	message[5] = (data_len & 0xFF00) >> 8
	message[6] = data_len & 0x00FF
	message[7:] = data
	socket_send(message)

def mesh_prov(target, seq):
	message = bytearray(6 + 1)
	message[0] = IPC_MESH_PROV
	message[1] = (target & 0xFF00) >> 8
	message[2] = target & 0x00FF
	message[3] = (seq & 0xFF000000) >> 24
	message[4] = (seq & 0x00FF0000) >> 16
	message[5] = (seq & 0x0000FF00) >> 8
	message[6] = (seq & 0x000000FF)
	socket_send(message)

def mesh_seq(seq):
	message = bytearray(4 + 1)
	message[0] = IPC_MESH_SEQ
	message[1] = (seq & 0xFF000000) >> 24
	message[2] = (seq & 0x00FF0000) >> 16
	message[3] = (seq & 0x0000FF00) >> 8
	message[4] = (seq & 0x000000FF)
	socket_send(message)


def button_send(target):
	mesh_send(target, OPCODE_VNG_COMMAND_COMMON, [0x14])

def door_send(target):
	tid = random.randrange(255)
	mesh_send(target, OPCODE_VNG_DOOR_SET, [1, 0, 3, (tid >> 8) & 0xFF, tid& 0xFF])

def command_ota_send(timeout, mac):
	smac = ':'.join('{:02X}'.format(x) for x in mac)
	print "Command ota, timeout {:02d}, mac [{}]".format(timeout, smac)
	data = range(7)

	data[0] = (timeout & 0xFF00) >> 8
	data[1] = timeout & 0x00FF
	data[2:7] = mac

	mesh_send(GROUP_VNG_COMMAND, OPCODE_VNG_COMMAND_OTA, data)
	time.sleep(1)
	mesh_send(GROUP_VNG_COMMAND, OPCODE_VNG_COMMAND_OTA, data)

def command_renew_send(unicast, mac):
	smac = ':'.join('{:02X}'.format(x) for x in mac)
	print "Command renew, unicast 0x{:04x}, mac [{}]".format(unicast, smac)
	
	data = range(7)
	
	data[0] = (unicast & 0xFF00) >> 8
	data[1] = unicast & 0x00FF
	data[2:7] = mac
	mesh_send(GROUP_VNG_COMMAND, OPCODE_VNG_COMMAND_RENEW, data)

def command_relay_send(unicast, enable, retransmit = 3):
	
	print "Command relay set, unicast 0x{:04x}, retransmit [{}]".format(unicast, retransmit)
	
	data = range(2)
	
	data[0] = enable
	data[1] = retransmit

	mesh_send(unicast, OPCODE_VNG_COMMAND_RELAY, data)

def command_ttl_send(unicast, ttl):

	print "Command ttl set, unicast 0x{:04x}, retransmit [{}]".format(unicast, ttl)
	
	data = range(1)
	
	data[0] = ttl

	mesh_send(unicast, OPCODE_VNG_COMMAND_TTL, data)

def levels_hub_send(hub_unicast, level):
	level = (int(0xFFFF)*level/100)
	tid = random.randrange(255)
	time = 0
	delay = 0

	data = range(5)
	data[0] = (level & 0xFF00) >> 8
	data[1] = level & 0x00FF
	data[2] = tid
	data[3] = time
	data[4] = delay

	mesh_send(hub_unicast, OPCODE_LEVELS_HUB_SET, data)


def levels_hub_enableds(hub_unicast, enableds):
	data = bytearray(5)
	data[0] = 0
	data[1] = 0
	data[2] = 0
	data[3] = 0
	data[4] = random.randrange(255)

	for line, enabled in enumerate(enableds):
		if enabled:
			data[3] = set_bit(data[3], line) 

	mesh_send(hub_unicast, OPCODE_LEVELS_GROUP_ENA, data)

def levels_group_send(group, level):
	level = (int(0xFFFF)*level/100)
	tid = random.randrange(255)
	time = 0
	delay = 0

	data = range(5)
	data[0] = (level & 0xFF00) >> 8
	data[1] = level & 0x00FF
	data[2] = tid
	data[3] = time
	data[4] = delay

	mesh_send(group, OPCODE_LEVELS_GROUP_SET, data)


def levels_group_add(hub_unicast, lines, groups):
	
	id = 1
	data = range(1 + lines*2)
	data[0] = 0

	for idx, group in enumerate(groups):
		if group != 0:
			data[0] = set_bit(data[0], idx)
			data[id] = (group & 0xFF00) >> 8
			data[id+1] = (group & 0x00FF)
			id += 2
	sdata = ''.join('{:02X}'.format(x) for x in data)
	print "group add {}".format(sdata)

	mesh_send(hub_unicast, OPCODE_LEVELS_GROUP_ADD, data)

def levels_group_del(hub_unicast, lines, groups):
	id = 1
	data = range(1 + lines*2)
	data[0] = 0

	for idx, group in enumerate(groups):
		if group != 0:
			data[0] = set_bit(data[0], idx)
			data[id] = (group & 0xFF00) >> 8
			data[id+1] = (group & 0x00FF)
			id += 2
	sdata = ''.join('{:02X}'.format(x) for x in data)
	print "group del {}".format(sdata)

	mesh_send(hub_unicast, OPCODE_LEVELS_GROUP_DEL, data)

def levels_group_clr(hub_unicast, lines, groups):
	id = 1
	data = range(1)
	data[0] = 0

	for idx, group in enumerate(groups):
		if group != 0:
			data[0] = set_bit(data[0], idx)

	sdata = ''.join('{:02X}'.format(x) for x in data)
	print "group clr {}".format(sdata)

	mesh_send(hub_unicast, OPCODE_LEVELS_GROUP_CLR, data)

def level_line_send(hub_unicast, line, level):
	level = (int(0xFFFF)*level/100)
	tid = random.randrange(255)
	time = 0
	delay = 0

	data = range(5)
	data[0] = (level & 0xFF00) >> 8
	data[1] = level & 0x00FF
	data[2] = tid
	data[3] = time
	data[4] = delay

	mesh_send(hub_unicast + line, OPCODE_LEVEL_LINE_SET, data)

def cfg_relay_set(target, relay):
	data = bytearray(2)
	data[0] = relay
	data[1] = (3 << 5) | 30 & 0xFF

	mesh_send(target , OPCODE_CFG_RELAY_SET, data)


def status_control(idx, mod, duration, period):
	data = bytearray(8)
	data[0] = (idx & 0xFF00) << 8
	data[1] = (idx & 0x00FF)
	data[2] = (mod & 0xFF00) << 8
	data[3] = (mod & 0x00FF)
	data[4] = (duration & 0xFF00) << 8
	data[5] = (duration & 0x00FF)
	data[6] = (period & 0xFF00) << 8
	data[7] = (period & 0x00FF)

	print "STATUS CONTROL: idx {}, mod {}, duration {}, period {}".format(idx, mod, duration, period)

	mesh_send(GROUP_STATUS_CONTROL, OPCODE_VNG_STATUS_CONTROL, data)


def status_control_old(idx, mod, duration):
	data = bytearray(8)
	data[0] = (idx & 0xFF00) << 8
	data[1] = (idx & 0x00FF)
	data[2] = (mod & 0xFF00) << 8
	data[3] = (mod & 0x00FF)
	data[4] = (duration & 0xFF00) << 8
	data[5] = (duration & 0x00FF)
	
	print "STATUS CONTROL: idx {}, mod {}, duration {}".format(idx, mod, duration)
	mesh_send(GROUP_STATUS_CONTROL, OPCODE_VNG_STATUS_CONTROL, data)

def psu_ports_set(psu_unicast, onoff):
	data = bytearray(2)
	data[0] = onoff
	data[1] = random.randrange(255)

	print "PSU 0x{:04X} SET {}".format(psu_unicast, onoff)

	mesh_send(psu_unicast, OPCODE_PSU_PORTS_SET, data)

def psu_port_set(psu_unicast, port, onoff):
	data = bytearray(2)
	data[0] = onoff
	data[1] = random.randrange(255)

	print "PSU 0x{:04X} SET {}".format(psu_unicast, onoff)
	mesh_send(psu_unicast + port, OPCODE_PSU_PORT_SET, data)

def psu_group_send(group, onoff):
	psu_port_set(group, 0, onoff)

def psu_group_add(psu_unicast, lines, groups):
	levels_group_add(psu_unicast, lines, groups)

def psu_group_del(psu_unicast, lines, groups):
	levels_group_del(psu_unicast, lines, groups)

def psu_group_clr(psu_unicast, lines, groups):
	levels_group_clr(psu_unicast, lines, groups)




def zephyr_init():
	print "ZEPHYR INIT"
	os.system("cd /home/dung/Documents/GIT_REPO/iot/device/zephyr/apps/v13/helper/sender/nrf5/build && ./zephyr/zephyr.exe -bt-dev=hci0")


## HUB DIM 0-100, res = 10
def animation_hub_lines():
	for i in [0, 5, 50, 95]:
		print "SEt HUB LEVEL {:02d}%".format(i)
		levels_hub_send(HUB_TARGET, i)
		time.sleep(0.5)
		status_control_old(0, 1, 2)
		time.sleep(5)

def animation_hub_line():
	levels_hub_send(HUB_TARGET, 0)
	time.sleep(2)
	for i in range(8):
		print "SET LINE ON {:02d}".format(i)
		level_line_send(HUB_TARGET, i, 100)
		time.sleep(2)

	for i in range(8):
		print "SET LINE OFF {:02d}".format(8-i-1)
		level_line_send(HUB_TARGET, 8-i-1, 0)
		time.sleep(2)

def animation_hub_group():
	print "SET HUB OFF"
	levels_hub_send(HUB_TARGET, 0)
	time.sleep(2)

	TRANSITION = 3
	for group in GROUP_HUB_F3:
		print "SET GROUP ON {:04X}".format(group)
		levels_group_send(group, 100)
		time.sleep(2)

	for group in GROUP_HUB_F3:
		print "SET GROUP OFF {:04X}".format(group)
		levels_group_send(group, 0)
		time.sleep(2)

	print "SET GROUP ON {:04X}".format(GROUP_LINE_0_1_2_3)
	levels_group_send(GROUP_LINE_0_1_2_3, 100)
	time.sleep(TRANSITION)
	print "SET GROUP FF {:04X}".format(GROUP_LINE_0_1_2_3)
	levels_group_send(GROUP_LINE_0_1_2_3, 0)
	time.sleep(TRANSITION)

	print "SET GROUP ON {:04X}".format(GROUP_LINE_4_5_6_7)
	levels_group_send(GROUP_LINE_4_5_6_7, 100)
	time.sleep(TRANSITION)
	print "SET GROUP OFF {:04X}".format(GROUP_LINE_4_5_6_7)
	levels_group_send(GROUP_LINE_4_5_6_7, 0)
	time.sleep(TRANSITION)

	print "SET GROUP ON {:04X}".format(GROUP_LINE_0_1_6_7)
	levels_group_send(GROUP_LINE_0_1_6_7, 100)
	time.sleep(TRANSITION)
	print "SET GROUP OFF {:04X}".format(GROUP_LINE_0_1_6_7)
	levels_group_send(GROUP_LINE_0_1_6_7, 0)
	time.sleep(TRANSITION)

	print "SET GROUP ON {:04X}".format(GROUP_LINE_2_3_4_5)
	levels_group_send(GROUP_LINE_2_3_4_5, 100)
	time.sleep(TRANSITION)
	print "SET GROUP OFF {:04X}".format(GROUP_LINE_2_3_4_5)
	levels_group_send(GROUP_LINE_2_3_4_5, 0)
	time.sleep(TRANSITION)

	print "SET GROUP ON {:04X}".format(GROUP_LINE_0_2_4_6)
	levels_group_send(GROUP_LINE_0_2_4_6, 100)
	time.sleep(TRANSITION)
	print "SET GROUP OFF {:04X}".format(GROUP_LINE_0_2_4_6)
	levels_group_send(GROUP_LINE_0_2_4_6, 0)
	time.sleep(TRANSITION)

	print "SET GROUP ON {:04X}".format(GROUP_LINE_1_3_5_7)
	levels_group_send(GROUP_LINE_1_3_5_7, 100)
	time.sleep(TRANSITION)
	print "SET GROUP OFF {:04X}".format(GROUP_LINE_1_3_5_7)
	levels_group_send(GROUP_LINE_1_3_5_7, 0)
	time.sleep(TRANSITION)


	print "SET GROUP ON {:04X}".format(GROUP_LINE_ALL)
	levels_group_send(GROUP_LINE_ALL, 100)
	time.sleep(TRANSITION)
	print "SET GROUP OFF {:04X}".format(GROUP_LINE_ALL)
	levels_group_send(GROUP_LINE_ALL, 0)
	time.sleep(TRANSITION)
	
def animation_psu_lines():

	print "SET PSU ON "
	psu_ports_set(HUB_TARGET, 1)
	time.sleep(2)
	print "SET PSU OFF "
	psu_ports_set(HUB_TARGET, 0)
	time.sleep(5)

def animation_psu_line():
	psu_ports_set(HUB_TARGET, 0)
	time.sleep(2)
	for i in range(8):
		print "SET LINE ON {:02d}".format(i)
		psu_port_set(HUB_TARGET, i, 1)
		time.sleep(2)

	for i in range(8):
		print "SET LINE OFF {:02d}".format(8-i-1)
		psu_port_set(HUB_TARGET, 8-i-1, 0)
		time.sleep(2)

def animation_psu_group():
	print "SET PSU OFF"
	psu_ports_set(HUB_TARGET, 0)
	time.sleep(2)

	TRANSITION = 3
	for group in GROUP_HUB_F3:
		print "SET GROUP ON {:04X}".format(group)
		psu_group_send(group, 1)
		time.sleep(2)

	for group in GROUP_HUB_F3:
		print "SET GROUP OFF {:04X}".format(group)
		psu_group_send(group, 0)
		time.sleep(2)

	print "SET GROUP ON {:04X}".format(GROUP_LINE_0_1_2_3)
	psu_group_send(GROUP_LINE_0_1_2_3, 1)
	time.sleep(TRANSITION)
	print "SET GROUP FF {:04X}".format(GROUP_LINE_0_1_2_3)
	psu_group_send(GROUP_LINE_0_1_2_3, 0)
	time.sleep(TRANSITION)

	print "SET GROUP ON {:04X}".format(GROUP_LINE_4_5_6_7)
	psu_group_send(GROUP_LINE_4_5_6_7, 1)
	time.sleep(TRANSITION)
	print "SET GROUP OFF {:04X}".format(GROUP_LINE_4_5_6_7)
	psu_group_send(GROUP_LINE_4_5_6_7, 0)
	time.sleep(TRANSITION)

	print "SET GROUP ON {:04X}".format(GROUP_LINE_0_1_6_7)
	psu_group_send(GROUP_LINE_0_1_6_7, 1)
	time.sleep(TRANSITION)
	print "SET GROUP OFF {:04X}".format(GROUP_LINE_0_1_6_7)
	psu_group_send(GROUP_LINE_0_1_6_7, 0)
	time.sleep(TRANSITION)

	print "SET GROUP ON {:04X}".format(GROUP_LINE_2_3_4_5)
	levels_group_send(GROUP_LINE_2_3_4_5, 1)
	time.sleep(TRANSITION)
	print "SET GROUP OFF {:04X}".format(GROUP_LINE_2_3_4_5)
	psu_group_send(GROUP_LINE_2_3_4_5, 0)
	time.sleep(TRANSITION)

	print "SET GROUP ON {:04X}".format(GROUP_LINE_0_2_4_6)
	psu_group_send(GROUP_LINE_0_2_4_6, 1)
	time.sleep(TRANSITION)
	print "SET GROUP OFF {:04X}".format(GROUP_LINE_0_2_4_6)
	psu_group_send(GROUP_LINE_0_2_4_6, 0)
	time.sleep(TRANSITION)

	print "SET GROUP ON {:04X}".format(GROUP_LINE_1_3_5_7)
	psu_group_send(GROUP_LINE_1_3_5_7, 1)
	time.sleep(TRANSITION)
	print "SET GROUP OFF {:04X}".format(GROUP_LINE_1_3_5_7)
	psu_group_send(GROUP_LINE_1_3_5_7, 0)
	time.sleep(TRANSITION)


	print "SET GROUP ON {:04X}".format(GROUP_LINE_ALL)
	psu_group_send(GROUP_LINE_ALL, 1)
	time.sleep(TRANSITION)
	print "SET GROUP OFF {:04X}".format(GROUP_LINE_ALL)
	psu_group_send(GROUP_LINE_ALL, 0)
	time.sleep(TRANSITION)


def hub_add_groups():

	#
	# ADD GROUP
	#
	TRANSITION = 0.5
	GROUP = GROUP_LINE_0_1
	levels_group_add(HUB_TARGET, 2, [GROUP, GROUP, 0, 0, 0, 0, 0, 0])
	time.sleep(TRANSITION)
	GROUP = GROUP_LINE_2_3
	levels_group_add(HUB_TARGET, 2, [0, 0, GROUP, GROUP, 0, 0, 0, 0])
	time.sleep(TRANSITION)
	GROUP = GROUP_LINE_4_5
	levels_group_add(HUB_TARGET, 2, [0, 0, 0, 0, GROUP, GROUP, 0, 0])
	time.sleep(TRANSITION)
	GROUP = GROUP_LINE_6_7
	levels_group_add(HUB_TARGET, 2, [0, 0, 0, 0, 0, 0, GROUP, GROUP])
	time.sleep(TRANSITION)
	GROUP = GROUP_LINE_0_1_2_3
	levels_group_add(HUB_TARGET, 4, [GROUP, GROUP, GROUP, GROUP, 0, 0, 0, 0])
	time.sleep(TRANSITION)
	GROUP = GROUP_LINE_4_5_6_7
	levels_group_add(HUB_TARGET, 4, [0, 0, 0, 0, GROUP, GROUP, GROUP, GROUP])
	time.sleep(TRANSITION)
	GROUP = GROUP_LINE_0_1_6_7
	levels_group_add(HUB_TARGET, 4, [GROUP, GROUP, 0, 0, 0, 0, GROUP, GROUP])
	time.sleep(TRANSITION)
	GROUP = GROUP_LINE_2_3_4_5
	levels_group_add(HUB_TARGET, 4, [0, 0, GROUP, GROUP, GROUP, GROUP, 0, 0])
	time.sleep(TRANSITION)
	GROUP = GROUP_LINE_0_2_4_6
	levels_group_add(HUB_TARGET, 4, [GROUP, 0, GROUP, 0, GROUP, 0, GROUP, 0])
	time.sleep(TRANSITION)
	GROUP = GROUP_LINE_1_3_5_7
	levels_group_add(HUB_TARGET, 4, [0, GROUP, 0, GROUP, 0, GROUP, 0, GROUP])
	time.sleep(TRANSITION)

	GROUP = GROUP_LINE_ALL
	levels_group_add(HUB_TARGET, 4, [GROUP, GROUP, GROUP, GROUP, 0, 0, 0, 0])
	time.sleep(TRANSITION)
	GROUP = GROUP_LINE_ALL
	levels_group_add(HUB_TARGET, 4, [0, 0, 0, 0, GROUP, GROUP, GROUP, GROUP])


def hub_del_groups():
	##
	## REMOVE GROUP
	##

	transition = 1

	GROUP = GROUP_LINE_0_1
	levels_group_del(HUB_TARGET, 2, [GROUP, GROUP, 0, 0, 0, 0, 0, 0])
	time.sleep(1)
	GROUP = GROUP_LINE_2_3
	levels_group_del(HUB_TARGET, 2, [0, 0, GROUP, GROUP, 0, 0, 0, 0])
	time.sleep(1)
	GROUP = GROUP_LINE_4_5
	levels_group_del(HUB_TARGET, 2, [0, 0, 0, 0, GROUP, GROUP, 0, 0])
	time.sleep(1)
	GROUP = GROUP_LINE_6_7
	levels_group_del(HUB_TARGET, 2, [0, 0, 0, 0, 0, 0, GROUP, GROUP])
	time.sleep(1)
	GROUP = GROUP_LINE_0_1_2_3
	levels_group_del(HUB_TARGET, 4, [GROUP, GROUP, GROUP, GROUP, 0, 0, 0, 0])
	time.sleep(1)
	GROUP = GROUP_LINE_4_5_6_7
	levels_group_del(HUB_TARGET, 4, [0, 0, 0, 0, GROUP, GROUP, GROUP, GROUP])
	time.sleep(1)
	GROUP = GROUP_LINE_0_1_6_7
	levels_group_del(HUB_TARGET, 4, [GROUP, GROUP, 0, 0, 0, 0, GROUP, GROUP])
	time.sleep(1)
	GROUP = GROUP_LINE_2_3_4_5
	levels_group_del(HUB_TARGET, 4, [0, 0, GROUP, GROUP, GROUP, GROUP, 0, 0])
	time.sleep(1)
	GROUP = GROUP_LINE_0_2_4_6
	levels_group_del(HUB_TARGET, 4, [GROUP, 0, GROUP, 0, GROUP, 0, GROUP, 0])
	time.sleep(1)
	GROUP = GROUP_LINE_1_3_5_7
	levels_group_del(HUB_TARGET, 4, [0, GROUP, 0, GROUP, 0, GROUP, 0, GROUP])
	time.sleep(1)

	GROUP = GROUP_LINE_ALL
	levels_group_del(HUB_TARGET, 4, [GROUP, GROUP, GROUP, GROUP, 0, 0, 0, 0])
	time.sleep(1)
	GROUP = GROUP_LINE_ALL
	levels_group_del(HUB_TARGET, 4, [0, 0, 0, 0, GROUP, GROUP, GROUP, GROUP])


def hub_clr_groups():
	###
	### CLR GROUP
	###
	levels_group_clr(HUB_TARGET, 8, [1, 1, 1, 1, 1, 1, 1, 1])



def psu_add_groups():

	#
	# ADD GROUP
	#
	hub_add_groups()


def psu_del_groups():
	##
	## REMOVE GROUP
	##

	hub_del_groups()


def psu_clr_groups():
	###
	### CLR GROUP
	###
	hub_clr_groups()

def mesh_status_test():
	###
	### MESH_STATUS
	###

	#big_period = mod * period , (every stats control)
	#(hub%5) = 3 -> hub_status = (3*5 -> 3*5 + 5) from received status control

	idx = 0
	mod = 1
	duration = 2
	period = 10
	while True:
		# status_control(idx, mod, duration, period)
		status_control_old(idx, mod, duration)
		idx = (idx + 1)%mod
		time.sleep(period)


def mesh_hub_test():
	###
	### COMMAND
	###	

	# hub_enableds = [0, 0, 0, 0, 0, 0, 0, 0]
	hub_enableds = [1, 1, 1, 1, 1, 1, 1, 1]
	print "SET HUB ENABLEDS (0->7)",  hub_enableds
	levels_hub_enableds(HUB_TARGET, hub_enableds )

	# hub_add_groups()
	# hub_del_groups()
	# hub_clr_groups()

	time.sleep(2)

	levels_hub_send(HUB_TARGET, 5)
	# levels_hub_send(HUB_TARGET, 0)
	# while True:
	# 	animation_hub_lines()
	# 	animation_hub_line()
	# 	animation_hub_group()

def mesh_psu_test():
	###
	### COMMAND
	###	

	psu_add_groups()
	# psu_del_groups()
	# psu_clr_groups()

	time.sleep(2)
	while True:
		# animation_psu_lines()
		# animation_psu_line()
		animation_psu_group()



def hub_test_deployment():

	hub_enableds = [1, 1, 1, 1, 1, 1, 1, 1]
	print "SET HUB ENABLEDS (0->7)",  hub_enableds
	levels_hub_enableds(HUB_TARGET, hub_enableds )
	time.sleep(1)
	hub_clr_groups()
	GROUP = GROUP_UNUSED
	levels_group_add(HUB_TARGET, 4, [GROUP, GROUP, GROUP, GROUP, 0, 0, 0, 0])
	time.sleep(1)
	levels_group_add(HUB_TARGET, 4, [0, 0, 0, 0, GROUP, GROUP, GROUP, GROUP])
	time.sleep(2)
	HUB_TARGET = GROUP_UNUSED
	while True:
		level = 0
		print "SET HUB LEVEL {:02d}%".format(level)
		levels_hub_send(HUB_TARGET, level)
		time.sleep(0.2)
		status_control_old(0, 1, 2)
		time.sleep(5)

		level = 50
		print "SET HUB LEVEL {:02d}%".format(level)
		levels_hub_send(HUB_TARGET, level)
		time.sleep(0.2)
		status_control_old(0, 1, 2)
		time.sleep(5)
		
		level = 100
		print "SET HUB LEVEL {:02d}%".format(level)
		levels_hub_send(HUB_TARGET, level)
		time.sleep(0.2)
		status_control_old(0, 1, 2)
		time.sleep(5)

def mesh_send_thread():
	print "PYTHON THREAD"
	#line 0->7


	###
	### MESH_FUNCTION
	###
	# mesh_hack(1)
	# mesh_hack(0)



	# 
	# mesh_prov(10, 10) 


	# mesh_seq(BUTTON_SEQ + 20)
	# mesh_prov(10, BUTTON_SEQ + 10) 
	# time.sleep(3)

	# mac = [0xE9, 0xB7, 0xD2, 0x1C, 0xE6, 0x3E]
	# command_renew_send(0x02, mac)
	# time.sleep(1)
	# command_renew_send(0x02, mac)
	# command_renew_send(0x02, mac)

	# command_ota_send(30, mac)

	# while True:
	# 	DOOR_TARGET = 0x7ec0
	# 	door_send(DOOR_TARGET)
	# 	door_send(DOOR_TARGET)
	# 	print "OPEN DOOR {:04x}".format(DOOR_TARGET)
	# 	time.sleep(10)

	# while True:

	# 	button_send(0xC000)
	# 	print "BUTTON OPEN DOOR {:04x}".format(BUTTON_TARGET)
	# 	time.sleep(10)

	# mesh_status_test()
	###
	### COMMAND
	###

	# MAC = [0xca, 0x23, 0xab, 0x74, 0x21, 0x0c]

	smallmac = [0xE9, 0xB7, 0xD2, 0x1C, 0xE6, 0x3E]

	command_ota_send(30, smallmac)


	# for i in range(5):
	# 	command_ota_send(69, MAC)
	# 	time.sleep(1)
	# command_renew_send(HUB_TARGET, MAC_HUB_F3)


	# thread.start_new_thread(mesh_hub_test, ())
	# thread.start_new_thread(mesh_status_test, ())
	# thread.start_new_thread(mesh_psu_test, ())

	###
	### HUB DEPLOYMENT TEST
	###
	# hub_test_deployment()



	# command_relay_send(0x7be2, 0, 5)

	# status_control_old(0, 1, 3)
	
	
	# command_ttl_send(0x7be2, 13)



if __name__ == "__main__":
	thread.start_new_thread(mesh_send_thread, ())
	# thread.start_new_thread(zephyr_init, ())
	while True:
		time.sleep(10)
