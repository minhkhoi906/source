/*
 * Copyright (c) 2017 VNG IoT Lab Limited
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/dts-v1/;
#include <nordic/nrf51822.dtsi>

/ {
	model = "NRF51 VNG REMOTE ";
	compatible = "nordic,pca10028-dk", "nordic,nrf51822-qfac",
		     "nordic,nrf51822";

	chosen {
		zephyr,console = &uart0;
		zephyr,sram = &sram0;
		zephyr,flash = &flash0;
	};
};

&gpiote {
	status ="ok";
};

&gpio0 {
	status ="ok";
};

&uart0 {
	current-speed = <115200>;
	status = "ok";
};

&i2c0 {
	status = "ok";
	clock-frequency = <I2C_BITRATE_FAST>;
	sda-pin = <29>;
	scl-pin = <30>;
};


&flash0 {
	/*
	 * For more information, see:
	 * http://docs.zephyrproject.org/devices/dts/flash_partitions.html
	 */
	partitions {
		compatible = "fixed-partitions";
		#address-cells = <1>;
		#size-cells = <1>;

		boot_partition: partition@0 {
			label = "mcuboot";
			reg = <0x00000000 0x0000>;
		};
		slot0_partition: partition@c000 {
			label = "image-0";
			reg = <0x00000000 0x32000>;
		};
		slot1_partition: partition@3e000 {
			label = "image-1";
			reg = <0x00032000 0x00000>;
		};
		scratch_partition: partition@70000 {
			label = "image-scratch";
			reg = <0x00032000 0x00000>;
		};

#if defined(CONFIG_FS_FLASH_STORAGE_PARTITION)
		storage_partition: partition@7a000 {
			label = "storage";
			reg = <0x00032000 0x00006000>;
		};
#endif
	};
};

