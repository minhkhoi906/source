/*
 * Copyright (c) 2017 VNG IoT Lab Limited.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __INC_BOARD_H
#define __INC_BOARD_H

#include <soc.h>


/* hub pins */

#define PWM_PIN   13

#define ADC_PIN	3

#define PIN_BYPASS      16

#define HW_WATCHDOG_PIN	15
#define SW_ENABLE_PIN	14


#endif /* __INC_BOARD_H */
