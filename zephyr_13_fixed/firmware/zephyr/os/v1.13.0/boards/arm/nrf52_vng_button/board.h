/*
 * Copyright (c) 2016-2018 Nordic Semiconductor ASA.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __INC_BOARD_H
#define __INC_BOARD_H

#include <soc.h>
#define PIN_BYPASS 22
#define PIN_LNA 23
#define PIN_PA 24

#define DEVICE_ID 0x1400

//#define SENSOR_GPIO_PIN 6
//#define LED_OPEN_GPIO_PIN 8
//#define LED_CLOSE_GPIO_PIN 9

#define SENSOR_GPIO_PIN 18
#define SENSOR_OPEN_LEVEL 1
#define SENSOR_CLOSE_LEVEL 0
#define LED_OPEN_LEVEL (0)

#define LED_OPEN_GPIO_PIN 16
#define LED_CLOSE_GPIO_PIN 17

#define LED_TIMER_SIGNAL 500

#define BUZZER_PWM_PIN 15


#define CONFIG_GPIO_NRF5_P0_DEV_NAME "GPIO_0"

#endif /* __INC_BOARD_H */
