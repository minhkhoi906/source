/*
 * Copyright (c) 2016-2018 Nordic Semiconductor ASA.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __INC_BOARD_H
#define __INC_BOARD_H

#include <soc.h>
#define PIN_BYPASS 22
#define PIN_LNA 23
#define PIN_PA 24


#define DEVICE_ID 0x0011

#define TUNSTILE_GPIO_PIN 7
#define SENSOR_GPIO_PIN 6

#define TUNSTILE_OPEN_LEVEL (1)
#define TUNSTILE_CLOSE_LEVEL (0)


#define LED_OPEN_GPIO_PIN 8
#define LED_CLOSE_GPIO_PIN 9

#define LED_OPEN_LEVEL (0)

#define PRESENT_OPENING (1)
#define TARGET_OPEN_LEVEL (1)

#define SENSOR_OPEN_LEVEL 1
#define SENSOR_CLOSE_LEVEL 0


#define CONFIG_GPIO_NRF5_P0_DEV_NAME "GPIO_0"

#endif /* __INC_BOARD_H */
