/*
 * Copyright (c) 2016-2018 Nordic Semiconductor ASA.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __INC_BOARD_H
#define __INC_BOARD_H

#include <soc.h>

#define PIN_BUZZER_CTRL       15
#define PIN_LED_RED_CTRL      17
#define PIN_LED_GREEN_CTRL    16

#define PIN_BYPASS     22

#endif /* __INC_BOARD_H */
