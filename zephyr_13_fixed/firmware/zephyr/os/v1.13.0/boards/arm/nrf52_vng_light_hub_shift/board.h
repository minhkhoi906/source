/*
 * Copyright (c) 2017 VNG IoT Lab Limited.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __INC_BOARD_H
#define __INC_BOARD_H

#include <soc.h>


/* hub pins */
#define HUB_LINE01_PIN   18
#define HUB_LINE02_PIN   29
#define HUB_LINE03_PIN   19
#define HUB_LINE04_PIN   30
#define HUB_LINE05_PIN   20
#define HUB_LINE06_PIN   31
#define HUB_LINE07_PIN   17
#define HUB_LINE08_PIN   2

#define HUB_LINE09_PIN   16
#define HUB_LINE10_PIN   3
#define HUB_LINE11_PIN   15
#define HUB_LINE12_PIN   4
#define HUB_LINE13_PIN   14
#define HUB_LINE14_PIN   5
#define HUB_LINE15_PIN   13
#define HUB_LINE16_PIN   6

// #define HUB_FAN_PIN 25


#define HUB_REG_LATCH_PIN 8
#define HUB_REG_CLOCK_PIN 9
#define HUB_REG_DATA_PIN  7


#define PIN_BYPASS 22

#endif /* __INC_BOARD_H */
