/*
 * Copyright (c) 2016-2018 Nordic Semiconductor ASA.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __INC_BOARD_H
#define __INC_BOARD_H

#include <soc.h>

/* board PSU */
#define ONOFF1_GPIO_PIN			11
#define ONOFF1_GPIO_NAME		CONFIG_GPIO_P0_DEV_NAME
#define ONOFF1_GPIO_PIN_PUD		GPIO_PUD_NORMAL

#define ONOFF2_GPIO_PIN			12
#define ONOFF2_GPIO_NAME		CONFIG_GPIO_P0_DEV_NAME
#define ONOFF2_GPIO_PIN_PUD		GPIO_PUD_NORMAL

#define ONOFF3_GPIO_PIN			13
#define ONOFF3_GPIO_NAME		CONFIG_GPIO_P0_DEV_NAME
#define ONOFF3_GPIO_PIN_PUD		GPIO_PUD_NORMAL

#define ONOFF4_GPIO_PIN			14
#define ONOFF4_GPIO_NAME		CONFIG_GPIO_P0_DEV_NAME
#define ONOFF4_GPIO_PIN_PUD		GPIO_PUD_NORMAL

/* Zero detector port */
#define ZDC_GPIO_PIN			15 

/* PA_LNA_BYPASS */
#define BYPASS_PIN				22

#endif /* __INC_BOARD_H */
