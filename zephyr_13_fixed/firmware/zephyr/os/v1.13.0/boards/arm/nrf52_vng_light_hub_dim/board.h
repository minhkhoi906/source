/*
 * Copyright (c) 2017 VNG IoT Lab Limited.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __INC_BOARD_H
#define __INC_BOARD_H

#include <soc.h>


/* hub pins */

#define HUB_PWM01_PIN   11


// #define HUB_PWM01_EN   10
// #define HUB_PWM02_EN   8
// #define HUB_PWM03_EN   6
// #define HUB_PWM04_EN   4
// #define HUB_PWM05_EN   2
// #define HUB_PWM06_EN   30
// #define HUB_PWM07_EN   28
// #define HUB_PWM08_EN   26

#define HUB_ADC01_PIN	3

#define PIN_BYPASS      22

#define HW_WATCHDOG_PIN	20
#define SW_ENABLE_PIN	7


#endif /* __INC_BOARD_H */
