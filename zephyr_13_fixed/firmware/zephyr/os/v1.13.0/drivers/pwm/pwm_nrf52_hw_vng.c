/*
 * Copyright (c) 2018 VNG Corp.
 *
 * Athor: Trung Bui Quang (trungbq@vng.com.vn)
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <soc.h>

#include "pwm.h"

#define SYS_LOG_DOMAIN "pwm/nrf52_hw"
#define SYS_LOG_LEVEL CONFIG_SYS_LOG_PWM_LEVEL
#include <logging/sys_log.h>
#define PWM_NRF52_MAXIMUM 0x0000FFFF
typedef NRF_PWM_Type *NRF_PWM_Type_Ptr;

struct pwm_nrf52_hw_channel {
    struct pwm_nrf52_hw_group *group;
    bool used;
    u8_t pin;
    u32_t period_cycles;
    u32_t pulse_cycles;
};

struct pwm_nrf52_hw_group
{
    NRF_PWM_Type *pwm;
    u32_t period_cycles;
    volatile uint16_t sequences[4];
    struct pwm_nrf52_hw_channel channels[4];
};

static
struct pwm_nrf52_hw_channel *
        pwm_nrf52_hw_channel_find(struct pwm_nrf52_hw_group *groups,
                                  uint8_t pin)
{
    u8_t index;

    for(index = 0; index < 4; index++) {

        struct pwm_nrf52_hw_channel *channel = groups->channels + index;
        if (channel->used == false)
            continue;

        if (channel->pin != pin)
            continue;

        return channel;
    }

    return NULL;
}

static
struct pwm_nrf52_hw_channel *
        pwm_nrf52_hw_channel_alloc(struct pwm_nrf52_hw_group *group,
                                   uint8_t pin)
{
    u8_t index;

    for(index = 0; index < 4; index++) {

        struct pwm_nrf52_hw_channel *channel = group->channels + index;
        if (channel->used == true)
            continue;

        channel->group = group;
        channel->pin = pin;
        channel->used = true;
        NRF_GPIO->DIRSET = BIT(pin);
        NRF_GPIO->OUTSET = BIT(pin);

        return channel;
    }

    return NULL;
}

static
struct pwm_nrf52_hw_channel *
        pwm_nrf52_hw_channel_get(struct pwm_nrf52_hw_group *groups, uint8_t pin)
{
    u8_t index;

    for(index = 0; index < 3; index ++) {
        struct pwm_nrf52_hw_channel *channel;
        channel = pwm_nrf52_hw_channel_find(groups + index, pin);

        if (channel != NULL)
            return channel;
    }

    for(index = 0; index < 3; index ++) {
        struct pwm_nrf52_hw_channel *channel;
        channel = pwm_nrf52_hw_channel_alloc(groups + index, pin);

        if (channel != NULL)
            return channel;
    }

    return NULL;
}

static int pwm_nrf52_hw_group_init(struct pwm_nrf52_hw_group *group,
                                   NRF_PWM_Type *pwm)
{
    u8_t index;

    for(index = 0; index < 4; index++) {
        struct pwm_nrf52_hw_channel *channel = group->channels + index;
        channel->group = group;
        channel->used = false;
    }

    pwm->ENABLE =  (PWM_ENABLE_ENABLE_Enabled << PWM_ENABLE_ENABLE_Pos);

    // configure
    pwm->PRESCALER =  (PWM_PRESCALER_PRESCALER_DIV_1 << PWM_PRESCALER_PRESCALER_Pos);
    pwm->MODE =  (PWM_MODE_UPDOWN_Up << PWM_MODE_UPDOWN_Pos);

    // setup decoder
    pwm->DECODER = (PWM_DECODER_LOAD_Individual << PWM_DECODER_LOAD_Pos) |
                   (PWM_DECODER_MODE_RefreshCount << PWM_DECODER_MODE_Pos);

    // clear shorts
    pwm->SHORTS = 0;

    // clear interrupts
    pwm->INTEN = 0;

    pwm->EVENTS_LOOPSDONE = 0;
    pwm->EVENTS_SEQSTARTED[0] = 0;
    pwm->EVENTS_SEQSTARTED[1] = 0;
    pwm->EVENTS_STOPPED = 0;
    group->period_cycles = PWM_NRF52_MAXIMUM;
    group->pwm = pwm;
    return 0;
}

static int pwm_nrf52_hw_init(struct device *dev)
{

    struct pwm_nrf52_hw_group *groups = dev->driver_data;
    NRF_PWM_Type_Ptr pwms[3] = {NRF_PWM0, NRF_PWM1, NRF_PWM2};

    u8_t index;


    for(index = 0; index < 3; index++) {

        int err = pwm_nrf52_hw_group_init(groups + index, pwms[index]);

        if (err != 0)
            return err;
    }

    SYS_LOG_DBG("init nrf52 hardware pwm device done");

    return 0;
}

static void pwm_nrf52_hw_group_refresh(struct pwm_nrf52_hw_group *group)
{
    u8_t index;
    u8_t uses = 0;
    NRF_PWM_Type *pwm = group->pwm;

    pwm->TASKS_SEQSTART[0] = 0;

    for(index = 0; index < 4; index++) {

        struct pwm_nrf52_hw_channel *channel = group->channels + index;

        if (channel->used == false)
            continue;

        uint32_t sequences = (channel->period_cycles == 0) ? 0 : 
                    channel->pulse_cycles * group->period_cycles /
                                 channel->period_cycles;  
        group->sequences[uses] =  sequences;

        pwm->PSEL.OUT[uses] = (channel->pin << PWM_PSEL_OUT_PIN_Pos) | (PWM_PSEL_OUT_CONNECT_Connected << PWM_PSEL_OUT_CONNECT_Pos);

        SYS_LOG_DBG("channel %u pin %u sequences %u",uses,pin,sequences);
        uses += 1;
    }

    if (uses == 0)
        return;

    for(index = uses; index < 4; index++)
        pwm->PSEL.OUT[index] = 0xFFFFFFFF;

    pwm->SEQ[0].PTR = (uint32_t) group->sequences;
    pwm->SEQ[0].CNT = ((sizeof(group->sequences) / sizeof(uint16_t)) << PWM_SEQ_CNT_CNT_Pos);
    pwm->SEQ[0].REFRESH  = 0;
    pwm->SEQ[0].ENDDELAY = 0;

    pwm->LOOP = (PWM_LOOP_CNT_Disabled << PWM_LOOP_CNT_Pos);

    pwm->SHORTS = PWM_SHORTS_LOOPSDONE_SEQSTART0_Msk;

    pwm->COUNTERTOP = group->period_cycles;

    pwm->EVENTS_STOPPED = 0;

    pwm->TASKS_SEQSTART[0] = 1;

}

static int pwm_nrf52_hw_pin_set(struct device *dev,
                               u32_t pin,
                               u32_t period_cycles,
                               u32_t pulse_cycles)
{


    struct pwm_nrf52_hw_group *groups = dev->driver_data;
    struct pwm_nrf52_hw_channel *channel;
    channel = pwm_nrf52_hw_channel_get(groups, pin);

    if (period_cycles < pulse_cycles) {
        SYS_LOG_ERR("pulse value too hight");
        return -EINVAL;
    }

    if (channel == NULL) {
        SYS_LOG_ERR("Not availabled free channel");
        return -ENOMEM;
    }



    channel->period_cycles = period_cycles;
    channel->pulse_cycles = pulse_cycles;

    if (channel->group->period_cycles > channel->period_cycles)
        channel->group->period_cycles = channel->period_cycles;


        SYS_LOG_INF("pin: %u, period: %u, pulse: %u",
                pin,
                channel->group->period_cycles,
                channel->pulse_cycles);
    pwm_nrf52_hw_group_refresh(channel->group);

    return 0;
}

static int pwm_nrf52_hw_get_cycles_per_sec(struct device *dev,
                                          u32_t pwm,
                                          u64_t *cycles)
{
    struct pwm_config *config;

    config = (struct pwm_config *)dev->config->config_info;

    /* HF timer frequency is derived from 16MHz source and prescaler is 0 */
    *cycles = 16 * 1024 * 1024;
    return 0;
}

static const struct pwm_driver_api pwm_nrf5_sw_drv_api_funcs = {
    .pin_set = pwm_nrf52_hw_pin_set,
    .get_cycles_per_sec = pwm_nrf52_hw_get_cycles_per_sec,
};

static u8_t pwm_nrf52_hw_0_data[sizeof(struct pwm_nrf52_hw_group) * 3];

DEVICE_AND_API_INIT(pwm_nrf5_sw_0,
                    CONFIG_PWM_NRF52_HW_0_DEV_NAME,
                    pwm_nrf52_hw_init,
                    pwm_nrf52_hw_0_data,
                    NULL,
                    POST_KERNEL,
                    CONFIG_KERNEL_INIT_PRIORITY_DEVICE,
                    &pwm_nrf5_sw_drv_api_funcs);
