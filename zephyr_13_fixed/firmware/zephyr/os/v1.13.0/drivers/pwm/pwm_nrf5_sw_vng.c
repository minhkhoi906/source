/*
 * Copyright (c) 2018 VNG Corp.
 *
 * Athor: Trung Bui Quang (trungbq@vng.com.vn)
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <soc.h>

#include "pwm.h"

#define SYS_LOG_DOMAIN "pwm/nrf5_sw"
#define SYS_LOG_LEVEL CONFIG_SYS_LOG_PWM_LEVEL
#include <logging/sys_log.h>
#define PWM_NRF5_MAXIMUM 0x0000FFFF

#define PWM_NRF_PPI(config, index) config->ppis[index]
#define PWM_NRF_GPIOTE(config, index) config->gpiotes[index]
#define PWM_NRF_SET_TASK(config, index) \
    (uint32_t)&(NRF_GPIOTE->TASKS_SET[config->gpiotes[index]])
#define PWM_NRF_CLEAR_TASK(config, index) \
    (uint32_t)&(NRF_GPIOTE->TASKS_CLR[config->gpiotes[index]])

#if defined(CONFIG_SOC_SERIES_NRF51X)
#elif defined(CONFIG_SOC_SERIES_NRF52X)
#define PWM_NRF_EVENT(index)  (uint32_t)&(NRF_TIMER3->EVENTS_COMPARE[index])
#else
#error Not supported SOC series
#endif
struct pwm_nrf5_sw_config {
    u32_t ppis[
 #if defined(CONFIG_BT_CTLR_SW_SWITCH_SINGLE_TIMER)
    1 +
 #endif /* defined(CONFIG_BT_CTLR_SW_SWITCH_SINGLE_TIMER) */

#if !defined(CONFIG_SOC_NRF52840) || (CONFIG_SOC_NRF52840 != 1)
    1 +
#endif /* !defined(CONFIG_SOC_NRF52840) || (CONFIG_SOC_NRF52840 != 1) */

#if !defined(CONFIG_BT_CTLR_GPIO_PA_PIN)
#if !defined(CONFIG_BT_CTLR_GPIO_LNA_PIN)
    2 +
#endif /* !defined(CONFIG_BT_CTLR_GPIO_LNA_PIN) */
#endif /* !defined(CONFIG_BT_CTLR_GPIO_PA_PIN) */

#if !defined(CONFIG_SOC_NRF52840) || (CONFIG_SOC_NRF52840 != 1)
    3 +
#endif /* !defined(CONFIG_SOC_NRF52840) || (CONFIG_SOC_NRF52840 != 1) */
    1];
    u8_t gpiotes[
#ifndef CONFIG_BT_CTLR_PA_LNA_GPIOTE_CHAN
    8
#else
    7
#endif
    ];
};

static const struct pwm_nrf5_sw_config pwm_nrf5_sw_0_config = {
    .ppis = {
     #if defined(CONFIG_BT_CTLR_SW_SWITCH_SINGLE_TIMER)
        7,
     #endif

    #if !defined(CONFIG_SOC_NRF52840)
        13,
    #endif /* !defined(CONFIG_SOC_NRF52840) */

    #if !defined(CONFIG_BT_CTLR_GPIO_PA_PIN)
    #if !defined(CONFIG_BT_CTLR_GPIO_LNA_PIN)
        14,
        15,
    #endif /* !defined(CONFIG_BT_CTLR_GPIO_LNA_PIN) */
    #endif /* !defined(CONFIG_BT_CTLR_GPIO_PA_PIN) */

    #if !defined(CONFIG_SOC_NRF52840)
        16,
        17,
        18,
    #endif /* !defined(CONFIG_SOC_NRF52840) || (CONFIG_SOC_NRF52840 != 1) */

        19
    },
    .gpiotes = {
    #ifndef CONFIG_BT_CTLR_PA_LNA_GPIOTE_CHAN
        0, 1, 2, 3, 4, 5, 6, 7
    #else
    #if CONFIG_BT_CTLR_PA_LNA_GPIOTE_CHAN != 0
        0,
    #endif
    #if CONFIG_BT_CTLR_PA_LNA_GPIOTE_CHAN != 1
        1,
    #endif
    #if CONFIG_BT_CTLR_PA_LNA_GPIOTE_CHAN != 2
        2,
    #endif
    #if CONFIG_BT_CTLR_PA_LNA_GPIOTE_CHAN != 3
        3,
    #endif
    #if CONFIG_BT_CTLR_PA_LNA_GPIOTE_CHAN != 4
        4,
    #endif
    #if CONFIG_BT_CTLR_PA_LNA_GPIOTE_CHAN != 5
        5,
    #endif
    #if CONFIG_BT_CTLR_PA_LNA_GPIOTE_CHAN != 6
        6,
    #endif
    #if CONFIG_BT_CTLR_PA_LNA_GPIOTE_CHAN == 7
    #error Please select another nRF5 GPIO PA/LNA GPIOTE Channel
    #endif
        7
    #endif

    }

};

struct pwm_nrf5_sw_channel {
    struct pwm_nrf5_sw_group *group;
    bool used;
    u8_t pin;
    u32_t main_ppi;
    u32_t duty_ppi;
    volatile uint32_t *main_task;
    volatile uint32_t *duty_task;
    u8_t gpiote;
    u8_t timer;

    u32_t period_cycles;
    u32_t pulse_cycles;
};

typedef struct pwm_nrf5_sw_channel *pwm_nrf5_sw_channel_ptr;

struct pwm_nrf5_sw_group
{
    u8_t timer;
    u32_t ppi;

    u32_t period_cycles;

    pwm_nrf5_sw_channel_ptr channels[
#if defined(CONFIG_SOC_SERIES_NRF51X)
    3
#elif defined(CONFIG_SOC_SERIES_NRF52X)
    4
#endif
    ];
};


struct pwm_nrf5_sw_settings
{
    struct pwm_nrf5_sw_group groups[
        #if defined(CONFIG_SOC_SERIES_NRF51X)
            1
        #elif defined(CONFIG_SOC_SERIES_NRF52X)
            1
        #endif
            ];
    struct pwm_nrf5_sw_channel channels[
        #if defined(CONFIG_SOC_SERIES_NRF51X)
            3
        #elif defined(CONFIG_SOC_SERIES_NRF52X)
            4
        #endif
            ];
};

static int pwm_nrf5_sw_init(struct device *dev)
{
    SYS_LOG_DBG("init nrf5 pwm device");
#if defined(CONFIG_SOC_SERIES_NRF51X)

#elif defined(CONFIG_SOC_SERIES_NRF52X)

    struct pwm_nrf5_sw_settings *settings = dev->driver_data;
    const struct pwm_nrf5_sw_config *config = dev->config->config_info;


    NRF_PPI->CHENCLR = BIT(PWM_NRF_PPI(config, 0)) |
                       BIT(PWM_NRF_PPI(config, 1)) |
                       BIT(PWM_NRF_PPI(config, 2)) |
                       BIT(PWM_NRF_PPI(config, 3)) |
                       BIT(PWM_NRF_PPI(config, 4)) |
                       BIT(PWM_NRF_PPI(config, 5));

    NRF_PPI->CH[PWM_NRF_PPI(config, 0)].EEP = PWM_NRF_EVENT(0);
    NRF_PPI->CH[PWM_NRF_PPI(config, 1)].EEP = PWM_NRF_EVENT(0);
    NRF_PPI->CH[PWM_NRF_PPI(config, 2)].EEP = PWM_NRF_EVENT(1);
    NRF_PPI->CH[PWM_NRF_PPI(config, 3)].EEP = PWM_NRF_EVENT(2);
    NRF_PPI->CH[PWM_NRF_PPI(config, 4)].EEP = PWM_NRF_EVENT(3);
    NRF_PPI->CH[PWM_NRF_PPI(config, 5)].EEP = PWM_NRF_EVENT(4);

    settings->groups[0].period_cycles = PWM_NRF5_MAXIMUM;
    settings->groups[0].ppi = PWM_NRF_PPI(config, 0);
    settings->groups[0].timer = 0;

    settings->groups[0].channels[0] = &settings->channels[0];
    settings->groups[0].channels[1] = &settings->channels[1];
    settings->groups[0].channels[2] = &settings->channels[2];
    settings->groups[0].channels[3] = &settings->channels[3];

    settings->channels[0].group = &settings->groups[0];
    settings->channels[0].used = false;
    settings->channels[0].main_ppi = 0;
    settings->channels[0].duty_ppi = 2;
    settings->channels[0].gpiote = 0;
    settings->channels[0].timer = 1;
    settings->channels[0].main_task = &NRF_PPI->CH[PWM_NRF_PPI(config, 0)].TEP;
    settings->channels[0].duty_task = &NRF_PPI->CH[PWM_NRF_PPI(config, 2)].TEP;

    settings->channels[1].group = &settings->groups[0];
    settings->channels[1].used = false;
    settings->channels[1].main_ppi = 0;
    settings->channels[1].duty_ppi = 3;
    settings->channels[1].gpiote = 1;
    settings->channels[1].timer = 2;
    settings->channels[1].main_task = &NRF_PPI->FORK[PWM_NRF_PPI(config, 0)].TEP;
    settings->channels[1].duty_task = &NRF_PPI->CH[PWM_NRF_PPI(config, 3)].TEP;

    settings->channels[2].group = &settings->groups[0];
    settings->channels[2].used = false;
    settings->channels[2].main_ppi = 1;
    settings->channels[2].duty_ppi = 4;
    settings->channels[2].gpiote = 2;
    settings->channels[2].timer = 3;
    settings->channels[2].main_task = &NRF_PPI->CH[PWM_NRF_PPI(config, 1)].TEP;
    settings->channels[2].duty_task = &NRF_PPI->CH[PWM_NRF_PPI(config, 4)].TEP;

    settings->channels[3].group = &settings->groups[0];
    settings->channels[3].used = false;
    settings->channels[3].main_ppi = 1;
    settings->channels[3].duty_ppi = 5;
    settings->channels[3].gpiote = 3;
    settings->channels[3].timer = 4;
    settings->channels[3].main_task = &NRF_PPI->FORK[PWM_NRF_PPI(config, 1)].TEP;
    settings->channels[3].duty_task = &NRF_PPI->CH[PWM_NRF_PPI(config, 5)].TEP;


    NRF_TIMER3->SHORTS = TIMER_SHORTS_COMPARE0_CLEAR_Msk;
    NRF_TIMER3->MODE = TIMER_MODE_MODE_Timer;
    NRF_TIMER3->PRESCALER = 0;
    NRF_TIMER3->BITMODE = TIMER_BITMODE_BITMODE_16Bit;

#else
#error Not supported SOC series
#endif
    return 0;
}

static
struct pwm_nrf5_sw_channel *
        pwm_nrf5_sw_channel_get(struct pwm_nrf5_sw_settings *settings,
                                const struct pwm_nrf5_sw_config *config,
                                uint8_t pin)
{
    size_t channels = sizeof(settings->channels) /
                      sizeof(struct pwm_nrf5_sw_channel);
    size_t index;

    for(index = 0; index < channels; index++) {
        struct pwm_nrf5_sw_channel *channel = &settings->channels[index];

        if (channel->used == false)
            continue;

        if (channel->pin == pin)
            return channel;
    }

    for(index = 0; index < channels; index++) {

        struct pwm_nrf5_sw_channel *channel = &settings->channels[index];
        u8_t gpiote = PWM_NRF_GPIOTE(config, channel->gpiote);
        if (channel->used == true)
            continue;

        channel->used = true;
        channel->pin = pin;
        NRF_GPIOTE->CONFIG[gpiote] = 0x00130003 | (pin << 8);

        /* configure GPIO pin as output */
        NRF_GPIO->DIRSET = BIT(pin);
        NRF_GPIO->OUTSET = BIT(pin);

        SYS_LOG_DBG("index: %d, pin: %u, gpiote: %u, ppi: %u",
                    (int) index,
                    channel->pin,
                    gpiote,
                    channel->duty_ppi);

        return channel;
    }

    return NULL;
}


static
void pwm_nrf5_sw_group_set_period(const struct pwm_nrf5_sw_config *config,
                                  struct pwm_nrf5_sw_group *group,
                                  u32_t period_cycles)
{
    size_t channels = sizeof(group->channels) / sizeof(pwm_nrf5_sw_channel_ptr);
    size_t index;
    for(index = 0; index < channels; index++) {
        struct pwm_nrf5_sw_channel *channel = group->channels[index];

        if (channel->used == false)
            continue;

#if defined(CONFIG_SOC_SERIES_NRF51X)
#elif defined(CONFIG_SOC_SERIES_NRF52X)
        SYS_LOG_DBG("gpiote: %u, main ppi: %u, duty ppi: %u",
                    PWM_NRF_GPIOTE(config, channel->gpiote),
                    PWM_NRF_PPI(config, channel->main_ppi),
                    PWM_NRF_PPI(config, channel->duty_ppi));

        if (channel->pulse_cycles == 0) {
            SYS_LOG_DBG("set");
            *channel->main_task = PWM_NRF_SET_TASK(config, channel->gpiote);
            *channel->duty_task = PWM_NRF_SET_TASK(config, channel->gpiote);
        } else if (channel->pulse_cycles == channel->period_cycles) {
            SYS_LOG_DBG("clear");
            *channel->main_task = PWM_NRF_CLEAR_TASK(config, channel->gpiote);
            *channel->duty_task = PWM_NRF_CLEAR_TASK(config, channel->gpiote);
        } else {
            SYS_LOG_DBG("dim");
            *channel->main_task = PWM_NRF_CLEAR_TASK(config, channel->gpiote);
            *channel->duty_task = PWM_NRF_SET_TASK(config, channel->gpiote);
        }

        NRF_PPI->CHENSET = BIT(PWM_NRF_PPI(config, channel->main_ppi)) |
                           BIT(PWM_NRF_PPI(config, channel->duty_ppi));

        NRF_TIMER3->CC[channel->timer] = channel->pulse_cycles *
                                         channel->period_cycles /
                                         period_cycles;
#else
#error Not supported SOC series
#endif
    }
#if defined(CONFIG_SOC_SERIES_NRF51X)

#elif defined(CONFIG_SOC_SERIES_NRF52X)
    NRF_TIMER3->CC[group->timer] = period_cycles;
    NRF_TIMER3->TASKS_CLEAR = 1;
    NRF_TIMER3->TASKS_START = 1;
#else
#error Not supported SOC series
#endif
    group->period_cycles = period_cycles;
}

static void pwm_nrf5_sw_channel_refresh(const struct pwm_nrf5_sw_config *config,
                                        struct pwm_nrf5_sw_channel *channel)
{
#if defined(CONFIG_SOC_SERIES_NRF51X)
#elif defined(CONFIG_SOC_SERIES_NRF52X)
    SYS_LOG_DBG("gpiote: %u, main ppi: %u, duty ppi: %u",
                PWM_NRF_GPIOTE(config, channel->gpiote),
                PWM_NRF_PPI(config, channel->main_ppi),
                PWM_NRF_PPI(config, channel->duty_ppi));
    if (channel->pulse_cycles == 0) {
        SYS_LOG_DBG("set");
        *channel->main_task = PWM_NRF_SET_TASK(config, channel->gpiote);
        *channel->duty_task = PWM_NRF_SET_TASK(config, channel->gpiote);
    } else if (channel->pulse_cycles == channel->period_cycles) {
        SYS_LOG_DBG("clear");
        *channel->main_task = PWM_NRF_CLEAR_TASK(config, channel->gpiote);
        *channel->duty_task = PWM_NRF_CLEAR_TASK(config, channel->gpiote);
    } else {
        SYS_LOG_DBG("dim");
        *channel->main_task = PWM_NRF_CLEAR_TASK(config, channel->gpiote);
        *channel->duty_task = PWM_NRF_SET_TASK(config, channel->gpiote);
    }

    NRF_PPI->CHENSET = BIT(PWM_NRF_PPI(config, channel->main_ppi)) |
                       BIT(PWM_NRF_PPI(config, channel->duty_ppi));

    NRF_TIMER3->CC[channel->timer] = channel->pulse_cycles *
                                     channel->period_cycles /
                                     channel->group->period_cycles;
    NRF_TIMER3->TASKS_CLEAR = 1;
    NRF_TIMER3->TASKS_START = 1;
#else
#error Not supported SOC series
#endif
}
static int pwm_nrf5_sw_pin_set(struct device *dev,
                               u32_t pin,
                               u32_t period_cycles,
                               u32_t pulse_cycles)
{
    struct pwm_nrf5_sw_group *group;
    struct pwm_nrf5_sw_settings *settings = dev->driver_data;
    const struct pwm_nrf5_sw_config *config = dev->config->config_info;
    struct pwm_nrf5_sw_channel *channel = pwm_nrf5_sw_channel_get(settings, config, pin);

    SYS_LOG_DBG("pin: %u, period: %u, pulse: %u",
                pin,
                period_cycles,
                pulse_cycles);

    if (period_cycles < pulse_cycles) {
        SYS_LOG_ERR("pulse value too hight");
        return -EINVAL;
    }
    if (channel == NULL) {
        SYS_LOG_ERR("Not availabled free channel");
        return -ENOMEM;
    }

    pulse_cycles &= PWM_NRF5_MAXIMUM;

    channel->period_cycles = period_cycles;
    channel->pulse_cycles = pulse_cycles;
    group = channel->group;

    if (period_cycles < group->period_cycles)
        pwm_nrf5_sw_group_set_period(dev->config->config_info,
                                     group,
                                     period_cycles);
    else
        pwm_nrf5_sw_channel_refresh(dev->config->config_info, channel);

    return 0;
}

static int pwm_nrf5_sw_get_cycles_per_sec(struct device *dev,
                                          u32_t pwm,
                                          u64_t *cycles)
{
    struct pwm_config *config;

    config = (struct pwm_config *)dev->config->config_info;

    /* HF timer frequency is derived from 16MHz source and prescaler is 0 */
    *cycles = 16 * 1024 * 1024;
    return 0;
}

static const struct pwm_driver_api pwm_nrf5_sw_drv_api_funcs = {
    .pin_set = pwm_nrf5_sw_pin_set,
    .get_cycles_per_sec = pwm_nrf5_sw_get_cycles_per_sec,
};

static u8_t pwm_nrf5_sw_0_data[sizeof(struct pwm_nrf5_sw_settings)];
DEVICE_AND_API_INIT(pwm_nrf5_sw_0,
                    CONFIG_PWM_NRF5_SW_0_DEV_NAME,
                    pwm_nrf5_sw_init,
                    pwm_nrf5_sw_0_data,
                    &pwm_nrf5_sw_0_config,
                    POST_KERNEL,
                    CONFIG_KERNEL_INIT_PRIORITY_DEVICE,
                    &pwm_nrf5_sw_drv_api_funcs);
