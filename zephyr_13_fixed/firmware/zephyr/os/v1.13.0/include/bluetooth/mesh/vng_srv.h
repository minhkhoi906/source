#ifndef VNG_SRV_H
#define VNG_SRV_H

#ifdef __cplusplus
extern "C" {
#endif

#define CID_VNG 0x00FF

struct bt_mesh_vng_srv {
    struct bt_mesh_model *model;
    void (*const get_func)(struct net_buf_simple* vng_data, void *user_data);
    void (*const set_func)(struct net_buf_simple* vng_data, void *user_data);
    void *user_data;
};

int bt_mesh_vng_srv_init(struct bt_mesh_model *model);

int bt_mesh_nfc_publish(struct bt_mesh_model *model, struct net_buf_simple* vng_data);
int bt_mesh_nfc_af_publish(struct bt_mesh_model *model, struct net_buf_simple* vng_data);

int bt_mesh_button_publish(struct bt_mesh_model *model, struct net_buf_simple* vng_data); 

int bt_mesh_device_health_publish(struct bt_mesh_model *model, struct net_buf_simple* vng_data); 



extern const struct bt_mesh_model_op bt_mesh_vng_srv_op[];

#define BT_MESH_MODEL_VNG_SRV(srv_data, pub) \
        BT_MESH_MODEL_VND(CID_VNG, BT_MESH_MODEL_ID_VNG_SRV, \
                  bt_mesh_vng_srv_op, pub, srv_data)

#ifdef __cplusplus
}
#endif

#endif // VNG_SRV_H