#ifndef VNG_MESH_THREAD_TRACKING_H
#define VNG_MESH_THREAD_TRACKING_H

#ifdef __cplusplus
extern "C" {
#endif

struct bt_mesh_vng_thread_tracking {
    struct bt_mesh_model *model;
    void *user_data; 
    u16_t tid;

};

extern const struct bt_mesh_model_op bt_mesh_vng_thread_tracking_op[];

int bt_mesh_vng_thread_tracking_init(struct bt_mesh_model *model);

// notify for tracker that, main-thread is terminating.
void bt_mesh_vng_thread_tracking_main_terminate(); 


void bt_mesh_vng_thread_tracking_setting_load();

#define BT_MESH_MODEL_VNG_THREAD_TRACKING(srv_data) \
        BT_MESH_MODEL_VND(CID_VNG, BT_MESH_MODEL_ID_VNG_THREAD_TRACKING, \
                  bt_mesh_vng_thread_tracking_op, NULL, srv_data)


#ifdef __cplusplus
}
#endif

#endif // VNG_MESH_THREAD_TRACKING_H