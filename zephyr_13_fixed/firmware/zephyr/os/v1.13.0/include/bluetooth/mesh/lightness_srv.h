#ifndef LIGHTNESS_SRV_H
#define LIGHTNESS_SRV_H

#ifdef __cplusplus
extern "C" {
#endif

struct bt_mesh_lightness_srv {
    struct bt_mesh_model *model;
    void (*const get_func)(u16_t lightness, void *user_data);
    void (*const set_func)(u16_t lightness, void *user_data);
    void *user_data;
};

int bt_mesh_light_lightness_srv_init(struct bt_mesh_model *model, bool primary);

extern const struct bt_mesh_model_op bt_mesh_light_lightness_srv_op[];

#define BT_MESH_MODEL_LIGHT_LIGHTNESS_SRV(srv_data) \
        BT_MESH_MODEL(BT_MESH_MODEL_ID_LIGHT_LIGHTNESS_SRV, \
                  bt_mesh_light_lightness_srv_op, NULL, srv_data)

#ifdef __cplusplus
}
#endif

#endif // LIGHTNESS_SRV_H
