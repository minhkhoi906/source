#ifndef GEN_ONOFF_CLI_H
#define GEN_ONOFF_CLI_H

#ifdef __cplusplus
extern "C" {
#endif

struct bt_mesh_gen_onoff_cli {
    struct bt_mesh_model *model;
    void (*const status_func)(u16_t addr, struct net_buf_simple* data, void *user_data);
    void *user_data;
};

int bt_mesh_gen_onoff_cli_init(struct bt_mesh_model *model, bool primary);


int bt_mesh_gen_onoff_get(struct bt_mesh_model *model, u16_t addr);

int bt_mesh_gen_onoff_set(struct bt_mesh_model *model, u16_t addr, struct net_buf_simple* data);

int bt_mesh_gen_onoff_set_unack(struct bt_mesh_model *model, u16_t addr, struct net_buf_simple* data);


extern const struct bt_mesh_model_op bt_mesh_gen_onoff_cli_op[];

#define BT_MESH_MODEL_GEN_ONOFF_CLI(cli_data, pub) \
        BT_MESH_MODEL(BT_MESH_MODEL_ID_GEN_ONOFF_CLI, \
                  bt_mesh_gen_onoff_cli_op, pub, cli_data)

#ifdef __cplusplus
}
#endif

#endif // LIGHTNESS_CLI_H
