#ifndef LEVEL_CLI_H
#define LEVEL_CLI_H

#ifdef __cplusplus
extern "C" {
#endif

struct bt_mesh_gen_level_cli {
    struct bt_mesh_model *model;
    void (*const state_func)(u16_t present, u16_t target, u8_t remain, void *user_data);
    void *user_data;
};

int bt_mesh_gen_level_cli_init(struct bt_mesh_model *model, bool primary);

extern const struct bt_mesh_model_op bt_mesh_gen_level_cli_op[];

#define BT_MESH_MODEL_GEN_LEVEL_CLI(cli_data) \
        BT_MESH_MODEL(BT_MESH_MODEL_ID_GEN_LEVEL_CLI, \
                  bt_mesh_gen_level_cli_op, NULL, cli_data)

#ifdef __cplusplus
}
#endif

#endif // LEVEL_CLI_H
