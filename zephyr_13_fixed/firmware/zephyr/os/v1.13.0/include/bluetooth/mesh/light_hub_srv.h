#ifndef LEVEL_ALL_SRV_H
#define LEVEL_ALL_SRV_H

#ifdef __cplusplus
extern "C" {
#endif

struct bt_mesh_light_hub_srv {
    struct bt_mesh_model *model;
    void (*const enableds_set_func)(u32_t enables, void *user_data, bool ack);
    void (*const levels_set_func)(u16_t level, u8_t time, u8_t delay, void *user_data, bool ack);
    void (*const groups_add_func)(u8_t linesmap, const u16_t groups, void *user_data, bool ack);
    void (*const groups_del_func)(u8_t linesmap, const u16_t groups, void *user_data, bool ack);
    void (*const groups_clr_func)(u8_t linesmap, u8_t lines, void *user_data, bool ack);
    void (*const dimming_config)(u8_t linesmap, u8_t off_0, u8_t scale_min, u8_t scale_max, void *user_data);
    void (*const status_config)(u8_t status_en, u8_t warn_en, u16_t check_period, u16_t notify_period, void *user_data);
    void (*const status_store)(u8_t store, void *user_data);

    void *user_data;
};

int bt_mesh_light_hub_srv_init(struct bt_mesh_model *model);

extern const struct bt_mesh_model_op bt_mesh_light_hub_srv_op[];

#define BT_MESH_MODEL_LIGHT_HUB_SRV(srv_data) \
        BT_MESH_MODEL(BT_MESH_MODEL_ID_LIGHT_HUB_SRV, \
                  bt_mesh_light_hub_srv_op, NULL, srv_data)

#ifdef __cplusplus
}
#endif

#endif // LEVEL_ALL_SRV_H
