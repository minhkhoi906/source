#ifndef VNG_STATUS_H
#define VNG_STATUS_H

#ifdef __cplusplus
extern "C" {
#endif

#define CID_VNG 0x00FF

struct bt_mesh_vng_status {
    struct bt_mesh_model *model;
    void (*const get_func)(u16_t *time, u16_t *main, u16_t *duration);
    void (*const set_func)(u16_t time, u16_t main, u16_t duration, u16_t period);

#ifdef CONFIG_BT_MESH_VNG_STATUS_GROUP
    void (*const groups_count_get)(u16_t index, u16_t main);
    void (*const groups_list_get)(u16_t index, u16_t main, u8_t line, u8_t gbits);
#endif 
    void *user_data;
};

int bt_mesh_vng_status_init(struct bt_mesh_model *model);

extern const struct bt_mesh_model_op bt_mesh_vng_status_op[];

#define BT_MESH_MODEL_VNG_STATUS(srv_data, pub) \
        BT_MESH_MODEL_VND(CID_VNG, BT_MESH_MODEL_ID_VNG_STATUS, \
                  bt_mesh_vng_status_op, pub, srv_data)

#ifdef __cplusplus
}
#endif

#endif // VNG_STATUS_H
