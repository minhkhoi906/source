#ifndef VNG_MESH_COMMAND_SRV_H
#define VNG_MESH_COMMAND_SRV_H

#ifdef __cplusplus
extern "C" {
#endif

struct bt_mesh_vng_command_srv {
    struct bt_mesh_model *model;
    void (*const notify)(u8_t dev_type, 
    					 u8_t success, u8_t failed, 
    					 u8_t success_repeat, u8_t fail_repeat, 
    					 void *user_data);
    void *user_data;
    u16_t tid;
};

extern const struct bt_mesh_model_op bt_mesh_vng_command_srv_op[];

#define BT_MESH_MODEL_VNG_COMMAND_SRV(srv_data) \
        BT_MESH_MODEL_VND(CID_VNG, BT_MESH_MODEL_ID_VNG_COMMAND_SRV, \
                  bt_mesh_vng_command_srv_op, NULL, srv_data)


int bt_mesh_vng_cmd_srv_init(struct bt_mesh_model *model, bool primary);
#ifdef __cplusplus
}
#endif

#endif // VNG_MESH_COMMAND_SRV_H