#ifndef LEVEL_SRV_H
#define LEVEL_SRV_H

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_TIDS CONFIG_BT_MESH_MODEL_GROUP_COUNT
struct tid_check
{
    u16_t tid;
    u16_t dst;
};

struct bt_mesh_gen_level_srv {
    struct bt_mesh_model *model;
    void (*const get_func)(u16_t timeout, void *user_data);
    void (*const set_func)(u16_t level, u8_t tid, u8_t time, u8_t delay, void *user_data, bool ack);
    void *user_data;
    struct tid_check tids[MAX_TIDS];
    u8_t freeslot;
};

int bt_mesh_gen_level_srv_init(struct bt_mesh_model *model);

extern const struct bt_mesh_model_op bt_mesh_gen_level_srv_op[];

#define BT_MESH_MODEL_GEN_LEVEL_SRV(srv_data, pub) \
        BT_MESH_MODEL(BT_MESH_MODEL_ID_GEN_LEVEL_SRV, \
                  bt_mesh_gen_level_srv_op, pub, srv_data)

#ifdef __cplusplus
}
#endif

#endif // LEVEL_SRV_H
