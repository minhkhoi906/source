/** @file
 *  @brief Bluetooth Mesh Profile APIs.
 */

/*
 * Copyright (c) 2017 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#ifndef __BT_MESH_H
#define __BT_MESH_H

#include <zephyr/types.h>
#include <stddef.h>
#include <net/buf.h>

#include <bluetooth/mesh/access.h>
#include <bluetooth/mesh/main.h>
#include <bluetooth/mesh/cfg_srv.h>
#include <bluetooth/mesh/health_srv.h>
#include <bluetooth/mesh/cfg_cli.h>
#include <bluetooth/mesh/health_cli.h>
#include <bluetooth/mesh/proxy.h>

#if defined(CONFIG_BT_MESH_GEN_LEVEL_CLI)
#include <bluetooth/mesh/level_cli.h>
#endif

#if defined(CONFIG_BT_MESH_GEN_LEVEL_SRV)
#include <bluetooth/mesh/level_srv.h>
#endif

#if defined(CONFIG_BT_MESH_LIGHT_HUB_SRV)
#include <bluetooth/mesh/light_hub_srv.h>
#endif

#if defined(CONFIG_BT_MESH_LIGHT_LIGHTNESS_CLI)
#include <bluetooth/mesh/lightness_cli.h>
#endif

#if defined(CONFIG_BT_MESH_LIGHT_LIGHTNESS_SRV)
#include <bluetooth/mesh/lightness_srv.h>
#endif

#if defined(CONFIG_BT_MESH_GEN_ONOFF_CLI)
#include <bluetooth/mesh/gen_onoff_cli.h>
#endif

#if defined(CONFIG_BT_MESH_GEN_ONOFF_SRV)
#include <bluetooth/mesh/gen_onoff_srv.h>
#endif

#if defined(CONFIG_BT_MESH_VNG_SRV)
#include <bluetooth/mesh/vng_srv.h>
#endif

#if defined(CONFIG_BT_MESH_VNG_OTA_SRV)
#include <bluetooth/mesh/vng_ota_srv.h>
#endif

#if defined(CONFIG_BT_MESH_VNG_STATUS)
#include <bluetooth/mesh/vng_status.h>
#endif

#if defined(BT_MESH_MODEL_ID_VNG_COMMAND_SRV)
#include <bluetooth/mesh/vng_command_srv.h>
#endif


#if defined(BT_MESH_MODEL_ID_VNG_THREAD_TRACKING)
#include <bluetooth/mesh/vng_thread_tracking.h>
#endif


#endif /* __BT_MESH_H */
