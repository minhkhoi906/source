/*  Bluetooth Mesh */

/*
 * Copyright (c) 2018 VNG Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <zephyr/types.h>
#include <misc/util.h>
#include <misc/byteorder.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <bluetooth/mesh.h>
#include <bluetooth/mesh/vng_thread_tracking.h>
#include <device.h>
#include <watchdog.h>
#include <debug/object_tracing.h>
#include "mesh.h"
#include "net.h"
#include "access.h"

#include "tid_check.h"
#ifdef CONFIG_SOC_FAMILY_NRF
#include <nrf.h>
#endif

#define BT_DBG_ENABLED IS_ENABLED(CONFIG_BT_MESH_DEBUG_VNG_THREAD_TRACKING)
#include "common/log.h"

#define THREAD_COMMAND 0x33

#define CID_VNG 0x00FF
#define GATEWAY_GROUP_ADDR 0xC000
#define BT_MESH_MODEL_OP_NOTIFY                    BT_MESH_MODEL_OP_3(0x04, CID_VNG)
#define BT_MESH_MODEL_OP_THEAD_COMMAND             BT_MESH_MODEL_OP_3(0x0E, CID_VNG)

#define THREADS_COUNT_INTERVAL 20 //s
#define RESET_DELAYED          5


struct thread_tracking_priv
{
    struct bt_mesh_model *model;

    struct k_delayed_work timer;

    struct k_delayed_work reset_timer;

    int threads;
    int dead_threads;

    struct __packed {

        u8_t reset; // reset device on thread-termination.
        u8_t notify; //notify on thread-termination.

    } setting;

};


static struct thread_tracking_priv thread_tracking = {
    .setting = { .reset = 1, .notify = 0 } ,
};

static void _reset_work(struct k_work* work);
static void _count_threads_work(struct k_work* work);

int bt_mesh_vng_thread_tracking_init(struct bt_mesh_model *model)
{
    struct bt_mesh_vng_thread_tracking *srv = model->user_data;
    srv->model = model;

    BT_MESH_MODEL_SETTING_VAL_DEFINE(thread_tracking_setting, model, 5);

    thread_tracking.model = model;

    k_delayed_work_init(&thread_tracking.reset_timer, _reset_work);
    k_delayed_work_init(&thread_tracking.timer, _count_threads_work);
    k_delayed_work_submit(&thread_tracking.timer, K_SECONDS(RESET_DELAYED));

    return 0;
}


void bt_mesh_vng_thread_tracking_setting_load()
{
    if (thread_tracking.model == NULL) {
        BT_DBG("call init first");
        return;
    }

    if (thread_tracking.model->setting_val.val->len >= sizeof(thread_tracking.setting)) {

        thread_tracking.setting.reset = thread_tracking.model->setting_val.val->data[0];
        thread_tracking.setting.notify = thread_tracking.model->setting_val.val->data[1];

         BT_DBG("reset %u, notify %u", thread_tracking.setting.reset, 
                                thread_tracking.setting.notify);
    }
}

void bt_mesh_vng_thread_tracking_main_terminate()
{
    if (thread_tracking.threads > 0)
        --thread_tracking.threads;
}

static void _thread_tracking_setting_store()
{
    if (thread_tracking.model == NULL) {
        BT_DBG("call init first");
        return;
    }

    bt_mesh_model_setting_val_update(thread_tracking.model, 
                            (const u8_t*)(&thread_tracking.setting), 
                            sizeof(thread_tracking.setting));


}

static void _thread_termination_notify()
{

    if (thread_tracking.model == NULL) {
        BT_DBG("call init first");
        return;
    }

    int error;
    NET_BUF_SIMPLE_DEFINE(msg, 2 + 12 + 4);

    struct bt_mesh_msg_ctx ctx = {
        .net_idx = bt_mesh.sub[0].net_idx,
        .app_idx = 0,
        .addr = GATEWAY_GROUP_ADDR,
        .send_ttl = BT_MESH_TTL_DEFAULT,
    };

    
    bt_mesh_model_msg_init(&msg, BT_MESH_MODEL_OP_NOTIFY);
    net_buf_simple_add_u8(&msg, THREAD_COMMAND);
    net_buf_simple_add_u8(&msg, thread_tracking.threads);
    net_buf_simple_add_u8(&msg, thread_tracking.dead_threads);
    
    error = bt_mesh_model_send(thread_tracking.model, &ctx, &msg, NULL, NULL);
    if (error != 0) {
        SYS_LOG_ERR("Send failed with error: %d", error);
    } else {
        SYS_LOG_INF("Send successed");
    }
}

static void _reset_work(struct k_work* work)
{
    NVIC_SystemReset();
}

static void _count_threads_work(struct k_work* work)
{
    int counter = 0;
    struct k_thread *thread_list = NULL;

    /* wait a bit to allow any initialization-only threads to terminate */
    thread_list   = (struct k_thread *)SYS_THREAD_MONITOR_HEAD;
    while (thread_list != NULL) {
        thread_list =
            (struct k_thread *)SYS_THREAD_MONITOR_NEXT(thread_list);
        counter++;
    } 
    
    BT_DBG("threads %d", counter);
    
    if (thread_tracking.threads <= counter)  {
        
        thread_tracking.threads = counter;
    } else {
        // one or more threads is dead. 
        thread_tracking.dead_threads = thread_tracking.threads - counter;

        thread_tracking.threads = counter;

        if (thread_tracking.setting.notify) {

            _thread_termination_notify();
        }

        if (thread_tracking.setting.reset) {
            BT_DBG("resetting...");
            k_delayed_work_submit(&thread_tracking.reset_timer, K_SECONDS(RESET_DELAYED));
        }
    }

    k_delayed_work_submit(&thread_tracking.timer, K_SECONDS(THREADS_COUNT_INTERVAL));

}

static void _vng_thread_command(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{

    struct bt_mesh_vng_thread_tracking* srv = model->user_data;

    u16_t tid;
    u8_t reset;
    u8_t notify;


    reset  = net_buf_simple_pull_u8(buf);  
    notify = net_buf_simple_pull_u8(buf);   
    tid    = net_buf_simple_pull_be16(buf);

    if(!valid_tid_u16(srv->tid, tid, TID_LAG_DEFAULT)) {
        BT_DBG("old tid %u, new tid %u -> invalid", srv->tid, tid);
        return;
    }

    srv->tid = tid;

    thread_tracking.setting.notify = notify;
    thread_tracking.setting.reset  = reset;

    BT_DBG("reset %u, notify %u", reset, notify);

    _thread_tracking_setting_store();

}


const struct bt_mesh_model_op bt_mesh_vng_thread_tracking_op[] = {
	{ BT_MESH_MODEL_OP_THEAD_COMMAND, 4, _vng_thread_command },
    BT_MESH_MODEL_OP_END,
};
