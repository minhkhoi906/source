/*  Bluetooth Mesh */

/*
 * Copyright (c) 2018 VNG Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <zephyr/types.h>
#include <misc/util.h>
#include <misc/byteorder.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <bluetooth/mesh.h>
#include <bluetooth/mesh/light_hub_srv.h>
#include "tid_check.h"

#define BT_DBG_ENABLED IS_ENABLED(CONFIG_BT_MESH_DEBUG_LIGHT_HUB_SRV)
#include "common/log.h"


#define STATUS_EN_BIT 0
#define WARN_EN_BIT   1


#define BYTE_BITS  8
#define GROUPS_MAX 8
// #define CID_VNG 0x00FF
#define BT_MESH_MODEL_OP_LEVELS_SET  BT_MESH_MODEL_OP_2(0x90, 0x00)

#define BT_MESH_MODEL_OP_ENABLES_SET BT_MESH_MODEL_OP_2(0x90, 0x01)
#define BT_MESH_MODEL_OP_GROUPS_ADD  BT_MESH_MODEL_OP_2(0x90, 0x02)
#define BT_MESH_MODEL_OP_GROUPS_DEL  BT_MESH_MODEL_OP_2(0x90, 0x03)
#define BT_MESH_MODEL_OP_GROUPS_CLR  BT_MESH_MODEL_OP_2(0x90, 0x04)

#define BT_MESH_MODEL_OP_DIMMING_CONFIG   BT_MESH_MODEL_OP_2(0x90, 0x05)

#define BT_MESH_MODEL_OP_LEVELS_SET_ACK  BT_MESH_MODEL_OP_2(0x90, 0x06)
#define BT_MESH_MODEL_OP_ENABLES_SET_ACK BT_MESH_MODEL_OP_2(0x90, 0x07)
#define BT_MESH_MODEL_OP_GROUPS_ADD_ACK  BT_MESH_MODEL_OP_2(0x90, 0x08)
#define BT_MESH_MODEL_OP_GROUPS_DEL_ACK  BT_MESH_MODEL_OP_2(0x90, 0x09)
#define BT_MESH_MODEL_OP_GROUPS_CLR_ACK  BT_MESH_MODEL_OP_2(0x90, 0x0A)

#define BT_MESH_MODEL_OP_STATUS_CONFIG   BT_MESH_MODEL_OP_2(0x90, 0x0B)
#define BT_MESH_MODEL_OP_STATUS_STORE    BT_MESH_MODEL_OP_2(0x90, 0x0C)

#define TID_LAG_DIM     50
#define TID_LAG_CONFIG  30

struct hub_internal
{
    u16_t dim_tid;
    u16_t config_tid;
};

static struct hub_internal _hub_internal;

int bt_mesh_light_hub_srv_init(struct bt_mesh_model *model)
{
    BT_DBG("");
    struct bt_mesh_light_hub_srv *srv = model->user_data;
    srv->model = model;
    return 0;
}

static u8_t _lines(u8_t linesmap)
{  
    u8_t count = 0;

    if(linesmap == 0xFF)
        return GROUPS_MAX;

    for(int i = 0; i < BYTE_BITS; i++) {
        count += (linesmap >> i) & BIT(0);
    }

    return count;
}

static inline bool _valid_config_tid(u16_t tid)
{
    if (!valid_tid_u16(_hub_internal.config_tid, tid, TID_LAG_CONFIG))
        return false;

    _hub_internal.config_tid = tid;

    return true;
}

static void _levels_set_handle(struct bt_mesh_model *model,
                        struct bt_mesh_msg_ctx *ctx,
                        struct net_buf_simple *buf, 
                        bool ack)
{
    u16_t level;
    u8_t tid;
    u8_t time;
    u8_t delay;
    u16_t tid16;

    struct bt_mesh_light_hub_srv *srv = model->user_data;


    level = net_buf_simple_pull_be16(buf);
    tid = net_buf_simple_pull_u8(buf);
    time = net_buf_simple_pull_u8(buf);
    delay = net_buf_simple_pull_u8(buf);

    if (buf->len >= sizeof(u16_t)) {

        tid16 = net_buf_simple_pull_be16(buf);
        if(!valid_tid_u16(_hub_internal.dim_tid, tid16, TID_LAG_DIM)) {

            BT_DBG("old tid %u, new tid %u, lag %u -> invalid", _hub_internal.dim_tid, tid16, TID_LAG_DIM);
            return;
        }

    } else {
        BT_DBG("drop old gateway, tid %u", tid);
        return;
    }

    _hub_internal.dim_tid = tid16;

    BT_DBG("level: %d, tid: %u, src 0x%04x",
           level, _hub_internal.dim_tid, ctx->addr);

    if (srv->levels_set_func == NULL) {
        BT_ERR("not found set_func");
        return;
    }


    srv->levels_set_func(level, time, delay, srv->user_data, ack);
}


static void _enables_set_handle(struct bt_mesh_model *model,
                        struct bt_mesh_msg_ctx *ctx,
                        struct net_buf_simple *buf,
                        bool ack)
{
    u32_t enableds;
    u16_t tid;

    struct bt_mesh_light_hub_srv *srv = model->user_data;

    
    enableds = net_buf_simple_pull_be32(buf);
    tid =  net_buf_simple_pull_be16(buf);

    if (!_valid_config_tid(tid))
        return;

    BT_DBG("enableds: 0x%08x, tid %u", enableds, tid);

    if (srv->enableds_set_func == NULL) {
        BT_ERR("not found ports_set_func");
        return;
    }

    srv->enableds_set_func(enableds, srv->user_data, ack);
}

static void _groups_add_handle(struct bt_mesh_model *model,
                        struct bt_mesh_msg_ctx *ctx,
                        struct net_buf_simple *buf,
                        bool ack)
{
    
    u8_t linesmap;
    u16_t group;
    u16_t tid;
  
    struct bt_mesh_light_hub_srv *srv = model->user_data;
    
    linesmap = net_buf_simple_pull_u8(buf);
    group = net_buf_simple_pull_be16(buf);
    tid = net_buf_simple_pull_be16(buf);
    BT_DBG("linesmap %02X, group %04X, tid %u", linesmap, group, tid);

    if (!_valid_config_tid(tid))
        return;

    BT_DBG("linesmap %02X, group %04X, tid %u", linesmap, group, tid);

    if (srv->groups_add_func == NULL) {
        BT_ERR("not found groups  add function");
        return;
    }

    srv->groups_add_func(linesmap, group, srv->user_data, ack);
}

static void _groups_del_handle(struct bt_mesh_model *model,
                        struct bt_mesh_msg_ctx *ctx,
                        struct net_buf_simple *buf,
                        bool ack)
{
    
    u8_t linesmap;
    u16_t group;
    u16_t tid;

    struct bt_mesh_light_hub_srv *srv = model->user_data;

    
    linesmap = net_buf_simple_pull_u8(buf);
    group = net_buf_simple_pull_be16(buf);
    tid = net_buf_simple_pull_be16(buf);

    if (!_valid_config_tid(tid))
        return;

    BT_DBG("linesmap %02X, group %04X, tid %u", linesmap, group, tid);
   
    if (srv->groups_del_func == NULL) {
        BT_ERR("not found groups del function");
        return;
    }

    srv->groups_del_func(linesmap, group, srv->user_data, ack);
}

static void _groups_clr_handle(struct bt_mesh_model *model,
                        struct bt_mesh_msg_ctx *ctx,
                        struct net_buf_simple *buf,
                        bool ack)
{
    u8_t linesmap;
    u16_t tid;
    u8_t lines;


    struct bt_mesh_light_hub_srv *srv = model->user_data;
    
    linesmap = net_buf_simple_pull_u8(buf);
    tid = net_buf_simple_pull_be16(buf);
    lines = _lines(linesmap);

    if (!_valid_config_tid(tid))
        return;

    BT_DBG("linesmap %02X, lines %u, tid %u", linesmap, lines, tid);

    if (srv->groups_clr_func == NULL) {
        BT_ERR("not found groups clr function");
        return;
    }

    srv->groups_clr_func(linesmap, lines, srv->user_data, ack);
   
}


static void _levels_set(struct bt_mesh_model *model,
                        struct bt_mesh_msg_ctx *ctx,
                        struct net_buf_simple *buf)
{
    _levels_set_handle(model, ctx, buf, false);
}

static void _levels_set_ack(struct bt_mesh_model *model,
                        struct bt_mesh_msg_ctx *ctx,
                        struct net_buf_simple *buf)
{
    _levels_set_handle(model, ctx, buf, true);
}


static void _enables_set(struct bt_mesh_model *model,
                        struct bt_mesh_msg_ctx *ctx,
                        struct net_buf_simple *buf)
{
    _enables_set_handle(model, ctx, buf, false);
}

static void _enables_set_ack(struct bt_mesh_model *model,
                        struct bt_mesh_msg_ctx *ctx,
                        struct net_buf_simple *buf)
{
    _enables_set_handle(model, ctx, buf, true);
}

static void _groups_add(struct bt_mesh_model *model,
                        struct bt_mesh_msg_ctx *ctx,
                        struct net_buf_simple *buf)
{
    _groups_add_handle(model, ctx, buf, false);
   
}

static void _groups_del(struct bt_mesh_model *model,
                        struct bt_mesh_msg_ctx *ctx,
                        struct net_buf_simple *buf)
{
    _groups_del_handle(model, ctx, buf, false);
}

static void _groups_clr(struct bt_mesh_model *model,
                        struct bt_mesh_msg_ctx *ctx,
                        struct net_buf_simple *buf)
{
    _groups_clr_handle(model, ctx, buf, false);

}


static void _groups_add_ack(struct bt_mesh_model *model,
                        struct bt_mesh_msg_ctx *ctx,
                        struct net_buf_simple *buf)
{
    _groups_add_handle(model, ctx, buf, true);
   
}

static void _groups_del_ack(struct bt_mesh_model *model,
                        struct bt_mesh_msg_ctx *ctx,
                        struct net_buf_simple *buf)
{
    _groups_del_handle(model, ctx, buf, true);
}

static void _groups_clr_ack(struct bt_mesh_model *model,
                        struct bt_mesh_msg_ctx *ctx,
                        struct net_buf_simple *buf)
{
    _groups_clr_handle(model, ctx, buf, true);
}


static void _dimming_config(struct bt_mesh_model *model,
                        struct bt_mesh_msg_ctx *ctx,
                        struct net_buf_simple *buf)
{
    u8_t linesmap;
    u8_t off_0;
    u8_t min, max;
    u16_t tid;
    
    struct bt_mesh_light_hub_srv *srv = model->user_data;

    BT_DBG("buf: %s", bt_hex(buf->data, buf->len));
    
    linesmap = net_buf_simple_pull_u8(buf);
    off_0 = net_buf_simple_pull_u8(buf);
    min = net_buf_simple_pull_u8(buf);
    max = net_buf_simple_pull_u8(buf);
    tid = net_buf_simple_pull_be16(buf);

    if (!_valid_config_tid(tid))
        return;

    if (srv->dimming_config == NULL) {
        BT_ERR("not found function");
        return;
    }

    srv->dimming_config(linesmap, off_0, min, max, srv->user_data);
   
}

static void _status_config(struct bt_mesh_model *model,
                        struct bt_mesh_msg_ctx *ctx,
                        struct net_buf_simple *buf)
{
    u8_t statusmask;
    u16_t check_period;
    u16_t notify_period;
    u16_t tid;
    
    struct bt_mesh_light_hub_srv *srv = model->user_data;

    BT_DBG("buf: %s", bt_hex(buf->data, buf->len));
    
    statusmask  = net_buf_simple_pull_u8(buf);
    check_period = net_buf_simple_pull_be16(buf);
    notify_period = net_buf_simple_pull_be16(buf);
    tid          = net_buf_simple_pull_be16(buf);

    if (!_valid_config_tid(tid))
        return;

    if (srv->status_config == NULL) {
        BT_ERR("not found function");
        return;
    }

    u8_t status_en = (statusmask & BIT(STATUS_EN_BIT)) != 0 ? 1 : 0; 
    u8_t warn_en   = (statusmask & BIT(WARN_EN_BIT))   != 0 ? 1 : 0; 

    srv->status_config(status_en, warn_en, check_period, notify_period, srv->user_data);
   
}


static void _status_store(struct bt_mesh_model *model,
                        struct bt_mesh_msg_ctx *ctx,
                        struct net_buf_simple *buf)
{
    u8_t store;
    u16_t tid;
    
    struct bt_mesh_light_hub_srv *srv = model->user_data;

    BT_DBG("buf: %s", bt_hex(buf->data, buf->len));
    
    store  = net_buf_simple_pull_u8(buf);
    tid    = net_buf_simple_pull_be16(buf);

    if (!_valid_config_tid(tid))
        return;

    if (srv->status_store == NULL) {
        BT_ERR("not found function");
        return;
    }

    srv->status_store(store, srv->user_data);
   
}


const struct bt_mesh_model_op bt_mesh_light_hub_srv_op[] = {
{ BT_MESH_MODEL_OP_LEVELS_SET, 5, _levels_set },
{ BT_MESH_MODEL_OP_ENABLES_SET, 4, _enables_set },
{ BT_MESH_MODEL_OP_GROUPS_ADD, 5, _groups_add },
{ BT_MESH_MODEL_OP_GROUPS_DEL, 5, _groups_del },
{ BT_MESH_MODEL_OP_GROUPS_CLR, 3, _groups_clr },
{ BT_MESH_MODEL_OP_LEVELS_SET_ACK, 5, _levels_set_ack },
{ BT_MESH_MODEL_OP_ENABLES_SET_ACK, 4, _enables_set_ack },
{ BT_MESH_MODEL_OP_GROUPS_ADD_ACK, 5, _groups_add_ack },
{ BT_MESH_MODEL_OP_GROUPS_DEL_ACK, 5, _groups_del_ack },
{ BT_MESH_MODEL_OP_GROUPS_CLR_ACK, 3, _groups_clr_ack },
{ BT_MESH_MODEL_OP_DIMMING_CONFIG, 4, _dimming_config },
{ BT_MESH_MODEL_OP_STATUS_CONFIG, 7, _status_config },
{ BT_MESH_MODEL_OP_STATUS_STORE, 3, _status_store },
BT_MESH_MODEL_OP_END,
};
