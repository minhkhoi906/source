#ifndef TID_CHECK_H
#define TID_CHECK_H

#define TID_LAG_DEFAULT 50

static inline bool valid_tid_u16(u16_t old, u16_t _new, u8_t lag)
{
    if(_new > old) {
        u32_t old32 = old + __UINT16_MAX__;
        return old32 - _new > lag;
    }
    
    return old - _new > lag;
    
}


static inline bool valid_tid_u8(u8_t old, u8_t _new, u8_t lag)
{
    if(_new > old) {
        u16_t old16 = old + __UINT8_MAX__;
        return old16 - _new > lag;
    }
    
    return old - _new > lag;
    
}


#endif //TID_CHECK_H
