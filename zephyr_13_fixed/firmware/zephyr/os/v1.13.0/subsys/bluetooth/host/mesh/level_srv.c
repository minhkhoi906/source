/*  Bluetooth Mesh */

/*
 * Copyright (c) 2018 VNG Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <zephyr/types.h>
#include <misc/util.h>
#include <misc/byteorder.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <bluetooth/mesh.h>
#include <bluetooth/mesh/level_srv.h>
#include "tid_check.h"

#define BT_DBG_ENABLED IS_ENABLED(CONFIG_BT_MESH_DEBUG_GEN_LEVEL_SRV)
#include "common/log.h"

#define BT_MESH_MODEL_OP_GEN_LEVEL_GET       BT_MESH_MODEL_OP_2(0x82, 0x05)
#define BT_MESH_MODEL_OP_GEN_LEVEL_SET       BT_MESH_MODEL_OP_2(0x82, 0x06)
#define BT_MESH_MODEL_OP_GEN_LEVEL_SET_UNACK BT_MESH_MODEL_OP_2(0x82, 0x07)
#define BT_MESH_MODEL_OP_GEN_LEVEL_STATUS    BT_MESH_MODEL_OP_2(0x82, 0x08)

int bt_mesh_gen_level_srv_init(struct bt_mesh_model *model)
{

    // BT_DBG("");
    struct bt_mesh_gen_level_srv *srv = model->user_data;
    srv->model = model;

    return 0;
}

static bool _valid_tid(struct bt_mesh_gen_level_srv *srv, u16_t tid, u16_t dst)
{
    int i;

    for (i = 0; i < MAX_TIDS; i++) {

        if (dst == srv->tids[i].dst) {

            if (!valid_tid_u16(srv->tids[i].tid, tid, TID_LAG_DEFAULT)) {
                
                BT_DBG("group_dst 0x%04x, old tid %u, new tid %u, lag %u -> invalid", dst, srv->tids[i].tid, tid, TID_LAG_DEFAULT);

                return false;
            }

            BT_DBG("group_dst 0x%04x, old tid %u, new tid %u slot %u", dst, srv->tids[i].tid, tid, i);

            srv->tids[i].tid = tid;
            return true;
            
        } 
    }

    // not in cache

    BT_DBG("group_dst 0x%04x, tid %u, slot %u", dst, tid, srv->freeslot);

    srv->tids[srv->freeslot].dst = dst;
    srv->tids[srv->freeslot].tid = tid;

    //first in, first out
    srv->freeslot = (srv->freeslot + 1) % MAX_TIDS;

    return true;

}

static void gen_level_data_get(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{

    // BT_DBG("net_idx 0x%04x app_idx 0x%04x src 0x%04x len %u: %s",
    //        ctx->net_idx, ctx->app_idx, ctx->addr, buf->len,
    //        bt_hex(buf->data, buf->len));

    u16_t timeout;
    u16_t tid;


    struct bt_mesh_gen_level_srv *srv = model->user_data;

    timeout = net_buf_simple_pull_be16(buf);
    tid     = net_buf_simple_pull_be16(buf);

    if(!_valid_tid(srv, tid, ctx->recv_dst))
            return;

    BT_DBG("tid: %u, src: 0x%04x", tid, ctx->addr);

    if (srv->get_func != NULL) {

        srv->get_func(timeout, srv->user_data);
    
    } else {
        BT_WARN("Not found set_func");
    }

}

static void gen_level_data_set(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{
    u16_t level;
    u8_t tid;
    u8_t time;
    u8_t delay;
    u16_t tid16;

    struct bt_mesh_gen_level_srv *srv = model->user_data;

    level = net_buf_simple_pull_be16(buf);
    tid = net_buf_simple_pull_u8(buf);
    time = net_buf_simple_pull_u8(buf);
    delay = net_buf_simple_pull_u8(buf);

    if (buf->len >= sizeof(u16_t)) {
        tid16 = net_buf_simple_pull_be16(buf);
        if(!_valid_tid(srv, tid16, ctx->recv_dst))
            return;

    } else {
        BT_DBG("drop old gateway, tid %u", tid);
        return;
    }

    BT_DBG("level: %d, tid: %u, src: 0x%04x",
        level,
        tid16, 
        ctx->addr);

    if (srv->set_func != NULL) {

        srv->set_func(level, tid, time, delay, srv->user_data, true);
    
    } else {
        BT_WARN("Not found set_func");
    }
    
}

static void gen_level_data_set_unack(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{
    u16_t level;
    u8_t tid;
    u8_t time;
    u8_t delay;
    u16_t tid16;

    struct bt_mesh_gen_level_srv *srv = model->user_data;

    // BT_DBG("net_idx 0x%04x app_idx 0x%04x src 0x%04x len %u: %s",
    //        ctx->net_idx, ctx->app_idx, ctx->addr, buf->len,
    //        bt_hex(buf->data, buf->len));

    level = net_buf_simple_pull_be16(buf);
    tid = net_buf_simple_pull_u8(buf);
    time = net_buf_simple_pull_u8(buf);
    delay = net_buf_simple_pull_u8(buf);


    if (buf->len >= sizeof(u16_t)) {
        tid16 = net_buf_simple_pull_be16(buf);

        if(!_valid_tid(srv, tid16, ctx->recv_dst))
            return;

    } else {
        BT_DBG("drop handle old gateway, tid %u", tid);
        return;
    }

    BT_DBG("level: %d, tid: %u, src: 0x%04x",
        level,
        tid16, 
        ctx->addr);

    if (srv->set_func != NULL)
        srv->set_func(level, tid, time, delay, srv->user_data, false);

}

const struct bt_mesh_model_op bt_mesh_gen_level_srv_op[] = {
    { BT_MESH_MODEL_OP_GEN_LEVEL_GET, 4, gen_level_data_get },
    { BT_MESH_MODEL_OP_GEN_LEVEL_SET, 5, gen_level_data_set },
    { BT_MESH_MODEL_OP_GEN_LEVEL_SET_UNACK, 5, gen_level_data_set_unack },
    BT_MESH_MODEL_OP_END,
};
