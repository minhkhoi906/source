/*  Bluetooth Mesh */

/*
 * Copyright (c) 2018 VNG Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <zephyr/types.h>
#include <misc/util.h>
#include <misc/byteorder.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <bluetooth/mesh.h>
#include <bluetooth/mesh/vng_srv.h>


#define BT_DBG_ENABLED IS_ENABLED(CONFIG_BT_MESH_DEBUG_VNG_SRV)
#include "common/log.h"

#define CID_VNG 0x00FF

#define BT_MESH_MODEL_OP_VNG    BT_MESH_MODEL_OP_3(0x04, CID_VNG)

enum Command {
    VNG_READER = 0x12,
    VNG_BUTTON = 0x14,
    VMG_DEVICE_HEALTH = 0x15,    VNG_READER_AF = 0x69,
};

static int bt_mesh_vng_srv_publish(enum Command cmd, struct bt_mesh_model *model, struct net_buf_simple* vng_data);

int bt_mesh_vng_srv_init(struct bt_mesh_model *model)
{
    BT_DBG("");
    struct bt_mesh_vng_srv *srv = model->user_data;
    srv->model = model;
    return 0;
}



int bt_mesh_nfc_publish(struct bt_mesh_model *model, struct net_buf_simple* vng_data) 
{
    return bt_mesh_vng_srv_publish(VNG_READER, model, vng_data);
}

int bt_mesh_nfc_af_publish(struct bt_mesh_model *model, struct net_buf_simple* vng_data) 
{
    return bt_mesh_vng_srv_publish(VNG_READER_AF, model, vng_data);
}

int bt_mesh_button_publish(struct bt_mesh_model *model, struct net_buf_simple* vng_data) 
{
    return bt_mesh_vng_srv_publish(VNG_BUTTON, model, vng_data);
}

int bt_mesh_device_health_publish(struct bt_mesh_model *model, struct net_buf_simple* vng_data) 
{
    return bt_mesh_vng_srv_publish(VMG_DEVICE_HEALTH, model, vng_data);
}


static int bt_mesh_vng_srv_publish(enum Command cmd, struct bt_mesh_model *model, struct net_buf_simple* vng_data)
{
    int err;
    u8_t command;
    struct net_buf_simple *msg = model->pub->msg;
    
    command = cmd;

    bt_mesh_model_msg_init(msg, BT_MESH_MODEL_OP_VNG);
    
    net_buf_simple_add_u8(msg, command);   
    net_buf_simple_add_mem(msg, vng_data->data, vng_data->len);
   

    err = bt_mesh_model_publish(model);
    if (err) {
        BT_ERR("model_publish() failed (err %d)", err);
        return err;
    }
    BT_DBG("model_publish() successed");
    return 0;
}



const struct bt_mesh_model_op bt_mesh_vng_srv_op[] = {
    BT_MESH_MODEL_OP_END,
};


