/*  Bluetooth Mesh */

/*
 * Copyright (c) 2017 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <stdbool.h>
#include <errno.h>

#include <net/buf.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/mesh.h>

#define BT_DBG_ENABLED IS_ENABLED(CONFIG_BT_MESH_DEBUG)
#include "common/log.h"

#include "test.h"
#include "adv.h"
#include "prov.h"
#include "net.h"
#include "beacon.h"
#include "lpn.h"
#include "friend.h"
#include "transport.h"
#include "access.h"
#include "foundation.h"
#include "proxy.h"
#include "settings.h"
#include "mesh.h"

#define UNSED_GROUP 0xCFFF
#define CID_VNG 0x00FF

#ifdef CONFIG_BT_MESH_CFG_CLI

#define MODEL_SUB_ADD(model_ptr, is_vnd, net_idx, subaddr)\
{\
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    addr = model_ptr->elem_idx + primary_addr;\
    if(is_vnd) {\
        SYS_LOG_DBG("vnd sub addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_sub_add_vnd(net_idx, primary_addr, addr,\
                subaddr, model_ptr->vnd.id, CID_VNG, NULL);\
    } else {\
        SYS_LOG_DBG("sig sub addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_sub_add(net_idx, primary_addr, addr,\
                subaddr, model_ptr->id, NULL);\
    }\
}

#else 

	#define MODEL_SUB_ADD(model_ptr, is_vnd, net_idx, subaddr)

#endif

#define MAX_GROUPS 60
struct model_subcribed_group
{
	struct bt_mesh_model* model;
	u16_t group;

} __packed;

struct mesh_renew
{
	
	struct k_delayed_work timer;
	struct k_delayed_work group_timer;
	struct {
		u16_t addr;
		u16_t net_idx;
		u8_t flags;
		u8_t net_key[16];
		u32_t iv_index;
		u8_t dev_key[16];
		u32_t seq;
	} ctx;

	struct {

		struct model_subcribed_group groups[MAX_GROUPS];
		u8_t count;

	} subcribed;

};

static struct mesh_renew mesh_renew = {
};


static void _store_model_group(struct bt_mesh_model *mod,
					struct bt_mesh_elem *elem,
					bool vnd, bool primary,
					void *user_data)
{
	
	int i;

	for (i = 0; i < CONFIG_BT_MESH_MODEL_GROUP_COUNT; i++) {

		if (BT_MESH_ADDR_IS_GROUP(mod->groups[i]) && mod->groups[i] != UNSED_GROUP) {

			if (mesh_renew.subcribed.count >= MAX_GROUPS) {
				BT_DBG("out of slot");
				return;
			}
			mesh_renew.subcribed.groups[mesh_renew.subcribed.count].model = mod;

			mesh_renew.subcribed.groups[mesh_renew.subcribed.count].group = mod->groups[i];
			
			++mesh_renew.subcribed.count;

		}
	}
	
}

static void store_groups()
{
	mesh_renew.subcribed.count = 0;

	bt_mesh_model_foreach(_store_model_group, NULL);
}

static void restore_groups_work(struct k_work* work)
{
	int i;
	BT_DBG("groups count %u", mesh_renew.subcribed.count);
	
	for (i = 0; i < mesh_renew.subcribed.count; ++i) {

		BT_DBG("model 0x%08x, group 0x%04x", mesh_renew.subcribed.groups[i].model->vnd.id, mesh_renew.subcribed.groups[i].group);
		bool is_vnd = mesh_renew.subcribed.groups[i].model->vnd.id != 0;
		MODEL_SUB_ADD(mesh_renew.subcribed.groups[i].model, is_vnd, bt_mesh.sub[0].net_idx, mesh_renew.subcribed.groups[i].group);
	}
	
}


static void mesh_renew_work(struct k_work* work)
{
	bt_mesh_provision(
		mesh_renew.ctx.net_key, 
		mesh_renew.ctx.net_idx, 
		mesh_renew.ctx.flags,
		mesh_renew.ctx.iv_index,
		mesh_renew.ctx.addr,
		mesh_renew.ctx.dev_key);

	bt_mesh.seq = mesh_renew.ctx.seq;

	k_delayed_work_submit(&mesh_renew.group_timer, 5000);
	
}

int bt_mesh_provision(const u8_t net_key[16], u16_t net_idx,
		      u8_t flags, u32_t iv_index, u16_t addr,
		      const u8_t dev_key[16])
{
	int err;

	BT_INFO("Primary Element: 0x%04x", addr);
	BT_DBG("net_idx 0x%04x flags 0x%02x iv_index 0x%04x",
	       net_idx, flags, iv_index);

	if (IS_ENABLED(CONFIG_BT_MESH_PB_GATT)) {
		bt_mesh_proxy_prov_disable();
	}



	err = bt_mesh_net_create(net_idx, flags, net_key, iv_index);
	if (err) {
		if (IS_ENABLED(CONFIG_BT_MESH_PB_GATT)) {
			bt_mesh_proxy_prov_enable();
		}

		return err;
	}

	bt_mesh.seq = 0;

	bt_mesh_comp_provision(addr);

	memcpy(bt_mesh.dev_key, dev_key, 16);

	if (IS_ENABLED(CONFIG_BT_SETTINGS)) {
		BT_DBG("Storing network information persistently");
		bt_mesh_store_net();
		bt_mesh_store_subnet(&bt_mesh.sub[0]);
		bt_mesh_store_iv(false);
	}

	bt_mesh_net_start();

	return 0;
}

static int _renew(u16_t addr, u16_t net_idx, const u8_t* netkey)
{
	mesh_renew.ctx.addr = addr;
	mesh_renew.ctx.net_idx = net_idx;
	mesh_renew.ctx.flags = 0;
	mesh_renew.ctx.iv_index = bt_mesh.iv_index;
	mesh_renew.ctx.seq = bt_mesh.seq + (CONFIG_BT_MESH_SEQ_STORE_RATE -
				(bt_mesh.seq % CONFIG_BT_MESH_SEQ_STORE_RATE));
	mesh_renew.ctx.seq--;
	
	memcpy(mesh_renew.ctx.net_key, netkey, 16);
	memcpy(mesh_renew.ctx.dev_key, bt_mesh.dev_key, 16);

	store_groups();
	
	bt_mesh_rpl_clear();
	bt_mesh_reset();

	k_delayed_work_submit(&mesh_renew.timer, 500);

	return 0;
}

int bt_mesh_renew_addr(u16_t addr)
{
	mesh_renew.ctx.addr = addr;
	mesh_renew.ctx.net_idx = 0;
	mesh_renew.ctx.flags = 0;
	mesh_renew.ctx.iv_index = bt_mesh.iv_index;
	mesh_renew.ctx.seq = bt_mesh.seq + (CONFIG_BT_MESH_SEQ_STORE_RATE -
				(bt_mesh.seq % CONFIG_BT_MESH_SEQ_STORE_RATE));

	mesh_renew.ctx.seq--;

	memcpy(mesh_renew.ctx.net_key, bt_mesh.sub[0].keys[0].net, 16);
	memcpy(mesh_renew.ctx.dev_key, bt_mesh.dev_key, 16);

	bt_mesh_rpl_clear();
	bt_mesh_reset();

	k_delayed_work_submit(&mesh_renew.timer, 500);

	return 0;

}

int bt_mesh_renew_netkey(u16_t net_idx, const u8_t* netkey)
{
	return _renew(bt_mesh_primary_addr(), net_idx, netkey);
}

void bt_mesh_reset(void)
{
	if (!bt_mesh.valid) {
		return;
	}

	bt_mesh.iv_index = 0;
	bt_mesh.seq = 0;
	bt_mesh.iv_update = 0;
	bt_mesh.pending_update = 0;
	bt_mesh.valid = 0;
	bt_mesh.ivu_duration = 0;
	bt_mesh.ivu_initiator = 0;

	k_delayed_work_cancel(&bt_mesh.ivu_timer);

	bt_mesh_cfg_reset();

	bt_mesh_rx_reset();
	bt_mesh_tx_reset();

	if (IS_ENABLED(CONFIG_BT_MESH_LOW_POWER)) {
		bt_mesh_lpn_disable(true);
	}

	if (IS_ENABLED(CONFIG_BT_MESH_FRIEND)) {
		bt_mesh_friend_clear_net_idx(BT_MESH_KEY_ANY);
	}

	if (IS_ENABLED(CONFIG_BT_MESH_GATT_PROXY)) {
		bt_mesh_proxy_gatt_disable();
	}

	if (IS_ENABLED(CONFIG_BT_SETTINGS)) {
		bt_mesh_clear_net();
	}

	memset(bt_mesh.dev_key, 0, sizeof(bt_mesh.dev_key));

	bt_mesh_scan_disable();
	bt_mesh_beacon_disable();

	bt_mesh_comp_unprovision();

	if (IS_ENABLED(CONFIG_BT_MESH_PROV)) {
		bt_mesh_prov_reset();
	}
}

bool bt_mesh_is_provisioned(void)
{
	return bt_mesh.valid;
}

int bt_mesh_prov_enable(bt_mesh_prov_bearer_t bearers)
{
	if (bt_mesh_is_provisioned()) {
		return -EALREADY;
	}

	if (IS_ENABLED(CONFIG_BT_DEBUG)) {
		const struct bt_mesh_prov *prov = bt_mesh_prov_get();
		struct bt_uuid_128 uuid = { .uuid.type = BT_UUID_TYPE_128 };

		memcpy(uuid.val, prov->uuid, 16);
		BT_INFO("Device UUID: %s", bt_uuid_str(&uuid.uuid));
	}

	if (IS_ENABLED(CONFIG_BT_MESH_PB_ADV) &&
	    (bearers & BT_MESH_PROV_ADV)) {
		/* Make sure we're scanning for provisioning inviations */
		bt_mesh_scan_enable();
		/* Enable unprovisioned beacon sending */
		bt_mesh_beacon_enable();
	}

	if (IS_ENABLED(CONFIG_BT_MESH_PB_GATT) &&
	    (bearers & BT_MESH_PROV_GATT)) {
		bt_mesh_proxy_prov_enable();
		bt_mesh_adv_update();
	}

	return 0;
}

int bt_mesh_prov_disable(bt_mesh_prov_bearer_t bearers)
{
	if (bt_mesh_is_provisioned()) {
		return -EALREADY;
	}

	if (IS_ENABLED(CONFIG_BT_MESH_PB_ADV) &&
	    (bearers & BT_MESH_PROV_ADV)) {
		bt_mesh_beacon_disable();
		bt_mesh_scan_disable();
	}

	if (IS_ENABLED(CONFIG_BT_MESH_PB_GATT) &&
	    (bearers & BT_MESH_PROV_GATT)) {
		bt_mesh_proxy_prov_disable();
		bt_mesh_adv_update();
	}

	return 0;
}

int bt_mesh_init(const struct bt_mesh_prov *prov,
		 const struct bt_mesh_comp *comp)
{
	int err;

	err = bt_mesh_test();
	if (err) {
		return err;
	}

	err = bt_mesh_comp_register(comp);
	if (err) {
		return err;
	}

	if (IS_ENABLED(CONFIG_BT_MESH_PROV)) {
		err = bt_mesh_prov_init(prov);
		if (err) {
			return err;
		}
	}

	bt_mesh_net_init();
	bt_mesh_trans_init();
	bt_mesh_beacon_init();
	bt_mesh_adv_init();

	if (IS_ENABLED(CONFIG_BT_MESH_PROXY)) {
		bt_mesh_proxy_init();
	}

	if (IS_ENABLED(CONFIG_BT_SETTINGS)) {
		bt_mesh_settings_init();
	}

	k_delayed_work_init(&mesh_renew.timer, mesh_renew_work);
	k_delayed_work_init(&mesh_renew.group_timer, restore_groups_work);

	return 0;
}
