/*  Bluetooth Mesh */

/*
 * Copyright (c) 2018 VNG Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <zephyr/types.h>
#include <misc/util.h>
#include <misc/byteorder.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <bluetooth/mesh.h>
#include <bluetooth/mesh/vng_status.h>
#include "tid_check.h"


#define BT_DBG_ENABLED IS_ENABLED(CONFIG_BT_MESH_DEBUG_VNG_STATUS)
#include "common/log.h"

#define TID_LAG_CONFIG 10
#define CID_VNG 0x00FF

#define BT_MESH_MODEL_OP_VNG_STATUS_SET          BT_MESH_MODEL_OP_3(0x05, CID_VNG)
#define BT_MESH_MODEL_OP_VNG_GROUPS_COUNT_GET    BT_MESH_MODEL_OP_3(0x11, CID_VNG)
#define BT_MESH_MODEL_OP_VNG_GROUPS_LIST_GET     BT_MESH_MODEL_OP_3(0x12, CID_VNG)

#ifdef CONFIG_BT_MESH_VNG_STATUS_GROUP
struct status_internal
{
    u16_t config_tid;
};

static struct status_internal _status_internal;
#endif

int bt_mesh_vng_status_init(struct bt_mesh_model *model)
{
    BT_DBG("");
    struct bt_mesh_vng_status *srv = model->user_data;
    srv->model = model;
    return 0;
}


static void vng_status_set(struct bt_mesh_model *model,
                           struct bt_mesh_msg_ctx *ctx,
                           struct net_buf_simple *buf)
{

    struct bt_mesh_vng_status *srv = model->user_data;

    BT_DBG("net_idx 0x%04x app_idx 0x%04x src 0x%04x len %u: %s",
           ctx->net_idx, ctx->app_idx, ctx->addr, buf->len,
           bt_hex(buf->data, buf->len));

    u16_t time = net_buf_simple_pull_be16(buf);
    u16_t main = net_buf_simple_pull_be16(buf);
    u16_t duration = net_buf_simple_pull_be16(buf);

    u16_t period = 0;
    
    if(buf->len >= 2)
      period = net_buf_simple_pull_be16(buf);

    if(srv->set_func) {
        srv->set_func(time, main, duration, period);
    }
}


#ifdef CONFIG_BT_MESH_VNG_STATUS_GROUP


static inline bool _valid_config_tid(u16_t tid)
{
    if (!valid_tid_u16(_status_internal.config_tid, tid, TID_LAG_CONFIG))
        return false;

    _status_internal.config_tid = tid;

    return true;
}


static void vng_groups_count_get(struct bt_mesh_model *model,
                           struct bt_mesh_msg_ctx *ctx,
                           struct net_buf_simple *buf)
{

    struct bt_mesh_vng_status *srv = model->user_data;

    BT_DBG("net_idx 0x%04x app_idx 0x%04x src 0x%04x len %u: %s",
           ctx->net_idx, ctx->app_idx, ctx->addr, buf->len,
           bt_hex(buf->data, buf->len));

    u16_t index = net_buf_simple_pull_be16(buf);
    u16_t main = net_buf_simple_pull_be16(buf);
    u16_t tid = net_buf_simple_pull_be16(buf);

    if (!_valid_config_tid(tid))
        return;

    if(srv->groups_count_get) {
        srv->groups_count_get(index, main);
    }

}

static void vng_groups_list_get(struct bt_mesh_model *model,
                           struct bt_mesh_msg_ctx *ctx,
                           struct net_buf_simple *buf)
{

    struct bt_mesh_vng_status *srv = model->user_data;

    BT_DBG("net_idx 0x%04x app_idx 0x%04x src 0x%04x len %u: %s",
           ctx->net_idx, ctx->app_idx, ctx->addr, buf->len,
           bt_hex(buf->data, buf->len));

    u16_t index = net_buf_simple_pull_be16(buf);
    u16_t main = net_buf_simple_pull_be16(buf);
    u8_t line = net_buf_simple_pull_u8(buf);
    u8_t  bits = net_buf_simple_pull_u8(buf);
    u16_t tid  = net_buf_simple_pull_be16(buf);

    if (!_valid_config_tid(tid))
        return;

    if(srv->groups_list_get) {
        srv->groups_list_get(index, main, line, bits);
    }

}

#endif 

const struct bt_mesh_model_op bt_mesh_vng_status_op[] = {
    { BT_MESH_MODEL_OP_VNG_STATUS_SET, 6, vng_status_set },

#ifdef CONFIG_BT_MESH_VNG_STATUS_GROUP
    { BT_MESH_MODEL_OP_VNG_GROUPS_COUNT_GET, 6, vng_groups_count_get },
    { BT_MESH_MODEL_OP_VNG_GROUPS_LIST_GET, 8, vng_groups_list_get },
#endif
    BT_MESH_MODEL_OP_END,
};


