/*  Bluetooth Mesh */

/*
 * Copyright (c) 2018 VNG Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <zephyr/types.h>
#include <misc/util.h>
#include <misc/byteorder.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <bluetooth/mesh.h>
#include <bluetooth/mesh/vng_command_srv.h>
#include <device.h>
#include <watchdog.h>

#include "mesh.h"
#include "net.h"
#include "access.h"

#include "tid_check.h"
#ifdef CONFIG_SOC_FAMILY_NRF
#include <nrf.h>
#endif

#define BT_DBG_ENABLED IS_ENABLED(CONFIG_BT_MESH_DEBUG_VNG_COMMAND_SRV)
#include "common/log.h"

#define OP_NETKEY_SET 0
#define OP_NETKEY_ADD 1
#define OP_NETKEY_DEL 2
#define OP_NETKEY_CLR 3

#define NETKEY_COMMON_SLOT 1
#define NETKEY_COMMON_IDX  9
#define NETKEY_USED_SLOT   0


#define RELAY_COUNT_DEFAULT 3
#define RELAY_INTERVAL_DEFAULT 20 //ms

#define MAC_ADDR_SIZE 6
#define CID_VNG 0x00FF

#define TID_LAG 5
#define NETKEYS 10

#define BT_MESH_MODEL_OP_CMD_OTA               BT_MESH_MODEL_OP_3(0x06, CID_VNG)
#define BT_MESH_MODEL_OP_CMD_RESET             BT_MESH_MODEL_OP_3(0x07, CID_VNG)
#define BT_MESH_MODEL_OP_CMD_PROV              BT_MESH_MODEL_OP_3(0x08, CID_VNG)
#define BT_MESH_MODEL_OP_CMD_RELAY             BT_MESH_MODEL_OP_3(0x09, CID_VNG)
#define BT_MESH_MODEL_OP_CMD_TTL               BT_MESH_MODEL_OP_3(0x0A, CID_VNG)
#define BT_MESH_MODEL_OP_CMD_TRANSMIT          BT_MESH_MODEL_OP_3(0x0B, CID_VNG)
#define BT_MESH_MODEL_OP_CMD_NETKEY            BT_MESH_MODEL_OP_3(0x0C, CID_VNG)
#define BT_MESH_MODEL_OP_CMD_NOTIFY            BT_MESH_MODEL_OP_3(0x0D, CID_VNG)


enum netkey_task
{
    ADD_COMMON_KEY = 0,
    ADD_NEW_KEY,
    BIND_COMMON_KEY,
    BIND_NEW_KEY,
};

extern void _net_key_del(u16_t del_ix);

extern void _prov_complete(u16_t net_idx, u16_t addr);

struct msg_ctx 
{
    u16_t tid;
    struct k_delayed_work timer;
    u8_t op;
    u8_t net_idx;
    bool initialized;
    u8_t tasks;
};

static const u8_t net_keys[NETKEYS][16] = {
    {0x7a, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,},
    {0x7b, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,},
    {0x7c, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,}, 
    {0x7d, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,},
    {0x7e, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,}, 
    {0x7f, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,}, 
    {0x80, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,},
    {0x81, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,}, 
    {0x82, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,},
    {0x83, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,}, 
};

static struct msg_ctx msg_ctx = {
    .tid = 0, 
    .tasks = 0,
    .initialized = false,
};

static bool _match_local_mac(u8_t* mac_be)
{
    bt_addr_le_t addr[10];
    int i;
    int count;

    if(mac_be == NULL)
        return false;

    bt_id_get(addr, &count);

    if(count > 0) {
        for(i=0; i<MAC_ADDR_SIZE; i++) {
            if(addr[0].a.val[i] != mac_be[MAC_ADDR_SIZE - i -1])
                return false;
        }

        return true;

    } else {
        return false;
    }

    BT_DBG("addr: %s", bt_addr_le_str(&addr[0]));
 
}


int bt_mesh_vng_cmd_srv_init(struct bt_mesh_model *model, bool primary)
{
    struct bt_mesh_vng_command_srv *srv = model->user_data;
    srv->model = model;
    return 0;
}


static void _vng_command_ota(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{
    
    //TODO erase code.

    
    u8_t mac_be[MAC_ADDR_SIZE];
    
    u16_t timeout = net_buf_simple_pull_be16(buf);

    memcpy(mac_be, buf->data, MAC_ADDR_SIZE);

    BT_DBG("timeout: %us", timeout);

    BT_DBG("mac_be: %s", bt_hex(mac_be, MAC_ADDR_SIZE));

    if(!_match_local_mac(mac_be))
        return;

    BT_DBG("Preparing for OTA...");
#ifdef CONFIG_SOC_FAMILY_NRF

    NRF_MWU->PREGION[0].SUBS &= ~(MWU_PREGION_SUBS_SR0_Include << MWU_PREGION_SUBS_SR0_Pos);
     __DSB();
    NRF_POWER->GPREGRET2 = timeout & 0xFF;
     __DSB();
    NRF_POWER->GPREGRET = (timeout >> 8)& 0xFF;
    __DSB();
    NRF_MWU->PREGION[0].SUBS |= (MWU_PREGION_SUBS_SR0_Include << MWU_PREGION_SUBS_SR0_Pos);
    __DSB();
    NVIC_SystemReset();


#endif
}

static void _vng_command_reset(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{
    BT_DBG("");
#ifdef CONFIG_SOC_FAMILY_NRF
    NVIC_SystemReset();
#endif
    
}



static void _vng_command_renew_addr(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{
    

    int err;

    u16_t addr;
    u8_t mac_be[MAC_ADDR_SIZE];
    
    addr = net_buf_simple_pull_be16(buf);
    memcpy(mac_be, buf->data, MAC_ADDR_SIZE);


    BT_DBG("new addr: 0x%04x", addr);
    BT_DBG("mac_be: %s", bt_hex(mac_be, MAC_ADDR_SIZE));

    if(BT_MESH_ADDR_IS_UNICAST(addr)) {

        if(!_match_local_mac(mac_be))
            return;

        BT_DBG("Renewing mesh addr...");
        
        err = bt_mesh_renew_addr(addr);

        if (err) {
            BT_DBG("Renew failed\n");
        }  
    }
}


static void _vng_command_relay_set(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{
    
    int err;

    u8_t enable;
    u8_t count = RELAY_COUNT_DEFAULT;
    u16_t interval = RELAY_INTERVAL_DEFAULT;

    u16_t primary;
    
    enable = net_buf_simple_pull_u8(buf);
    if(buf->len > 0) {
        count = net_buf_simple_pull_u8(buf);
        if(buf->len > 1)
            interval = net_buf_simple_pull_be16(buf);
    }

    enable = (enable != 0) ? BT_MESH_RELAY_ENABLED : BT_MESH_RELAY_DISABLED;

    primary = bt_mesh_primary_addr();

    BT_DBG("enable: %d, count %d", enable, count);

    err =  bt_mesh_cfg_relay_set(bt_mesh.sub[0].net_idx, primary, enable,
          BT_MESH_TRANSMIT(count, interval), NULL, NULL);

    if(err) {
        BT_ERR("bt_mesh_cfg_relay_set(), err %d", err);
    }

}




static void _vng_command_ttl_set(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{
    
    int err;

    u8_t ttl;
    u16_t primary;
  
    
    ttl = net_buf_simple_pull_u8(buf);
    
    primary = bt_mesh_primary_addr();

    BT_DBG("ttl: %d", ttl);

    err =  bt_mesh_cfg_ttl_set(bt_mesh.sub[0].net_idx, primary, ttl, NULL);

    if(err) {
        BT_ERR("bt_mesh_cfg_ttl_set(), err %d", err);
    }
    
}


static void _vng_command_transmit_set(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{
    
  
    u8_t transmit;
    u16_t primary;
      
    transmit = net_buf_simple_pull_u8(buf);
    
    primary = bt_mesh_primary_addr();

    BT_DBG("transmit: %d", transmit);
    
    BT_DBG("not supported");    
}

static void netkey_work(struct k_work* work)
{
    int err;
    
    if(msg_ctx.tasks & BIT(ADD_COMMON_KEY)) {

        msg_ctx.tasks &= ~BIT(ADD_COMMON_KEY);

        err = bt_mesh_cfg_net_key_add(bt_mesh.sub[NETKEY_USED_SLOT].net_idx, 
                        bt_mesh_primary_addr(), 
                        NETKEY_COMMON_IDX,
			            net_keys[NETKEY_COMMON_IDX], NULL);

        // if(err == 0)
        //      msg_ctx.tasks |= BIT(BIND_COMMON_KEY);

        k_delayed_work_submit(&msg_ctx.timer, 300);

    } else if(msg_ctx.tasks & BIT(BIND_COMMON_KEY)) {

        BT_DBG("BIND_COMMON_KEY");
        msg_ctx.tasks &= ~BIT(BIND_COMMON_KEY);

        _prov_complete(NETKEY_COMMON_IDX, bt_mesh_primary_addr());

        k_delayed_work_submit(&msg_ctx.timer, 300);

    } else if(msg_ctx.tasks & BIT(ADD_NEW_KEY)) {
        BT_DBG("ADD_NEW_KEY");
        msg_ctx.tasks &= ~BIT(ADD_NEW_KEY);
        if(bt_mesh.sub[NETKEY_USED_SLOT].net_idx == msg_ctx.net_idx) {
            BT_DBG("Match old key");
            return;
        }
        _net_key_del(bt_mesh.sub[NETKEY_USED_SLOT].net_idx);

        err = bt_mesh_cfg_net_key_add(bt_mesh.sub[NETKEY_COMMON_SLOT].net_idx, 
                    bt_mesh_primary_addr(), 
                    msg_ctx.net_idx,
                    net_keys[msg_ctx.net_idx], NULL);
        if(err == 0)
            msg_ctx.tasks |= BIT(BIND_NEW_KEY);
        
        k_delayed_work_submit(&msg_ctx.timer, 300);

    } else if(msg_ctx.tasks & BIT(BIND_NEW_KEY)) {

        msg_ctx.tasks &= ~BIT(BIND_NEW_KEY);
        _prov_complete(msg_ctx.net_idx, bt_mesh_primary_addr());
    }   
}

static void _vng_command_netkey(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{
    
    u16_t tid;
    u8_t op;
    u8_t index;
    u16_t primary;

    op = net_buf_simple_pull_u8(buf);
    
    index = net_buf_simple_pull_u8(buf);

    tid = net_buf_simple_pull_be16(buf);

    if(!valid_tid_u16(msg_ctx.tid, tid, TID_LAG)) {
        BT_DBG("invalid tid %u", tid);
        return;
    }

    msg_ctx.tid = tid;

    primary = bt_mesh_primary_addr();
    
    if(op == OP_NETKEY_SET) {
        
        BT_DBG("OP_NETKEY_SET, old idx %u, new idx %u", bt_mesh.sub[NETKEY_USED_SLOT].net_idx, index);
        bt_mesh_renew_netkey(index, net_keys[index]);
        return;

    } else if(op == OP_NETKEY_ADD) {

        BT_WARN("not support OP_NETKEY_ADD");

    } else if(op == OP_NETKEY_DEL) {

        _net_key_del(index);

    } else {
        BT_WARN("not support");
    }

}

static void _vng_command_notify_set(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{
    u8_t device_type;
    u8_t success;
    u8_t failed;
    u8_t success_repeat;
    u8_t fail_repeat;
    
    u16_t tid_ctx;

    struct bt_mesh_vng_command_srv *srv = model->user_data;

    device_type = net_buf_simple_pull_u8(buf);

    success = net_buf_simple_pull_u8(buf);
    failed = net_buf_simple_pull_u8(buf);
    success_repeat = net_buf_simple_pull_u8(buf);
    fail_repeat    = net_buf_simple_pull_u8(buf);
    tid_ctx =   net_buf_simple_pull_be16(buf);

    if(!valid_tid_u16(srv->tid, tid_ctx, TID_LAG)) {
        BT_DBG("invalid tid %u", tid_ctx);
        return;
    }

    srv->tid = tid_ctx;
    SYS_LOG_DBG("TID: %d", srv->tid);

    BT_DBG("net_idx 0x%04x app_idx 0x%04x src 0x%04x len %u: %s",
           ctx->net_idx, ctx->app_idx, ctx->addr, buf->len,
           bt_hex(buf->data, buf->len));

    if(srv->notify){
        srv->notify(device_type, 
                    success, failed, 
                    success_repeat, 
                    fail_repeat , 
                    srv->user_data);
    }
}

const struct bt_mesh_model_op bt_mesh_vng_command_srv_op[] = {
	{ BT_MESH_MODEL_OP_CMD_OTA, 8, _vng_command_ota },
	{ BT_MESH_MODEL_OP_CMD_RESET, 1, _vng_command_reset },
    { BT_MESH_MODEL_OP_CMD_PROV, 8, _vng_command_renew_addr },
    { BT_MESH_MODEL_OP_CMD_RELAY, 1, _vng_command_relay_set },
    { BT_MESH_MODEL_OP_CMD_TTL, 1, _vng_command_ttl_set },
    { BT_MESH_MODEL_OP_CMD_TRANSMIT, 1, _vng_command_transmit_set },
    { BT_MESH_MODEL_OP_CMD_NETKEY, 4, _vng_command_netkey },
    { BT_MESH_MODEL_OP_CMD_NOTIFY, 7, _vng_command_notify_set},
    BT_MESH_MODEL_OP_END,
};
