#ifndef _COMPOSITION_H
#define _COMPOSITION_H

#ifdef __cplusplus
extern "C" {
#endif

#include <zephyr.h>
#include <bluetooth/mesh.h>
#include <board.h>

#include <host/mesh/net.h>
#include <host/mesh/adv.h>
#include <host/mesh/access.h>

#include "ble_mesh.h"

#define STATE_OFF				0x00
#define STATE_ON				0x01

#define GPIO_QUANTITY			8
#define INIT_TID				0

#define PERIOD_DEFAULT			5

#define COMPARE_TIME_ZDC_ONOFF_HUB		309

#define CID_VNG 						0x00FF
#define GROUP_ADDR_STATUS_TO_GW 		0xE000
#define GROUP_ADDR_STATUS_CONTROL		0xD000
#define VNG_MESH_RENEW_GROUP 			0xC002

#define GROUP_ADRR_UNUSED 				0xCFFF		

#define	EDGE    		(GPIO_INT_EDGE | GPIO_INT_ACTIVE_HIGH)

#define TIME_TO_LIVE			10

#define BT_MESH_MODEL_ID_VND_ONOFF_SRV				0x2001
#define BT_MESH_MODEL_ID_VND_ONOFF_CFG_SRV			0x2002
#define BT_MESH_MODEL_ID_VND_STT_DEVICE_SRV			0x2003

/* Model Operation Codes */
#define BT_MESH_MODEL_OP_GEN_ONOFF_GET			BT_MESH_MODEL_OP_2(0x82, 0x01)
#define BT_MESH_MODEL_OP_GEN_ONOFF_SET_ACK		BT_MESH_MODEL_OP_2(0x82, 0x02)
#define BT_MESH_MODEL_OP_GEN_ONOFF_SET_UNACK	BT_MESH_MODEL_OP_2(0x82, 0x03)

#define BT_MESH_MODEL_OP_VND_VNG_STATUS			BT_MESH_MODEL_OP_3(0x04, CID_VNG)
#define BT_MESH_MODEL_OP_VND_VNG_CTRL_STT		BT_MESH_MODEL_OP_3(0x05, CID_VNG)
#define BT_MESH_MODEL_OP_VND_VNG_GET_SUB_COUNT	BT_MESH_MODEL_OP_3(0x11, CID_VNG)
#define BT_MESH_MODEL_OP_VND_VNG_GET_SUB_LIST	BT_MESH_MODEL_OP_3(0x12, CID_VNG)

#define BT_MESH_MODEL_OP_VND_SET_ALL			BT_MESH_MODEL_OP_2(0x83, 0x05)

#define BT_MESH_MODEL_OP_VND_ADD_SUB_GROUP		BT_MESH_MODEL_OP_2(0x90, 0x02)
#define BT_MESH_MODEL_OP_VND_DEL_SUB_GROUP		BT_MESH_MODEL_OP_2(0x90, 0x03)
#define BT_MESH_MODEL_OP_VND_CLR_SUB_GROUP		BT_MESH_MODEL_OP_2(0x90, 0x04)

#define BT_MESH_MODEL_OP_VND_SUB_COUNT 			BT_MESH_MODEL_OP_2(0x90, 0x20)
#define BT_MESH_MODEL_OP_VND_SUB_LIST 			BT_MESH_MODEL_OP_2(0x90, 0x24)

#define VNG_CFG_SUB_ADD 			0x00
#define VNG_CFG_SUB_DEL 			0x01
#define VNG_CFG_SUB_CLR 			0x02

#define PIN_SET(pin) 	NRF_GPIO->OUTSET = BIT(pin)
#define PIN_CLR(pin) 	NRF_GPIO->OUTCLR = BIT(pin)
    
struct gen_onoff_state {
	u8_t onoff_value;
	u16_t mod_tid;
	u8_t transition;
	u8_t delay;
	u32_t gpio_port;

	struct tid_group_store {
		u16_t group_addr;
		u16_t group_tid;
	} groups[CONFIG_BT_MESH_MODEL_GROUP_COUNT];

	struct device *gpio_dev;
};

struct cfg_sub_state {
	u16_t mod_tid;
};

extern struct gen_onoff_state onoff_state[];

extern struct bt_mesh_model root_models[];
extern struct bt_mesh_model vnd_models[];
extern struct bt_mesh_model secondary_0_models[];
extern struct bt_mesh_model secondary_1_models[];
extern struct bt_mesh_model secondary_2_models[];
extern struct bt_mesh_model secondary_3_models[];
extern struct bt_mesh_model secondary_4_models[];
extern struct bt_mesh_model secondary_5_models[];
extern struct bt_mesh_model secondary_6_models[];

extern struct bt_mesh_elem elements[];

extern const struct bt_mesh_comp comp;

extern struct k_delayed_work status_work;

extern struct device *gpio_dev;

void status_work_timeout(struct k_work *work);

void zero_detect(struct device *gpiob, struct gpio_callback *cb,
		    u32_t pins);

void start_timer(void);

void init_sub_group_dt(void);

#ifdef __cplusplus
}
#endif

#endif /* _COMPOSITION_H */
