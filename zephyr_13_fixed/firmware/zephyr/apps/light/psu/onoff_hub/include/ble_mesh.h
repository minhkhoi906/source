#ifndef _BLUETOOTH_H
#define _BLUETOOTH_H

#ifdef __cplusplus
extern "C" {
#endif

#include <gpio.h>

#include <settings/settings.h>
	
#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <bluetooth/l2cap.h>
#include <bluetooth/hci.h>
#include <bluetooth/mesh.h>

#define MODELS_BIND(models, vnd, net_idx, app_idx) \
{ \
    size_t size = ARRAY_SIZE(models);\
    size_t idx; \
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    for(idx = 0; idx < size; idx++) { \
        addr = elements[models[idx].elem_idx].addr;\
        if(vnd) {\
            SYS_LOG_DBG("vnd bind addr [0x%04x]", addr);\
            bt_mesh_cfg_mod_app_bind_vnd(net_idx, primary_addr, addr, app_idx, models[idx].vnd.id, \
                models[idx].vnd.company, NULL);\
        } else {\
            SYS_LOG_DBG("bind addr [0x%04x]", addr);\
            bt_mesh_cfg_mod_app_bind(net_idx, primary_addr, addr,\
            app_idx, models[idx].id, NULL);\
        }\
    } \
}

#define MODEL_PUB(model, vnd, net_idx, pub)\
{\
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    addr = elements[model.elem_idx].addr;\
    if(vnd) {\
        SYS_LOG_DBG("vnd pub addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_pub_set_vnd(net_idx, primary_addr, addr,\
            model.vnd.id, model.vnd.company, pub, NULL);\
    } else {\
        SYS_LOG_DBG("pub addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_pub_set(net_idx, primary_addr, addr,\
            model.id, pub, NULL);\
    }\
}

#define MODEL_SUB_ADD(model, vnd, net_idx, sub_addr)\
{\
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    addr = elements[model->elem_idx].addr;\
    if(vnd) {\
        SYS_LOG_DBG("vnd sub addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_sub_add_vnd(net_idx, primary_addr, addr,\
            sub_addr, model->vnd.id, model->vnd.company, NULL);\
    } else {\
        SYS_LOG_DBG("sub addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_sub_add(net_idx, primary_addr, addr,\
            sub_addr, model->id, NULL);\
    }\
}

#define MODEL_SUB_DEL(model, vnd, net_idx, sub_addr)\
{\
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    addr = elements[model->elem_idx].addr;\
    if(vnd) {\
        SYS_LOG_DBG("vnd del addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_sub_del_vnd(net_idx, primary_addr, addr,\
            sub_addr, model->vnd.id, model->vnd.company, NULL);\
    } else {\
        SYS_LOG_DBG("del addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_sub_del(net_idx, primary_addr, addr,\
            sub_addr, model->id, NULL);\
    }\
}

#define MODEL_SUB_OVERWRITE(model, vnd, net_idx, sub_addr)\
{\
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    addr = elements[model->elem_idx].addr;\
    if(vnd) {\
        SYS_LOG_DBG("vnd overwrite addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_sub_overwrite_vnd(net_idx, primary_addr, addr,\
            sub_addr, model->vnd.id, model->vnd.company, NULL);\
    } else {\
        SYS_LOG_DBG("overwrite addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_sub_overwrite(net_idx, primary_addr, addr,\
            sub_addr, model->id, NULL);\
    }\
}

#define COMMAND_ID			0xFF
#define DEVICE_TYPE			0x0028

extern const u16_t net_idx;
extern const u8_t app_idx;

void bt_ready(int err);

#ifdef __cplusplus
}
#endif

#endif /* _BLUETOOTH_H */