#include "../include/composition.h"
#include "../include/ble_mesh.h"
#include "../include/watch_dog.h"

/*
 * The include must follow the define for it to take effect.
 * If it isn't, the domain defaults to "general"
 */

#define SYS_LOG_DOMAIN "ONOFF_HUB_Main"
#include <logging/sys_log.h>

struct device *gpio_dev;
static struct gpio_callback gpio_cb;

static void gpio_init(struct device *dev)
{
	gpio_pin_configure(dev,
					   ZDC_GPIO_PIN, 
					   GPIO_DIR_IN | GPIO_INT | GPIO_PUD_NORMAL | EDGE);

	gpio_init_callback(&gpio_cb, zero_detect, BIT(ZDC_GPIO_PIN));
	gpio_add_callback(dev, &gpio_cb);

	/* GPIO configiuratin & setting */

	for (int i = 0; i < GPIO_QUANTITY; ++i) {
		gpio_pin_configure(dev,
						   onoff_state[i].gpio_port, 
						   GPIO_DIR_OUT | GPIO_PUD_NORMAL);

		gpio_pin_write(dev, onoff_state[i].gpio_port, STATE_ON);
	}

	gpio_pin_configure(dev,
					   BYPASS_PIN, 
					   GPIO_DIR_OUT | GPIO_PUD_PULL_DOWN);

	gpio_pin_write(dev, BYPASS_PIN, STATE_OFF);
}

void main(void)
{
	int err;

	SYS_LOG_DBG("Initializing...");

	/* Initialize watch dog */
	watchdog_init();

	random_init();

	gpio_dev = device_get_binding(CONFIG_GPIO_P0_DEV_NAME);

	/* Initialize the Bluetooth Subsystem */
	err = bt_enable(bt_ready);
	if (err) {
		SYS_LOG_ERR("Bluetooth init failed (err %d)", err);
	}

	gpio_init(gpio_dev);

	k_delayed_work_init(&status_work, status_work_timeout);

	start_timer();
}
