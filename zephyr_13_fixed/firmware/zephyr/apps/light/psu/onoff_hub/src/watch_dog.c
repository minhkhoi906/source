#include <watchdog.h>
#include <debug/object_tracing.h>
#include <entropy.h>

#include "../include/watch_dog.h"

/*
 * The include must follow the define for it to take effect.
 * If it isn't, the domain defaults to "general"
 */

#define SYS_LOG_DOMAIN "ONOFF_HUB_Watchdog"
#include <logging/sys_log.h>

static u8_t channel_wdt;
static struct wdt_timeout_cfg m_cfg_wdt = {
	.callback = NULL,
	.flags = WDT_FLAG_RESET_SOC,
};
static struct device *wdt;
static struct k_delayed_work wtd_work;
static int max_thread_quantity;

static inline int thread_monitor(void)
{
	int obj_counter = 0;
	struct k_thread *thread_list = NULL;

	/* wait a bit to allow any initialization-only threads to terminate */

	thread_list   = (struct k_thread *)SYS_THREAD_MONITOR_HEAD;
	while (thread_list != NULL) {
		thread_list = (struct k_thread *)SYS_THREAD_MONITOR_NEXT(thread_list);
		obj_counter++;
	}
	
	return obj_counter;
}

static void watchdog_timeout(struct k_work *work)
{
	int thread_quantity;
	thread_quantity = thread_monitor();

	if (thread_quantity == max_thread_quantity) {
		wdt_feed(wdt, channel_wdt);
	}

	k_delayed_work_submit(&wtd_work, WDT_FEED_TIMEOUT);
}

void watchdog_init(void)
{
	int err;

	max_thread_quantity = 7;

	wdt = device_get_binding(WDT_DEV_NAME);

	m_cfg_wdt.window.max = WDT_TIMEOUT;

	channel_wdt = wdt_install_timeout(wdt, &m_cfg_wdt);
	if (channel_wdt < 0) {
		SYS_LOG_ERR("Watchdog install error\n");
	}

	err = wdt_setup(wdt, 0);
	if (err < 0) {
		SYS_LOG_ERR("Watchdog setup error\n");
	}

	SYS_LOG_DBG("Watch dog initialized, thread quantity [%d]", max_thread_quantity);

	/* Initialize queue handle watchdog */
	k_delayed_work_init(&wtd_work, watchdog_timeout);
	k_delayed_work_submit(&wtd_work, K_SECONDS(5));
}

struct device *rand_dev;

void random_init(void)
{
	rand_dev = device_get_binding(CONFIG_ENTROPY_NAME);
	if (!rand_dev) {
		SYS_LOG_DBG("Error: no entropy device");
		return;
	}
}

#define BUFFER_LENGTH	3

static u16_t rand(void)
{
	u8_t buffer[BUFFER_LENGTH] = {0};
	int r;
	u16_t random;

	r = entropy_get_entropy(rand_dev, buffer, BUFFER_LENGTH - 1);
	if (r) {
		SYS_LOG_DBG("entropy_get_entropy failed: %d", r);
		return -1;
	}

	if (buffer[BUFFER_LENGTH - 1] != 0) {
		SYS_LOG_DBG("entropy_get_entropy buffer overflow");
	}

	random = random | (u32_t)buffer[0] << 8
					| (u32_t)buffer[1] << 0;

	return random;
}

u16_t rand_in_range(struct range t)
{
	if (t.max == 0) {
		return 0;
	}
	
	return t.min + rand()%(t.max-t.min+1);
}
