#include <bluetooth/mesh.h>
#include <kernel.h>
#include <host/mesh/tid_check.h>

#include "../include/composition.h"
#include "../include/ble_mesh.h"
#include "../include/watch_dog.h"

/*
 * The include must follow the define for it to take effect.
 * If it isn't, the domain defaults to "general"
 */

#define SYS_LOG_DOMAIN "ONOFF_Composition"
#include <logging/sys_log.h>

/* Vendor Model message handlers*/
static void gen_onoff_get(struct bt_mesh_model *model,
					    struct bt_mesh_msg_ctx *ctx,
					    struct net_buf_simple *buf);

static void gen_onoff_set_unack(struct bt_mesh_model *model,
					    struct bt_mesh_msg_ctx *ctx,
					    struct net_buf_simple *buf);

static void gen_onoff_set_ack(struct bt_mesh_model *model,
					    struct bt_mesh_msg_ctx *ctx,
					    struct net_buf_simple *buf);

static void vnd_onoff_set_all(struct bt_mesh_model *model,
					    struct bt_mesh_msg_ctx *ctx,
					    struct net_buf_simple *buf);

static void vnd_cfg_add_sub_group(struct bt_mesh_model *model,
					    struct bt_mesh_msg_ctx *ctx,
					    struct net_buf_simple *buf);

static void vnd_cfg_del_sub_group(struct bt_mesh_model *model,
					    struct bt_mesh_msg_ctx *ctx,
					    struct net_buf_simple *buf);

static void vnd_cfg_clr_sub_group(struct bt_mesh_model *model,
					    struct bt_mesh_msg_ctx *ctx,
					    struct net_buf_simple *buf);

/* Handle status control device vendor model */
static void vnd_status_device(struct bt_mesh_model *model,
				       struct bt_mesh_msg_ctx *ctx,
				       struct net_buf_simple *buf);

static void vnd_get_sub_count(struct bt_mesh_model *model,
				       struct bt_mesh_msg_ctx *ctx,
				       struct net_buf_simple *buf);

static void vnd_get_sub_list(struct bt_mesh_model *model,
				       struct bt_mesh_msg_ctx *ctx,
				       struct net_buf_simple *buf);

static u8_t encode_data_sub_count_line(u8_t cnt, u16_t crc);
static void vng_device_status(void);
static void send_sub_count_line(void);
static void send_sub_list_line(u8_t line);
static void onoff_processing(void);
static bool check_tid_sub(u16_t dst, u16_t cur_tid, struct gen_onoff_state *state);
static void _onoff_isr(void);

static int model_sub_add(struct bt_mesh_model *mol, 
						bool vnd, 
						u16_t net_idx, 
						u16_t sub_addr);

static int model_sub_del(struct bt_mesh_model *mol, 
						bool vnd, 
						u16_t net_idx, 
						u16_t sub_addr);

static int model_sub_overwrite(struct bt_mesh_model *mol, 
						bool vnd, 
						u16_t net_idx, 
						u16_t sub_addr);

static void update_sub_group_dt(struct gen_onoff_state *dt,
						 		u8_t op,
						 		u16_t sub_addr);

/*
 * OnOff Model Server Op Dispatch Table
 *
 */

static const struct bt_mesh_model_op gen_onoff_srv_op[] = {
	{ BT_MESH_MODEL_OP_GEN_ONOFF_GET, 4, gen_onoff_get },
	{ BT_MESH_MODEL_OP_GEN_ONOFF_SET_ACK, 6, gen_onoff_set_ack },
	{ BT_MESH_MODEL_OP_GEN_ONOFF_SET_UNACK, 6, gen_onoff_set_unack },
	BT_MESH_MODEL_OP_END,
};

static const struct bt_mesh_model_op vnd_onoff_srv_op[] = {
	{ BT_MESH_MODEL_OP_VND_SET_ALL, 6, vnd_onoff_set_all },
	BT_MESH_MODEL_OP_END,
};

static const struct bt_mesh_model_op vnd_cfg_srv_op[] = {
	{ BT_MESH_MODEL_OP_VND_ADD_SUB_GROUP, 1, vnd_cfg_add_sub_group },
	{ BT_MESH_MODEL_OP_VND_DEL_SUB_GROUP, 1, vnd_cfg_del_sub_group },
	{ BT_MESH_MODEL_OP_VND_CLR_SUB_GROUP, 1, vnd_cfg_clr_sub_group },
	BT_MESH_MODEL_OP_END,
};

/* Mapping of message handlers for Vendor */
static const struct bt_mesh_model_op vng_stt_device_ops[] = {
	{ BT_MESH_MODEL_OP_VND_VNG_CTRL_STT, 8, vnd_status_device},
	{ BT_MESH_MODEL_OP_VND_VNG_GET_SUB_COUNT, 6, vnd_get_sub_count},
	{ BT_MESH_MODEL_OP_VND_VNG_GET_SUB_LIST, 8, vnd_get_sub_list},
	BT_MESH_MODEL_OP_END,
};

/*
 * Server Configuration Declaration
 */

static struct bt_mesh_cfg_srv cfg_srv = {
#if defined(CONFIG_BT_MESH_RELAY)
	.relay = BT_MESH_RELAY_ENABLED,
#else
	.relay = BT_MESH_RELAY_NOT_SUPPORTED,
#endif
	.beacon = BT_MESH_BEACON_ENABLED,
#if defined(CONFIG_BT_MESH_FRIEND)
	.frnd = BT_MESH_FRIEND_ENABLED,
#else
	.frnd = BT_MESH_FRIEND_NOT_SUPPORTED,
#endif
#if defined(CONFIG_BT_MESH_GATT_PROXY)
	.gatt_proxy = BT_MESH_GATT_PROXY_ENABLED,
#else
	.gatt_proxy = BT_MESH_GATT_PROXY_NOT_SUPPORTED,
#endif
	.default_ttl = 7,

	/* 3 transmissions with 20ms interval */
	.net_transmit = BT_MESH_TRANSMIT(1, 20),
	.relay_retransmit = BT_MESH_TRANSMIT(1, 20),
};

/*
 * Health Server Declaration
 */

static struct bt_mesh_health_srv health_srv = {
};

/*
 * Client Configuration Declaration
 */

static struct bt_mesh_cfg_cli cfg_cli = {
};

static struct bt_mesh_vng_command_srv vng_command_srvs = {
};

static struct cfg_sub_state vnd_cfg = {
	.mod_tid = INIT_TID,
};

static struct cfg_sub_state vnd_get_dt = {
	.mod_tid = INIT_TID,
};

static struct gen_onoff_state onoff_state_all = {
	.mod_tid = INIT_TID,
};

/* Definitions of models publication context */
BT_MESH_HEALTH_PUB_DEFINE(health_pub, 0);

BT_MESH_MODEL_PUB_DEFINE(vnd_onoff_pub_srv, NULL, 3 + 10);

/* Definitions of models user data */
struct gen_onoff_state onoff_state[] = {
	{	
		.gpio_port = ONOFF1_GPIO_PIN, .mod_tid = INIT_TID, .onoff_value = STATE_ON, 
		.groups = {[0 ... (CONFIG_BT_MESH_MODEL_GROUP_COUNT - 1)].group_tid = INIT_TID}
	},
	{	
		.gpio_port = ONOFF2_GPIO_PIN, .mod_tid = INIT_TID, .onoff_value = STATE_ON,
		.groups = {[0 ... (CONFIG_BT_MESH_MODEL_GROUP_COUNT - 1)].group_tid = INIT_TID}
	},
	{	
		.gpio_port = ONOFF3_GPIO_PIN, .mod_tid = INIT_TID, .onoff_value = STATE_ON,
		.groups = {[0 ... (CONFIG_BT_MESH_MODEL_GROUP_COUNT - 1)].group_tid = INIT_TID}
	},
	{
		.gpio_port = ONOFF4_GPIO_PIN, .mod_tid = INIT_TID, .onoff_value = STATE_ON,
		.groups = {[0 ... (CONFIG_BT_MESH_MODEL_GROUP_COUNT - 1)].group_tid = INIT_TID}
	},
	{
		.gpio_port = ONOFF5_GPIO_PIN, .mod_tid = INIT_TID, .onoff_value = STATE_ON,
		.groups = {[0 ... (CONFIG_BT_MESH_MODEL_GROUP_COUNT - 1)].group_tid = INIT_TID}
	},
	{
		.gpio_port = ONOFF6_GPIO_PIN, .mod_tid = INIT_TID, .onoff_value = STATE_ON,
		.groups = {[0 ... (CONFIG_BT_MESH_MODEL_GROUP_COUNT - 1)].group_tid = INIT_TID}
	},
	{
		.gpio_port = ONOFF7_GPIO_PIN, .mod_tid = INIT_TID, .onoff_value = STATE_ON,
		.groups = {[0 ... (CONFIG_BT_MESH_MODEL_GROUP_COUNT - 1)].group_tid = INIT_TID}
	},
	{
		.gpio_port = ONOFF8_GPIO_PIN, .mod_tid = INIT_TID, .onoff_value = STATE_ON,
		.groups = {[0 ... (CONFIG_BT_MESH_MODEL_GROUP_COUNT - 1)].group_tid = INIT_TID}
	},
};

struct k_delayed_work status_work;

const u8_t app_idx;
/* timer backup random from 80s to 100s*/

static struct range status_bk_range = {
	.min = 0,
	.max = 0,
};

/* timer control status random from 0 to max_time*/
static struct range status_ctrl_range = {
	.min = 0,
};

/*
 * Element Model Declarations
 *
 * Element 0  Root Models
 */

struct bt_mesh_model root_models[] = {
	BT_MESH_MODEL_CFG_SRV(&cfg_srv),
	BT_MESH_MODEL_CFG_CLI(&cfg_cli),
	BT_MESH_MODEL_HEALTH_SRV(&health_srv, &health_pub),
	BT_MESH_MODEL(BT_MESH_MODEL_ID_GEN_ONOFF_SRV, 
				  gen_onoff_srv_op, NULL, 
				  &onoff_state[0]),
	BT_MESH_MODEL(BT_MESH_MODEL_ID_VND_ONOFF_SRV,
				  vnd_onoff_srv_op, &vnd_onoff_pub_srv,
				  &onoff_state_all),
	BT_MESH_MODEL(BT_MESH_MODEL_ID_VND_ONOFF_CFG_SRV,
				  vnd_cfg_srv_op, NULL, &vnd_cfg)
};
/* Element 1 Models */
struct bt_mesh_model secondary_0_models[] = {
	BT_MESH_MODEL(BT_MESH_MODEL_ID_GEN_ONOFF_SRV, 
				  gen_onoff_srv_op, NULL,
				  &onoff_state[1]),
};
/* Element 2 Models */
struct bt_mesh_model secondary_1_models[] = {
	BT_MESH_MODEL(BT_MESH_MODEL_ID_GEN_ONOFF_SRV, 
				  gen_onoff_srv_op, NULL,
				  &onoff_state[2]),
};
/* Element 3 Models */
struct bt_mesh_model secondary_2_models[] = {
	BT_MESH_MODEL(BT_MESH_MODEL_ID_GEN_ONOFF_SRV, 
				  gen_onoff_srv_op, NULL,
				  &onoff_state[3]),
};
/* Element 4 Models */
struct bt_mesh_model secondary_3_models[] = {
	BT_MESH_MODEL(BT_MESH_MODEL_ID_GEN_ONOFF_SRV, 
				  gen_onoff_srv_op, NULL,
				  &onoff_state[4]),
};
/* Element 5 Models */
struct bt_mesh_model secondary_4_models[] = {
	BT_MESH_MODEL(BT_MESH_MODEL_ID_GEN_ONOFF_SRV, 
				  gen_onoff_srv_op, NULL,
				  &onoff_state[5]),
};
/* Element 6 Models */
struct bt_mesh_model secondary_5_models[] = {
	BT_MESH_MODEL(BT_MESH_MODEL_ID_GEN_ONOFF_SRV, 
				  gen_onoff_srv_op, NULL,
				  &onoff_state[6]),
};
/* Element 7 Models */
struct bt_mesh_model secondary_6_models[] = {
	BT_MESH_MODEL(BT_MESH_MODEL_ID_GEN_ONOFF_SRV, 
				  gen_onoff_srv_op, NULL,
				  &onoff_state[7]),
};

struct bt_mesh_model vnd_models[] = {
	BT_MESH_MODEL_VND(CID_VNG, 
					  BT_MESH_MODEL_ID_VND_STT_DEVICE_SRV, 
					  vng_stt_device_ops, NULL, &vnd_get_dt),
	BT_MESH_MODEL_VNG_COMMAND_SRV(&vng_command_srvs),
};

struct bt_mesh_elem elements[] = {
	BT_MESH_ELEM(0, root_models, vnd_models),
	BT_MESH_ELEM(0, secondary_0_models, BT_MESH_MODEL_NONE),
	BT_MESH_ELEM(0, secondary_1_models, BT_MESH_MODEL_NONE),
	BT_MESH_ELEM(0, secondary_2_models, BT_MESH_MODEL_NONE),
	BT_MESH_ELEM(0, secondary_3_models, BT_MESH_MODEL_NONE),
	BT_MESH_ELEM(0, secondary_4_models, BT_MESH_MODEL_NONE),
	BT_MESH_ELEM(0, secondary_5_models, BT_MESH_MODEL_NONE),
	BT_MESH_ELEM(0, secondary_6_models, BT_MESH_MODEL_NONE),
};

struct bt_mesh_model *model_srv_sw[] = {
	&root_models[3],
	&secondary_0_models[0],
	&secondary_1_models[0],
	&secondary_2_models[0],
	&secondary_3_models[0],
	&secondary_4_models[0],
	&secondary_5_models[0],
	&secondary_6_models[0],
};

const struct bt_mesh_comp comp = {
	.cid = CID_VNG,
	.elem = elements,
	.elem_count = ARRAY_SIZE(elements),
};

/* Generic Model message handlers*/
static void gen_onoff_get(struct bt_mesh_model *model,
					    struct bt_mesh_msg_ctx *ctx,
					    struct net_buf_simple *buf)
{
	struct gen_onoff_state *state = model->user_data;

	u16_t elem_addr = bt_mesh_primary_addr() + model->elem_idx;
	u16_t dst_addr = ctx->recv_dst;

	u16_t timeout, cur_tid;
	u16_t ctrl_timeout;

	timeout = net_buf_simple_pull_be16(buf);
	cur_tid = net_buf_simple_pull_be16(buf);

	if (elem_addr == dst_addr) {
		if (!valid_tid_u16(state->mod_tid, cur_tid, 5)) {
	        SYS_LOG_DBG("Error TID");
	        return;
	    }

		state->mod_tid = cur_tid;

	} else {
		if (!check_tid_sub(dst_addr, cur_tid, state)) {
			return;
		}
	}

	status_ctrl_range.max = timeout;
	ctrl_timeout = rand_in_range(status_ctrl_range);

	k_delayed_work_cancel(&status_work);
	k_delayed_work_submit(&status_work, K_SECONDS(ctrl_timeout));
}

static void gen_onoff_set_unack(struct bt_mesh_model *model,
							  struct bt_mesh_msg_ctx *ctx,
							  struct net_buf_simple *buf)
{
	struct gen_onoff_state *state = model->user_data;

	u8_t onoff, tid_old;
	u8_t transition, delay;

	u16_t cur_tid;

	u16_t elem_addr = bt_mesh_primary_addr() + model->elem_idx;
	SYS_LOG_DBG("elem_addr: %04x", elem_addr);
	u16_t dst_addr = ctx->recv_dst;

	onoff = net_buf_simple_pull_u8(buf);
	tid_old = net_buf_simple_pull_u8(buf);
	transition = net_buf_simple_pull_u8(buf);
	delay = net_buf_simple_pull_u8(buf);

	cur_tid = net_buf_simple_pull_be16(buf);

	SYS_LOG_DBG("Current TID: %d Dst: %04x", cur_tid
										, dst_addr);

	if (onoff > STATE_ON) {
		return;
	}

	if (elem_addr == dst_addr) {
		if (!valid_tid_u16(state->mod_tid, cur_tid, 5)) {
	        SYS_LOG_DBG("Error TID");
	        return;
	    }

		state->mod_tid = cur_tid;

	} else {
		if (!check_tid_sub(dst_addr, cur_tid, state)) {
			return;
		}
	}

	state->transition = transition;
	state->delay = delay;
	state->onoff_value = onoff;
	
	SYS_LOG_DBG("port : %u, value: %d", state->gpio_port
									  , state->onoff_value);

	onoff_processing();
}

static void gen_onoff_set_ack(struct bt_mesh_model *model,
		    struct bt_mesh_msg_ctx *ctx,
		    struct net_buf_simple *buf)
{
	gen_onoff_set_unack(model, ctx, buf);

	u16_t ctrl_timeout;

	status_ctrl_range.max = PERIOD_DEFAULT;
	ctrl_timeout = rand_in_range(status_ctrl_range);

	SYS_LOG_DBG("timeout: %d", ctrl_timeout);

	k_delayed_work_cancel(&status_work);
	k_delayed_work_submit(&status_work, K_SECONDS(ctrl_timeout));
}

static bool check_tid_sub(u16_t dst, u16_t cur_tid, struct gen_onoff_state *state)
{
	u16_t tid;

	for (int i = 0; i < ARRAY_SIZE(state->groups); i++) {
		if (state->groups[i].group_addr == dst) {
			SYS_LOG_DBG("sub addr %04x", state->groups[i].group_addr);
			tid = state->groups[i].group_tid;

			if (!valid_tid_u16(tid, cur_tid, 5)) {
		        SYS_LOG_DBG("Error TID");
		        return false;
		    }

		    state->groups[i].group_tid = cur_tid;
		    return true;
		}
	}

	SYS_LOG_DBG("Group sub not found.");

	return true;
} 

/* Handle status control device vendor model */
static void vnd_status_device(struct bt_mesh_model *model,
		       struct bt_mesh_msg_ctx *ctx,
		       struct net_buf_simple *buf)
{
	SYS_LOG_DBG("");
	u16_t index;
	u16_t mode;
	u16_t primary_addr;
	u16_t ctrl_timeout;
	u16_t max_time;
	u16_t period;

	primary_addr = bt_mesh_primary_addr();

	index = net_buf_simple_pull_be16(buf);
	mode = net_buf_simple_pull_be16(buf);
	max_time = net_buf_simple_pull_be16(buf);
	period = net_buf_simple_pull_be16(buf);

	if (primary_addr % mode == index) {
		if (period >= 0) {
			SYS_LOG_DBG("Period: %d", period);
			status_bk_range.min = period;
			status_bk_range.max = period * 2;
		}

		/* get random timeout for status packet */
		status_ctrl_range.max = max_time;
		ctrl_timeout = rand_in_range(status_ctrl_range);

		k_delayed_work_cancel(&status_work);
		SYS_LOG_DBG("Status control infor, mode: %d, index: %d, ctrl_timeout: %d", mode
															   					 , index
															   					 , ctrl_timeout); 
		k_delayed_work_submit(&status_work, K_SECONDS(ctrl_timeout));
	}
}

static void vnd_get_sub_count(struct bt_mesh_model *model,
		       struct bt_mesh_msg_ctx *ctx,
		       struct net_buf_simple *buf)
{
	SYS_LOG_DBG("");

	struct cfg_sub_state *state = model->user_data;
	u16_t idx, mod, cur_tid;
	u16_t primary_addr;

	primary_addr = bt_mesh_primary_addr();

	idx = net_buf_simple_pull_be16(buf);
	mod = net_buf_simple_pull_be16(buf);
	cur_tid = net_buf_simple_pull_be16(buf);

	if (!valid_tid_u16(state->mod_tid, cur_tid, 5)) {
        SYS_LOG_DBG("Invalid TID");
        return;
    }

    state->mod_tid = cur_tid;

	if (primary_addr % mod == idx) {
		send_sub_count_line();
	}
}

static void vnd_get_sub_list(struct bt_mesh_model *model,
		       struct bt_mesh_msg_ctx *ctx,
		       struct net_buf_simple *buf)
{
	SYS_LOG_DBG("");

	struct cfg_sub_state *state = model->user_data;
	u8_t line, gbits;
	u16_t idx, mod, cur_tid;
	u16_t primary_addr;

	primary_addr = bt_mesh_primary_addr();

	idx = net_buf_simple_pull_be16(buf);
	mod = net_buf_simple_pull_be16(buf);
	line = net_buf_simple_pull_u8(buf);
	gbits = net_buf_simple_pull_u8(buf);
	cur_tid = net_buf_simple_pull_be16(buf);

	if (!valid_tid_u16(state->mod_tid, cur_tid, 5)) {
        SYS_LOG_DBG("Invalid TID");
        return;
    }

    state->mod_tid = cur_tid;

	if (primary_addr % mod == idx) {
		send_sub_list_line(line);
	}
}

/* control all port */
static void vnd_onoff_set_all(struct bt_mesh_model *model,
					    struct bt_mesh_msg_ctx *ctx,
					    struct net_buf_simple *buf)
{
	struct gen_onoff_state *state = model->user_data;
	u8_t onoff, tid_old;
	u8_t transition, delay;

	u16_t cur_tid;

	onoff = net_buf_simple_pull_u8(buf);
	tid_old = net_buf_simple_pull_u8(buf);
	transition = net_buf_simple_pull_u8(buf);
	delay = net_buf_simple_pull_u8(buf);

	if (onoff > STATE_ON) {
		return;
	}
	
	cur_tid = net_buf_simple_pull_be16(buf);

	if (!valid_tid_u16(state->mod_tid, cur_tid, 5)) {
        SYS_LOG_DBG("Invalid TID");
        return;
    }

    state->onoff_value = onoff;
    state->mod_tid = cur_tid;

	for (int i = 0; i < GPIO_QUANTITY; ++i) {
		onoff_state[i].onoff_value = onoff;
	}

	SYS_LOG_DBG("ONOFF: %u", onoff);

	onoff_processing();
}

static void vnd_cfg_add_sub_group(struct bt_mesh_model *model,
		       struct bt_mesh_msg_ctx *ctx,
		       struct net_buf_simple *buf)
{
	struct bt_mesh_model *mod;
	struct cfg_sub_state *tmp = model->user_data;
	struct gen_onoff_state *state;

	u8_t mask;
	u16_t sub_addr;
	u16_t tid_cfg;

	bool vnd = false;
	
	mask = net_buf_simple_pull_u8(buf);
	sub_addr = net_buf_simple_pull_be16(buf);
	tid_cfg = net_buf_simple_pull_be16(buf);

	/* check TID */
	if (!valid_tid_u16(tmp->mod_tid, tid_cfg, 5)) {
		SYS_LOG_DBG("Error TID");
		return;
	}

	tmp->mod_tid = tid_cfg;

	SYS_LOG_DBG("ports_mask 0x%02x", mask);

	int err;
	
	for (int idx = 0; idx < GPIO_QUANTITY; ++idx) {
		if ((mask >> idx) & 0x01) {
			mod = model_srv_sw[idx];
			err = model_sub_add(mod, vnd, bt_mesh.sub[0].net_idx, sub_addr);

			SYS_LOG_DBG("port %u, group 0x%04x, err %d", idx
													   , sub_addr
													   , err);

			if (err) {
				SYS_LOG_DBG("Add sub fail!");
				return;
			}
			state = model_srv_sw[idx]->user_data;
			update_sub_group_dt(state, 
								VNG_CFG_SUB_ADD, 
								sub_addr);
		}
	}
}

static void vnd_cfg_del_sub_group(struct bt_mesh_model *model,
		       struct bt_mesh_msg_ctx *ctx,
		       struct net_buf_simple *buf)
{
	struct bt_mesh_model *mod;
	struct gen_onoff_state *state;
	struct cfg_sub_state *tmp = model->user_data;

	u8_t mask;
	u16_t sub_addr;
	u16_t tid_cfg;

	bool vnd = false;

	mask = net_buf_simple_pull_u8(buf);
	sub_addr = net_buf_simple_pull_be16(buf);
	tid_cfg = net_buf_simple_pull_be16(buf);

	if (!valid_tid_u16(tmp->mod_tid, tid_cfg, 5)) {
		SYS_LOG_DBG("Error TID");
		return;
	}

	tmp->mod_tid = tid_cfg;

	SYS_LOG_DBG("ports_mask 0x%02x", mask);
	int err;

	for (int idx = 0; idx < GPIO_QUANTITY; ++idx) {
		if ((mask >> idx) & 0x01) {
			mod = model_srv_sw[idx];
			err = model_sub_del(mod, vnd, bt_mesh.sub[0].net_idx, sub_addr);

			SYS_LOG_DBG("port %u, group 0x%04x, err %d", idx
													   , sub_addr
													   , err);

			if (err) {
				SYS_LOG_DBG("Del sub fail!");
				return;
			}
			state = model_srv_sw[idx]->user_data;
			update_sub_group_dt(state, 	
								VNG_CFG_SUB_DEL, 
								sub_addr);
		}
	}
}

static void vnd_cfg_clr_sub_group(struct bt_mesh_model *model,
		       struct bt_mesh_msg_ctx *ctx,
		       struct net_buf_simple *buf)
{
	struct bt_mesh_model *mod;
	struct gen_onoff_state *state;
	struct cfg_sub_state *tmp = model->user_data;

	u8_t mask;
	u16_t tid_cfg;

	bool vnd = false;

	mask = net_buf_simple_pull_u8(buf);
	tid_cfg = net_buf_simple_pull_be16(buf);

	SYS_LOG_DBG("ports_mask 0x%02x", mask);

	if (!valid_tid_u16(tmp->mod_tid, tid_cfg, 5)) {
		SYS_LOG_DBG("Error TID");
		return;
	}

	tmp->mod_tid = tid_cfg;

	int err;

	for (int idx = 0; idx < GPIO_QUANTITY; ++idx) {
		if ((mask >> idx) & 0x01) {
			mod = model_srv_sw[idx];
			SYS_LOG_DBG("port %u", idx);
			
			err = model_sub_overwrite(mod, vnd, bt_mesh.sub[0].net_idx, GROUP_ADRR_UNUSED);
			if (err) {
				SYS_LOG_DBG("Overwrite sub fail!");
				return;
			}
			err = model_sub_del(mod, vnd, bt_mesh.sub[0].net_idx, GROUP_ADRR_UNUSED);
			if (err) {
				SYS_LOG_DBG("Del sub fail!");
				return;
			}

			state = model_srv_sw[idx]->user_data;
			update_sub_group_dt(state, 	
								VNG_CFG_SUB_CLR, 
								0);
		}
	}
}

static void vng_device_status(void)
{
	SYS_LOG_DBG("");
	int err;

	struct net_buf_simple *msg = root_models[4].pub->msg;

	if(msg == NULL) {
		SYS_LOG_WRN("No pub ctx");
		return;
	}

	bt_mesh_model_msg_init(msg, BT_MESH_MODEL_OP_VND_VNG_STATUS);

	u16_t primary_addr = bt_mesh_primary_addr();

	net_buf_simple_add_u8(msg, COMMAND_ID);
	net_buf_simple_add_be16(msg, DEVICE_TYPE);
	net_buf_simple_add_be16(msg, primary_addr);

	u8_t temp = 0;

	for (int i = 0; i < GPIO_QUANTITY; ++i) {
		if (onoff_state[i].onoff_value == STATE_ON) {
			temp = temp | (1 << i);
		}
	}

	net_buf_simple_add_u8(msg, temp);

	root_models[4].pub->ttl = bt_mesh_default_ttl_get();

	err = bt_mesh_model_publish(&root_models[4]);
	if (err) {
		SYS_LOG_ERR("vng_device_status err %d", err);
	}
}

static u8_t encode_data_sub_count_line(u8_t sub_cnt, u16_t crc)
{
	return ((sub_cnt << 4) & 0xF0) | ((u8_t)crc & 0x0F);
}

static void send_sub_count_line(void)
{
	u8_t dt;
	u16_t crc;
	u16_t subs[CONFIG_BT_MESH_MODEL_GROUP_COUNT];
	size_t sub_count;
	struct bt_mesh_model *mod;

	NET_BUF_SIMPLE_DEFINE(msg, 2 + 8 + 4);
	bt_mesh_model_msg_init(&msg, BT_MESH_MODEL_OP_VND_SUB_COUNT);

	for (int idx = 0; idx < GPIO_QUANTITY; ++idx) {
		sub_count = 0;
		mod = model_srv_sw[idx];

		for (int ii = 0; ii < CONFIG_BT_MESH_MODEL_GROUP_COUNT; ++ii) {
			if (mod->groups[ii] != BT_MESH_ADDR_UNASSIGNED) {
				subs[sub_count++] = mod->groups[ii];
			}
		}

		crc = crc16_ccitt(0, 
						  (const u8_t *)subs, 
						  sub_count * sizeof(u16_t));

		dt = encode_data_sub_count_line((u8_t)sub_count, crc);

		net_buf_simple_add_u8(&msg, dt);
	}

	struct bt_mesh_msg_ctx ctx = {
        .net_idx = bt_mesh.sub[0].net_idx,
        .app_idx = app_idx,
        .addr = GROUP_ADDR_STATUS_TO_GW,
        .send_ttl = BT_MESH_TTL_DEFAULT,
    };

    int err = bt_mesh_model_send(&vnd_models[0], &ctx, &msg, NULL, NULL);

    if (err != 0) {
        SYS_LOG_ERR("send failed with error: %d", err);
    }
}

static void send_sub_list_line(u8_t line)
{
	struct bt_mesh_model *mod;

	NET_BUF_SIMPLE_DEFINE(msg, 2 + 9 + 4);

	u16_t subs[CONFIG_BT_MESH_MODEL_GROUP_COUNT];
	size_t sub_count = 0;

	mod = model_srv_sw[line];

	for (int idx = 0; idx < CONFIG_BT_MESH_MODEL_GROUP_COUNT; ++idx) {
		if (mod->groups[idx] != BT_MESH_ADDR_UNASSIGNED) {
			subs[sub_count++] = mod->groups[idx];
		}
	}

	u8_t dt =  ((line << 4) & 0xF0) | (sub_count & 0x0F);

	SYS_LOG_DBG("\nSubs: %d, edata: %02x\n", sub_count
										   , dt);

	u8_t pkg_cnt = ((sub_count - 1) / 4) + 1;
	u8_t cnt = 0;

	struct bt_mesh_msg_ctx ctx = {
        .net_idx = bt_mesh.sub[0].net_idx,
        .app_idx = app_idx,
        .addr = GROUP_ADDR_STATUS_TO_GW,
        .send_ttl = BT_MESH_TTL_DEFAULT,
    };

    int err;

	for (int i = 0; i < pkg_cnt; ++i) {
		bt_mesh_model_msg_init(&msg, BT_MESH_MODEL_OP_VND_SUB_LIST);
		net_buf_simple_add_u8(&msg, dt);

		for (int ii = 0; ii < 4; ++ii) {
			if (cnt == sub_count) {
				break;
			}

			net_buf_simple_add_be16(&msg, subs[cnt++]);
		}
		
		err = bt_mesh_model_send(&vnd_models[0], &ctx, &msg, NULL, NULL);

	    if (err != 0) {
	        SYS_LOG_ERR("send failed with error: %d", err);
	    }
	}
}

static void onoff_processing(void)
{
	SYS_LOG_DBG("");
	gpio_pin_enable_callback(gpio_dev, ZDC_GPIO_PIN);
}

void status_work_timeout(struct k_work *work)
{
	vng_device_status();
	u16_t timeout = rand_in_range(status_bk_range);

	SYS_LOG_DBG("timeout: %d", timeout);

	if (timeout == 0) {
		return;
	}

	k_delayed_work_submit(&status_work, K_SECONDS(timeout));
}

void zero_detect(struct device *gpiob, struct gpio_callback *cb,
		    u32_t pins)
{
	SYS_LOG_DBG("");
	gpio_pin_disable_callback(gpio_dev, ZDC_GPIO_PIN);

	NRF_TIMER2->TASKS_START = 1;
}

static void _onoff_isr(void)
{
	SYS_LOG_DBG("");
	if (NRF_TIMER2->EVENTS_COMPARE[0]) {

		NRF_TIMER2->EVENTS_COMPARE[0] = 0;

		for (int i = 0; i < GPIO_QUANTITY; ++i) {
			if (onoff_state[i].onoff_value) {
				PIN_SET(onoff_state[i].gpio_port);
			} else {
				PIN_CLR(onoff_state[i].gpio_port);
			}
		}
	}

	NRF_TIMER2->TASKS_STOP = 1;
	NRF_TIMER2->TASKS_CLEAR = 1;
}

void start_timer(void)
{		
	SYS_LOG_DBG("");
	NVIC_ClearPendingIRQ(NRF5_IRQ_TIMER2_IRQn);
    IRQ_CONNECT(NRF5_IRQ_TIMER2_IRQn, 1, _onoff_isr, 0, 0);
    irq_enable(NRF5_IRQ_TIMER2_IRQn);

	NRF_TIMER2->MODE = TIMER_MODE_MODE_Timer;  				// Set the timer in Counter Mode
	NRF_TIMER2->TASKS_CLEAR = 1;               				// Clear the task first to be usable for later
	NRF_TIMER2->PRESCALER = 8;                          	// Set prescaler. Higher number gives slower timer. Prescaler = 0 gives 16MHz timer
	NRF_TIMER2->BITMODE = TIMER_BITMODE_BITMODE_16Bit;		// Set counter to 16 bit resolution
	NRF_TIMER2->CC[0] = COMPARE_TIME_ZDC_ONOFF_HUB;         // Set value for TIMER2 compare register 0

	// Enable interrupt on Timer 2, for CC[0] compare match events
	NRF_TIMER2->INTENSET = (TIMER_INTENSET_COMPARE0_Enabled << TIMER_INTENSET_COMPARE0_Pos);
		
	NRF_TIMER2->TASKS_STOP = 1;
}

void init_sub_group_dt(void)
{
	struct gen_onoff_state *dt;
	struct bt_mesh_model *mod; 

	for (int i = 0; i < ARRAY_SIZE(onoff_state); ++i) {

		dt = &onoff_state[i];
		mod = model_srv_sw[i];

		for (int ii = 0; ii < ARRAY_SIZE(dt->groups); ++ii) {
			dt->groups[ii].group_addr = mod->groups[ii];
			printf("[%04x %d]", dt->groups[ii].group_addr
							  , dt->groups[ii].group_tid);
		}

		printf("\n");
	}
}

static void update_sub_group_dt(struct gen_onoff_state *dt,
						 		u8_t op,
						 		u16_t sub_addr)
{
	switch(op) {
		case VNG_CFG_SUB_ADD:
			SYS_LOG_DBG("[ADD]");

			for (int i = 0; i < ARRAY_SIZE(dt->groups); ++i) {
				if (dt->groups[i].group_addr == sub_addr) {
					SYS_LOG_DBG("Tried to add existing subscription.");
					return;
				}
			}

			for (int i = 0; i < ARRAY_SIZE(dt->groups); ++i) {
				if (dt->groups[i].group_addr == BT_MESH_ADDR_UNASSIGNED) {
					dt->groups[i].group_addr = sub_addr;
					break;
				}
			}

			break;
		case VNG_CFG_SUB_DEL:
			SYS_LOG_DBG("[DEL]");
			for (int i = 0; i < ARRAY_SIZE(dt->groups); ++i) {
				if (dt->groups[i].group_addr == sub_addr) {
					dt->groups[i].group_addr = BT_MESH_ADDR_UNASSIGNED;
					dt->groups[i].group_tid = INIT_TID;
					break;
				}
			}

			break;
		case VNG_CFG_SUB_CLR:
			SYS_LOG_DBG("[CLR]");
			for (int i = 0; i < ARRAY_SIZE(dt->groups); ++i) {
				dt->groups[i].group_addr = BT_MESH_ADDR_UNASSIGNED;
				dt->groups[i].group_tid = INIT_TID;
			}

			break;
		default:
			SYS_LOG_DBG("Error: Invalid Option!");
			break;
	}

	return;
}

static int model_sub_add(struct bt_mesh_model *model, 
						 bool vnd, 
						 u16_t net_idx, 
						 u16_t sub_addr)
{
	u16_t addr;
	u16_t pri_addr = bt_mesh_primary_addr();

	addr = elements[model->elem_idx].addr;
	int err;

	if(vnd) {

        SYS_LOG_DBG("vnd [0x%04x]", addr);

        err = bt_mesh_cfg_mod_sub_add_vnd(net_idx, pri_addr, addr,
            sub_addr, model->vnd.id, model->vnd.company, NULL);

        return err;
    } else {

        SYS_LOG_DBG("[0x%04x]", addr);

        err = bt_mesh_cfg_mod_sub_add(net_idx, pri_addr, addr,
            sub_addr, model->id, NULL);

        return err;
    }

    return 1;
}

static int model_sub_del(struct bt_mesh_model *model, 
						 bool vnd, 
						 u16_t net_idx, 
						 u16_t sub_addr)
{
	u16_t addr;
	u16_t pri_addr = bt_mesh_primary_addr();

	addr = elements[model->elem_idx].addr;
	int err;

	if(vnd) {

        SYS_LOG_DBG("vnd [0x%04x]", addr);

        err = bt_mesh_cfg_mod_sub_del_vnd(net_idx, pri_addr, addr,
            sub_addr, model->vnd.id, model->vnd.company, NULL);

        return err;
    } else {

        SYS_LOG_DBG("[0x%04x]", addr);

        err = bt_mesh_cfg_mod_sub_del(net_idx, pri_addr, addr,
            sub_addr, model->id, NULL);

        return err;
    }

    return 1;
}

static int model_sub_overwrite(struct bt_mesh_model *model, 
							 bool vnd, 
							 u16_t net_idx, 
							 u16_t sub_addr)
{
	u16_t addr;
	u16_t pri_addr = bt_mesh_primary_addr();

	addr = elements[model->elem_idx].addr;
	int err;

	if(vnd) {

        SYS_LOG_DBG("vnd [0x%04x]", addr);

        err = bt_mesh_cfg_mod_sub_overwrite_vnd(net_idx, pri_addr, addr,
            sub_addr, model->vnd.id, model->vnd.company, NULL);

        return err;
    } else {

        SYS_LOG_DBG("[0x%04x]", addr);

        err = bt_mesh_cfg_mod_sub_overwrite(net_idx, pri_addr, addr,
            sub_addr, model->id, NULL);

        return err;
    }

    return 1;
}
