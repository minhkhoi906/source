#include <zephyr.h>

#include <settings/settings.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <bluetooth/l2cap.h>
#include <bluetooth/hci.h>
#include <bluetooth/mesh.h>
#include <misc/byteorder.h>

#include "../include/ble_mesh.h"
#include "../include/composition.h"
#include "../include/flash_uicr.h"

#define SYS_LOG_DOMAIN "ONOFF_HUB_BLE"
#include <logging/sys_log.h>

static bool setting_loading = false;

/* Release */

static const u8_t app_key[16] = {
    0x4d, 0x48, 0x63, 0x43, 0x41, 0x51, 0x45, 0x45, 
    0x49, 0x4c, 0x79, 0x53, 0x4a, 0x69, 0x4c, 0x2f,
};

static const u8_t dev_key[16] = {
    0x55, 0x65, 0x41, 0x58, 0x64, 0x65, 0x78, 0x4d, 
    0x76, 0x74, 0x4c, 0x65, 0x56, 0x31, 0x70, 0x52, 
};

static const u8_t net_key[16] = {
    0x7a, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 
    0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b, 
};

static const u32_t iv_index;
static u8_t flags;

const u16_t net_idx;
const u8_t app_idx;

struct bt_mesh_model root_models[6];
struct bt_mesh_model vnd_models[2];
struct bt_mesh_model secondary_0_models[1];
struct bt_mesh_model secondary_1_models[1];
struct bt_mesh_model secondary_2_models[1];
struct bt_mesh_model secondary_3_models[1];
struct bt_mesh_model secondary_4_models[1];
struct bt_mesh_model secondary_5_models[1];
struct bt_mesh_model secondary_6_models[1];

static void prov_complete(u16_t net_idx, u16_t addr)
{
	if(setting_loading)
        return;

    u16_t primary_addr, element_addr;																				
    primary_addr = bt_mesh_primary_addr();
    element_addr = elements[0].addr;

	SYS_LOG_DBG("provisioning complete for net_idx 0x%04x addr 0x%04x",
		    net_idx, addr);

     /* Add Application Key */
    bt_mesh_cfg_app_key_add(net_idx, element_addr,
                			net_idx, app_idx, 
                			app_key, NULL);

    bool vnd = false;
	MODELS_BIND(root_models, vnd, net_idx, app_idx);
	MODELS_BIND(secondary_0_models, vnd, net_idx, app_idx);
	MODELS_BIND(secondary_1_models, vnd, net_idx, app_idx);
	MODELS_BIND(secondary_2_models, vnd, net_idx, app_idx);
	MODELS_BIND(secondary_3_models, vnd, net_idx, app_idx);
	MODELS_BIND(secondary_4_models, vnd, net_idx, app_idx);
	MODELS_BIND(secondary_5_models, vnd, net_idx, app_idx);
	MODELS_BIND(secondary_6_models, vnd, net_idx, app_idx);

	struct bt_mesh_cfg_mod_pub mod_pub = {
        .addr = GROUP_ADDR_STATUS_TO_GW,
        .app_idx = app_idx,
        .ttl = TIME_TO_LIVE,
        .transmit = BT_MESH_TRANSMIT(0, 20)
    };

    MODEL_PUB(root_models[4], vnd, net_idx, &mod_pub);

    vnd = true;

    MODELS_BIND(vnd_models, vnd, net_idx, app_idx)

	/* 
	 * Add model subscription 
	 */

    /* model control status device */ 
    bt_mesh_cfg_mod_sub_add_vnd(net_idx, 
								primary_addr, 
								element_addr,
		            			GROUP_ADDR_STATUS_CONTROL, 
		            			BT_MESH_MODEL_ID_VND_STT_DEVICE_SRV,
		            			CID_VNG, 
		            			NULL);

    /* model renew addr */ 
    bt_mesh_cfg_mod_sub_add_vnd(net_idx, 
						    	primary_addr, 
						    	primary_addr,
						        VNG_MESH_RENEW_GROUP, 
						        BT_MESH_MODEL_ID_VNG_COMMAND_SRV, 
						        CID_VNG, NULL);
}

static void prov_reset(void)
{
	bt_mesh_prov_enable(BT_MESH_PROV_ADV | BT_MESH_PROV_GATT);
}

static u8_t dev_uuid[16] = { 0xdd, 0xdd };

static const struct bt_mesh_prov prov = {
	.uuid = dev_uuid,
	.output_size = 0,
	.complete = prov_complete,
	.reset = prov_reset,
};

/*
 * Bluetooth Ready Callback
 */

void bt_ready(int err)
{
	struct bt_le_oob oob;

	if (err) {
		SYS_LOG_ERR("Bluetooth init failed (err %d", err);
		return;
	}

	SYS_LOG_DBG("Bluetooth initialized");

	err = bt_mesh_init(&prov, &comp);
	if (err) {
		SYS_LOG_ERR("Initializing mesh failed (err %d)", err);
		return;
	}

	if (IS_ENABLED(CONFIG_BT_SETTINGS)) {
		setting_loading = true;
		settings_load();
		setting_loading = false;
	}

	/* Use identity address as device UUID */
	if (bt_le_oob_get_local(BT_ID_DEFAULT, &oob)) {
		SYS_LOG_ERR("Identity Address unavailable");
	} else {
		memcpy(dev_uuid, oob.addr.a.val, 6);
	}

	/* for deploy */
	err = bt_mesh_provision(net_key, net_idx, flags, 
							iv_index, flash_unicast_get(),
							dev_key);

	init_sub_group_dt();
	
	if (err == -EALREADY) {
		SYS_LOG_DBG("Using stored settings");
	} else if (err) {
		SYS_LOG_DBG("Provisioning failed (err %d)", err);
		return;
	} else {
		SYS_LOG_DBG("Provisioning completed");
	}

	bt_mesh_prov_enable(BT_MESH_PROV_GATT | BT_MESH_PROV_ADV);

	SYS_LOG_DBG("Mesh initialized");
}
