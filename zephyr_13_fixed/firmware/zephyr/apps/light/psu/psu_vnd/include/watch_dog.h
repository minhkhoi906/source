#ifndef _WDT_H
#define _WDT_H

#ifdef __cplusplus
extern "C" {
#endif

#define WDT_FEED_TIMEOUT		K_SECONDS(1)
#define WDT_TIMEOUT 			K_SECONDS(15)

#define WDT_DEV_NAME 			CONFIG_WDT_0_NAME

struct range {
	u16_t 	min;
	u16_t	max;
};

void watchdog_init(void);

void random_init(void);

u16_t rand_in_range(struct range t);

#ifdef __cplusplus
}
#endif

#endif /* _WDT_H */