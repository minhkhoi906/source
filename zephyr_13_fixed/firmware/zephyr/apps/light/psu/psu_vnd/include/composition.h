#ifndef _COMPOSITION_H
#define _COMPOSITION_H

#ifdef __cplusplus
extern "C" {
#endif

#include <zephyr.h>
#include <bluetooth/mesh.h>
#include <board.h>

#include "ble_mesh.h"

#define STATE_OFF				0x00
#define STATE_ON				0x01

#define GPIO_QUANTITY			4
#define INIT_TID				-1

#define DELTA_TIME 				K_MSEC(10)

#define CID_VNG 						0x00FF
#define GROUP_ADDR_STATUS_TO_GW 		0xE000
#define GROUP_ADDR_STATUS_CONTROL		0xD000
#define VNG_MESH_RENEW_GROUP 			0xC002

#define	EDGE    		(GPIO_INT_EDGE | GPIO_INT_ACTIVE_HIGH)

#define TIME_TO_LIVE			10

#define BT_MESH_MODEL_ID_VND_PSU_SRV			0x2001
#define BT_MESH_MODEL_ID_VND_STT_DEVICE_SRV		0x2002

#define BT_MESH_MODEL_OP_VND_VNG_STATUS			BT_MESH_MODEL_OP_3(0x04, CID_VNG)
#define BT_MESH_MODEL_OP_VND_VNG_CTRL_STT		BT_MESH_MODEL_OP_3(0x05, CID_VNG)

#define BT_MESH_MODEL_OP_VND_PSU_GET			BT_MESH_MODEL_OP_2(0x83, 0x00)
#define BT_MESH_MODEL_OP_VND_PSU_SET 			BT_MESH_MODEL_OP_2(0x83, 0x01)
#define BT_MESH_MODEL_OP_VND_PSU_SET_UNACK		BT_MESH_MODEL_OP_2(0x83, 0x02)

struct psu_vendor_state {
	u8_t target_port;
	u8_t onoff_value;
	u8_t last_tid;
	u8_t current[4];
	u32_t gpio_port[4];
	struct device *gpio_dev;
};

extern struct psu_vendor_state vnd_user_data;

extern struct bt_mesh_model root_models[];
extern struct bt_mesh_model vnd_models[];

extern struct bt_mesh_elem elements[];

extern const struct bt_mesh_comp comp;

extern struct k_delayed_work status_work;

extern struct device *gpio_dev;

void status_work_timeout(struct k_work *work);

void zero_detect(struct device *gpiob, struct gpio_callback *cb,
		    u32_t pins);

void start_timer(void);

#ifdef __cplusplus
}
#endif

#endif /* _COMPOSITION_H */