#include <bluetooth/mesh.h>

#include "../include/composition.h"
#include "../include/ble_mesh.h"
#include "../include/watch_dog.h"

/*
 * The include must follow the define for it to take effect.
 * If it isn't, the domain defaults to "general"
 */

#define SYS_LOG_DOMAIN "PSU_Composition"
#include <logging/sys_log.h>

/* Vendor Model message handlers*/
static void vnd_get_psu(struct bt_mesh_model *model,
					    struct bt_mesh_msg_ctx *ctx,
					    struct net_buf_simple *buf);

static void vnd_set_psu_unack(struct bt_mesh_model *model,
							  struct bt_mesh_msg_ctx *ctx,
							  struct net_buf_simple *buf);

static void vnd_set_psu(struct bt_mesh_model *model,
					    struct bt_mesh_msg_ctx *ctx,
					    struct net_buf_simple *buf);

/* Handle status control device vendor model */
static void vnd_status_device(struct bt_mesh_model *model,
						       struct bt_mesh_msg_ctx *ctx,
						       struct net_buf_simple *buf);

static void psu_status(void);

/* Mapping of message handlers for Vendor 4_ports server (0x2001) */
static const struct bt_mesh_model_op vnd_psu_srv_op[] = {
	{ BT_MESH_MODEL_OP_VND_PSU_GET, 0, vnd_get_psu },
	{ BT_MESH_MODEL_OP_VND_PSU_SET, 3, vnd_set_psu },
	{ BT_MESH_MODEL_OP_VND_PSU_SET_UNACK, 3, vnd_set_psu_unack },
	BT_MESH_MODEL_OP_END,
};

/* Mapping of message handlers for Vendor status (0x2002) */
static const struct bt_mesh_model_op vng_stt_device_op[] = {
	{ BT_MESH_MODEL_OP_VND_VNG_CTRL_STT, 6, vnd_status_device},
	BT_MESH_MODEL_OP_END,
};

/*
 * Server Configuration Declaration
 */

static struct bt_mesh_cfg_srv cfg_srv = {
#if defined(CONFIG_BT_MESH_RELAY)
	.relay = BT_MESH_RELAY_ENABLED,
#else
	.relay = BT_MESH_RELAY_NOT_SUPPORTED,
#endif
	.beacon = BT_MESH_BEACON_ENABLED,
#if defined(CONFIG_BT_MESH_FRIEND)
	.frnd = BT_MESH_FRIEND_ENABLED,
#else
	.frnd = BT_MESH_FRIEND_NOT_SUPPORTED,
#endif
#if defined(CONFIG_BT_MESH_GATT_PROXY)
	.gatt_proxy = BT_MESH_GATT_PROXY_ENABLED,
#else
	.gatt_proxy = BT_MESH_GATT_PROXY_NOT_SUPPORTED,
#endif
	.default_ttl = 7,

	/* 3 transmissions with 20ms interval */
	.net_transmit = BT_MESH_TRANSMIT(1, 20),
	.relay_retransmit = BT_MESH_TRANSMIT(1, 20),
};

/*
 * Health Server Declaration
 */

static struct bt_mesh_health_srv health_srv = {
};

/*
 * Client Configuration Declaration
 */

static struct bt_mesh_cfg_cli cfg_cli = {
};

static struct bt_mesh_vng_command_srv vng_command_srvs = {
};

/* Definitions of models publication context */
BT_MESH_HEALTH_PUB_DEFINE(health_pub, 0);

BT_MESH_MODEL_PUB_DEFINE(vnd_psu_pub_srv, NULL, 3 + 7);

/* Definitions of models user data */
struct psu_vendor_state vnd_user_data = {
	.gpio_port = {
		ONOFF1_GPIO_PIN,
		ONOFF2_GPIO_PIN,
		ONOFF3_GPIO_PIN,
		ONOFF4_GPIO_PIN
	},
	.last_tid = INIT_TID,
};

/* timer backup random from 80s to 100s*/

static struct range status_bk_range = {
	.min = 80,
	.max = 100,
};

/* timer control status random from 0 to max_time*/
static struct range status_ctrl_range = {
	.min = 0,
};

/*
 *
 * Element Model Declarations
 *
 * Element Root Models
 */

struct bt_mesh_model root_models[] = {
	BT_MESH_MODEL_CFG_SRV(&cfg_srv),
	BT_MESH_MODEL_CFG_CLI(&cfg_cli),
	BT_MESH_MODEL_HEALTH_SRV(&health_srv, &health_pub),
	BT_MESH_MODEL(BT_MESH_MODEL_ID_VND_PSU_SRV, 
				  vnd_psu_srv_op, 
				  &vnd_psu_pub_srv, 
				  &vnd_user_data),
};

struct bt_mesh_model vnd_models[] = {
	BT_MESH_MODEL_VND(CID_VNG, 
					  BT_MESH_MODEL_ID_VND_STT_DEVICE_SRV, 
					  vng_stt_device_op, NULL, NULL),
	BT_MESH_MODEL_VNG_COMMAND_SRV(&vng_command_srvs),
};

struct bt_mesh_elem elements[] = {
	BT_MESH_ELEM(0, root_models, vnd_models),
};

const struct bt_mesh_comp comp = {
	.cid = CID_VNG,
	.elem = elements,
	.elem_count = ARRAY_SIZE(elements),
};

/* Vendor Model message handlers*/
static void vnd_get_psu(struct bt_mesh_model *model,
		    struct bt_mesh_msg_ctx *ctx,
		    struct net_buf_simple *buf)
{
	struct net_buf_simple *msg = NET_BUF_SIMPLE(3 + 6 + 4);
	struct psu_vendor_state *state = model->user_data;
	u16_t primary_addr = bt_mesh_primary_addr();
	
	bt_mesh_model_msg_init(msg, BT_MESH_MODEL_OP_VND_VNG_STATUS);

	net_buf_simple_add_u8(msg, COMMAND_ID);
	net_buf_simple_add_be16(msg, DEVICE_TYPE);
	net_buf_simple_add_be16(msg, primary_addr);

	u8_t temp = 0;

	for (int i = 0; i < GPIO_QUANTITY; ++i) {
		if (state->current[i] == STATE_ON) {
			temp = temp | (1 << i);
		}
	}	

	SYS_LOG_DBG("Get current value [%d %d %d %d], temp: 0x%02x",state->current[3]
																,state->current[2]
																,state->current[1]
																,state->current[0]
																,temp);

	net_buf_simple_add_u8(msg, temp);
	
	if (bt_mesh_model_send(model, ctx, msg, NULL, NULL)) {
		SYS_LOG_ERR("Unable to send VENDOR Status response");
	}
}

static void vnd_set_psu_unack(struct bt_mesh_model *model,
			  struct bt_mesh_msg_ctx *ctx,
			  struct net_buf_simple *buf)
{
	struct net_buf_simple *msg = model->pub->msg;
	struct psu_vendor_state *state = model->user_data;
	int i;

	u8_t onoff, mask, tid;

	mask = net_buf_simple_pull_u8(buf);
	onoff = net_buf_simple_pull_u8(buf);
	tid = net_buf_simple_pull_u8(buf);

	if (onoff > STATE_ON) {
		return;
	}

	if (state->last_tid == tid) {
		return;
	}

	state->target_port = mask;
	state->onoff_value = onoff;
	state->last_tid = tid;

	for (int i = 0; i < GPIO_QUANTITY; ++i) {
		if((state->target_port >> i) & 0x01) {
			state->current[i] = state->onoff_value;
		}
	}

	gpio_pin_enable_callback(gpio_dev, ZDC_GPIO_PIN);
}

static void vnd_set_psu(struct bt_mesh_model *model,
		    struct bt_mesh_msg_ctx *ctx,
		    struct net_buf_simple *buf)
{
	vnd_set_psu_unack(model, ctx, buf);
	vnd_get_psu(model, ctx, buf);
}

/* Handle status control device vendor model */
static void vnd_status_device(struct bt_mesh_model *model,
		       struct bt_mesh_msg_ctx *ctx,
		       struct net_buf_simple *buf)
{
	SYS_LOG_DBG("");
	u16_t index;
	u16_t mode;
	u16_t primary_addr;
	u16_t ctrl_timeout;
	u16_t max_time;

	primary_addr = bt_mesh_primary_addr();

	index = net_buf_simple_pull_be16(buf);
	mode = net_buf_simple_pull_be16(buf);
	max_time = net_buf_simple_pull_be16(buf);

	if (primary_addr % mode == index) {
		/* get random timeout for status packet */
		status_ctrl_range.max = max_time;
		ctrl_timeout = rand_in_range(status_ctrl_range);

		k_delayed_work_cancel(&status_work);
		SYS_LOG_DBG("Status control infor, mode: %d, index: %d, ctrl_timeout: %d", mode
															   					 , index
															   					 , ctrl_timeout); 
		k_delayed_work_submit(&status_work, K_SECONDS(ctrl_timeout));
	}
}

static void psu_status(void)
{
	int err;

	struct net_buf_simple *msg = root_models[3].pub->msg;

	bt_mesh_model_msg_init(msg, BT_MESH_MODEL_OP_VND_VNG_STATUS);

	u16_t primary_addr = bt_mesh_primary_addr();

	net_buf_simple_add_u8(msg, COMMAND_ID);
	net_buf_simple_add_be16(msg, DEVICE_TYPE);
	net_buf_simple_add_be16(msg, primary_addr);

	u8_t temp = 0;

	for (int i = 0; i < GPIO_QUANTITY; ++i) {
		if (vnd_user_data.current[i] == STATE_ON) {
			temp = temp | (1 << i);
		}
	}

	net_buf_simple_add_u8(msg, temp);

	root_models[3].pub->ttl = bt_mesh_default_ttl_get();

	err = bt_mesh_model_publish(&root_models[3]);
	if (err) {
		SYS_LOG_ERR("psu_status err %d", err);
	}
}

struct k_delayed_work status_work;

void status_work_timeout(struct k_work *work)
{
	psu_status();
	u16_t timeout = rand_in_range(status_bk_range);
	k_delayed_work_submit(&status_work, K_SECONDS(timeout));
}

void zero_detect(struct device *gpiob, struct gpio_callback *cb,
		    u32_t pins)
{
	SYS_LOG_DBG("");
	gpio_pin_disable_callback(gpio_dev, ZDC_GPIO_PIN);

	NRF_TIMER2->TASKS_START = 1;
}

static void _onoff_isr(void)
{
	if (NRF_TIMER2->EVENTS_COMPARE[0]) {
		NRF_TIMER2->EVENTS_COMPARE[0] = 0;
		for(int i = 0; i < GPIO_QUANTITY; i++) {
			if((vnd_user_data.target_port >> i) & 0x01) {
				gpio_pin_write(gpio_dev,
						       vnd_user_data.gpio_port[i],
						       (u32_t)vnd_user_data.onoff_value);
			}
		}
	}

	NRF_TIMER2->TASKS_STOP = 1;
	NRF_TIMER2->TASKS_CLEAR = 1;
}

void start_timer(void)
{		
	SYS_LOG_DBG("");
	NVIC_ClearPendingIRQ(NRF5_IRQ_TIMER2_IRQn);
    IRQ_CONNECT(NRF5_IRQ_TIMER2_IRQn, 1, _onoff_isr, 0, 0);
    irq_enable(NRF5_IRQ_TIMER2_IRQn);

	NRF_TIMER2->MODE = TIMER_MODE_MODE_Timer;  			// Set the timer in Counter Mode
	NRF_TIMER2->TASKS_CLEAR = 1;               			// clear the task first to be usable for later
	NRF_TIMER2->PRESCALER = 8;                          //Set prescaler. Higher number gives slower timer. Prescaler = 0 gives 16MHz timer
	NRF_TIMER2->BITMODE = TIMER_BITMODE_BITMODE_16Bit;	//Set counter to 16 bit resolution
	NRF_TIMER2->CC[0] = 343;                            //Set value for TIMER2 compare register 0

	// Enable interrupt on Timer 2, for CC[0] compare match events
	NRF_TIMER2->INTENSET = (TIMER_INTENSET_COMPARE0_Enabled << TIMER_INTENSET_COMPARE0_Pos);
		
	// NRF_TIMER2->TASKS_START = 1;               // Start TIMER2
	NRF_TIMER2->TASKS_STOP = 1;
}




