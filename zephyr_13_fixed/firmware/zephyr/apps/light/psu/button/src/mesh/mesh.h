#ifndef MESH_H
#define MESH_H

#include <zephyr.h>

extern u8_t setting_vol;

void bt_ready(int err);
void status_init();
void press_transmit();

#endif // MESH_H
