TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
DEFINES += CONFIG_ADC_CONFIGURABLE_INPUTS

INCLUDEPATH += \
    ../../../../os/v1.13.0/include \
    ../../../../os/v1.13.0/subsys/bluetooth \
    ../../../../os/v1.13.0/boards/arm/nrf52_vng_light_hub_dim

DISTFILES += \
    CMakeLists.txt \
    prj_nrf52_vng_light_hub_dim.conf

HEADERS += \
    board/mesh.h \
    board/hub.h \
    board/flash_uicr.h

SOURCES += \
    main.c \
    board/mesh.c \
    board/hub.c

