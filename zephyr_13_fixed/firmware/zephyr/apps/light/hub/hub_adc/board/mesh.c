#include "mesh.h"
#include "hub.h"
#include <board.h>
#include <bluetooth/mesh.h>
#include <misc/byteorder.h>
#include <host/mesh/net.h>
#include <host/mesh/adv.h>
#include <host/mesh/access.h>
#include <settings/settings.h>
#include <bluetooth/mesh.h>

#include <host/mesh/foundation.h>
#include <pwm.h>
#include <kernel.h>
#define SYS_LOG_DOMAIN "app"
#include "common/log.h"
#include <logging/sys_log.h>

#include <crc16.h>
#include "flash_uicr.h"

#define GBITS_DEFAULT       8
#define GLIST_DATA_LEN_MAX   7
#define GCOUNT_DATA_LEN_MAX 8

#define GLIST_INFO_0(line, gbits) ((line << 4 & 0xF0) | (gbits & 0x0F))
#define GLIST_INFO_1(pno, pid)    ((pno << 4 & 0xF0) | (pid & 0x0F))
#define GCOUNT_LINE_DATA(gcount, crc16) ((gcount << 4 & 0xF0) | (crc & 0x0F))

#define SET_BUF_BIT(buf, n, b) { \
    if (b) \
        buf[n/8] |= BIT(7 - n % 8);\
    else \
        buf[n/8] &= ~BIT(7 -n % 8);\
}

enum {
    GROUP_LIST_LINE0,
    GROUP_LIST_LINE1,
    GROUP_LIST_LINE2,
    GROUP_LIST_LINE3,
    GROUP_LIST_LINE4,
    GROUP_LIST_LINE5,
    GROUP_LIST_LINE6,
    GROUP_LIST_LINE7,
    GROUP_COUNT,
    LINE_ENABLEDS,

    GROUP_TASKS_COUNT

};

NET_BUF_SIMPLE_DEFINE_STATIC(groups_count_buf, HUB_LINES);
NET_BUF_SIMPLE_DEFINE_STATIC(groups_list_buf, CONFIG_BT_MESH_MODEL_GROUP_COUNT * sizeof(u16_t));
struct _groups_status
{
    struct bt_mesh_model* model;
    struct k_delayed_work timer; 

    ATOMIC_DEFINE(tasks, GROUP_TASKS_COUNT);   
    struct {
        u8_t gbits;
        struct net_buf_simple* buf;
        u8_t line;
        u8_t pno;
        u8_t pid;
    } glist;

    struct {
        struct net_buf_simple* buf;
        u8_t line;
    } gcount;

    struct {
        u8_t val;
        struct k_delayed_work timer; 
    } enableds;

};

extern void _prov_complete(u16_t _net_idx, u16_t _addr);
static void _prov_reset(void);
static void _mesh_status_set(u16_t time, u16_t main, u16_t duration, u16_t period);
static void _levels_set(u16_t level, u8_t time, u8_t delay, void *user_data, bool ack);
static void _enableds_set(u32_t enables, void *user_data, bool ack);
static void _groups_add(u8_t linesmap,  u16_t group, void *user_data, bool ack);
static void _groups_del(u8_t linesmap,  u16_t group, void *user_data, bool ack);
static void _groups_clr(u8_t linesmap, u8_t lines, void *user_data, bool ack);
static void _groups_list_get(u16_t index, u16_t main, u8_t line, u8_t gbits);
static void _groups_count_get(u16_t index, u16_t main);
static void _groups_list_delayed_get(u8_t linesmap, u16_t delayed);
static void _groups_status_send(bool glist, struct net_buf_simple* buf);
static void _groups_status_work(struct k_work* work);
static void _line_enables_status_work(struct k_work* work);

static void _dimming_config(u8_t linesmap, u8_t off_0, u8_t scale_min, u8_t scale_max, void *user_data);
static void _status_config(u8_t status_en, u8_t warn_en, u16_t check_period, u16_t notify_period, void *user_data);
static void _status_store(u8_t store, void *user_data);

static void _mesh_hub_setting_load();

static void _level_get(u16_t timeout, void *user_data);

static void _level_set(u16_t level,
                       u8_t tid,
                       u8_t time,
                       u8_t delay,
                       void *user_data,
                       bool ack);

static void _provision(struct k_work *);
static void _configure_1st(struct k_work *work);
static void _configure_2nd(struct k_work *work);
static void _configure_3rd(struct k_work *work);

static struct k_work _provision_work;
static struct k_delayed_work _configure_1st_work;
static struct k_delayed_work _configure_2nd_work;
static struct k_delayed_work _configure_3rd_work;

static uint8_t uuid[16] = { 0xdd, 0xdd };
static struct bt_mesh_prov prov = {
    .uuid = uuid,
    .output_size = 0,
    .complete = _prov_complete,
    .reset = _prov_reset
};

static const u8_t _app_idx = 0;

// NEW KEY

static const u8_t _app_key[16] = {
   0x4d, 0x48, 0x63, 0x43, 0x41, 0x51, 0x45, 0x45, 0x49, 0x4c, 0x79, 0x53, 0x4a, 0x69, 0x4c, 0x2f,
};

static const u8_t _dev_key[16] = {
    0x55, 0x65, 0x41, 0x58, 0x64, 0x65, 0x78, 0x4d, 0x76, 0x74, 0x4c, 0x65, 0x56, 0x31, 0x70, 0x52, 
};

static const u8_t _net_key[16] = {
    0x7a, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b, 
};

static u16_t _net_idx;
static const u32_t _iv_index;
static u8_t _flags;

static u16_t _addr ;

static bool _setting_loading = false;

static struct bt_mesh_cfg_srv _cfg_srv = {
    .relay = BT_MESH_RELAY_ENABLED,
    .beacon = BT_MESH_BEACON_ENABLED,
#if defined(CONFIG_BT_MESH_FRIEND)
    .frnd = BT_MESH_FRIEND_ENABLED,
#else
    .frnd = BT_MESH_FRIEND_NOT_SUPPORTED,
#endif
#if defined(CONFIG_BT_MESH_GATT_PROXY)
    .gatt_proxy = BT_MESH_GATT_PROXY_ENABLED,
#else
    .gatt_proxy = BT_MESH_GATT_PROXY_NOT_SUPPORTED,
#endif
    .default_ttl = 3,

    /* 4 transmissions with 20ms interval */
    .net_transmit = BT_MESH_TRANSMIT(3, 20),
    .relay_retransmit = BT_MESH_TRANSMIT(3, 20),
};


static struct bt_mesh_health_srv _health_srv = {
};

BT_MESH_HEALTH_PUB_DEFINE(_health_pub, 0);

static struct bt_mesh_cfg_cli _cfg_cli = {
};

static struct bt_mesh_gen_level_srv _gel_level_srvs[] = {
{ .get_func = _level_get, .set_func = _level_set},
{ .get_func = _level_get, .set_func = _level_set},
{ .get_func = _level_get, .set_func = _level_set},
{ .get_func = _level_get, .set_func = _level_set},
{ .get_func = _level_get, .set_func = _level_set},
{ .get_func = _level_get, .set_func = _level_set},
{ .get_func = _level_get, .set_func = _level_set},
{ .get_func = _level_get, .set_func = _level_set},
};



static struct bt_mesh_light_hub_srv light_hub_srvs[] = {
    { 
      .enableds_set_func = _enableds_set, 
      .levels_set_func = _levels_set,
      .groups_add_func = _groups_add,
      .groups_del_func = _groups_del,
      .groups_clr_func = _groups_clr,
      .dimming_config  = _dimming_config,
      .status_config   = _status_config,
      .status_store    = _status_store,
    },
};


static struct bt_mesh_model models0[] = {
    BT_MESH_MODEL_GEN_LEVEL_SRV(&_gel_level_srvs[0], NULL),
    BT_MESH_MODEL_LIGHT_HUB_SRV(&light_hub_srvs[0]),
    BT_MESH_MODEL_CFG_SRV(&_cfg_srv),
    BT_MESH_MODEL_CFG_CLI(&_cfg_cli),
    BT_MESH_MODEL_HEALTH_SRV(&_health_srv, &_health_pub),
};

static struct bt_mesh_model models1[] = {
    BT_MESH_MODEL_GEN_LEVEL_SRV(&_gel_level_srvs[1], NULL),
};

static struct bt_mesh_model models2[] = {
    BT_MESH_MODEL_GEN_LEVEL_SRV(&_gel_level_srvs[2], NULL)
};

static struct bt_mesh_model models3[] = {
    BT_MESH_MODEL_GEN_LEVEL_SRV(&_gel_level_srvs[3], NULL)
};

static struct bt_mesh_model models4[] = {
    BT_MESH_MODEL_GEN_LEVEL_SRV(&_gel_level_srvs[4], NULL)
};

static struct bt_mesh_model models5[] = {
    BT_MESH_MODEL_GEN_LEVEL_SRV(&_gel_level_srvs[5], NULL)
};

static struct bt_mesh_model models6[] = {
    BT_MESH_MODEL_GEN_LEVEL_SRV(&_gel_level_srvs[6], NULL)
};

static struct bt_mesh_model models7[] = {
    BT_MESH_MODEL_GEN_LEVEL_SRV(&_gel_level_srvs[7], NULL)
};


static struct bt_mesh_model *modelss[] = {models0, models1, models2, models3,
                models4, models5, models6, models7};

static struct bt_mesh_vng_status vng_status[] = {
    {
        .set_func = _mesh_status_set,
        .groups_count_get = _groups_count_get,
        .groups_list_get  = _groups_list_get,
    },
};

static struct bt_mesh_vng_command_srv vng_command_srvs[] = {
    {.user_data = NULL},
};

#ifdef CONFIG_BT_MESH_VNG_THREAD_TRACKING
static struct bt_mesh_vng_thread_tracking vng_thread_tracking = {
    
};
#endif


BT_MESH_MODEL_PUB_DEFINE(hub_status_pub, NULL, 3 + 15);

static struct bt_mesh_model vnd_models[] = {
    BT_MESH_MODEL_VNG_STATUS(&vng_status[0], &hub_status_pub),
    BT_MESH_MODEL_VNG_COMMAND_SRV(&vng_command_srvs[0]),

#ifdef CONFIG_BT_MESH_VNG_THREAD_TRACKING
    BT_MESH_MODEL_VNG_THREAD_TRACKING(&vng_thread_tracking),
#endif
};

static struct bt_mesh_elem elems[] = {
    BT_MESH_ELEM(0, models0, vnd_models),
    BT_MESH_ELEM(0, models1, BT_MESH_MODEL_NONE),
    BT_MESH_ELEM(0, models2, BT_MESH_MODEL_NONE),
    BT_MESH_ELEM(0, models3, BT_MESH_MODEL_NONE),
    BT_MESH_ELEM(0, models4, BT_MESH_MODEL_NONE),
    BT_MESH_ELEM(0, models5, BT_MESH_MODEL_NONE),
    BT_MESH_ELEM(0, models6, BT_MESH_MODEL_NONE),
    BT_MESH_ELEM(0, models7, BT_MESH_MODEL_NONE),
};


static const struct bt_mesh_comp comp = {
    .cid = CID_VNG,
    .elem = elems,
    .elem_count = ARRAY_SIZE(elems),
};


static struct _groups_status _groups_status = {
    .model = &vnd_models[0],
    .glist = {.buf = &groups_list_buf, .gbits = GBITS_DEFAULT},
    .gcount = {.buf = &groups_count_buf},
    .enableds = {},
};

void _prov_complete(u16_t net_idx, u16_t addr)
{
    _net_idx = net_idx;
    SYS_LOG_INF("net idx %u", net_idx);
    if(_setting_loading)
        return;
    SYS_LOG_INF("Provisioned completed");

    k_delayed_work_init(&_configure_1st_work, _configure_1st);
    k_delayed_work_submit(&_configure_1st_work, 100);
}

static void _prov_reset(void)
{
    bt_mesh_prov_enable(BT_MESH_PROV_ADV | BT_MESH_PROV_GATT);
}

static void _mesh_status_set(u16_t time, u16_t main, u16_t duration, u16_t period)
{
   SYS_LOG_INF("time: %u, main: %u, duration: %u, period: %u", time, main, duration, period);
    if( main == 0)
        return;

    u16_t mod = bt_mesh_primary_addr() % main;

    if (mod == time) {
        hub_report_activate(10, duration*1000);
        hub_report_period_set(period);  
    }
    
}


static void _level_get(u16_t timeout, void *user_data)
{
    hub_line_level_get(user_data, timeout);
}

static void _level_set(u16_t level,
                       u8_t tid,
                       u8_t time,
                       u8_t delay,
                       void *user_data, bool ack)
{
    hub_line_level_set(user_data, level, ack);
}

static void _levels_set(u16_t level, u8_t time, u8_t delay, void *user_data, bool ack)
{
    hub_level_set(level, ack);
}

int mesh_init()
{
    int err;
    struct bt_le_oob oob;
    err = bt_le_oob_get_local(BT_ID_DEFAULT, &oob);

    if (err != 0) {
        SYS_LOG_ERR("Get LE local Out Of Band information failed");
        return err;
    }

    memset(uuid, 0, sizeof(uuid));
    memcpy(uuid, oob.addr.a.val, 6);

    err = bt_mesh_init(&prov, &comp);

    if (err != 0) {
        SYS_LOG_ERR("Initialize Mesh support failed");
        return err;
    }

    SYS_LOG_INF("Mesh initialized");

    if (IS_ENABLED(CONFIG_BT_SETTINGS)) {
        _setting_loading = true;
        settings_load();
        SYS_LOG_INF("done");
        _setting_loading = false;
    }


    _mesh_hub_setting_load();

#ifdef CONFIG_BT_MESH_VNG_THREAD_TRACKING
    bt_mesh_vng_thread_tracking_setting_load();
#endif

    k_work_init(&_provision_work, _provision);
    k_work_submit(&_provision_work);

    return 0;
}

static void _mesh_hub_setting_load()
{
    u8_t *setting_data;
    u16_t setting_len;
    int error;

    error = hub_setting_set(models0[1].setting_val.val->data, 
                    models0[1].setting_val.val->len);
    if (error) {

        SYS_LOG_INF("store new setting");
        hub_setting_get(&setting_data, &setting_len);

        bt_mesh_model_setting_val_update((&models0[1]), 
                    setting_data, setting_len);
    }
        
    
}


static void _mesh_hub_level_report(const u8_t *values, u8_t lines)
{
    int error;
    struct net_buf_simple *msg = vnd_models[0].pub->msg;
    bt_mesh_model_msg_init(msg, BT_MESH_MODEL_OP_VND_LEVELS);
    net_buf_simple_add_mem(msg, values, lines);

    vnd_models[0].pub->ttl = bt_mesh_default_ttl_get();

    error = bt_mesh_model_publish(&vnd_models[0]);

    if (error != 0) {
        SYS_LOG_ERR("publish failed with error: %d", error);
    } else {
        SYS_LOG_INF("publish successed");
    }
}

static void _mesh_hub_status_report(const u8_t *values, u8_t lines)
{
    int error;
    struct net_buf_simple *msg = vnd_models[0].pub->msg;
    bt_mesh_model_msg_init(msg, BT_MESH_MODEL_OP_VND_STATUS);
    net_buf_simple_add_mem(msg, values, lines);

    vnd_models[0].pub->ttl = bt_mesh_default_ttl_get();
    
    error = bt_mesh_model_publish(&vnd_models[0]);

    if (error != 0) {
        SYS_LOG_ERR("publish failed with error: %d", error);
    } else {
        SYS_LOG_INF("publish successed");
    }
}

static void _mesh_hub_warning_report(const u8_t *status, u8_t lines)
{
    SYS_LOG_INF("");
    int error;
    NET_BUF_SIMPLE_DEFINE(msg, 2 + 12 + 4);

    u8_t ttl = bt_mesh_default_ttl_get();
    struct bt_mesh_msg_ctx ctx = {
        .net_idx = bt_mesh.sub[0].net_idx,
        .app_idx = _app_idx,
        .addr = CONTROL_GROUP_ADDR,
        .send_ttl = ttl,
    };

    bt_mesh_model_msg_init(&msg, BT_MESH_MODEL_OP_VND_WARNING);
    net_buf_simple_add_mem(&msg, status, lines);
    error = bt_mesh_model_send(&vnd_models[0], &ctx, &msg, NULL, NULL);
    if (error != 0) {
        SYS_LOG_ERR("Send failed with error: %d", error);
    } else {
        SYS_LOG_INF("Send successed");
    }
}

static void _provision(struct k_work *work)
{   
    _addr = flash_unicast_get();
    SYS_LOG_INF("Address going to provision is %d",_addr);
    int err = bt_mesh_provision(_net_key,
                                _net_idx,
                                _flags,
                                _iv_index,
                                _addr,
                                _dev_key);

    if (err == -EALREADY) {
        SYS_LOG_DBG("Using stored settings");
    } else if (err) {
        SYS_LOG_DBG("Provisioning failed (err %d)", err);
    }

}

static void _configure_1st(struct k_work *work)
{
    /* Add Application Key */
    bt_mesh_cfg_app_key_add(_net_idx,
                            elems[0].addr,
            _net_idx,
            _app_idx,
            _app_key,
            NULL);

    MODELS_BIND(models0, false , _net_idx, _app_idx);

    k_delayed_work_init(&_configure_2nd_work, _configure_2nd);
    k_delayed_work_submit(&_configure_2nd_work, 100);
}

static void _configure_2nd(struct k_work *work)
{
    MODELS_BIND(models1, false , _net_idx, _app_idx);
    MODELS_BIND(models2, false , _net_idx, _app_idx);
    MODELS_BIND(models3, false , _net_idx, _app_idx);
    MODELS_BIND(models4, false , _net_idx, _app_idx);
    MODELS_BIND(vnd_models, true , _net_idx, _app_idx);
    k_delayed_work_init(&_configure_3rd_work, _configure_3rd);
    k_delayed_work_submit(&_configure_3rd_work, 100);

    SYS_LOG_INF("done");
}

static void _configure_3rd(struct k_work *work)
{
    u16_t primary_addr = bt_mesh_primary_addr();
    MODELS_BIND(models5, false , _net_idx, _app_idx);
    MODELS_BIND(models6, false , _net_idx, _app_idx);
    MODELS_BIND(models7, false , _net_idx, _app_idx);
   

    struct bt_mesh_cfg_mod_pub level_pub = {
        .addr = STATUS_DESTINATION_GROUP_ADDR,
                .app_idx = _app_idx,
                .ttl = 3,
                .transmit = BT_MESH_PUB_TRANSMIT(1, 50),
    };

    MODEL_PUB(vnd_models[0], true, _net_idx, &level_pub);

    bt_mesh_cfg_mod_sub_add_vnd(_net_idx,
                                primary_addr,
                                primary_addr,
                                STATUS_CONTROL_GROUP_ADDR,
                                BT_MESH_MODEL_ID_VNG_STATUS,
                                CID_VNG,
                                NULL);

    
    bt_mesh_cfg_mod_sub_add(_net_idx,
                            primary_addr,
                            primary_addr,
                            HUB_GROUP_ADDR,
                            BT_MESH_MODEL_ID_LIGHT_HUB_SRV,
                            NULL);    

    bt_mesh_cfg_mod_sub_add_vnd(_net_idx, 
                                primary_addr, 
                                primary_addr,
                                VNG_MESH_RENEW_GROUP, 
                                BT_MESH_MODEL_ID_VNG_COMMAND_SRV, 
                                CID_VNG, 
                                NULL);

#ifdef CONFIG_BT_MESH_VNG_THREAD_TRACKING

    bt_mesh_cfg_mod_sub_add_vnd(_net_idx, 
                                primary_addr, 
                                primary_addr,
                                VNG_MESH_THREAD_TRACKING_GROUP, 
                                BT_MESH_MODEL_ID_VNG_THREAD_TRACKING, 
                                CID_VNG, 
                                NULL);
#endif 
    SYS_LOG_INF("done");

}


static void _enableds_set(u32_t enableds, void *user_data, bool ack)
{
    
    u8_t *setting_data;
    u16_t setting_len;

    hub_enables_set(enableds);

    hub_setting_get(&setting_data, &setting_len);
    bt_mesh_model_setting_val_update((&models0[1]), 
                            setting_data, setting_len);

    if (ack) {

        _groups_status.enableds.val = enableds;
        k_delayed_work_cancel(&_groups_status.enableds.timer);
        k_delayed_work_submit(&_groups_status.enableds.timer, 10);
    }
}

static void _dimming_config(u8_t linesmap, u8_t off_0, u8_t scale_min, u8_t scale_max, void *user_data)
{
    

    u8_t *setting_data;
    u16_t setting_len;

    hub_pwm_scale_set(linesmap, off_0, scale_min, scale_max);

    hub_setting_get(&setting_data, &setting_len);
    bt_mesh_model_setting_val_update((&models0[1]), 
                            setting_data, setting_len);
}

static void _status_config(u8_t status_en, u8_t warn_en, u16_t check_period, u16_t notify_period, void *user_data)
{
    u8_t *setting_data;
    u16_t setting_len;

    hub_status_config_set(status_en, warn_en, check_period, notify_period);
    
    hub_setting_get(&setting_data, &setting_len);
    bt_mesh_model_setting_val_update((&models0[1]), 
                            setting_data, setting_len);

}

static void _status_store(u8_t store, void *user_data)
{
    u8_t *setting_data;
    u16_t setting_len;
    int error;

    error = hub_status_store_set(store);

    if (error)
        return;

    hub_setting_get(&setting_data, &setting_len);
    bt_mesh_model_setting_val_update((&models0[1]), 
                            setting_data, setting_len);

}

static void _groups_add(u8_t linesmap, u16_t group, void *user_data, bool ack)
{
    u8_t *setting_data;
    u16_t setting_len;

    hub_groups_add(linesmap, group);
    
    hub_setting_get(&setting_data, &setting_len);
    bt_mesh_model_setting_val_update((&models0[1]), 
                            setting_data, setting_len);

    if(ack)
        _groups_list_delayed_get(linesmap, 2000);
}

static void _groups_del(u8_t linesmap,  u16_t group, void *user_data, bool ack)
{
    u8_t *setting_data;
    u16_t setting_len;

    hub_groups_del(linesmap, group);
    
    hub_setting_get(&setting_data, &setting_len);
    bt_mesh_model_setting_val_update((&models0[1]), 
                            setting_data, setting_len);
    
    if(ack)
        _groups_list_delayed_get(linesmap, 2000);


}

static void _groups_clr(u8_t linesmap, u8_t lines, void *user_data, bool ack)
{
    u8_t *setting_data;
    u16_t setting_len;

    hub_groups_clr(linesmap, lines);
    
    hub_setting_get(&setting_data, &setting_len);
    bt_mesh_model_setting_val_update((&models0[1]), 
                            setting_data, setting_len);

    
    if(ack)
        _groups_list_delayed_get(linesmap, 2000);
}

static void _line_enables_status_work(struct k_work* work)
{
    SYS_LOG_INF("");
    int error;
    NET_BUF_SIMPLE_DEFINE(msg, 2 + 12 + 4);

    u8_t ttl = bt_mesh_default_ttl_get();
    struct bt_mesh_msg_ctx ctx = {
        .net_idx = bt_mesh.sub[0].net_idx,
        .app_idx = _app_idx,
        .addr = STATUS_DESTINATION_GROUP_ADDR,
        .send_ttl = ttl,
    };

    bt_mesh_model_msg_init(&msg, BT_MESH_MODEL_OP_LINE_ENABLES);
    
    net_buf_simple_add_u8(&msg, _groups_status.enableds.val);
    SYS_LOG_INF("buf %u: %s", msg.len, bt_hex(msg.data, msg.len));
    
    error = bt_mesh_model_send(_groups_status.model, &ctx, &msg, NULL, NULL);
    if (error != 0) {
        SYS_LOG_ERR("Send failed with error: %d", error);
    } else {
        SYS_LOG_INF("Send successed");
    }
}

static void _groups_list_delayed_get(u8_t linesmap, u16_t delayed)
{
    
    for (int line = 0; line < HUB_LINES; ++line) {

        if (linesmap & BIT(line)) {

            atomic_set_bit(_groups_status.tasks, line);

        }
    }
    
    k_delayed_work_cancel(&_groups_status.timer);
    k_delayed_work_submit(&_groups_status.timer, delayed);

}
static void _groups_list_get(u16_t index, u16_t main, u8_t line, u8_t gbits)
{
    if( main == 0)
        return;

    u16_t mod = bt_mesh_primary_addr() % main;

    if (mod != index)
        return;

    _groups_status.glist.gbits = gbits;
    
    u8_t linesmap = BIT(line);

    _groups_list_delayed_get(linesmap, 10);

}
static void _groups_count_get(u16_t index, u16_t main)
{
    if( main == 0)
        return;

    u16_t mod = bt_mesh_primary_addr() % main;

    if (mod != index)
        return;
    
    atomic_set_bit(_groups_status.tasks, GROUP_COUNT);

    k_delayed_work_cancel(&_groups_status.timer);
    k_delayed_work_submit(&_groups_status.timer, 10);
}


static void _groups_status_send(bool glist, struct net_buf_simple* buf)
{
    SYS_LOG_INF("");
    int error;
    NET_BUF_SIMPLE_DEFINE(msg, 2 + 12 + 4);

    u8_t len;
    u8_t ttl = bt_mesh_default_ttl_get();
    struct bt_mesh_msg_ctx ctx = {
        .net_idx = bt_mesh.sub[0].net_idx,
        .app_idx = _app_idx,
        .addr = STATUS_DESTINATION_GROUP_ADDR,
        .send_ttl = ttl,
    };

    if (glist) {

        bt_mesh_model_msg_init(&msg, BT_MESH_MODEL_OP_GROUPS_LIST);
    
        u8_t glist_info_0 = GLIST_INFO_0(_groups_status.glist.line, 
                                        _groups_status.glist.gbits);

        u8_t glist_info_1 = GLIST_INFO_1(_groups_status.glist.pno, 
                                        _groups_status.glist.pid);

        
        ++_groups_status.glist.pid;

        net_buf_simple_add_u8(&msg, glist_info_0);
        net_buf_simple_add_u8(&msg, glist_info_1);

        len = buf->len > GLIST_DATA_LEN_MAX ? GLIST_DATA_LEN_MAX : buf->len;

    } else {

        bt_mesh_model_msg_init(&msg, BT_MESH_MODEL_OP_GROUPS_COUNT);
        len = buf->len > GCOUNT_DATA_LEN_MAX ? GCOUNT_DATA_LEN_MAX : buf->len;
    }
    

    net_buf_simple_add_mem(&msg, buf->data, len);
    SYS_LOG_INF("buf %u: %s", len, bt_hex(msg.data, msg.len));
    
    // remove sent-data.
    net_buf_simple_pull(buf, len);

    error = bt_mesh_model_send(_groups_status.model, &ctx, &msg, NULL, NULL);
    if (error != 0) {
        SYS_LOG_ERR("Send failed with error: %d", error);
    } else {
        SYS_LOG_INF("Send successed");
    }
}

static void _groups_status_work(struct k_work* work)
{
    // check if tosend-buf has data.

    if (_groups_status.glist.buf->len > 0) {

        _groups_status_send(true, _groups_status.glist.buf);
        k_delayed_work_submit(&_groups_status.timer, 200);
        return;
    }

    if (_groups_status.gcount.buf->len > 0) {

         _groups_status_send(false, _groups_status.gcount.buf);
         k_delayed_work_submit(&_groups_status.timer, 200);
         return;
    }

    for(int task = 0; task < GROUP_TASKS_COUNT; ++task) {

        if (atomic_test_and_clear_bit(_groups_status.tasks, task)) {

            if (task < GROUP_COUNT) { // group-list task, task = line-idx

                SYS_LOG_INF("group-list, line %u, gbits %u", task, _groups_status.glist.gbits);
                u8_t  buf[CONFIG_BT_MESH_MODEL_GROUP_COUNT * sizeof(u16_t)];
                u16_t bbidx = 0; //buffer bit-index
                u8_t len;
                u16_t group;
                u8_t bit;
                struct bt_mesh_model* mod = &modelss[task][0];

                for (int gidx = 0; gidx < CONFIG_BT_MESH_MODEL_GROUP_COUNT; ++gidx) {
                    
                    group = mod->groups[gidx];
                    if (BT_MESH_ADDR_IS_GROUP(group) && group != VNG_MESH_UNUSED_GROUP) {
                        // msb-first
                        SYS_LOG_INF("\tgroup %04X, idx %u, bbidx %u", group, gidx, bbidx);
                        for (int bidx = _groups_status.glist.gbits - 1; bidx >= 0; --bidx) {
                            bit = (group & BIT(bidx)) > 0 ? 1 : 0;
    
                            SET_BUF_BIT(buf, bbidx, bit);
                            ++bbidx;
                        }
                    }
                }
                
                len = (bbidx > 0) ? (bbidx - 1) / 8 /*bits*/ + 1 : 0;
                net_buf_simple_add_mem(_groups_status.glist.buf, buf, len);
                _groups_status.glist.pno = (len > 0) ? (len - 1) / GLIST_DATA_LEN_MAX + 1 : 0; 
                _groups_status.glist.pid = 0;
                _groups_status.glist.line = task;
                SYS_LOG_INF("line %u, pno %u, buf %u: %s", task, _groups_status.glist.pno , len, bt_hex(buf, len));

                if (_groups_status.glist.pno == 0) {
                    //groups-list is empty
                    net_buf_simple_add_u8(_groups_status.glist.buf, 0);
                }
                k_delayed_work_submit(&_groups_status.timer, 10);

                return;

            } else { // group-count task
                SYS_LOG_INF("group-count");

                u16_t groups[CONFIG_BT_MESH_MODEL_GROUP_COUNT];
                u8_t gcount;
                u16_t group;
                u16_t crc;
                struct bt_mesh_model* mod;
                u8_t edata;

                net_buf_simple_reset(_groups_status.gcount.buf);

                for (int line = 0; line < HUB_LINES; ++line) {
                    
                    mod = &modelss[line][0];
                    gcount = 0;

                    for (int gidx = 0; gidx < CONFIG_BT_MESH_MODEL_GROUP_COUNT; ++gidx) {
                        
                        group = mod->groups[gidx];

                        if (BT_MESH_ADDR_IS_GROUP(group) && group != VNG_MESH_UNUSED_GROUP) {
                            
                            groups[gcount++] = sys_cpu_to_be16(group);
                        }
                    }

                    crc = crc16_ccitt(0, (const u8_t *)groups, gcount * sizeof(u16_t));

                    edata = GCOUNT_LINE_DATA(gcount, crc);

                    net_buf_simple_add_u8(_groups_status.gcount.buf, edata);

                }

                k_delayed_work_submit(&_groups_status.timer, 10);
                return;

            }
        }
        
    }

}


int mesh_hub_init()
{
    int error;

    u8_t* setting_data;
    u16_t setting_len;

    bt_mesh_vng_status_init(&vnd_models[0]);
    bt_mesh_light_hub_srv_init(&models0[1]);

#ifdef CONFIG_BT_MESH_VNG_THREAD_TRACKING
    bt_mesh_vng_thread_tracking_init(&vnd_models[2]);
#endif

    k_delayed_work_init(&_groups_status.timer, _groups_status_work);
    k_delayed_work_init(&_groups_status.enableds.timer, _line_enables_status_work);

    error = hub_init(_mesh_hub_level_report,
                     _mesh_hub_status_report,
                     _mesh_hub_warning_report);

    if (error != 0)
        return error;

    bt_mesh_gen_level_srv_init(&models0[0]);
    bt_mesh_gen_level_srv_init(&models1[0]);
    bt_mesh_gen_level_srv_init(&models2[0]);
    bt_mesh_gen_level_srv_init(&models3[0]);
    bt_mesh_gen_level_srv_init(&models4[0]);
    bt_mesh_gen_level_srv_init(&models5[0]);
    bt_mesh_gen_level_srv_init(&models6[0]);
    bt_mesh_gen_level_srv_init(&models7[0]);


    hub_setting_get(&setting_data, &setting_len);
    BT_MESH_MODEL_SETTING_VAL_DEFINE(enables_setting,
                                     (models0 + 1),
                                     60);

    for(u8_t line = 0; line < HUB_LINES; line ++) {
        struct bt_mesh_gen_level_srv *level_srv = &_gel_level_srvs[line];
        level_srv->user_data = hub_line_init(line);
        if (level_srv->user_data == NULL) {
            SYS_LOG_ERR("Init line %u failed", 0);
            hub_release();
            return -ENODEV;
        }
    }
    SYS_LOG_INF("done");
    return 0;
}


int mesh_hub_line_group_sub_add(u8_t line, u16_t group)
{

    MODEL_SUB_ADD(modelss[line][0], false, bt_mesh.sub[0].net_idx, group);
    return 0;
}

int mesh_hub_line_group_sub_del(u8_t line, u16_t group)
{

    MODEL_SUB_DEL(modelss[line][0], false, bt_mesh.sub[0].net_idx, group);
    return 0;
}

int mesh_hub_line_group_sub_clr(u8_t line)
{
    MODEL_SUB_OVERRIDE(modelss[line][0], false, bt_mesh.sub[0].net_idx, VNG_MESH_UNUSED_GROUP)
    // SYS_LOG_WRN("Clear sub-list not supported");

    return 0;
}

void mesh_hub_setting_store()
{
    u8_t *setting_data;
    u16_t setting_len;
    
    hub_setting_get(&setting_data, &setting_len);
    bt_mesh_model_setting_val_update((&models0[1]), 
                            setting_data, setting_len);
}
