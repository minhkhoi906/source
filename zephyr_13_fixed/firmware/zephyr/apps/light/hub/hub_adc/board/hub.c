#include "hub.h"
#include "mesh.h"
#include <board.h>

#include <adc.h>
#include <entropy.h>
#include <errno.h>
#include <gpio.h>
#include <pwm.h>
#define SYS_LOG_DOMAIN "app"
#include <common/log.h>
#include <logging/sys_log.h>
#include <misc/util.h>
#include <bluetooth/mesh.h>



#define HUB_WATCHDOG_PERIOD 400
#define PWM_POLACITY   true
#define PWM_PERIOD     1000
#define PWM_LEVEL_MAX 0x0000FFFF

#define PWM_PERCENT_RANGE_MINIMUM 15
#define PWM_PERCENT_RANGE_MAXIMUM 85
#define PWM_PERCENT_RANGE_OFFSET_0 2
#define PWM_PERCENT_RANGE_OFFSET_DISABLE 100 // disable scaling

#define PWM_PERCENT_RANGE_FULL 100

#define ADC_PSEL_BASE     1
#define ADC_CHANNEL_BASE  0
#define ADC_CHANNEL_COUNT 8
#define ADC_VOL_REF      3  //vol
#define ADC_RESOLUTION   12 //bits
#define ADC_VAL_MAX      4096 //2^12


#define HUB_STORE_DEFAULT   0
#define STATUS_EN_DEFAULT   0
#define WARN_EN_DEFAULT     1
#define WARN_NOTIFY_PERIOD_DEFAULT    60
#define WARN_CHECK_PERIOD_DEFAULT     15
#define WARN_FAULT_RECHECKS  3 //times
#define WARN_FAULT_RECHECK_PERIOD     2 

#define STATUS_PERIOD_MIN             30

#define HUB_ENABLEDS_DEFAULT 0x00000000

#define STATUS_TIMEOUT_DEFAULT 3 //s


#define HUB_LINE_ADC_DRIVER_FAILED_MINIMUM 2000
#define HUB_LINE_ADC_DRIVER_FAILED_MAXIMUM 2400
#define HUB_LINE_ADC_NORMAL_MIMINUM 2450
#define HUB_LINE_ADC_NORMAL_MAXINUM 3900
#define HUB_LINE_ADC_LED_FAILED_MINIMUM 4000


#define CONFIG_DELAYED 10 //ms
  
#define TIMER_PERIOD 1000
#define TIMER NRF_TIMER2
#define TIMER_CC_PERIOD 0

#define PIN_SET(pin) \
    NRF_GPIO->OUTCLR = BIT(pin)
#define PIN_CLR(pin) \
    NRF_GPIO->OUTSET = BIT(pin)

struct _hub_line {
    u16_t level;
    u8_t status;
    struct device *pwm;
    u32_t pwm_pin;
    u32_t status_pin;
    bool enabled;
    u8_t index;
};

struct _line_setting
{
    u8_t off_0;
    u8_t min;
    u8_t max;

} __packed;


struct _status_setting
{
    u8_t  status_en;
    u8_t  warn_en;
    u16_t warn_check_period;
    u16_t warn_notify_period;
} __packed;

struct _level_store
{
    u8_t store;
    u16_t levels[HUB_LINES];

} __packed;
struct _hub_setting {
    u32_t enableds;
    struct _line_setting lines[HUB_LINES];
    struct _status_setting status;
    struct _level_store levels;
} __packed;


struct _hub_config {
    u8_t   linesmap_add;
    u8_t   linesmap_del;
    u8_t   linesmap_clr;

    u16_t  groups_add[HUB_LINES];
    u16_t  groups_del[HUB_LINES];

};


struct _hub_watchdog {
    volatile u32_t tick_count;
    u32_t state;
};


struct _hub {
    struct _hub_watchdog watchdog;

    struct _hub_line lines[HUB_LINES];

    hub_level_report level;
    struct k_delayed_work level_work;

    hub_status_report status;
    struct k_delayed_work status_work;
    struct k_delayed_work activate_work;

    hub_warning_report warning;
    struct k_delayed_work warning_work;
    bool hardrwaring;

    struct _hub_setting setting;

    struct k_delayed_work config_work;
    struct _hub_config config;

    struct k_delayed_work restore_work;

    u32_t report_period;

    u8_t faults;
};

static struct _hub _hub;
struct device *_gpio;
struct device* _adc;
struct device* _soft_pwm;
struct device* _entropy;

static u32_t _level_to_pulse(u8_t index, u16_t level, bool polacity);
static void _level_work(struct k_work* work);
static void _status_work(struct k_work* work);
static void _warning_work(struct k_work* work);
static void _config_work(struct k_work* work);
static u32_t _pwm_pin(u8_t line);
static u32_t _adc_pin(u8_t line);
static struct device *_pwm(u8_t line);
static u8_t _status(uint32_t pin, u8_t percent, u8_t index);
static int _hub_random(u32_t min, u32_t max, u32_t* randomed);
static void _level_restore_work(struct k_work* work);

static void _hub_wdt_init();
static void _hub_wdt_isr();

int hub_init(hub_level_report level,
             hub_status_report status,
             hub_warning_report warning)
{
    SYS_LOG_INF("Configure adc");

    _soft_pwm = device_get_binding(CONFIG_PWM_NRF52_HW_0_DEV_NAME);

    if (_soft_pwm == NULL) {
        SYS_LOG_ERR("Setup pwm device failed");
        return -ENODEV;
    }

    _adc = device_get_binding(CONFIG_ADC_0_NAME);

    if (_adc == NULL) {
        SYS_LOG_ERR("Setup adc device failed");
        return -ENODEV;
    }

    _gpio = device_get_binding(CONFIG_GPIO_P0_DEV_NAME);

    if (_gpio == NULL) {
        SYS_LOG_ERR("Setup GPIO device failed");
        return -ENODEV;
    }

    _entropy = device_get_binding(CONFIG_ENTROPY_NAME);
    if(_entropy == NULL) {
        SYS_LOG_ERR("Setup entropy device failed");
        return -ENODEV;
    }

    SYS_LOG_INF("Configure software pin");

    gpio_pin_configure(_gpio, SW_ENABLE_PIN, GPIO_DIR_OUT);
    gpio_pin_write(_gpio, SW_ENABLE_PIN, 1);

    SYS_LOG_INF("Configure bypass pin");

    gpio_pin_configure(_gpio, PIN_BYPASS, GPIO_DIR_OUT | GPIO_PUD_PULL_DOWN);
    gpio_pin_write(_gpio, PIN_BYPASS, 0);


    SYS_LOG_INF("Configure hardware watchdog");
    _hub_wdt_init();

    k_delayed_work_init(&_hub.level_work, _level_work);
    // k_delayed_work_submit(&_hub.level_work, K_SECONDS(STATUS_PERIOD_MIN));

    k_delayed_work_init(&_hub.status_work, _status_work);
    // k_delayed_work_submit(&_hub.status_work, K_SECONDS(STATUS_PERIOD_MIN));

    k_delayed_work_init(&_hub.warning_work, _warning_work);
    k_delayed_work_submit(&_hub.warning_work, K_SECONDS(WARN_CHECK_PERIOD_DEFAULT));

    k_delayed_work_init(&_hub.config_work, _config_work);

    k_delayed_work_init(&_hub.restore_work, _level_restore_work);
    k_delayed_work_submit(&_hub.restore_work, 2000); //wait for setting load;

    _hub.config.linesmap_add = 0x00;
    _hub.config.linesmap_del = 0x00;
    _hub.config.linesmap_clr = 0x00;

    _hub.level = level;
    _hub.status = status;
    _hub.warning = warning;
    _hub.hardrwaring = true;
    _hub.faults = 0;

    _hub.setting.enableds = HUB_ENABLEDS_DEFAULT;

    for(int index = 0; index < HUB_LINES; index++) {
        _hub.setting.lines[index].off_0 = PWM_PERCENT_RANGE_OFFSET_0;
        _hub.setting.lines[index].min = PWM_PERCENT_RANGE_MINIMUM;
        _hub.setting.lines[index].max = PWM_PERCENT_RANGE_MAXIMUM;
    }

    _hub.setting.status.status_en   = STATUS_EN_DEFAULT;
    _hub.setting.status.warn_en     = WARN_EN_DEFAULT;
    _hub.setting.status.warn_notify_period = WARN_NOTIFY_PERIOD_DEFAULT; //seconds
    _hub.setting.status.warn_check_period  = WARN_CHECK_PERIOD_DEFAULT; //seconds
    
    _hub.setting.levels.store       = HUB_STORE_DEFAULT;
    

    for(int index = 0; index < HUB_LINES; index++) {
        _hub.lines[index].index = index;
    }

    return 0;
}
static u16_t _level_to_percent(u16_t level)
{
    return PWM_POLACITY == true? level * 100 / PWM_LEVEL_MAX:
                                 100 - level * 100 / PWM_LEVEL_MAX;
}

void hub_level_set(u16_t level, bool ack)
{
    if (_hub.hardrwaring == true) {
        _hub.hardrwaring = false;
        gpio_pin_write(_gpio, SW_ENABLE_PIN, 0);
    }

    for(int index = 0; index < HUB_LINES; index++) {

        struct _hub_line *line = _hub.lines + index;

        line->level = level;
        u32_t pulse = _level_to_pulse(line->index, level, PWM_POLACITY);
        SYS_LOG_INF("line: %d, pin %d, level: %u, pulse: %u", index, line->pwm_pin, level, pulse);
        pwm_pin_set_usec(line->pwm, line->pwm_pin, PWM_PERIOD, pulse);
    }

    if (ack) {
        hub_report_activate(10, K_SECONDS(STATUS_TIMEOUT_DEFAULT));
    }

}


void hub_enables_set(u32_t enableds)
{
    SYS_LOG_INF("enableds: 0x%08x", enableds);
    _hub.setting.enableds = enableds;

    for(int index = 0; index < HUB_LINES; index++) {

        struct _hub_line *line = _hub.lines + index;

        if ((BIT(index) & enableds) == 0)
            line->enabled = false;
        else
            line->enabled = true;
    }
}


void hub_pwm_scale_set(u8_t linesmap, u8_t off_0, u8_t min, u8_t max)
{
    u8_t valid_min = (0 <= min && min < 100) ? min : PWM_PERCENT_RANGE_MINIMUM;
    u8_t valid_max = (0 <= max && max < 100) ? max : PWM_PERCENT_RANGE_MAXIMUM;
    u8_t valid_off = (0 <= off_0 && off_0 <= 100) ? off_0 : PWM_PERCENT_RANGE_OFFSET_0;

    SYS_LOG_INF("linesmask 0x%02X, off_0 %02d, min %02d, max %02d", linesmap, off_0, min, max);

    for(int index = 0; index < HUB_LINES; index++) {
        if(linesmap & BIT(index)) {
            _hub.setting.lines[index].off_0 = valid_off;
            _hub.setting.lines[index].min = valid_min;
            _hub.setting.lines[index].max = valid_max;
        }
    }
}

void hub_status_config_set(u8_t status_en, u8_t warn_en, u16_t check_period, u16_t notify_period)
{
    SYS_LOG_INF("status_en %u, warn_en %u, check_period %u, notify_period %u (s)", 
                        status_en, warn_en, check_period, notify_period);

    _hub.setting.status.status_en = status_en;
    _hub.setting.status.warn_en   = warn_en;
    _hub.setting.status.warn_check_period = check_period;
    _hub.setting.status.warn_notify_period = notify_period;

    k_delayed_work_cancel(&_hub.warning_work);
    k_delayed_work_submit(&_hub.warning_work,
                        K_SECONDS(_hub.setting.status.warn_check_period));
}


int hub_status_store_set(u8_t store)
{
    if(store == 0)
        return -1;

    _hub.setting.levels.store = 1;

    for(u8_t index = 0; index < HUB_LINES; index++)
        _hub.setting.levels.levels[index] = _hub.lines[index].level;

    return 0;
}


void hub_groups_add(u8_t linesmap, u16_t group)
{
    SYS_LOG_INF("");

    _hub.config.linesmap_add |= linesmap;
    for(int index = 0; index < HUB_LINES; index++) {

        if(linesmap & BIT(index)) {
            _hub.config.groups_add[index] = group;
            break;
        }
    }

    k_delayed_work_cancel(&_hub.config_work);
    k_delayed_work_submit(&_hub.config_work, CONFIG_DELAYED);

}

void hub_groups_del(u8_t linesmap, u16_t group)
{
    SYS_LOG_INF("");

    _hub.config.linesmap_del |= linesmap;
    for(int index = 0; index < HUB_LINES; index++) {
 
        if(linesmap & BIT(index)) {
            _hub.config.groups_del[index] = group;
            break;
        }
    }

    k_delayed_work_cancel(&_hub.config_work);
    k_delayed_work_submit(&_hub.config_work, CONFIG_DELAYED);

}

void hub_groups_clr(u8_t linesmap, u8_t lines)
{
    SYS_LOG_INF("");
    _hub.config.linesmap_clr |= linesmap;

    k_delayed_work_cancel(&_hub.config_work);
    k_delayed_work_submit(&_hub.config_work, CONFIG_DELAYED);

}


void hub_report_period_set(u16_t period)
{
    _hub.report_period = period;
}

void hub_report_activate(u32_t min_duration, u32_t max_duration)
{
    u32_t duration = 0;
    int error;

    error = _hub_random(min_duration, max_duration, &duration);

    // SYS_LOG_INF("period: %u (ms)", max_duration);

    if (error != 0)
        duration = 10;

    SYS_LOG_INF("level duration: %u", duration);

    k_delayed_work_cancel(&_hub.level_work);
    k_delayed_work_submit(&_hub.level_work, duration);

    error = _hub_random(max_duration, max_duration, &duration);

    // SYS_LOG_INF("period: %u", period);

    if (_hub.setting.status.status_en == 0) {
        SYS_LOG_INF("status en = 0");
        return;
    }

    if (error != 0)
        duration = 10;

    SYS_LOG_INF("status duration: %u", duration);

    k_delayed_work_cancel(&_hub.status_work);
    k_delayed_work_submit(&_hub.status_work, duration);
}

struct _hub_line *hub_line_init(u8_t index)
{

    int error;
    struct _hub_line *line = _hub.lines + index;
    struct adc_channel_cfg cfg = {
        .gain = ADC_GAIN_1_5,
        .reference = ADC_REF_INTERNAL,
        .acquisition_time = ADC_ACQ_TIME_DEFAULT,
        .differential = 0,
    };

    line->pwm = _pwm(index);
    line->status_pin = _adc_pin(index);
    line->pwm_pin = _pwm_pin(index);
    line->level = PWM_LEVEL_MAX;
    // SYS_LOG_INF("line: %u, index, status pin: %u, pwm pin: %u",
    //             index,
    //             line->status_pin,
    //             line->pwm_pin);

    cfg.channel_id = ADC_CHANNEL_BASE + line->status_pin;
    cfg.input_positive = ADC_PSEL_BASE + line->status_pin;

    error = adc_channel_setup(_adc, &cfg);

    if (error != 0) {
        SYS_LOG_ERR("setup adc pin: %u, failed with error: %d",
                    line->status_pin,
                    error);

        return NULL;
    }

    line->enabled = false;
    return line;
}


void hub_line_level_set(struct _hub_line *line, u16_t level, bool ack)
{
    if (line->enabled != true) {
        SYS_LOG_DBG("Line has not enabled yet");
        return;
    }

    if (_hub.hardrwaring == true) {
        //do not leave hardware pwm pins floating (unknown)
        hub_level_set(PWM_LEVEL_MAX, false);
    }

    line->level = level;
    u32_t pulse = _level_to_pulse(line->index, level, PWM_POLACITY);

    SYS_LOG_INF("line %u, level: %u, pulse: %u", line->index, level, pulse);
    pwm_pin_set_usec(line->pwm, line->pwm_pin, PWM_PERIOD, pulse);

    if (ack) {
        hub_report_activate(10, K_SECONDS(STATUS_TIMEOUT_DEFAULT));
    }
    
}

void hub_line_level_get(struct _hub_line *line, u16_t timeout)
{
    hub_report_activate(10, K_SECONDS(timeout));
}

void hub_setting_get(u8_t **data, u16_t *len)
{
    *data = (u8_t*)(&_hub.setting);
    *len  =  sizeof(_hub.setting);
}

int hub_setting_set(const u8_t *data, u16_t len)
{
    u16_t remain_len = len;

    
    if(len >= sizeof(_hub.setting)) {

        SYS_LOG_INF("match  _hub.setting len %u", len);
        memcpy(&_hub.setting, data, len);
        hub_enables_set(_hub.setting.enableds);

        SYS_LOG_INF("status config: status_en %u, warn_en %u, check_period %u, notify_period %u",
                                _hub.setting.status.status_en,
                                _hub.setting.status.warn_en,
                                _hub.setting.status.warn_check_period,
                                _hub.setting.status.warn_notify_period);

        return 0;

    }
    
    if (remain_len >= sizeof(_hub.setting.enableds)) {

        memcpy(&_hub.setting.enableds, data, sizeof(_hub.setting.enableds));
        hub_enables_set(_hub.setting.enableds);

        SYS_LOG_INF("enableds 0x%08x", _hub.setting.enableds);

        remain_len -= sizeof(_hub.setting.enableds);

    } 
    
    if (remain_len >= sizeof(_hub.setting.lines)) {
        memcpy(_hub.setting.lines, data + sizeof(_hub.setting.enableds), 
                                          sizeof(_hub.setting.lines));
        remain_len -= sizeof(_hub.setting.lines);

        SYS_LOG_INF("lines dim config");

    }

    if (remain_len >= sizeof(_hub.setting.status)) {
        memcpy(&_hub.setting.status, data + sizeof(_hub.setting.enableds) + 
                                            sizeof(_hub.setting.lines), 
                                            sizeof(_hub.setting.status));
        
        SYS_LOG_INF("status config: status_en %u, warn_en %u, check_period %u, notify_period %u",
                                _hub.setting.status.status_en,
                                _hub.setting.status.warn_en,
                                _hub.setting.status.warn_check_period,
                                _hub.setting.status.warn_notify_period);

        remain_len -= sizeof(_hub.setting.lines);
    }

    if (remain_len >= sizeof(_hub.setting.levels)) {

        memcpy(&_hub.setting.levels, data + sizeof(_hub.setting.enableds) + 
                                            sizeof(_hub.setting.lines) + 
                                            sizeof(_hub.setting.status), 
                                            sizeof(_hub.setting.levels));
        
        SYS_LOG_INF("store %u", _hub.setting.levels.store);
        
    }

    return -1;
}

void hub_release()
{
    k_delayed_work_cancel(&_hub.warning_work);
}

static u32_t _level_to_pulse(u8_t index, u16_t level, bool polacity)
{
    u32_t percent = (u32_t) level * 100 / PWM_LEVEL_MAX;

    u32_t scaled_percent;

    if(_hub.setting.lines[index].off_0 != PWM_PERCENT_RANGE_OFFSET_DISABLE) {
        scaled_percent = (percent <= _hub.setting.lines[index].off_0) ? 0 :   
                                                _hub.setting.lines[index].min +
                                                percent *
                                               (_hub.setting.lines[index].max -
                                                _hub.setting.lines[index].min) /
                                                PWM_PERCENT_RANGE_FULL;

    } else {
        scaled_percent = percent;
    }
    // SYS_LOG_INF("off_0 %02d, min %02d, max %02d", _hub.setting.lines[index].off_0,
    //     _hub.setting.lines[index].min, _hub.setting.lines[index].max);
    // SYS_LOG_INF("level %04d, percent: %02d, scaled_percent %02d", level, percent, scaled_percent);
    
    if (polacity == false)
        return scaled_percent * PWM_PERIOD / PWM_PERCENT_RANGE_FULL;

    return (PWM_PERCENT_RANGE_FULL - scaled_percent) * PWM_PERIOD /
            PWM_PERCENT_RANGE_FULL;
}


static void _level_work(struct k_work* work)
{
    u8_t levels[HUB_LINES];

    for(u8_t index = 0; index < HUB_LINES; index++)
        levels[index] = _level_to_percent(_hub.lines[index].level);

    _hub.level(levels, HUB_LINES);

    if (_hub.report_period >= STATUS_PERIOD_MIN) {

        SYS_LOG_INF("resend after %u", _hub.report_period);
        k_delayed_work_submit(&_hub.level_work, K_SECONDS(_hub.report_period));
    }  
}

static void _status_work(struct k_work* work)
{
    u8_t status[HUB_LINES];

    for(u8_t index = 0; index < HUB_LINES; index++) {
        struct _hub_line *line = _hub.lines + index;
        status[index] = _status(line->status_pin,
                                _level_to_percent(line->level), line->index);
    }

    _hub.status(status, HUB_LINES);
}


static void _warning_work(struct k_work* work)
{
    u8_t status[HUB_LINES];
    bool warning = false;
    bool has_fault = false;
    // SYS_LOG_INF("begin");
    for(u8_t index = 0; index < HUB_LINES; index++) {

        struct _hub_line *line = _hub.lines + index;
        // SYS_LOG_INF("level: %u", _hub.lines[index].level);
        line->status = _status(line->status_pin, _level_to_percent(line->level), line->index);
        status[index] = line->status;
        if (line->enabled == false)
            continue;

        if (status[index] != HUB_LINE_STATUS_NORMAL)
            has_fault = true;
            
    }

    if(has_fault) {
        if(++_hub.faults >= WARN_FAULT_RECHECKS) {
            _hub.faults = 0;
            warning = true;
        }
    } else {
        _hub.faults = 0;
    }

    // SYS_LOG_INF("end");

    if(_hub.faults > 0) {
        SYS_LOG_INF("Do fault recheck");
        k_delayed_work_submit(&_hub.warning_work,
                              K_SECONDS(WARN_FAULT_RECHECK_PERIOD));
    } else if (warning != true) {
        SYS_LOG_INF("recheck after %u (s)", _hub.setting.status.warn_check_period);
        k_delayed_work_submit(&_hub.warning_work,
                            K_SECONDS(_hub.setting.status.warn_check_period));
    } else {
        SYS_LOG_INF("Do warning");
        if (_hub.setting.status.warn_en)
            _hub.warning(status, HUB_LINES);

        k_delayed_work_submit(&_hub.warning_work,
                            K_SECONDS(_hub.setting.status.warn_notify_period));
    }
}

static void _config_work(struct k_work* work)
{
    for(int index = 0; index < HUB_LINES; index++) {

        if((_hub.config.linesmap_add >> index) & BIT(0)) {
            SYS_LOG_INF("add line %u, group: 0x%04x", index, 
                        _hub.config.groups_add[index]);

            _hub.config.linesmap_add &= ~(BIT(index));
            mesh_hub_line_group_sub_add(index, _hub.config.groups_add[index]);
                
            /*give other works a chance*/
            k_delayed_work_submit(&_hub.config_work, CONFIG_DELAYED);
            return;
        }

        if((_hub.config.linesmap_del >> index) & BIT(0)) {
            SYS_LOG_INF("del line %u, group: 0x%04x", index, 
                        _hub.config.groups_del[index]);

            _hub.config.linesmap_del &= ~(BIT(index));
            mesh_hub_line_group_sub_del(index, _hub.config.groups_del[index]);
                
            /*give other works a chance*/
            k_delayed_work_submit(&_hub.config_work, CONFIG_DELAYED);
            return;
        }

        if((_hub.config.linesmap_clr >> index) & BIT(0)) {
            SYS_LOG_INF("clr line %u", index);

            _hub.config.linesmap_clr &= ~(BIT(index));
            mesh_hub_line_group_sub_clr(index);
                
            /*give other works a chance*/
            k_delayed_work_submit(&_hub.config_work, CONFIG_DELAYED);
            return;
        }
    }
}

static void _level_restore_work(struct k_work* work)
{
    if (_hub.setting.levels.store == 0) {
        SYS_LOG_INF("nothing todo");
        return;
    }

    SYS_LOG_INF("restoring levels ...");

    _hub.setting.levels.store = 0;
    
    for (int index = 0; index < HUB_LINES; ++index) {
        hub_line_level_set(&_hub.lines[index], _hub.setting.levels.levels[index], false);
    }


    mesh_hub_setting_store();
}

static u32_t _pwm_pin(u8_t line)
{
    switch (line) {
    case 0:
        return HUB_PWM01_PIN;
    case 1:
        return HUB_PWM02_PIN;
    case 2:
        return HUB_PWM03_PIN;
    case 3:
        return HUB_PWM04_PIN;
    case 4:
        return HUB_PWM05_PIN;
    case 5:
        return HUB_PWM06_PIN;
    case 6:
        return HUB_PWM07_PIN;
    case 7:
        return HUB_PWM08_PIN;
    default:
        return 0xFFFFFFFF;
    }
}

static u32_t _adc_pin(u8_t line)
{
    switch (line) {
    case 0:
        return HUB_ADC01_PIN;
    case 1:
        return HUB_ADC02_PIN;
    case 2:
        return HUB_ADC03_PIN;
    case 3:
        return HUB_ADC04_PIN;
    case 4:
        return HUB_ADC05_PIN;
    case 5:
        return HUB_ADC06_PIN;
    case 6:
        return HUB_ADC07_PIN;
    case 7:
        return HUB_ADC08_PIN;
    default:
        return 0xFFFFFFFF;
    }
}


static struct device *_pwm(u8_t line)
{
    switch (line) {
    case 0:
        return _soft_pwm;
    case 1:
        return _soft_pwm;
    case 2:
        return _soft_pwm;
    case 3:
        return _soft_pwm;
    case 4:
        return _soft_pwm;
    case 5:
        return _soft_pwm;
    case 6:
        return _soft_pwm;
    case 7:
        return _soft_pwm;
    default:
        return NULL;
    }
}

static u8_t _status(uint32_t pin, u8_t percent, u8_t index)
{
    int error;
    u16_t value;

    struct adc_sequence adc_sequence = {
        .channels = BIT(pin),
        .buffer = &value,
        .buffer_size = 2,
        .resolution = ADC_RESOLUTION,
    };

    error = adc_read(_adc, &adc_sequence);
 

    if(error != 0)
        return HUB_LINE_STATUS_GPIO_FAILED;

    if(value >= HUB_LINE_ADC_DRIVER_FAILED_MINIMUM  && value <= HUB_LINE_ADC_DRIVER_FAILED_MAXIMUM) {
        if(percent <= _hub.setting.lines[index].min) {
            // SYS_LOG_INF("pin: %u, percent: %u, value: %u, HUB_LINE_STATUS_NORMAL", pin, percent, value);
            return HUB_LINE_STATUS_NORMAL;
        } else {
            // SYS_LOG_INF("pin: %u, percent: %u, value: %u, HUB_LINE_STATUS_DRIVER_FAILED", pin, percent, value);
            return HUB_LINE_STATUS_DRIVER_FAILED;
        }   
    }

    if(value >= HUB_LINE_ADC_NORMAL_MIMINUM &&  value <= HUB_LINE_ADC_NORMAL_MAXINUM) {
        // SYS_LOG_INF("pin: %u, percent: %u, value: %u, HUB_LINE_STATUS_NORMAL", pin, percent, value);
        return HUB_LINE_STATUS_NORMAL;
    }

    if(value >= HUB_LINE_ADC_LED_FAILED_MINIMUM) {
        // SYS_LOG_INF("pin: %u, percent: %u, value: %u, HUB_LINE_STATUS_LED_FAILED", pin, percent, value);
        return HUB_LINE_STATUS_LED_FAILED;
    }

    {
        // SYS_LOG_INF("pin: %u, percent: %u, value: %u, HUB_LINE_STATUS_UNDEFINED", pin, percent, value);
        return HUB_LINE_STATUS_UNDEFINED;
    }

    
}

static int _hub_random(u32_t min, u32_t max, u32_t *randomed)
{
    int error;
    u32_t rand;

    int del;

    error = entropy_get_entropy(_entropy, (u8_t *)(&rand), sizeof(rand));

    if(error != 0) {
        SYS_LOG_ERR("random failed with error: %d", error);
        return error;
    }

    del = (max - min <= 0) ? 10: (max - min); 

    *randomed = min + (rand % del);
    return 0;

}


static void _hub_wdt_isr()
{
    if(TIMER->EVENTS_COMPARE[TIMER_CC_PERIOD]) {
        
        TIMER->EVENTS_COMPARE[TIMER_CC_PERIOD] = 0;

        if(++_hub.watchdog.tick_count >= 30) {

            if(_hub.watchdog.state) {
                // printk("HW_WATCHDOG_PIN\n");
                PIN_SET(HW_WATCHDOG_PIN);
            } else {
                PIN_CLR(HW_WATCHDOG_PIN);
            }
            _hub.watchdog.state = !_hub.watchdog.state;
            _hub.watchdog.tick_count = 0;
        }
        
    }
}


static void _hub_wdt_init()
{
    SYS_LOG_DBG("");
    NVIC_ClearPendingIRQ(NRF5_IRQ_TIMER2_IRQn);
    IRQ_CONNECT(NRF5_IRQ_TIMER2_IRQn, 1, _hub_wdt_isr, 0, 0);
    irq_enable(NRF5_IRQ_TIMER2_IRQn);

    NRF_GPIO->DIRSET = BIT(HW_WATCHDOG_PIN);
    PIN_CLR(HW_WATCHDOG_PIN);

    TIMER->MODE = TIMER_MODE_MODE_Timer;
    TIMER->PRESCALER = 8;
    TIMER->BITMODE = TIMER_BITMODE_BITMODE_16Bit;
    TIMER->INTENSET = BIT(TIMER_INTENSET_COMPARE0_Pos);
    TIMER->SHORTS = TIMER_SHORTS_COMPARE0_CLEAR_Msk;
    TIMER->CC[TIMER_CC_PERIOD] = TIMER_PERIOD;
    
    TIMER->EVENTS_COMPARE[TIMER_CC_PERIOD] = 0; 

    TIMER->TASKS_STOP = 1;
    TIMER->TASKS_START = 1;

    _hub.watchdog.state = 0;
    _hub.watchdog.tick_count = 0;
}
