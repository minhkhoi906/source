#ifndef HUB_H
#define HUB_H
#include <zephyr.h>

#define HUB_LINE_STATUS_NORMAL 0
#define HUB_LINE_STATUS_DISABLED 1
#define HUB_LINE_STATUS_GPIO_FAILED 2
#define HUB_LINE_STATUS_LED_FAILED 3
#define HUB_LINE_STATUS_DRIVER_FAILED 4
#define HUB_LINE_STATUS_UNDEFINED 5
#define HUB_LINES 8

typedef void (*hub_level_report)(const u8_t *level, u8_t lines);
typedef void (*hub_status_report)(const u8_t *status, u8_t lines);
typedef void (*hub_warning_report)(const u8_t *status, u8_t lines);

struct _hub_line;

int hub_init(hub_level_report level,
             hub_status_report status,
             hub_warning_report warning);

void hub_level_set(u16_t level, bool ack);

void hub_enables_set(u32_t enables);

void hub_pwm_scale_set(u8_t linesmap, u8_t off_0, u8_t min, u8_t max);

void hub_status_config_set(u8_t status_en, u8_t warn_en, u16_t check_period, u16_t nority_period);

int hub_status_store_set(u8_t store);

void hub_groups_add(u8_t linesmap, u16_t group);
void hub_groups_del(u8_t linesmap, u16_t group);
void hub_groups_clr(u8_t linesmap, u8_t lines);

void hub_report_activate(u32_t min_duration, u32_t max_duration);
void hub_report_period_set(u16_t period);

struct _hub_line *hub_line_init(u8_t line);

void hub_line_level_set(struct _hub_line *line, u16_t level, bool ack);

void hub_line_level_get(struct _hub_line *line, u16_t timeout);

void hub_setting_get(u8_t **data, u16_t *len);

int hub_setting_set(const u8_t *data, u16_t len);

void hub_release();

#endif // HUB_H
