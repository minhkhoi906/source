#ifndef MESH_H
#define MESH_H

#include <zephyr.h>

#define CID_VNG 0x00FF
#define CONTROL_GROUP_ADDR 0xC000
#define STATUS_CONTROL_GROUP_ADDR  0xD000
#define STATUS_DESTINATION_GROUP_ADDR  0xE000
#define WARNING_DESTINATION_GROUP_ADDR  0xC000
#define HUB_GROUP_ADDR 0xC001
#define VNG_MESH_RENEW_GROUP 0xC002
#define VNG_MESH_THREAD_TRACKING_GROUP 0xC003
#define VNG_MESH_UNUSED_GROUP 0xCFFF

#define BT_MESH_MODEL_OP_GROUPS_COUNT  BT_MESH_MODEL_OP_2(0x90, 0x20)
#define BT_MESH_MODEL_OP_GROUPS_LIST   BT_MESH_MODEL_OP_2(0x90, 0x21)
#define BT_MESH_MODEL_OP_LINE_ENABLES  BT_MESH_MODEL_OP_2(0x90, 0x22)
#define BT_MESH_MODEL_OP_VND_STATUS    BT_MESH_MODEL_OP_2(0x82, 0x07)
#define BT_MESH_MODEL_OP_VND_LEVELS    BT_MESH_MODEL_OP_2(0x82, 0x08)
#define BT_MESH_MODEL_OP_VND_WARNING   BT_MESH_MODEL_OP_2(0x82, 0x09)

#define MODELS_BIND(models, is_vnd, net_idx, app_idx) \
do { \
    size_t size = ARRAY_SIZE(models);\
    size_t idx; \
    u16_t addr, primary_addr; \
    primary_addr = bt_mesh_primary_addr();\
    for(idx = 0; idx < size; idx++) { \
        addr = models[idx].elem_idx + primary_addr;\
        if(is_vnd) {\
            SYS_LOG_DBG("vnd bind addr [0x%04x]", addr);\
            bt_mesh_cfg_mod_app_bind_vnd(net_idx, primary_addr, addr, app_idx, models[idx].vnd.id, \
                models[idx].vnd.company, NULL);\
        } else {\
            SYS_LOG_DBG("bind addr [0x%04x]", addr);\
            bt_mesh_cfg_mod_app_bind(net_idx, primary_addr, addr,\
            app_idx, models[idx].id, NULL);\
        }\
    } \
} while(0)



#define MODEL_PUB(model, is_vnd, net_idx, pub)\
{\
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    addr = model.elem_idx + primary_addr;\
    if(is_vnd) {\
        SYS_LOG_DBG("vnd pub addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_pub_set_vnd(net_idx, primary_addr, addr,\
            model.vnd.id, model.vnd.company, pub, NULL);\
    } else {\
        SYS_LOG_DBG("sig pub addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_pub_set(net_idx, primary_addr, addr,\
            model.id, pub, NULL);\
    }\
}

#define MODEL_SUB_ADD(model, is_vnd, net_idx, subaddr)\
{\
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    addr = model.elem_idx + primary_addr;\
    if(is_vnd) {\
        SYS_LOG_DBG("vnd sub addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_sub_add_vnd(net_idx, primary_addr, addr,\
                subaddr, model.vnd.id, CID_VNG, NULL);\
    } else {\
        SYS_LOG_DBG("sig sub addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_sub_add(net_idx, primary_addr, addr,\
                subaddr, model.id, NULL);\
    }\
}

#define MODEL_SUB_OVERRIDE(model, is_vnd, net_idx, subaddr)\
{\
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    addr = model.elem_idx + primary_addr;\
    if(is_vnd) {\
        SYS_LOG_DBG("sig vnd sub addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_sub_overwrite_vnd(net_idx, primary_addr, addr,\
                subaddr, model.vnd.id, CID_VNG, NULL);\
    } else {\
        SYS_LOG_DBG("sig sub addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_sub_overwrite(net_idx, primary_addr, addr,\
                subaddr, model.id, NULL);\
    }\
}


#define MODEL_SUB_DEL(model, is_vnd, net_idx, subaddr)\
{\
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    addr = model.elem_idx + primary_addr;\
    if(is_vnd) {\
        SYS_LOG_DBG("sig vnd sub addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_sub_del_vnd(net_idx, primary_addr, addr,\
                subaddr, model.vnd.id, CID_VNG, NULL);\
    } else {\
        SYS_LOG_DBG("sig sub addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_sub_del(net_idx, primary_addr, addr,\
                subaddr, model.id, NULL);\
    }\
}




int mesh_init();

int mesh_hub_init();

int mesh_hub_line_group_sub_add(u8_t line, u16_t group);
int mesh_hub_line_group_sub_del(u8_t line, u16_t group);
int mesh_hub_line_group_sub_clr(u8_t line);

void mesh_hub_setting_store();

#endif // MESH_H
