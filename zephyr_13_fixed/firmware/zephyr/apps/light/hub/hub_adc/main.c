#include "board/mesh.h"
#include "board/hub.h"
#include <bluetooth/bluetooth.h>
#include <bluetooth/mesh.h>
#include <kernel.h>
#define SYS_LOG_DOMAIN "app"
#include "common/log.h"
#include <logging/sys_log.h>

static void _bt_ready(int err)
{

    if (err) {
        SYS_LOG_ERR("Bluetooth init failed (err %d)", err);
        return;
    }

    SYS_LOG_INF("Bluetooth initialized");
    mesh_init();
}

void main()
{
    int err;

    SYS_LOG_INF("Firmware ver 3.0");
    /* Initialize the Mesh Network*/
    err = mesh_hub_init();

    if (err != 0)
        return;

    /* Initialize the Bluetooth Subsystem */

    err = bt_enable(_bt_ready);

    if (err) {
        SYS_LOG_ERR("Bluetooth init failed (err %d)", err);
        return;
    }

#ifdef CONFIG_BT_MESH_VNG_THREAD_TRACKING
    bt_mesh_vng_thread_tracking_main_terminate();
#endif
}

