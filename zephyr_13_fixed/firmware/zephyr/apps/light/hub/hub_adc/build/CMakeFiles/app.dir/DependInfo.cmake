# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/quanghm/Project/iot-device/firmware/zephyr/apps/light/hub/hub_adc/board/hub.c" "/home/quanghm/Project/iot-device/firmware/zephyr/apps/light/hub/hub_adc/build/CMakeFiles/app.dir/board/hub.c.obj"
  "/home/quanghm/Project/iot-device/firmware/zephyr/apps/light/hub/hub_adc/board/mesh.c" "/home/quanghm/Project/iot-device/firmware/zephyr/apps/light/hub/hub_adc/build/CMakeFiles/app.dir/board/mesh.c.obj"
  "/home/quanghm/Project/iot-device/firmware/zephyr/apps/light/hub/hub_adc/main.c" "/home/quanghm/Project/iot-device/firmware/zephyr/apps/light/hub/hub_adc/build/CMakeFiles/app.dir/main.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "BUILD_VERSION=v1.0-154-gca87a283"
  "KERNEL"
  "NRF52832_XXAA"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/kernel/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/arch/arm/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/arch/arm/soc/nordic_nrf/nrf52"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/arch/arm/soc/nordic_nrf/nrf52/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/arch/arm/soc/nordic_nrf/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/boards/arm/nrf52_vng_light_hub_dim"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/include/drivers"
  "zephyr/include/generated"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/lib/libc/minimal/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/lib/crypto/tinycrypt/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/hal/cmsis/Include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/hal/nordic/nrfx"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/hal/nordic/nrfx/drivers/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/hal/nordic/nrfx/hal"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/hal/nordic/nrfx/mdk"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/hal/nordic/."
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/debug/segger/."
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/subsys/settings/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/subsys/bluetooth"
  "../src"
  "../src/mesh"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/subsys/bluetooth/controller/hal"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include-fixed"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
