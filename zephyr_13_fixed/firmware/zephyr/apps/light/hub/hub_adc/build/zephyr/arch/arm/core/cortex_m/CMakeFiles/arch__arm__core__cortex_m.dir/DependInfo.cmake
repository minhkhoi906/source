# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "ASM"
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_ASM
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/arch/arm/core/cortex_m/nmi_on_reset.S" "/home/quanghm/Project/iot-device/firmware/zephyr/apps/light/hub/hub_adc/build/zephyr/arch/arm/core/cortex_m/CMakeFiles/arch__arm__core__cortex_m.dir/nmi_on_reset.S.obj"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/arch/arm/core/cortex_m/reset.S" "/home/quanghm/Project/iot-device/firmware/zephyr/apps/light/hub/hub_adc/build/zephyr/arch/arm/core/cortex_m/CMakeFiles/arch__arm__core__cortex_m.dir/reset.S.obj"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/arch/arm/core/cortex_m/vector_table.S" "/home/quanghm/Project/iot-device/firmware/zephyr/apps/light/hub/hub_adc/build/zephyr/arch/arm/core/cortex_m/CMakeFiles/arch__arm__core__cortex_m.dir/vector_table.S.obj"
  )
set(CMAKE_ASM_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_ASM
  "BUILD_VERSION=v1.0-154-gca87a283"
  "KERNEL"
  "NRF52832_XXAA"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR_SUPERVISOR__"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_ASM_TARGET_INCLUDE_PATH
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/kernel/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/arch/arm/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/arch/arm/soc/nordic_nrf/nrf52"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/arch/arm/soc/nordic_nrf/nrf52/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/arch/arm/soc/nordic_nrf/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/boards/arm/nrf52_vng_light_hub_dim"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/include/drivers"
  "zephyr/include/generated"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/lib/libc/minimal/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/lib/crypto/tinycrypt/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/hal/cmsis/Include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/hal/nordic/nrfx"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/hal/nordic/nrfx/drivers/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/hal/nordic/nrfx/hal"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/hal/nordic/nrfx/mdk"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/hal/nordic/."
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/debug/segger/."
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/subsys/settings/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/subsys/bluetooth"
  "/home/quanghm/Project/iot-device/firmware/zephyr/apps/light/hub/hub_adc/src"
  "/home/quanghm/Project/iot-device/firmware/zephyr/apps/light/hub/hub_adc/src/mesh"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/subsys/bluetooth/controller/hal"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include-fixed"
  )
set(CMAKE_DEPENDS_CHECK_C
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/arch/arm/core/cortex_m/exc_manage.c" "/home/quanghm/Project/iot-device/firmware/zephyr/apps/light/hub/hub_adc/build/zephyr/arch/arm/core/cortex_m/CMakeFiles/arch__arm__core__cortex_m.dir/exc_manage.c.obj"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/arch/arm/core/cortex_m/nmi.c" "/home/quanghm/Project/iot-device/firmware/zephyr/apps/light/hub/hub_adc/build/zephyr/arch/arm/core/cortex_m/CMakeFiles/arch__arm__core__cortex_m.dir/nmi.c.obj"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/arch/arm/core/cortex_m/prep_c.c" "/home/quanghm/Project/iot-device/firmware/zephyr/apps/light/hub/hub_adc/build/zephyr/arch/arm/core/cortex_m/CMakeFiles/arch__arm__core__cortex_m.dir/prep_c.c.obj"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/arch/arm/core/cortex_m/scb.c" "/home/quanghm/Project/iot-device/firmware/zephyr/apps/light/hub/hub_adc/build/zephyr/arch/arm/core/cortex_m/CMakeFiles/arch__arm__core__cortex_m.dir/scb.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "BUILD_VERSION=v1.0-154-gca87a283"
  "KERNEL"
  "NRF52832_XXAA"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR_SUPERVISOR__"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/kernel/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/arch/arm/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/arch/arm/soc/nordic_nrf/nrf52"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/arch/arm/soc/nordic_nrf/nrf52/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/arch/arm/soc/nordic_nrf/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/boards/arm/nrf52_vng_light_hub_dim"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/include/drivers"
  "zephyr/include/generated"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/lib/libc/minimal/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/lib/crypto/tinycrypt/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/hal/cmsis/Include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/hal/nordic/nrfx"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/hal/nordic/nrfx/drivers/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/hal/nordic/nrfx/hal"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/hal/nordic/nrfx/mdk"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/hal/nordic/."
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/ext/debug/segger/."
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/subsys/settings/include"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/subsys/bluetooth"
  "/home/quanghm/Project/iot-device/firmware/zephyr/apps/light/hub/hub_adc/src"
  "/home/quanghm/Project/iot-device/firmware/zephyr/apps/light/hub/hub_adc/src/mesh"
  "/home/quanghm/Project/iot-device/firmware/zephyr/os/v1.13.0/subsys/bluetooth/controller/hal"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include-fixed"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
