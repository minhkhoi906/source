/* microbit.c - BBC micro:bit specific hooks */

/*
 * Copyright (c) 2017 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <gpio.h>
#include <board.h>
#include <soc.h>
#include <misc/printk.h>
#include <ctype.h>
#include <flash.h>
#include <gpio.h>
#include <pwm.h>

#include <display/mb_display.h>

#include <bluetooth/mesh.h>

#include "board.h"

#define SCROLL_SPEED   K_MSEC(300)

#define BUZZER_PIN     EXT_P0_GPIO_PIN
#define BEEP_DURATION  K_MSEC(60)

#define SEQ_PER_BIT  976
#define SEQ_PAGE     (NRF_FICR->CODEPAGESIZE * (NRF_FICR->CODESIZE - 1))
#define SEQ_MAX      (NRF_FICR->CODEPAGESIZE * 8 * SEQ_PER_BIT)
#define LED	LED0_GPIO_PIN
#define PORT	LED0_GPIO_PORT

static struct device *_gpio;
static struct device *nvm;
static struct device *led;

static struct k_work button_work;

void board_seq_update(u32_t seq)
{
	u32_t loc, seq_map;
	int err;

	if (seq % SEQ_PER_BIT) {
		return;
	}

	loc = (SEQ_PAGE + ((seq / SEQ_PER_BIT) / 32));

	err = flash_read(nvm, loc, &seq_map, sizeof(seq_map));
	if (err) {
		printk("flash_read err %d\n", err);
		return;
	}

	seq_map >>= 1;

	flash_write_protection_set(nvm, false);
	err = flash_write(nvm, loc, &seq_map, sizeof(seq_map));
	flash_write_protection_set(nvm, true);
	if (err) {
		printk("flash_write err %d\n", err);
	}
}

static u32_t get_seq(void)
{
	u32_t seq_map, seq = 0;
	int err, i;

	for (i = 0; i < NRF_FICR->CODEPAGESIZE / sizeof(seq_map); i++) {
		err = flash_read(nvm, SEQ_PAGE + (i * sizeof(seq_map)),
				 &seq_map, sizeof(seq_map));
		if (err) {
			printk("flash_read err %d\n", err);
			return seq;
		}

		printk("seq_map 0x%08x\n", seq_map);

		if (seq_map) {
			seq = ((i * 32) +
			       (32 - popcount(seq_map))) * SEQ_PER_BIT;
			if (!seq) {
				return 0;
			}

			break;
		}
	}

	seq += SEQ_PER_BIT;
	if (seq >= SEQ_MAX) {
		seq = 0;
	}

	if (seq) {
		seq_map >>= 1;
		flash_write_protection_set(nvm, false);
		err = flash_write(nvm, SEQ_PAGE + (i * sizeof(seq_map)),
				  &seq_map, sizeof(seq_map));
		flash_write_protection_set(nvm, true);
		if (err) {
			printk("flash_write err %d\n", err);
		}
	} else {
		printk("Performing flash erase of page 0x%08x\n", SEQ_PAGE);
		err = flash_erase(nvm, SEQ_PAGE, NRF_FICR->CODEPAGESIZE);
		if (err) {
			printk("flash_erase err %d\n", err);
		}
	}

	return seq;
}

static void button_send_pressed(struct k_work *work)
{
	printk("button_send_pressed()\n");
    board_button_pressed();
}

static void button_pressed(struct device *dev, struct gpio_callback *cb,
			   uint32_t pins)
{
	if (pins & BIT(SW0_GPIO_PIN)) {
		k_work_submit(&button_work);
	} else {
	}
}


void board_play_tune(const char *str)
{
}

void board_heartbeat(u8_t hops, u16_t feat)
{
}

void board_other_dev_pressed(u16_t addr)
{
	printk("board_other_dev_pressed(0x%04x)\n", addr);
    static int count = 0;
    gpio_pin_write(led, LED, count % 2);
    count++;
}

void board_attention(bool attention)
{
}

static void configure_button(void)
{
	static struct gpio_callback button_cb;

	k_work_init(&button_work, button_send_pressed);

	_gpio = device_get_binding(SW0_GPIO_NAME);

	gpio_pin_configure(_gpio, SW0_GPIO_PIN,
			   (GPIO_DIR_IN | GPIO_INT | GPIO_INT_EDGE |
			    GPIO_INT_ACTIVE_LOW));

    gpio_init_callback(&button_cb, button_pressed, BIT(SW0_GPIO_PIN) );
	gpio_add_callback(_gpio, &button_cb);

	gpio_pin_enable_callback(_gpio, SW0_GPIO_PIN);
}

static void configure_led(void)
{
    led = device_get_binding(PORT);
    /* Set LED pin as output */
    gpio_pin_configure(led, LED, GPIO_DIR_OUT);
}


void board_init(u16_t *addr, u32_t *seq)
{
	printk("SEQ_PAGE 0x%08x\n", SEQ_PAGE);

	nvm = device_get_binding(CONFIG_SOC_FLASH_NRF5_DEV_NAME);

    *addr = NRF_UICR->CUSTOMER[0];
	if (!*addr || *addr == 0xffff) {
#if defined(NODE_ADDR)
		*addr = NODE_ADDR;
#else
        *addr = 0x0b0c;
#endif
	}

	*seq = get_seq();

	configure_button();
    configure_led();
}
