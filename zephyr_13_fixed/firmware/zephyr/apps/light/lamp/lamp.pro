TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    src/main.c \
    src/vng_light_lamp.c

SUBDIRS += \
    lamp.pro

DISTFILES += \
    sample.yaml \
    prj_nrf52_vng_light_lamp.conf \
    CMakeLists.txt

HEADERS += \
    src/board.h
