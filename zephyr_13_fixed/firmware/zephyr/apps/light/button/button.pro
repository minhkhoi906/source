TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += \
    ../../../os/v1.11.0/include \
    ../../../os/v1.11.0/subsys/bluetooth
SOURCES += \
    src/main.c \
    src/nrf52_vng_light_button.c

HEADERS += \
    src/board.h

DISTFILES += \
    prj_nrf52_vng_light_button.conf \
    CMakeLists.txt
