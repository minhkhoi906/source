/* microbit.c - BBC micro:bit specific hooks */

/*
 * Copyright (c) 2017 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <board.h>
#include <gpio.h>
#include <misc/util.h>

static struct k_work button_work;
struct device *_gpio;
extern void board_button_pressed(void);

static void _button_send_pressed(struct k_work *work)
{
    board_button_pressed();
}

static void button_pressed(struct device *dev, struct gpio_callback *cb,
               uint32_t pins)
{
    if (pins & BIT(BUT_GPIO_PIN)) {
        k_work_submit(&button_work);
    } else {
    }
}


void board_init()
{
    static struct gpio_callback button_cb;

    k_work_init(&button_work, _button_send_pressed);

    _gpio = device_get_binding(BUT_GPIO_NAME);

    gpio_pin_configure(_gpio, BUT_GPIO_PIN,
               (GPIO_DIR_IN | GPIO_INT | GPIO_INT_EDGE |
                GPIO_INT_ACTIVE_LOW));

    gpio_init_callback(&button_cb, button_pressed, BIT(SW0_GPIO_PIN) );
    gpio_add_callback(_gpio, &button_cb);

    gpio_pin_enable_callback(_gpio, SW0_GPIO_PIN);
}

