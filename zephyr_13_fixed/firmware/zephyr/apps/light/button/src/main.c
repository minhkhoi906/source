/*
 * Copyright (c) 2016 Nordic Semiconductor ASA
 * Copyright (c) 2015-2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include "common/log.h"
#include <logging/sys_log.h>

#include <device.h>
#include <uart.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/mesh.h>
#include <bluetooth/mesh.h>
#include <crc16.h>
#include <misc/byteorder.h>

extern void board_init();

struct k_work store_work;
struct k_work restore_work;

#define CID_VNG_GATEWAY 0x00FF
#define MOD_VNG 0x0000
#define VNG_GATEWAY_GROUP_ADDR 0xC000
#define OP_VENDOR_EVENT BT_MESH_MODEL_OP_3(0x00, CID_VNG_GATEWAY)
#define BUTTON 0x2100
struct k_work store_work;


static const u8_t app_key[16] = {
    0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
    0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
};

static struct bt_mesh_cfg_srv _cfg_srv = {
    .relay = BT_MESH_RELAY_DISABLED,
    .beacon = BT_MESH_BEACON_ENABLED,
#if defined(CONFIG_BT_MESH_FRIEND)
    .frnd = BT_MESH_FRIEND_ENABLED,
#else
    .frnd = BT_MESH_FRIEND_NOT_SUPPORTED,
#endif
#if defined(CONFIG_BT_MESH_GATT_PROXY)
    .gatt_proxy = BT_MESH_GATT_PROXY_ENABLED,
#else
    .gatt_proxy = BT_MESH_GATT_PROXY_NOT_SUPPORTED,
#endif
    .default_ttl = 7,

    /* 3 transmissions with 20ms interval */
    .net_transmit = BT_MESH_TRANSMIT(2, 20),
    .relay_retransmit = BT_MESH_TRANSMIT(2, 20),
};

static struct bt_mesh_health_srv health_srv = {};

BT_MESH_HEALTH_PUB_DEFINE(health_pub, 0);

static struct bt_mesh_model root_models[] = {
    BT_MESH_MODEL_CFG_SRV(&_cfg_srv),
    BT_MESH_MODEL_HEALTH_SRV(&health_srv, &health_pub)
};

static void vnd_event(struct bt_mesh_model *model,
                       struct bt_mesh_msg_ctx *ctx,
                       struct net_buf_simple *raw)
{
    SYS_LOG_INF("");
}

static const struct bt_mesh_model_op vnd_ops[] = {
{ OP_VENDOR_EVENT, 0, vnd_event },
BT_MESH_MODEL_OP_END};

BT_MESH_MODEL_PUB_DEFINE(vng_pub, NULL, 2 + 2);

static struct bt_mesh_model vnd_models[] = {
    BT_MESH_MODEL_VND(CID_VNG_GATEWAY, MOD_VNG, vnd_ops, &vng_pub, NULL),
};

static struct bt_mesh_elem _elements[] = {
    BT_MESH_ELEM(0, root_models, vnd_models),
};

static const struct bt_mesh_comp comp = {
    .cid = CID_VNG_GATEWAY,
    .elem = _elements,
    .elem_count = ARRAY_SIZE(_elements),
};

static uint8_t uuid[16] = {0};

static void prov_complete(u16_t net_idx, u16_t addr, bool restored)
{
    u8_t err;
//    SYS_LOG_INF("net index: 0x%04x, address: 0x%04x", net_idx, addr);

    if (restored != true)
        k_work_submit(&store_work);

    err = bt_mesh_srv_app_key_set(net_idx, 0, app_key, false);

    if (err != 0) {
        SYS_LOG_ERR("set app key failed with status: 0x%02X", err);
        return;
    }

   err = bt_mesh_srv_moddel_pub_set(&vnd_models[0],
                                    VNG_GATEWAY_GROUP_ADDR,
                                    0, 0, 7, 0, 0);


    if (err != 0) {
        SYS_LOG_ERR("set publish address failed with status: 0x%02X", err);
        return;
    }
}

static void prov_reset(void)
{
    bt_mesh_prov_enable(BT_MESH_PROV_ADV | BT_MESH_PROV_GATT);
}



static struct bt_mesh_prov prov = {
    .uuid = uuid,
    .output_size = 0,
    .complete = prov_complete,
    .reset = prov_reset

};



static void bt_ready(int err)
{

    struct bt_le_oob oob;

    if (err) {
        SYS_LOG_ERR("Bluetooth init failed (err %d)", err);
        return;
    }

    SYS_LOG_INF("Bluetooth initialized");

    err = bt_le_oob_get_local(&oob);

    if (err != 0) {
        SYS_LOG_ERR("get local oob failed with error: %d", err);
        return;
    }

    memset(uuid, 0, sizeof(uuid));
    memcpy(uuid, oob.addr.a.val, 6);

    err = bt_mesh_init(&prov, &comp);

    if (err) {
        SYS_LOG_ERR("Initializing mesh failed (err %d)", err);
        return;
    }

    k_work_submit(&restore_work);
    SYS_LOG_INF("Submit restore");
}

void board_button_pressed(void)
{
    int err;
    struct net_buf_simple *msg = vng_pub.msg;
    struct bt_mesh_model *model = &vnd_models[0];

    SYS_LOG_INF("pressed");

    bt_mesh_model_msg_init(msg, OP_VENDOR_EVENT);
    net_buf_simple_add_be16(msg, BUTTON);
    err = bt_mesh_model_publish(model);
    if (err) {
        SYS_LOG_ERR("bt_mesh_model_publish err %d", err);
    }

}

static void store(struct k_work *work)
{
    bt_mesh_store(true);
}

static void restore(struct k_work *work)
{

    int err = bt_mesh_restore();

    if (err == 0) {
        return;
    }

    bt_mesh_prov_enable(BT_MESH_PROV_ADV | BT_MESH_PROV_GATT);
    SYS_LOG_INF("Provisioning enabled");
}

void main(void)
{
    int err;

    BT_DBG("Start");

    board_init();

    k_work_init(&store_work, store);
    k_work_init(&restore_work, restore);

    /* Initialize the Bluetooth Subsystem */
    err = bt_enable(bt_ready);

    if (err) {
        printk("Bluetooth init failed (err %d)\n", err);
        return;
    }
}
