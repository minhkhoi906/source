#include "mesh/mesh.h"
#include "board/dim.h"
#include <bluetooth/bluetooth.h>
#include <bluetooth/mesh.h>
#include <kernel.h>
#define SYS_LOG_DOMAIN "app"
#include "common/log.h"
#include <logging/sys_log.h>


void main()
{
    int err;

    /* Initialize the Bluetooth Subsystem */

    err = bt_enable(bt_ready);

    if (err) {
        SYS_LOG_ERR("Bluetooth init failed (err %d)", err);
        return;
    }
    status_init();


    err = dim_init();
   if (err) {
        SYS_LOG_ERR("Dim init failed (err %d)", err);
        return;
    }

#ifdef CONFIG_BT_MESH_VNG_THREAD_TRACKING
    bt_mesh_vng_thread_tracking_main_terminate();
#endif
}

