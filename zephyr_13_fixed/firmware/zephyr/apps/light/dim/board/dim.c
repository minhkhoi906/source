#include "dim.h"
#include "../mesh/mesh.h"
#include <board.h>

#include <adc.h>
#include <entropy.h>
#include <errno.h>
#include <gpio.h>
#include <pwm.h>
#define SYS_LOG_DOMAIN "app"
#include <common/log.h>
#include <logging/sys_log.h>
#include <misc/util.h>
#include <bluetooth/mesh.h>

#define DIM_WATCHDOG_PERIOD 400
#define PWM_PERIOD     1000
#define PWM_LEVEL_MAX 0x0000FFFF
  
#define TIMER_PERIOD 1000
#define TIMER NRF_TIMER2
#define TIMER_CC_PERIOD 0

#define PIN_SET(pin) \
    NRF_GPIO->OUTCLR = BIT(pin)
#define PIN_CLR(pin) \
    NRF_GPIO->OUTSET = BIT(pin)

struct _dim {
    u16_t level;
    struct device *pwm;
    u32_t pwm_pin;
};


struct _watchdog {
    volatile u32_t tick_count;
    u32_t state;
};

struct _dim _light_dim = {
    .level = 0,
    .pwm_pin = PWM_PIN,
};
struct _watchdog _dim_watchdog = {
    .tick_count = 0,
    .state = 0,
}; 

struct device *_gpio;
struct device* _entropy;

static void _dim_wdt_init();
static void _dim_wdt_isr();

int dim_init()
{

    _light_dim.pwm = device_get_binding(CONFIG_PWM_NRF52_HW_0_DEV_NAME);

    if (_light_dim.pwm == NULL) {
        SYS_LOG_ERR("Setup pwm device failed");
        return -ENODEV;
    }

    _gpio = device_get_binding(CONFIG_GPIO_P0_DEV_NAME);

    if (_gpio == NULL) {
        SYS_LOG_ERR("Setup GPIO device failed");
        return -ENODEV;
    }

    _entropy = device_get_binding(CONFIG_ENTROPY_NAME);
    if(_entropy == NULL) {
        SYS_LOG_ERR("Setup entropy device failed");
        return -ENODEV;
    }

    SYS_LOG_INF("Configured enable pin");

    gpio_pin_configure(_gpio, SW_ENABLE_PIN, GPIO_DIR_OUT);
    gpio_pin_write(_gpio, SW_ENABLE_PIN, 1);

    SYS_LOG_INF("Configured bypass pin");

    gpio_pin_configure(_gpio, PIN_BYPASS, GPIO_DIR_OUT | GPIO_PUD_PULL_DOWN);
    gpio_pin_write(_gpio, PIN_BYPASS, 0);


    SYS_LOG_INF("Configured hardware watchdog");
    _dim_wdt_init();

    return 0;
}

void dim_level_set(u16_t level, bool ack)
{
        _light_dim.level = level;
        u32_t pulse = (level * PWM_PERIOD) / PWM_LEVEL_MAX;
        SYS_LOG_INF(" pwm pin %d, level: %u, pulse: %u", _light_dim.pwm_pin, level, pulse);
        pwm_pin_set_usec(_light_dim.pwm, _light_dim.pwm_pin, PWM_PERIOD, pulse);
}


void dim_level_get( u16_t timeout)
{
    // 
}

static void _dim_wdt_isr()
{
    if(TIMER->EVENTS_COMPARE[TIMER_CC_PERIOD]) {
        
        TIMER->EVENTS_COMPARE[TIMER_CC_PERIOD] = 0;

        if(++_dim_watchdog.tick_count >= 30) {

            if(_dim_watchdog.state) {
                PIN_SET(HW_WATCHDOG_PIN);
            } else {
                PIN_CLR(HW_WATCHDOG_PIN);
            }
            _dim_watchdog.state = !_dim_watchdog.state;
            _dim_watchdog.tick_count = 0;
        }
        
    }
}


static void _dim_wdt_init()
{
    SYS_LOG_DBG("");
    NVIC_ClearPendingIRQ(NRF5_IRQ_TIMER2_IRQn);
    IRQ_CONNECT(NRF5_IRQ_TIMER2_IRQn, 1, _dim_wdt_isr, 0, 0);
    irq_enable(NRF5_IRQ_TIMER2_IRQn);

    NRF_GPIO->DIRSET = BIT(HW_WATCHDOG_PIN);
    PIN_CLR(HW_WATCHDOG_PIN);

    TIMER->MODE = TIMER_MODE_MODE_Timer;
    TIMER->PRESCALER = 8;
    TIMER->BITMODE = TIMER_BITMODE_BITMODE_16Bit;
    TIMER->INTENSET = BIT(TIMER_INTENSET_COMPARE0_Pos);
    TIMER->SHORTS = TIMER_SHORTS_COMPARE0_CLEAR_Msk;
    TIMER->CC[TIMER_CC_PERIOD] = TIMER_PERIOD;
    
    TIMER->EVENTS_COMPARE[TIMER_CC_PERIOD] = 0; 

    TIMER->TASKS_STOP = 1;
    TIMER->TASKS_START = 1;

    _dim_watchdog.state = 0;
    _dim_watchdog.tick_count = 0;
}
