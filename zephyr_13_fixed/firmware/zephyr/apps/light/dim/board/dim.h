#ifndef HUB_H
#define HUB_H
#include <zephyr.h>

struct _dim_line;

int dim_init();

void dim_level_set(u16_t level, bool ack); 

void dim_level_get( u16_t timeout); 


#endif // HUB_H
