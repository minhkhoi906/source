cmake_minimum_required(VERSION 3.8.2)
set(QEMU_EXTRA_FLAGS -s)

set(BOARD nrf52_vng_light_dim)

include($ENV{ZEPHYR_BASE}/cmake/app/boilerplate.cmake NO_POLICY_SCOPE)
project(NONE)

target_link_libraries(app PUBLIC subsys__bluetooth)

target_sources(app PRIVATE
           main.c
           board/board.c
           board/dim.c
           mesh/mesh.c
           )

zephyr_include_directories(
	/
    mesh/
	$ENV{ZEPHYR_BASE}/subsys/bluetooth/controller/hal
)

# zephyr_compile_definitions(-DTID_MODEL_OLD)
