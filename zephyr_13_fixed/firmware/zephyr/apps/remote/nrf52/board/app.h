

#ifndef APP_H
#define APP_H
#include <i2c.h>
#include <gpio.h>

#define THREADS_MONITOR_MAIN_START 	  threads_monitor_main_start();
#define THREADS_MONITOR_MAIN_TERMINAL threads_monitor_main_termial();
/*
	should be called at start of main function.
*/

//config I2C
#define SI7021_DEFAULT_ADDRESS	0x40

#define SI7021_MEASRH_HOLD_CMD           0xE5
#define SI7021_MEASRH_NOHOLD_CMD         0xF5 
#define SI7021_MEASTEMP_HOLD_CMD         0xE3 
#define SI7021_MEASTEMP_NOHOLD_CMD       0xF3 
#define SI7021_READPREVTEMP_CMD          0xE0 
#define SI7021_RESET_CMD                 0xFE
#define SI7021_WRITERHT_REG_CMD          0xE6 
#define SI7021_READRHT_REG_CMD           0xE7 
#define SI7021_WRITEHEATER_REG_CMD       0x51 
#define SI7021_READHEATER_REG_CMD        0x11 
#define SI7021_ID1_CMD                   0xFA0F 
#define SI7021_ID2_CMD                   0xFCC9 
#define SI7021_FIRMVERS_CMD              0x84B8 


struct device* i2c_dev;

void threads_monitor_main_start();

/*
	should be called at end of main function.
*/
void threads_monitor_main_termial();

int rand_number_blocking_generate(u32_t min, u32_t max, u32_t* rn);

#endif