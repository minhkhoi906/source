#include <device.h>
#include <gpio.h>
#include <board.h>
 
#include <bluetooth/bluetooth.h>
#include <bluetooth/mesh.h>
#include <crc16.h>
#include <misc/byteorder.h>
#include <watchdog.h>
#include "common/log.h"
#define SYS_LOG_DOMAIN "app"
 
#include <logging/sys_log.h>
#include <irsend.h>
 
#include <host/mesh/net.h>
#include <host/mesh/adv.h>
#include <host/mesh/access.h>
#include <settings/settings.h>
 
#include "flash_uicr.h"
 
#define TYPE_STATUS 0x01
#define DEVICE_ID 0x0048
#define CID_VNG 0x00FF
#define GATEWAY_GROUP_ADDR 0xC000
#define STATUS_GROUP_ADDR  0xD000
#define STATUS_DST_GROUP_ADDR  0xE000
#define MOD_VNG_SENSOR 0x0005
#define BT_MESH_MODEL_OP_VNG_SENSOR         BT_MESH_MODEL_OP_2(0x90, 0x10)
// #define BT_MESH_MODEL_OP_VNG_STATUS_SRV     BT_MESH_MODEL_OP_3(0x04, CID_VNG)
#define BT_MESH_MODEL_OP_VNG_SENSOR_CONFIG  BT_MESH_MODEL_OP_2(0x90, 0x11)
#define BT_MESH_MODEL_OP_VNG                BT_MESH_MODEL_OP_3(0x04, CID_VNG)
#define VNG_MESH_RENEW_GROUP 0xC002

#define STATUS_INTERVAL_MIN 30
#define STATUS_INTERVAL_MAX 300
#define STATUS_RANDOM_RANGE  10
#define BT_SETTINGS_SIZE(in_size) ((((((in_size) - 1) / 3) * 4) + 4) + 1)
#define BUF_IR_CONFIG   16
#define FLAG_TID    5
 
#define UART_MESH_TRANSMIT_STACK_SIZE 1024
 
#define BT_MESH_BUF_SIZE 36
#define BT_MESH_USER_DATA_MIN 4
#if defined(CONFIG_BT_MESH_BUFFERS)
#define MESH_BUF_COUNT CONFIG_BT_MESH_BUFFERS
#else
#define MESH_BUF_COUNT 12
#endif
 
#define PUB_PERIOD(seconds) ((0x01<<6 | seconds) & 0xFF)
#define MODELS_BIND(models, vnd, net_idx, app_idx) \
do { \
    size_t size = ARRAY_SIZE(models);\
    size_t idx; \
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    for(idx = 0; idx < size; idx++) { \
        addr = elems[models[idx].elem_idx].addr;\
        if(vnd) {\
            SYS_LOG_DBG("vnd bind addr [0x%04x]", addr);\
            bt_mesh_cfg_mod_app_bind_vnd(net_idx, primary_addr, addr, app_idx, models[idx].vnd.id, \
                models[idx].vnd.company, NULL);\
        } else {\
            SYS_LOG_DBG("bind addr [0x%04x]", addr);\
            bt_mesh_cfg_mod_app_bind(net_idx, primary_addr, addr,\
            app_idx, models[idx].id, NULL);\
        }\
    } \
} while(0)

#define MODEL_PUB(model, vnd, net_idx, pub)\
{\
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    addr = elems[model.elem_idx].addr;\
    if(vnd) {\
        SYS_LOG_DBG("vnd pub addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_pub_set_vnd(net_idx, primary_addr, addr,\
            model.vnd.id, model.vnd.company, pub, NULL);\
    } else {\
        SYS_LOG_DBG("pub addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_pub_set(net_idx, primary_addr, addr,\
            model.id, pub, NULL);\
    }\
}
 
NET_BUF_POOL_DEFINE(mesh_pool,
                    MESH_BUF_COUNT,
                    BT_MESH_BUF_SIZE,
                    BT_MESH_USER_DATA_MIN,
                    NULL);
 
struct value_ir {
    u8_t type;
    u8_t bits;
    uint32_t cmd;
    u16_t save_tid;
};

/* config flash setting remote */
 
static uint16_t foo_val_ir[BUF_IR_CONFIG];
 
static int foo_settings_set(int argc, char **argv, char *val)
{
    int err;
    int len = BUF_IR_CONFIG;
    if (argc == 1) {
        if (!strcmp(argv[0], "buf")) {
 
            err = settings_bytes_from_str(val, foo_val_ir, &len);
            if(err) {
                printk("parse str failed\n");
                return err;
            }
            
        }
    }
 
    return -ENOENT;
}

static void save_data()
{
   
    char buf[BT_SETTINGS_SIZE(BUF_IR_CONFIG)];
 
    char *str;
 
    printk("save buf: %s\n", bt_hex(foo_val_ir, BUF_IR_CONFIG));
    str = settings_str_from_bytes(foo_val_ir, BUF_IR_CONFIG, buf, sizeof(buf));
    if (!str) {
        printk("Unable to encode foo as value\n");
        return;
    }
    settings_save_one("foo/buf", str);
 
 
} 
 
struct settings_handler my_conf = {
    .name = "foo",
    .h_set = foo_settings_set
};

int my_settings_init(void)
{
    int err;
 
    err = settings_subsys_init();
    if (err) {
        printk("settings_subsys_init failed (err %d)\n", err);
        return err;
    }
 
    err = settings_register(&my_conf);
    if (err) {
        printk("settings_register failed (err %d)\n", err);
        return err;
    }
 
    return 0;
}

static BT_STACK_NOINIT(tx_thread_stack, UART_MESH_TRANSMIT_STACK_SIZE);
static struct k_thread tx_thread_handle;
static K_FIFO_DEFINE(tx_queue);
 
struct _mesh
{
    struct k_fifo *tx_queue;
    struct net_buf* rx_buf;
};
 
struct mesh_status
{
    struct k_delayed_work timer;
    u32_t  next_period;
    u32_t  last_status;
    u16_t  period;
};

static struct value_ir value_ir;
extern void _prov_complete(u16_t net_idx, u16_t addr);
static void prov_reset(void);
 
static void status_init();
static void status_signal(struct k_work* work);
static void status_prepare(u32_t ms_min, u32_t ms_max);
static void mesh_status_set_func(struct net_buf_simple* vng_data, void *user_data);
 
static uint8_t uuid[16] = {0xFF, 0xFF};
 
static struct bt_mesh_prov prov = {
    .uuid = uuid,
    .output_size = 0,
    .complete = _prov_complete,
    .reset = prov_reset
 
};

static struct _mesh _mesh = {
    .tx_queue = &tx_queue,
    .rx_buf = NULL,
};
 
static const u8_t app_idx_0 = 0;
 
 
 
 
static const u8_t app_key_0[16] = {
   0x4d, 0x48, 0x63, 0x43, 0x41, 0x51, 0x45, 0x45, 
   0x49, 0x4c, 0x79, 0x53, 0x4a, 0x69, 0x4c, 0x2f,
};
 
static const u8_t dev_key[16] = {
    0x55, 0x65, 0x41, 0x58, 0x64, 0x65, 0x78, 0x4d, 
    0x76, 0x74, 0x4c, 0x65, 0x56, 0x31, 0x70, 0x52, 
};
 
static const u8_t net_key[16] = {
    0x7a, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 
    0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b, 
 
};
 
static const u8_t app_idx_1 = 1;
 
static const u8_t app_key_1[16] = {
    0x02, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
    0x02, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
};

// static const u16_t net_idx = 0;
static u16_t _net_idx = 0;
static const u32_t iv_index = 0;
static u8_t flags;
 
static u16_t addr = 0x000C;//0x7F0E;
 
 
static bool setting_loading = false;
 
static struct bt_mesh_cfg_srv cfg_srv = {
    .relay = BT_MESH_RELAY_ENABLED,
    .beacon = BT_MESH_BEACON_ENABLED,
#if defined(CONFIG_BT_MESH_FRIEND)
    .frnd = BT_MESH_FRIEND_ENABLED,
#else
    .frnd = BT_MESH_FRIEND_NOT_SUPPORTED,
#endif
#if defined(CONFIG_BT_MESH_GATT_PROXY)
    .gatt_proxy = BT_MESH_GATT_PROXY_ENABLED,
#else
    .gatt_proxy = BT_MESH_GATT_PROXY_NOT_SUPPORTED,
#endif
    .default_ttl = 7,
 
    /* 3 transmissions with 20ms interval */
    .net_transmit = BT_MESH_TRANSMIT(3, 20),
    .relay_retransmit = BT_MESH_TRANSMIT(3, 20),
};

static inline bool valid_tid(u16_t old, u16_t new, u8_t lag)
{
    if(new > old)
        return true;
 
    return old - new > lag;
    
}

static void remote_config(struct bt_mesh_model *model,
                                    struct bt_mesh_msg_ctx *ctx,
                                    struct net_buf_simple *raw) {
 
    int tid  = net_buf_simple_pull_u8(raw);
 
    if(tid == 1){
        // _value_config.headermark 
        foo_val_ir[0] = net_buf_simple_pull_be16(raw);
        // _value_config.headerspace 
        foo_val_ir[1] = net_buf_simple_pull_be16(raw);
 
        // SYS_LOG_DBG("headermark :%d & headerspace :%d", foo_val_ir[0], foo_val_ir[1]);
        // _value_config.onemark
         foo_val_ir[2] = net_buf_simple_pull_be16(raw);
        // _value_config.onespace 
        foo_val_ir[3] = net_buf_simple_pull_be16(raw);
        // SYS_LOG_DBG("onemark: %d onespace : 0x%d", foo_val_ir[2], foo_val_ir[3]);
        save_data();
 
    }else  if(tid == 2){
         // _value_config.zeromark 
         foo_val_ir[4] = net_buf_simple_pull_be16(raw);
        // _value_config.zerospace 
        foo_val_ir[5] = net_buf_simple_pull_be16(raw);
        // SYS_LOG_DBG("zeromark & zerospace : %d & %d", foo_val_ir[4], foo_val_ir[5]);
        // _value_config.gapmark 
        foo_val_ir[6]= net_buf_simple_pull_be16(raw);
        // _value_config.gapspace 
        foo_val_ir[7]= net_buf_simple_pull_be16(raw);
        // SYS_LOG_DBG("gapmark & gapspace : %d & %d", foo_val_ir[6], foo_val_ir[7]);
        save_data();
 
    }
 
};

static void remote_ir(  struct bt_mesh_model *model,
             struct bt_mesh_msg_ctx *ctx,
             struct net_buf_simple *raw) {
    int freg = net_buf_simple_pull_u8(raw);
    value_ir.type = 0;
    value_ir.bits  = 0;
 
    if(freg > 0 ){
 
        uint16_t tid = net_buf_simple_pull_be16(raw);
        
      value_ir.type = net_buf_simple_pull_u8(raw);
        value_ir.bits  = net_buf_simple_pull_u8(raw);
        SYS_LOG_DBG("tid = %d ",tid);
 
        if(!valid_tid(value_ir.save_tid, tid, FLAG_TID))
            return;
 
        value_ir.save_tid = tid;
        SYS_LOG_DBG("value_ir.save_tid = %d ",value_ir.save_tid);
 
        if(value_ir.type > 3){
 
            if(value_ir.bits < 32){
 
            uint16_t cmd1 = net_buf_simple_pull_be16(raw);
            value_ir.cmd  = net_buf_simple_pull_be16(raw);
 
            }else{
                value_ir.cmd   = net_buf_simple_pull_be32(raw);
            }
            // SYS_LOG_DBG("value_ir.type >3: %d value_ir.bits %d value_ir.cmd 0x%04X", value_ir.type, value_ir.bits ,value_ir.cmd);
            // SYS_LOG_DBG("headermark : %d & headerspace :0x%d", foo_val_ir[0], foo_val_ir[1]);
            // SYS_LOG_DBG("onemark: %d onespace : %d", foo_val_ir[2], foo_val_ir[3]);
            // SYS_LOG_DBG("zeromark & zerospace : %d & %d", foo_val_ir[4], foo_val_ir[5]);
            // SYS_LOG_DBG("gapmark & gapspace : %d & %d", foo_val_ir[6], foo_val_ir[7]);

             send_data_ir(foo_val_ir[0], foo_val_ir[1],
                         foo_val_ir[2], foo_val_ir[3],
                         foo_val_ir[4], foo_val_ir[5],
                         foo_val_ir[6], foo_val_ir[7],
                         value_ir.cmd, 1, value_ir.bits, freg);
 
 
 
        }else {
 
            if(value_ir.bits < 32){
 
                uint16_t cmd1 = net_buf_simple_pull_be16(raw);
                value_ir.cmd  = net_buf_simple_pull_be16(raw);
            }else{
                value_ir.cmd   = net_buf_simple_pull_be32(raw);
            }
            // SYS_LOG_DBG("value_ir.type: %d value_ir.bits %d value_ir.cmd 0x%04X", value_ir.type, value_ir.bits ,value_ir.cmd);
 
            send_generic(value_ir.type  , value_ir.cmd, freg);
        }
    }
};

static struct bt_mesh_health_srv health_srv = {
};
 
 
BT_MESH_HEALTH_PUB_DEFINE(health_pub, 0);
 
static struct bt_mesh_cfg_cli cfg_cli = {
};
 
 
static struct bt_mesh_vng_srv vng_srvs[] = {
    {.user_data = NULL},
};
 
BT_MESH_MODEL_PUB_DEFINE(ir_srv_pub, NULL, 3 + 15);
 
static struct mesh_status mesh_status = {
    .period = STATUS_RANDOM_RANGE,
};
 
static struct bt_mesh_vng_status vng_status[] = {
    {.user_data = &mesh_status, .set_func = mesh_status_set_func},
};
 
static struct bt_mesh_vng_command_srv vng_command_srvs[] = {
    {.user_data = NULL},
};
 
BT_MESH_MODEL_PUB_DEFINE(ir_status_pub, NULL, 3 + 15);
 
static struct bt_mesh_model vnd_models0[] = {
    BT_MESH_MODEL_VNG_SRV(&vng_srvs[0], &ir_srv_pub),
    BT_MESH_MODEL_VNG_STATUS(&vng_status[0], &ir_status_pub),
    BT_MESH_MODEL_VNG_COMMAND_SRV(&vng_command_srvs[0]),
    
};

static const struct bt_mesh_model_op ops[] = {
    
    { BT_MESH_MODEL_OP_VNG_SENSOR, 0, remote_ir},
    { BT_MESH_MODEL_OP_VNG_SENSOR_CONFIG, 0, remote_config},
      BT_MESH_MODEL_OP_END,
};
 
 
static struct bt_mesh_model models0[] = {
    BT_MESH_MODEL_CFG_SRV(&cfg_srv),
    BT_MESH_MODEL_CFG_CLI(&cfg_cli),
    BT_MESH_MODEL_HEALTH_SRV(&health_srv, &health_pub),
    BT_MESH_MODEL(MOD_VNG_SENSOR, 
                  ops, NULL , NULL)
 
};
 
static struct bt_mesh_elem elems[] = {
    BT_MESH_ELEM(0, models0, vnd_models0),
};
 
static const struct bt_mesh_comp comp = {
    .cid = CID_VNG,
    .elem = elems,
    .elem_count = ARRAY_SIZE(elems),
}; 

static void mesh_status_set_func(struct net_buf_simple* vng_data, void *user_data)
{
    u16_t index, mod_block;
    u16_t primary_addr;
 
    if(vng_data->len < 6) {
        SYS_LOG_WRN("Data len is too short");
        return;
    }
 
    primary_addr = bt_mesh_primary_addr();
    index = net_buf_simple_pull_be16(vng_data);
    mod_block = net_buf_simple_pull_be16(vng_data);
    mesh_status.period = net_buf_simple_pull_be16(vng_data);
 
    SYS_LOG_DBG("index: %u, mod: %u, prim: %u", index, mod_block, primary_addr);
    if(mod_block == 0 || primary_addr % mod_block == index) {
       status_prepare(0, K_SECONDS(mesh_status.period));
    }
   
}
 
static void status_init()
{
    k_delayed_work_init(&mesh_status.timer, status_signal);
    status_prepare(0, K_SECONDS(STATUS_RANDOM_RANGE));
}

static void status_signal(struct k_work* work)
{
    struct net_buf* buf;
    buf = net_buf_alloc(&mesh_pool, K_NO_WAIT);
    if(buf == NULL) {
        SYS_LOG_ERR("Out of pool");
        return;
    }
    net_buf_add_u8(buf, TYPE_STATUS);
    net_buf_put(_mesh.tx_queue, buf);
    
}
 
static void status_prepare(u32_t ms_min, u32_t ms_max)
{
    int err;
 
    err = rand_number_blocking_generate(ms_min, ms_max, 
                                        &mesh_status.next_period);
    if(err) {
        SYS_LOG_ERR("RNG failed\n");
        mesh_status.next_period = ms_max;
    }
 
    SYS_LOG_DBG("Next status: %u", mesh_status.next_period);
    k_delayed_work_cancel(&mesh_status.timer);
    k_delayed_work_submit(&mesh_status.timer, mesh_status.next_period);
}

static void status_transmit(struct net_buf* buf) 
{
    int err;
    struct net_buf_simple *msg = vnd_models0[1].pub->msg;
    u32_t current;
    /*Send status in timeout from last msg*/
    status_prepare(K_SECONDS(STATUS_INTERVAL_MAX), K_SECONDS(STATUS_INTERVAL_MAX*2));
 
    if(!bt_mesh_is_provisioned()) {
        SYS_LOG_ERR("Device is still not provisioned\n");
        return;
    }
 
    if(msg == NULL) {
         SYS_LOG_ERR("No pub ctx\n");
        return;
    }
 
    current = k_uptime_get_32();
    if(mesh_status.last_status > current) {
        mesh_status.last_status = 0;
    }
    if(mesh_status.last_status != 0 && current > mesh_status.last_status && 
            (current - mesh_status.last_status < K_SECONDS(STATUS_INTERVAL_MIN))) {
        SYS_LOG_WRN("Too early, elapsed: %u ms", current - mesh_status.last_status);
        return;
    }

     mesh_status.last_status = current;
 
    bt_mesh_model_msg_init(msg, BT_MESH_MODEL_OP_VNG);
    u16_t primary_addr = bt_mesh_primary_addr();
    net_buf_simple_add_u8(msg, 0xFF); // add data vao goi msg
    net_buf_simple_add_be16(msg, DEVICE_ID);
    net_buf_simple_add_be16(msg, primary_addr);
    
    err = bt_mesh_model_publish(&vnd_models0[1]);
    if(err) {
        BT_ERR("SENSOR publish status failed");
    }
}

void _prov_complete(u16_t net_idx, u16_t addr)
{
    _net_idx = net_idx;
    if(setting_loading)
        return;
    
    SYS_LOG_INF("Provisioned completed");
 
    bool vnd = false;
    u16_t primary_addr;
 
     /* Add Application Key */
    bt_mesh_cfg_app_key_add(net_idx, elems[0].addr,
                net_idx, app_idx_0, app_key_0, NULL);
 
    bt_mesh_cfg_app_key_add(net_idx, elems[0].addr,
                net_idx, app_idx_1, app_key_1, NULL);
 
    MODELS_BIND(models0, vnd , net_idx, app_idx_0);
 
    vnd = true;
    MODELS_BIND(vnd_models0, vnd, net_idx, app_idx_0);
    MODELS_BIND(vnd_models0, vnd, net_idx, app_idx_1);
 
    struct bt_mesh_cfg_mod_pub ir_srv_pub = {
        .addr = GATEWAY_GROUP_ADDR,
        .app_idx = app_idx_0,
        .ttl = 7,
        .transmit = BT_MESH_PUB_TRANSMIT(1, 50),
    };

    MODEL_PUB(vnd_models0[0], vnd, net_idx, &ir_srv_pub);
 
    struct bt_mesh_cfg_mod_pub ir_status_pub = {
        .addr = STATUS_DST_GROUP_ADDR,
        .app_idx = app_idx_1,
        .ttl = 7,
        .transmit = BT_MESH_PUB_TRANSMIT(1, 50),
    };
 
    MODEL_PUB(vnd_models0[1], vnd, net_idx, &ir_status_pub);
 
    primary_addr = bt_mesh_primary_addr();
    /* Add model subscription */
    bt_mesh_cfg_mod_sub_add_vnd(net_idx, primary_addr, primary_addr,
                STATUS_GROUP_ADDR, BT_MESH_MODEL_ID_VNG_STATUS, CID_VNG, NULL);
 
    bt_mesh_cfg_mod_sub_add_vnd(net_idx, primary_addr, primary_addr,
                VNG_MESH_RENEW_GROUP, BT_MESH_MODEL_ID_VNG_COMMAND_SRV, CID_VNG, NULL);
    
}

static void prov_reset(void) 
{
    bt_mesh_prov_enable(BT_MESH_PROV_ADV | BT_MESH_PROV_GATT);
}
 
static void tx_thread(void *p1, void *p2, void *p3)
{
    while (1) {
        struct net_buf *buf;
        u8_t type;
        /* Wait until a buffer is available */
        buf = net_buf_get(_mesh.tx_queue, K_FOREVER);
        type = net_buf_pull_u8(buf);
        switch(type) {
 
              case TYPE_STATUS: 
                status_transmit(buf);
                break;
            default:
                break;
        }
       
        net_buf_unref(buf);
 
        /* Give other threads a chance to run if _tx_queue keeps getting
         * new data all the time.
         */
        k_yield();
    }
}

// khoi tao model .
static void model_init()
{
    bt_mesh_vng_srv_init(&vnd_models0[0]); 
    bt_mesh_vng_status_init(&vnd_models0[1]);//STATUS
    status_init();
}
 
static void bt_ready(int err)
{
 
    struct bt_le_oob oob;
 
    if (err) {
        SYS_LOG_ERR("Bluetooth init failed (err %d)", err);
        return;
    }
 
    SYS_LOG_INF("Bluetooth initialized");
 
    err = bt_le_oob_get_local(BT_ID_DEFAULT, &oob);
 
    if (err != 0) {
        SYS_LOG_ERR("get local oob failed with error: %d", err);
        return;
    }
 
    memset(uuid, 0, sizeof(uuid));
    memcpy(uuid, oob.addr.a.val, 6);
 
    err = bt_mesh_init(&prov, &comp);
 
    if (err) {
        SYS_LOG_ERR("Initializing mesh failed (err %d)", err);
        return;
    }

     SYS_LOG_DBG("Mesh initialized\n");
 
 
    if (IS_ENABLED(CONFIG_BT_SETTINGS)) {
        SYS_LOG_DBG("Loading stored settings\n");
        setting_loading = true;
        settings_load();
        setting_loading = false;
    }
 
    addr = flash_unicast_get();
    // SYS_LOG_DBG("UNICAST: 0x%04x", addr);
 
    err = bt_mesh_provision(net_key, _net_idx, flags, iv_index, addr,
                dev_key);
    if (err == -EALREADY) {
        printk("Using stored settings\n");
    } else if (err) {
        printk("Provisioning failed (err %d)\n", err);
        return;
    }
 
}

void main(void)
{
    THREADS_MONITOR_MAIN_START
    int err;
    SYS_LOG_INF("Start");
    my_settings_init();
    model_init();
    err = bt_enable(bt_ready);
 
    if (err) {
        SYS_LOG_ERR("Bluetooth init failed (err %d)", err);
        return;
    }
 
 
    /* Spawn the TX thread and start feeding commands and data to the
     * controller */
 
     k_thread_create(&tx_thread_handle,
                    tx_thread_stack,
                    K_THREAD_STACK_SIZEOF(tx_thread_stack),
                    tx_thread,
                    NULL,
                    NULL,
                    NULL,
                    K_PRIO_COOP(7),
                    0,
                    K_NO_WAIT);
 
     THREADS_MONITOR_MAIN_TERMINAL
 
}
