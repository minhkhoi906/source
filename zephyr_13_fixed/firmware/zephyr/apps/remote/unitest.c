#include <device.h>
#include <gpio.h>
#include <board.h>
#include <pwm.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/mesh.h>

// #include <irsend.h>
// 

// #define PWM_MESH_TRANSMIT_STACK_SIZE 1024

#define BT_PWM_BUF_SIZE 36
#define BT_PWM_USER_DATA_MIN 4
#define PWM_BUF_COUNT 12

#define SONY_HDR_MARK 2400
#define SONY_SPACE  600
#define SONY_ONE_MARK 1200
#define SONY_ZERO_MARK 600
#define SONY_GAP 40000


#define SAMSUNG_BITS                		32U
#define SAMSUNG_TICK                     560U
#define SAMSUNG_HDR_MARK_TICKS             8U
#define SAMSUNG_HDR_MARK           (SAMSUNG_HDR_MARK_TICKS * SAMSUNG_TICK)

#define SAMSUNG_HDR_SPACE_TICKS            8U
#define SAMSUNG_HDR_SPACE          (SAMSUNG_HDR_SPACE_TICKS * SAMSUNG_TICK)

#define SAMSUNG_BIT_MARK_TICKS             1U
#define SAMSUNG_BIT_MARK           (SAMSUNG_BIT_MARK_TICKS * SAMSUNG_TICK)

#define SAMSUNG_ONE_SPACE_TICKS            3U
#define SAMSUNG_ONE_SPACE          (SAMSUNG_ONE_SPACE_TICKS * SAMSUNG_TICK)

#define SAMSUNG_ZERO_SPACE_TICKS           1U
#define SAMSUNG_ZERO_SPACE         (SAMSUNG_ZERO_SPACE_TICKS * SAMSUNG_TICK)

#define SAMSUNG_RPT_SPACE_TICKS            4U
#define SAMSUNG_RPT_SPACE          (SAMSUNG_RPT_SPACE_TICKS * SAMSUNG_TICK)

#define SAMSUNG_MIN_MESSAGE_LENGTH_TICKS 193U
#define SAMSUNG_MIN_MESSAGE_LENGTH (SAMSUNG_MIN_MESSAGE_LENGTH_TICKS * \
                                    SAMSUNG_TICK) //108080
#define SAMSUNG_MIN_GAP_TICKS      (SAMSUNG_MIN_MESSAGE_LENGTH_TICKS - \
    (SAMSUNG_HDR_MARK_TICKS + SAMSUNG_HDR_SPACE_TICKS + \
     SAMSUNG_BITS * (SAMSUNG_BIT_MARK_TICKS + SAMSUNG_ONE_SPACE_TICKS) + \
     SAMSUNG_BIT_MARK_TICKS))
#define SAMSUNG_MIN_GAP           720000// (SAMSUNG_MIN_GAP_TICKS * SAMSUNG_TICK)//



// NET_BUF_POOL_DEFINE(pwm_pool,
//                     PWM_BUF_COUNT,
//                     BT_PWM_BUF_SIZE,
//                     BT_PWM_USER_DATA_MIN,
//                     NULL);


// static K_FIFO_DEFINE(tx_queue);


// const uint16_t kNecHdrMark = kNecHdrMarkTicks * kNecTick;



NET_BUF_SIMPLE_DEFINE(rx_buf, 1000);

struct pwm_dev_ctx {

	struct device* pwm_dev;
	u32_t period;
	u32_t pulse_with;
  int status;
};

struct change_pwm{
	uint16_t duration ;
	uint8_t status;
	uint8_t active;

};

static struct change_pwm change_pwm;
static struct pwm_dev_ctx pwm_dev_ctx;

 void timer1_isr()
{
  if ((NRF_TIMER0->EVENTS_COMPARE[0] != 0) && ((NRF_TIMER0->INTENSET & TIMER_INTENSET_COMPARE0_Msk) != 0))
  {
		NRF_TIMER0->EVENTS_COMPARE[0] = 0;	       //Clear compare register 0 event	
		NRF_TIMER0->TASKS_STOP = 1;

		if(change_pwm.active == 0) {
			pwm_pin_set_usec(pwm_dev_ctx.pwm_dev ,19 ,0 ,0);
			return;
		}

		if(change_pwm.status == 1) {

			pwm_pin_set_usec(pwm_dev_ctx.pwm_dev ,19 ,26 ,13);
			NRF_TIMER0->CC[0] = (change_pwm.duration /16);
			NRF_TIMER0->TASKS_START = 1;
			
			
		}
		else if (change_pwm.status == 0){

			
			
			pwm_pin_set_usec(pwm_dev_ctx.pwm_dev ,19 ,0 ,0);

			NRF_TIMER0->CC[0] = (change_pwm.duration /16);
			NRF_TIMER0->TASKS_START = 1;
			
		}

		if(rx_buf.len == 0){

				change_pwm.active = 0;
				NRF_RADIO->TASKS_START = 1;
				return;
		}
		change_pwm.duration = net_buf_simple_pull_le16(&rx_buf);
			// printk("RF_TIMER2->CC[0] %d ", net_buf_simple_pull_le16(&rx_buf));
		change_pwm.status = net_buf_simple_pull_u8(&rx_buf);
			// printk("change_pwm.duration  = %d \n",change_pwm.status);
		
	}
}


static void timer1_init() {

	NVIC_ClearPendingIRQ(NRF5_IRQ_TIMER0_IRQn);
  	IRQ_CONNECT(NRF5_IRQ_TIMER0_IRQn, 0, timer1_isr, 0, 0);
   	irq_enable(NRF5_IRQ_TIMER0_IRQn);

  	NRF_TIMER0->MODE = TIMER_MODE_MODE_Timer;              // Set the timer in Counter Mode
  	NRF_TIMER0->TASKS_CLEAR = 1;
  	NRF_TIMER0->PRESCALER = 8;                             // clear the task first to be usable for later
	NRF_TIMER0->BITMODE = TIMER_BITMODE_BITMODE_32Bit;		   //Set counter to 16 bit resolution
	                             //Set value for TIMER2 compare register 0                                  //Set value for TIMER2 compare register 1
	NRF_TIMER0->SHORTS = TIMER_SHORTS_COMPARE0_CLEAR_Msk;
 //  // Enable interrupt on Timer 2, both for CC[0] and CC[1] compare match events
	NRF_TIMER0->INTENSET = (TIMER_INTENSET_COMPARE0_Enabled << TIMER_INTENSET_COMPARE0_Pos);
  
	NRF_TIMER0->TASKS_STOP = 1;
  	

};



void ir_on(uint16_t usec) {

	pwm_pin_set_usec(pwm_dev_ctx.pwm_dev ,9 ,26 ,13);
	k_busy_wait(usec);
}

void ir_off(uint16_t usec) {
	
	pwm_pin_set_usec(pwm_dev_ctx.pwm_dev ,9 ,0 ,0);
	k_busy_wait(usec);
}

void sendData(unsigned long data) {
	int i;

	net_buf_simple_reset(&rx_buf);

	for(i = 0; i< 3 ;i++) {

		net_buf_simple_add_le16(&rx_buf, SONY_HDR_MARK);
		net_buf_simple_add_u8(&rx_buf,1);
		net_buf_simple_add_le16(&rx_buf, SONY_SPACE);
		net_buf_simple_add_u8(&rx_buf,0);
	    for (unsigned long  mask = 1UL << (12 - 1);  mask;  mask >>= 1) {
	      if (data & mask) {
	        net_buf_simple_add_le16(&rx_buf, SONY_ONE_MARK);
	        net_buf_simple_add_u8(&rx_buf,1);
	        net_buf_simple_add_le16(&rx_buf, SONY_SPACE);
	        net_buf_simple_add_u8(&rx_buf,0);
	      } else {
	        net_buf_simple_add_le16(&rx_buf, SONY_ZERO_MARK);
	        net_buf_simple_add_u8(&rx_buf,1);
	        net_buf_simple_add_le16(&rx_buf, SONY_SPACE);
	        net_buf_simple_add_u8(&rx_buf,0);
	        }
	      }

	      net_buf_simple_add_le16(&rx_buf,SONY_GAP);
	      net_buf_simple_add_u8(&rx_buf,0);
	      
	 }
}

static void scan_cb(const bt_addr_le_t *addr, s8_t rssi, u8_t adv_type,
		    struct net_buf_simple *buf)
{
	//do nothing;
	// printk("Scan cb\n");

}

static void bt_ready(int err)
{
 
	struct bt_le_scan_param scan_param = {
		.type       = BT_HCI_LE_SCAN_PASSIVE,
		.filter_dup = BT_HCI_LE_SCAN_FILTER_DUP_DISABLE,
		.interval   = 0x0010,
		.window     = 0x0010,
	};

	err = bt_le_scan_start(&scan_param, scan_cb);
	if (err) {
		printk("Starting scanning failed (err %d)\n", err);
		return;
	}

}


void send_data_samsung(const uint16_t headermark, const uint16_t headerspace,
                    const uint16_t onemark , const uint16_t onespace,
                    const uint16_t zeromark, const uint16_t zerospace,
                    const uint16_t gapmark, const uint16_t gapspace,
                    unsigned long data) {

  int i;

  net_buf_simple_reset(&rx_buf);

  for(i = 0; i< 3 ;i++) {

    net_buf_simple_add_le16(&rx_buf, headermark);
    net_buf_simple_add_u8(&rx_buf,1);
    net_buf_simple_add_le16(&rx_buf, headerspace);
    net_buf_simple_add_u8(&rx_buf,0);
      for (unsigned long  mask = 1UL << (32 - 1);  mask;  mask >>= 1) {
        if (data & mask) {
          net_buf_simple_add_le16(&rx_buf, onemark);
          net_buf_simple_add_u8(&rx_buf,1);
          net_buf_simple_add_le16(&rx_buf, onespace);
          net_buf_simple_add_u8(&rx_buf,0);
        } else {
          net_buf_simple_add_le16(&rx_buf, zeromark);
          net_buf_simple_add_u8(&rx_buf,1);
          net_buf_simple_add_le16(&rx_buf, zerospace);
          net_buf_simple_add_u8(&rx_buf,0);
          }
        }

        net_buf_simple_add_le16(&rx_buf, gapmark);
        net_buf_simple_add_u8(&rx_buf,1);
        net_buf_simple_add_le16(&rx_buf,gapspace);
        net_buf_simple_add_u8(&rx_buf,0);

        
   }

  printk("pulse: [%s]", bt_hex(rx_buf.data, rx_buf.len));
  change_pwm.duration = net_buf_simple_pull_le16(&rx_buf);
  change_pwm.status = net_buf_simple_pull_u8(&rx_buf);
  NRF_TIMER0->CC[0] = 1;  
  NRF_RADIO->TASKS_DISABLE = 1;
  NRF_TIMER0->TASKS_START = 1;
  change_pwm.active =1;
}


void send_data_sony(const uint16_t headermark, const uint16_t headerspace,
                    const uint16_t onemark , const uint16_t onespace,
                    const uint16_t zeromark, const uint16_t zerospace,
                    const uint16_t gapmark, const uint16_t gapspace,
                    unsigned long data ) {
  int i;

  net_buf_simple_reset(&rx_buf);

  for(i = 0; i< 3 ;i++) {

    net_buf_simple_add_le16(&rx_buf, headermark);
    net_buf_simple_add_u8(&rx_buf,1);
    net_buf_simple_add_le16(&rx_buf, headerspace);
    net_buf_simple_add_u8(&rx_buf,0);
      for (unsigned long  mask = 1UL << (12 - 1);  mask;  mask >>= 1) {
        if (data & mask) {
          net_buf_simple_add_le16(&rx_buf, onemark);
          net_buf_simple_add_u8(&rx_buf,1);
          net_buf_simple_add_le16(&rx_buf, onespace);
          net_buf_simple_add_u8(&rx_buf,0);
        } else {
          net_buf_simple_add_le16(&rx_buf, zeromark);
          net_buf_simple_add_u8(&rx_buf,1);
          net_buf_simple_add_le16(&rx_buf, zerospace);
          net_buf_simple_add_u8(&rx_buf,0);
          }
        }

        net_buf_simple_add_le16(&rx_buf,gapspace);
        net_buf_simple_add_u8(&rx_buf,0);
        
   }

  printk("pulse: [%s]", bt_hex(rx_buf.data, rx_buf.len));
  change_pwm.duration = net_buf_simple_pull_le16(&rx_buf);
  change_pwm.status = net_buf_simple_pull_u8(&rx_buf);
  NRF_TIMER0->CC[0] = 1;  
  NRF_RADIO->TASKS_DISABLE = 1;
  NRF_TIMER0->TASKS_START = 1;
  change_pwm.active =1;


}


void send_data_hitachi(const uint16_t headermark, const uint16_t headerspace,
                    const uint16_t onemark , const uint16_t onespace,
                    const uint16_t zeromark, const uint16_t zerospace,
                    const uint16_t gapmark, const uint16_t gapspace,
                    uint64_t data) {

  int i,j;

  net_buf_simple_reset(&rx_buf);

  for(i = 0; i< 1 ;i++) {

    net_buf_simple_add_le16(&rx_buf, headermark);
    net_buf_simple_add_u8(&rx_buf,1);
    net_buf_simple_add_le16(&rx_buf, headerspace);
    net_buf_simple_add_u8(&rx_buf,0);
      for (unsigned long  mask = 1UL << (32 - 1);  mask;  mask >>= 1) {
        if (data & mask) {
          net_buf_simple_add_le16(&rx_buf, onemark);
          net_buf_simple_add_u8(&rx_buf,1);
          net_buf_simple_add_le16(&rx_buf, onespace);
          net_buf_simple_add_u8(&rx_buf,0);
        } else {
          net_buf_simple_add_le16(&rx_buf, zeromark);
          net_buf_simple_add_u8(&rx_buf,1);
          net_buf_simple_add_le16(&rx_buf, zerospace);
          net_buf_simple_add_u8(&rx_buf,0);
          }
        }

        net_buf_simple_add_le16(&rx_buf, gapmark);
        net_buf_simple_add_u8(&rx_buf,1);
        net_buf_simple_add_le16(&rx_buf,gapspace);
        net_buf_simple_add_u8(&rx_buf,0);
        
   }

  printk("pulse: [%s]", bt_hex(rx_buf.data, rx_buf.len));
  change_pwm.duration = net_buf_simple_pull_le16(&rx_buf);
  change_pwm.status = net_buf_simple_pull_u8(&rx_buf);
  NRF_TIMER0->CC[0] = 1;  
  NRF_RADIO->TASKS_DISABLE = 1;
  NRF_TIMER0->TASKS_START = 1;
  change_pwm.active =1;
}


void main(){

	pwm_dev_ctx.pwm_dev = device_get_binding(CONFIG_PWM_NRF5_SW_0_DEV_NAME);
	if(!pwm_dev_ctx.pwm_dev) {

		printk("Cannot find %s!\n", CONFIG_PWM_NRF5_SW_0_DEV_NAME);
        // return 1;
	}

	int err;
    // SYS_LOG_INF("Start");

    err = bt_enable(bt_ready);
    timer1_init();

    if (err) {
        // SYS_LOG_ERR("Bluetooth init failed (err %d)", err);
        return;
    }
	

	while(1) {
		// send_data_samsung(SAMSUNG_HDR_MARK, SAMSUNG_HDR_SPACE,
  //                   SAMSUNG_BIT_MARK , SAMSUNG_ONE_SPACE,
  //                   SAMSUNG_BIT_MARK, SAMSUNG_ZERO_SPACE,
  //                   SAMSUNG_BIT_MARK, SAMSUNG_MIN_GAP,
  //                   0xE0E040BF);
   send_data_sony(SONY_HDR_MARK, SONY_SPACE,
                  SONY_ONE_MARK, SONY_SPACE,
                  SONY_ZERO_MARK,SONY_SPACE,
                  0,             SONY_GAP,
                  0xa90);

  //  send_data_hitachi(NEC_HDR_MARK, NEC_HDR_SPACE,
  //                   NEC_BITS_MARK , NEC_ONESPACE,
  //                   NEC_BITS_MARK, NEC_ZEROSPACE,
  //                   NEC_BITS_MARK, NEC_MIN_GAP,
  //                   0xE1A244BB);

		k_sleep(3000);
    // ir_on(1000);
    // ir_off(1000);
	}

}