
#include <device.h>
#include <uart.h>
#include <gpio.h>
#include <board.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/mesh.h>
#include <crc16.h>
#include <misc/byteorder.h>
#include <watchdog.h>
#include "common/log.h"

#include <logging/sys_log.h>

#include <host/mesh/net.h>
#include <host/mesh/adv.h>
#include <host/mesh/access.h>
#include <settings/settings.h>
#include <kernel.h>

#include <uart.h>


#undef SYS_LOG_DOMAIN
#define SYS_LOG_DOMAIN "app"


#define CID_VNG 0x00FF
#define MOD_VND_VNG_GATEWAY 0x0000
#define MOD_VNG_GATEWAY 0x0004
#define OP_VENDOR_EVENT                       BT_MESH_MODEL_OP_3(0x00, CID_VNG)
#define BT_MESH_MODEL_OP_VNG_STATUS_SRV       BT_MESH_MODEL_OP_3(0x04, CID_VNG)

#define BT_MESH_MODEL_OP_VND_STATUS           BT_MESH_MODEL_OP_2(0x82, 0x07)
#define BT_MESH_MODEL_OP_VND_LEVELS           BT_MESH_MODEL_OP_2(0x82, 0x08)
#define BT_MESH_MODEL_OP_VND_WARNING          BT_MESH_MODEL_OP_2(0x82, 0x09)

#define GATEWAY_GROUP_ADDR 0xC000
#define STATUS_GROUP_ADDR 0xE000

#define MODELS_BIND(models, is_vnd, net_idx, app_idx) \
do { \
    size_t size = ARRAY_SIZE(models);\
    size_t idx; \
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    for(idx = 0; idx < size; idx++) { \
        addr = elems[models[idx].elem_idx].addr;\
        if(is_vnd) {\
            SYS_LOG_DBG("vnd bind addr [0x%04x]", addr);\
            bt_mesh_cfg_mod_app_bind_vnd(net_idx, primary_addr, addr, app_idx, models[idx].vnd.id, \
                models[idx].vnd.company, NULL);\
        } else {\
            SYS_LOG_DBG("bind addr [0x%04x]", addr);\
            bt_mesh_cfg_mod_app_bind(net_idx, primary_addr, addr,\
            app_idx, models[idx].id, NULL);\
        }\
    } \
} while(0)


#define MODEL_PUB(_model, _vnd_, _net_idx, _pub)\
{\
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    addr = _model->elem_idx + primary_addr;\
    if(_vnd_) {\
        BT_DBG("vnd pub, elem [0x%04x], model [0x%04x], group [0x%04x]",\
            addr, _model->vnd.id, _pub->addr);\
        bt_mesh_cfg_mod_pub_set_vnd(_net_idx, primary_addr, addr,\
            _model->vnd.id,_model->vnd.company, _pub, NULL);\
    } else {\
        BT_DBG("vnd pub, elem [0x%04x], model [0x%04x], group [0x%04x]",\
            addr, _model->vnd.id, _pub->addr);\
        bt_mesh_cfg_mod_pub_set(_net_idx, primary_addr, addr,\
            _model->id, _pub, NULL);\
    }\
}

#define MODEL_SUB(_model, _vnd_, _net_idx, _subaddr)\
{\
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    addr = _model->elem_idx + primary_addr;\
    if(_vnd_) {\
        BT_DBG("vnd sub overwrite, elem [0x%04x], model [0x%04x], group [0x%04x]",\
                addr, _model->vnd.id, _subaddr);\
        bt_mesh_cfg_mod_sub_add_vnd(_net_idx, primary_addr, addr,\
                _subaddr, _model->vnd.id, CID_VNG, NULL);\
    } else {\
        BT_DBG("sig sub overwrite, elem [0x%04x], model [0x%04x], group [0x%04x]",\
                addr, _model->id, _subaddr);\
        bt_mesh_cfg_mod_sub_add(_net_idx, primary_addr, addr,\
                _subaddr, _model->id, NULL);\
    }\
}



static void prov_complete(u16_t net_idx, u16_t addr);
static void prov_reset(void);
static void bt_ready(int err);
 
static void mesh_event(struct bt_mesh_model *model,
                       struct bt_mesh_msg_ctx *ctx,
                       struct net_buf_simple *raw);

static void hub_status(struct bt_mesh_model *model,
                       struct bt_mesh_msg_ctx *ctx,
                       struct net_buf_simple *raw);

static void hub_levels(struct bt_mesh_model *model,
                       struct bt_mesh_msg_ctx *ctx,
                       struct net_buf_simple *raw);

static void hub_warning(struct bt_mesh_model *model,
                       struct bt_mesh_msg_ctx *ctx,
                       struct net_buf_simple *raw);


static const u8_t app_idx = 0;
static const u8_t app_key[16] = {
    0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
    0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
};

static const u8_t app_idx_1 = 1;

static const u8_t app_key_1[16] = {
    0x02, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
    0x02, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
};

static const u8_t dev_key[16] = {
    0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
    0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
};

static const u8_t net_key[16] = {
    0x41, 0x4e, 0xf3, 0x9c, 0x1a, 0xf7, 0x51, 0x5d,
    0x17, 0xce, 0x78, 0xf1, 0x77, 0x92, 0xc7, 0x4c,
};

static const u16_t net_idx;
static const u32_t iv_index;
static u8_t flags;

static u16_t addr = 0x1000;


static bool setting_loading = false;

static u8_t uuid[16] = {0xFF, 0xFF};

static struct bt_mesh_prov prov = {
    .uuid = uuid,
    .output_size = 0,
    .complete = prov_complete,
    .reset = prov_reset

};


static struct bt_mesh_cfg_srv cfg_srv = {
    .relay = BT_MESH_RELAY_ENABLED,
    .beacon = BT_MESH_BEACON_ENABLED,
#if defined(CONFIG_BT_MESH_FRIEND)
    .frnd = BT_MESH_FRIEND_ENABLED,
#else
    .frnd = BT_MESH_FRIEND_NOT_SUPPORTED,
#endif
#if defined(CONFIG_BT_MESH_GATT_PROXY)
    .gatt_proxy = BT_MESH_GATT_PROXY_ENABLED,
#else
    .gatt_proxy = BT_MESH_GATT_PROXY_NOT_SUPPORTED,
#endif
    .default_ttl = 1,

    /* 3 transmissions with 20ms interval */
    .net_transmit = BT_MESH_TRANSMIT(2, 20),
    .relay_retransmit = BT_MESH_TRANSMIT(2, 20),
};


static struct bt_mesh_health_srv health_srv = {
};

BT_MESH_HEALTH_PUB_DEFINE(health_pub, 0);

static struct bt_mesh_cfg_cli cfg_cli = {
};

static const struct bt_mesh_model_op vnd_ops[] = {
    { BT_MESH_MODEL_OP_VNG_STATUS_SRV, 0, mesh_event}, 
    BT_MESH_MODEL_OP_END,
};

static const struct bt_mesh_model_op ops[] = {
    { BT_MESH_MODEL_OP_VND_LEVELS, 0, hub_levels},
    { BT_MESH_MODEL_OP_VND_STATUS, 0, hub_status},
    { BT_MESH_MODEL_OP_VND_WARNING, 0, hub_warning},
    BT_MESH_MODEL_OP_END,
};


static struct bt_mesh_gen_onoff_srv vng_device_srvs[] = {
    {.user_data = NULL},
};


static struct bt_mesh_model models0[] = {
    BT_MESH_MODEL_CFG_SRV(&cfg_srv),
    BT_MESH_MODEL_CFG_CLI(&cfg_cli),
    BT_MESH_MODEL_HEALTH_SRV(&health_srv, &health_pub),
    BT_MESH_MODEL(MOD_VNG_GATEWAY, 
                  ops, NULL , NULL),
};


static struct bt_mesh_model vnd_models0[] = {
    BT_MESH_MODEL_GEN_ONOFF_SRV(&vng_device_srvs[0], NULL),
    BT_MESH_MODEL_VND(CID_VNG, MOD_VND_VNG_GATEWAY, 
                   vnd_ops, NULL, NULL),
};



static struct bt_mesh_elem elems[] = {
    BT_MESH_ELEM(0, models0, vnd_models0),
};

static const struct bt_mesh_comp comp = {
    .cid = CID_VNG,
    .elem = elems,
    .elem_count = ARRAY_SIZE(elems),
};


static void mesh_event(struct bt_mesh_model *model,
                       struct bt_mesh_msg_ctx *ctx,
                       struct net_buf_simple *raw)
{

    SYS_LOG_DBG("src: 0x%04x, buf: %s\n",ctx->addr, bt_hex(raw->data, raw->len));
}

static void hub_status(struct bt_mesh_model *model,
                       struct bt_mesh_msg_ctx *ctx,
                       struct net_buf_simple *raw)
{
    // hub_forward(HUB_STATUS, model, ctx, raw);
    SYS_LOG_DBG("src: 0x%04x, buf: %s\n",ctx->addr, bt_hex(raw->data, raw->len));

}

static void hub_levels(struct bt_mesh_model *model,
                       struct bt_mesh_msg_ctx *ctx,
                       struct net_buf_simple *raw)
{

    // hub_forward(HUB_LEVELS, model, ctx, raw);
    SYS_LOG_DBG("src: 0x%04x, buf: %s\n",ctx->addr, bt_hex(raw->data, raw->len));

}

static void hub_warning(struct bt_mesh_model *model,
                       struct bt_mesh_msg_ctx *ctx,
                       struct net_buf_simple *raw)
{

    // hub_forward(HUB_WARNING, model, ctx, raw);
    SYS_LOG_DBG("src: 0x%04x, buf: %s\n",ctx->addr, bt_hex(raw->data, raw->len));

}

static void prov_complete(u16_t net_idx, u16_t addr)
{
    if(setting_loading)
        return;
    
    SYS_LOG_INF("Provisioned completed");

     /* Add Application Key */
    bt_mesh_cfg_app_key_add(net_idx, elems[0].addr,
                net_idx, app_idx, app_key, NULL);
    bt_mesh_cfg_app_key_add(net_idx, elems[0].addr,
                net_idx, app_idx_1, app_key_1, NULL);

    MODELS_BIND(models0, false , net_idx, app_idx);
    MODELS_BIND(vnd_models0, true, net_idx, app_idx);
    MODELS_BIND(vnd_models0, true, net_idx, app_idx_1);

    MODEL_SUB((&vnd_models0[1]), true , net_idx, GATEWAY_GROUP_ADDR);
    MODEL_SUB((&vnd_models0[1]), true , net_idx, STATUS_GROUP_ADDR);

    MODEL_SUB((&models0[3]), false , net_idx, GATEWAY_GROUP_ADDR);
    MODEL_SUB((&models0[3]), false , net_idx, STATUS_GROUP_ADDR);
}


static void prov_reset(void) 
{
    bt_mesh_prov_enable(BT_MESH_PROV_ADV | BT_MESH_PROV_GATT);
}


static void bt_ready(int err)
{
 
    struct bt_le_oob oob;

    if (err) {
        SYS_LOG_ERR("Bluetooth init failed (err %d)", err);
        return;
    }

    SYS_LOG_INF("Bluetooth initialized");

    err = bt_le_oob_get_local(BT_ID_DEFAULT, &oob);

    if (err != 0) {
        SYS_LOG_ERR("get local oob failed with error: %d", err);
        return;
    }

    memset(uuid, 0, sizeof(uuid));
    memcpy(uuid, oob.addr.a.val, 6);

    err = bt_mesh_init(&prov, &comp);

    if (err) {
        SYS_LOG_ERR("Initializing mesh failed (err %d)", err);
        return;
    }

    SYS_LOG_DBG("Mesh initialized\n");


    if (IS_ENABLED(CONFIG_BT_SETTINGS)) {
        SYS_LOG_DBG("Loading stored settings\n");
        setting_loading = true;
        settings_load();
        setting_loading = false;
    }

    err = bt_mesh_provision(net_key, net_idx, flags, iv_index, addr,
                dev_key);
    if (err == -EALREADY) {
        printk("Using stored settings\n");
    } else if (err) {
        printk("Provisioning failed (err %d)\n", err);
        return;
    } 

}

void main(void)
{
    int err;
    err = bt_enable(bt_ready);

    if (err) {
        
        SYS_LOG_ERR("Bluetooth init failed (err %d)", err);
        return;
    }


}

