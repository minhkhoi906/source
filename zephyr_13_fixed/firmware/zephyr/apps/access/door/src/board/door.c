#include "door.h"
#include "mesh/mesh.h"
#include <board.h>

#include <entropy.h>
#include <errno.h>
#include <gpio.h>

#define SYS_LOG_DOMAIN "app"
#include <common/log.h>
#include <logging/sys_log.h>
#include <misc/util.h>
#include <bluetooth/mesh.h>

struct model_onoff_ctx_st model_onoff_ctx[] = {
{.dev = NULL,
    .pin_relay = DOOR_GPIO_PIN,
    .pin_led_on = LED_OPEN_GPIO_PIN,
    .pin_led_off = LED_CLOSE_GPIO_PIN,
    .pin_sensor = SENSOR_GPIO_PIN},
};


static struct gpio_callback gpio_cb;

void door_close(struct k_work* work)
{
    if(! model_onoff_ctx[0].dev) {
        SYS_LOG_ERR("onoff_ctx has no dev gpio");
        return;
    }
    k_delayed_work_cancel(&model_onoff_ctx[0].timer);
    model_onoff_ctx[0].present = !PRESENT_OPENING;
    gpio_pin_write(model_onoff_ctx[0].dev, model_onoff_ctx[0].pin_relay, !DOOR_OPEN_LEVEL);
    gpio_pin_write(model_onoff_ctx[0].dev, model_onoff_ctx[0].pin_led_on, !LED_OPEN_LEVEL);
    gpio_pin_write(model_onoff_ctx[0].dev, model_onoff_ctx[0].pin_led_off, LED_OPEN_LEVEL);
    SYS_LOG_DBG("Door closed");

}

void door_open(){
    SYS_LOG_DBG("Open door");
    if(! model_onoff_ctx[0].dev) {
        SYS_LOG_ERR("onoff_ctx has no dev gpio");
        return;
    }
    if(!model_onoff_ctx[0].delay) model_onoff_ctx[0].delay = 1;
    k_delayed_work_submit(&model_onoff_ctx[0].timer, model_onoff_ctx[0].delay *1000);
    if(model_onoff_ctx[0].present == PRESENT_OPENING)return;
    model_onoff_ctx[0].present = PRESENT_OPENING;
    gpio_pin_write(model_onoff_ctx[0].dev, model_onoff_ctx[0].pin_relay, DOOR_OPEN_LEVEL);
    gpio_pin_write(model_onoff_ctx[0].dev, model_onoff_ctx[0].pin_led_on, LED_OPEN_LEVEL);
    gpio_pin_write(model_onoff_ctx[0].dev, model_onoff_ctx[0].pin_led_off, !LED_OPEN_LEVEL);
}

static void sensor_cb(struct device *dev, struct gpio_callback *cb,
            u32_t pins)
{
    for(int i=0; i<ARRAY_SIZE(model_onoff_ctx); i++) {

        if(cb->pin_mask & BIT(model_onoff_ctx[i].pin_sensor)) {

            SYS_LOG_DBG("sensor pin [%d] change state", model_onoff_ctx[i].pin_sensor);
            status_transmit(NULL);
            return ;
        }
    }
    SYS_LOG_ERR("not found button pressed");

}


int door_init()
{
    struct device* dev = device_get_binding(CONFIG_GPIO_P0_DEV_NAME);
    if(!dev) {
        BT_ERR("no dev gpio");
        return -1;
    }
    int i;
    for(i=0; i<ARRAY_SIZE(model_onoff_ctx); i++) {
        model_onoff_ctx[i].dev = dev;
        gpio_pin_configure(model_onoff_ctx[i].dev, model_onoff_ctx[i].pin_relay, GPIO_DIR_OUT);
        gpio_pin_configure(model_onoff_ctx[i].dev, model_onoff_ctx[i].pin_led_on, GPIO_DIR_OUT);
        gpio_pin_configure(model_onoff_ctx[i].dev, model_onoff_ctx[i].pin_led_off, GPIO_DIR_OUT);
        gpio_pin_configure(model_onoff_ctx[i].dev, model_onoff_ctx[i].pin_sensor, GPIO_DIR_IN);
        k_delayed_work_init(&model_onoff_ctx[i].timer, door_close);
        gpio_pin_write(model_onoff_ctx[i].dev, model_onoff_ctx[i].pin_relay, !DOOR_OPEN_LEVEL);
        gpio_pin_write(model_onoff_ctx[i].dev, model_onoff_ctx[i].pin_led_on, !LED_OPEN_LEVEL);
        gpio_pin_write(model_onoff_ctx[i].dev, model_onoff_ctx[i].pin_led_off, LED_OPEN_LEVEL);

//      Init sensor pin
        gpio_pin_configure(model_onoff_ctx[i].dev, model_onoff_ctx[i].pin_sensor,
                        GPIO_DIR_IN | GPIO_INT | GPIO_INT_DEBOUNCE | GPIO_INT_EDGE | GPIO_INT_DOUBLE_EDGE | GPIO_PUD_NORMAL);
        gpio_init_callback(&gpio_cb, sensor_cb, BIT(model_onoff_ctx[i].pin_sensor));
        gpio_add_callback(dev, &gpio_cb);
        gpio_pin_enable_callback(model_onoff_ctx[i].dev, model_onoff_ctx[i].pin_sensor);
//        gpio_pin_disable_callback(model_onoff_ctx[i].dev, model_onoff_ctx[i].pin_sensor);
    }
    return 0;
}

u8_t get_sensor_state(){
    static u32_t val;
    gpio_pin_read(model_onoff_ctx[0].dev, model_onoff_ctx[0].pin_sensor, &val);
    if(val) return SENSOR_OPEN_LEVEL;
    return SENSOR_CLOSE_LEVEL;
}
