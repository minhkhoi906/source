#ifndef _DOOR_H
#define _DOOR_H
#include <zephyr.h>

int door_init();
void door_open();
void door_close(struct k_work* work);
u8_t get_sensor_state();

struct model_onoff_ctx_st {
    u8_t target;
    u16_t delay;
    u16_t tid;
    u8_t dir;
    u8_t present;
    struct device* dev;
    struct k_delayed_work timer;
    u32_t pin_relay;
    u32_t pin_led_on;
    u32_t pin_led_off;
    u32_t pin_sensor;
};
extern  struct  model_onoff_ctx_st model_onoff_ctx[];
#endif // _DOOR_H
