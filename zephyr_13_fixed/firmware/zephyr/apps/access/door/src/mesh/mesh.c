#include <device.h>
#include <uart.h>
#include <gpio.h>
#include <board.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/mesh.h>

#include <crc16.h>
#include <misc/byteorder.h>
#include <watchdog.h>
#include "common/log.h"
#include <logging/sys_log.h>
#include <app.h>

#include <host/mesh/net.h>
#include <host/mesh/adv.h>
#include <host/mesh/access.h>
#include <host/mesh/foundation.h>

#include <settings/settings.h>
#include <bluetooth/mesh/gen_onoff_srv.h>
#include <bluetooth/mesh/vng_status.h>

#include "../flash_uicr.h"
#include "../board/door.h"
#include "mesh.h"

#define CID_VNG 0x00FF
#define GATEWAY_GROUP_ADDR 0xC000
#define STATUS_GROUP_ADDR  0xD000
#define STATUS_DST_GROUP_ADDR  0xE000
#define VNG_MESH_RENEW_GROUP 0xC002

#define BT_MESH_MODEL_OP_VNG    BT_MESH_MODEL_OP_3(0x04, CID_VNG)

#define STATUS_INTERVAL_MIN  30
#define STATUS_INTERVAL_MAX  300

#define UART_MESH_TRANSMIT_STACK_SIZE 1024

#define BT_MESH_BUF_SIZE 36
#define BT_MESH_USER_DATA_MIN 4
#if defined(CONFIG_BT_MESH_BUFFERS)
#define MESH_BUF_COUNT CONFIG_BT_MESH_BUFFERS
#else
#define MESH_BUF_COUNT 12
#endif

#define EXCEPTION_TIMES 5
#define LACK_U16_T 0x00010000


#define PUB_PERIOD(seconds) ((0x01<<6 | seconds) & 0xFF)
#define MODELS_BIND(models, __vnd, net_idx, app_idx) \
    do { \
    size_t size = ARRAY_SIZE(models);\
    size_t idx; \
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    for(idx = 0; idx < size; idx++) { \
    addr = elems[models[idx].elem_idx].addr;\
    if(__vnd) {\
    SYS_LOG_DBG("vnd bind addr [0x%04x]", addr);\
    bt_mesh_cfg_mod_app_bind_vnd(net_idx, primary_addr, addr, app_idx, models[idx].vnd.id, \
    models[idx].vnd.company, NULL);\
    } else {\
    SYS_LOG_DBG("bind addr [0x%04x]", addr);\
    bt_mesh_cfg_mod_app_bind(net_idx, primary_addr, addr,\
    app_idx, models[idx].id, NULL);\
    }\
    } \
    } while(0)


#define MODEL_PUB(model, __vnd, net_idx, pub)\
{\
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    addr = elems[model.elem_idx].addr;\
    if(__vnd) {\
    SYS_LOG_DBG("vnd pub addr [0x%04x]", addr);\
    bt_mesh_cfg_mod_pub_set_vnd(net_idx, primary_addr, addr,\
    model.vnd.id, model.vnd.company, pub, NULL);\
    } else {\
    SYS_LOG_DBG("pub addr [0x%04x]", addr);\
    bt_mesh_cfg_mod_pub_set(net_idx, primary_addr, addr,\
    model.id, pub, NULL);\
    }\
    }

#ifdef MESH_TEST
struct mesh_test
{
    struct k_delayed_work timer;
};

static void mesh_test_work(struct k_work* work);
static int test_mesh_transmit();

static struct mesh_test mesh_test;

#endif
struct mesh_status
{
    struct k_delayed_work timer;
    u32_t  next_period;
    u32_t  last_status;
};


static void onoff_set_func(struct net_buf_simple* data, void *user_data);

extern void _prov_complete(u16_t net_idx, u16_t addr);
static void prov_reset(void);

static void status_prepare(u32_t ms_min, u32_t ms_max);
static void mesh_status_set_func(u16_t time, u16_t main, u16_t duration, u16_t period);

// static struct gpio_callback gpio_cb;

static struct mesh_status mesh_status;

static uint8_t uuid[16] = {0xFF, 0xFF};

static uint16_t current_tid[2] = {0x0000,0x0000};


static struct bt_mesh_prov prov = {
    .uuid = uuid,
    .output_size = 0,
    .complete = _prov_complete,
    .reset = prov_reset

};

static const u8_t app_idx_0 = 0;

static const u8_t app_key_0[16] = {
   0x4d, 0x48, 0x63, 0x43, 0x41, 0x51, 0x45, 0x45, 0x49, 0x4c, 0x79, 0x53, 0x4a, 0x69, 0x4c, 0x2f,
};

static const u8_t dev_key[16] = {
    0x55, 0x65, 0x41, 0x58, 0x64, 0x65, 0x78, 0x4d, 0x76, 0x74, 0x4c, 0x65, 0x56, 0x31, 0x70, 0x52,
};

static const u8_t net_key[16] = {
    0x7a, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,
};

static u16_t net_idx = 0;
static const u32_t iv_index = 0;
static u8_t flags;
//static u16_t addr = 0x2000;

static u16_t primary_net_idx;

static bool setting_loading = false;

static struct bt_mesh_cfg_srv cfg_srv = {
    .relay = BT_MESH_RELAY_DISABLED,
    .beacon = BT_MESH_BEACON_ENABLED,
#if defined(CONFIG_BT_MESH_FRIEND)
    .frnd = BT_MESH_FRIEND_ENABLED,
#else
    .frnd = BT_MESH_FRIEND_NOT_SUPPORTED,
#endif
#if defined(CONFIG_BT_MESH_GATT_PROXY)
    .gatt_proxy = BT_MESH_GATT_PROXY_ENABLED,
#else
    .gatt_proxy = BT_MESH_GATT_PROXY_NOT_SUPPORTED,
#endif
    .default_ttl = 4,

    /* 3 transmissions with 20ms interval */
    .net_transmit = BT_MESH_TRANSMIT(3, 20),
    .relay_retransmit = BT_MESH_TRANSMIT(3, 20),
};


static struct bt_mesh_health_srv health_srv = {
};

BT_MESH_HEALTH_PUB_DEFINE(health_pub, 0);

static struct bt_mesh_cfg_cli cfg_cli = {
};

static struct bt_mesh_gen_onoff_srv onoff_srv[] = {
{.set_func = onoff_set_func, .get_func = NULL, .user_data = &model_onoff_ctx[0]},
};

static struct bt_mesh_vng_status vng_status_srv[] = {
{.user_data = NULL, .set_func = mesh_status_set_func},
};

static struct bt_mesh_vng_command_srv vng_command_srvs[] = {
    {.user_data = NULL},
};

static struct bt_mesh_model vnd_models0[] = {
    BT_MESH_MODEL_VNG_STATUS(&vng_status_srv[0], NULL),
    BT_MESH_MODEL_VNG_COMMAND_SRV(&vng_command_srvs[0]),
};

static struct bt_mesh_model models0[] = {
    BT_MESH_MODEL_CFG_SRV(&cfg_srv),
    BT_MESH_MODEL_CFG_CLI(&cfg_cli),
    BT_MESH_MODEL_HEALTH_SRV(&health_srv, &health_pub),
    BT_MESH_MODEL_GEN_ONOFF_SRV(&onoff_srv[0], NULL),
};

static struct bt_mesh_elem elems[] = {
    BT_MESH_ELEM(0, models0, vnd_models0),
};

static const struct bt_mesh_comp comp = {
    .cid = CID_VNG,
    .elem = elems,
    .elem_count = ARRAY_SIZE(elems),
};

void _prov_complete(u16_t net_idx_, u16_t addr)
{
    if(setting_loading)
        return;
    net_idx = net_idx_;

    SYS_LOG_INF("Provisioned completed");

    u16_t primary_addr;

    /* Add Application Key */
    bt_mesh_cfg_app_key_add(net_idx, elems[0].addr,
            net_idx, app_idx_0, app_key_0, NULL);

    MODELS_BIND(models0, false , net_idx, app_idx_0);

    MODELS_BIND(vnd_models0, true, net_idx, app_idx_0);

    u8_t ttl = bt_mesh_default_ttl_get();

    struct bt_mesh_cfg_mod_pub pub = {
        .addr = GATEWAY_GROUP_ADDR,
                .app_idx = app_idx_0,
                .ttl = ttl,
                .transmit = BT_MESH_PUB_TRANSMIT(1, 50),
    };

    MODEL_PUB(vnd_models0[0], true, net_idx, &pub);

    primary_addr = bt_mesh_primary_addr();

    bt_mesh_cfg_mod_sub_add_vnd(net_idx, primary_addr, primary_addr,
                                STATUS_GROUP_ADDR, BT_MESH_MODEL_ID_VNG_STATUS, CID_VNG, NULL);
    bt_mesh_cfg_mod_sub_add_vnd(net_idx, primary_addr, primary_addr,
                VNG_MESH_RENEW_GROUP, BT_MESH_MODEL_ID_VNG_COMMAND_SRV, CID_VNG, NULL);


    primary_net_idx = net_idx_;
}


static void prov_reset(void)
{
    bt_mesh_prov_enable(BT_MESH_PROV_ADV | BT_MESH_PROV_GATT);
}

static void mesh_status_set_func(u16_t time, u16_t main, u16_t duration, u16_t period)
{  SYS_LOG_INF("time: %u, main: %u, duration: %u, period: %u", time, main, duration, period);
    if( main == 0)
        return;

    u16_t mod = bt_mesh_primary_addr() % main;

    if (mod == time) 
        status_prepare(10, duration*1000);

    else if(period > 0) {
        u16_t min_duration = mod * period;
        u16_t max_duration = min_duration + duration;
        SYS_LOG_DBG("mod %u, min %u, max %u",mod, min_duration, max_duration);
        status_prepare(min_duration * 1000, max_duration*1000);
    }
}

void status_init()
{
    SYS_LOG_DBG("Mesh status init");
    k_delayed_work_init(&mesh_status.timer, status_transmit);
    status_prepare(K_SECONDS(STATUS_INTERVAL_MIN), K_SECONDS(STATUS_INTERVAL_MAX));
    bt_mesh_vng_status_init(&vnd_models0[0]);
}


#ifdef MESH_TEST
static int test_mesh_transmit()
{

    int err;
    NET_BUF_SIMPLE_DEFINE(msg, 3 + 12 + 4);
    bt_mesh_model_msg_init(&msg, BT_MESH_MODEL_OP_VNG);
    net_buf_simple_add_u8(&msg, 0xFF);
    net_buf_simple_add_be16(&msg, DEVICE_ID);
    u16_t primary_addr = bt_mesh_primary_addr();
    net_buf_simple_add_be16(&msg, primary_addr);

    net_buf_simple_add_u8(&msg, 0);

    struct bt_mesh_msg_ctx ctx = {
        .net_idx = bt_mesh.sub[0].net_idx,
        .app_idx = app_idx_0,
        .addr = STATUS_DST_GROUP_ADDR,
        .send_ttl = BT_MESH_TTL_DEFAULT,
    };

    err = bt_mesh_model_send(&vnd_models0[0], &ctx, &msg, NULL, NULL);

    if (err != 0) {
        SYS_LOG_ERR("send failed with error: %d", err);
    }

    return err;

}

static void mesh_test_work(struct k_work* work)
{
    int err;

    SYS_LOG_DBG("");
    err = test_mesh_transmit();

    if(err) {
        printk("Stored settings not work, resetting mesh...\n");
        bt_mesh_renew_netkey(0, net_key);
    }
    
}
#endif

void status_transmit(struct k_work* work)
{
    int err;
    SYS_LOG_DBG("Mesh status transmit");

    /*Send status in timeout from last msg*/
    status_prepare(K_SECONDS(STATUS_INTERVAL_MAX), K_SECONDS(STATUS_INTERVAL_MAX*2));

    if(!bt_mesh_is_provisioned()) {
        SYS_LOG_ERR("Device is still not provisioned\n");
        return;
    }

    NET_BUF_SIMPLE_DEFINE(msg, 3 + 12 + 4);
    bt_mesh_model_msg_init(&msg, BT_MESH_MODEL_OP_VNG);
    net_buf_simple_add_u8(&msg, 0xFF);
    net_buf_simple_add_be16(&msg, DEVICE_ID);
    u16_t primary_addr = bt_mesh_primary_addr();
    net_buf_simple_add_be16(&msg, primary_addr);

    u8_t state = get_sensor_state();
    net_buf_simple_add_u8(&msg, state);

    struct bt_mesh_msg_ctx ctx = {
        .net_idx = bt_mesh.sub[0].net_idx,
                .app_idx = app_idx_0,
                .addr = STATUS_DST_GROUP_ADDR,
                .send_ttl = BT_MESH_TTL_DEFAULT,
    };

    err = bt_mesh_model_send(&vnd_models0[0], &ctx, &msg, NULL, NULL);

    if (err != 0) {
        SYS_LOG_ERR("send failed with error: %d", err);
    }

}




static void status_prepare(u32_t ms_min, u32_t ms_max)
{
    int err;

    err = rand_number_blocking_generate(ms_min, ms_max,
                                        &mesh_status.next_period);
    if(err) {
        SYS_LOG_ERR("RNG failed\n");
        mesh_status.next_period = ms_max;
    }

    SYS_LOG_DBG("Next status: %u", mesh_status.next_period);
    k_delayed_work_cancel(&mesh_status.timer);
    k_delayed_work_submit(&mesh_status.timer, mesh_status.next_period);
}

static bool tid_check(uint16_t tid, uint8_t dir){
    if(dir != 0x00) dir = 0x01;
    if(tid == current_tid[dir]) return false;

    if(tid > current_tid[dir]){
        uint32_t current_tid_u32_t = current_tid[dir] + LACK_U16_T;
        if ((current_tid_u32_t - tid) < EXCEPTION_TIMES)
        {
            return false;
        }
        current_tid[dir] = tid;
        return true;
    }
    if((current_tid[dir] - tid ) < EXCEPTION_TIMES){
        return false;
    } 
    current_tid[dir] = tid;
    return true;
}


static void onoff_set_func(struct net_buf_simple* buf, void *user_data)
{
    struct model_onoff_ctx_st* onoff_ctx = user_data;
    onoff_ctx->target = net_buf_simple_pull_u8(buf);
    onoff_ctx->delay = net_buf_simple_pull_be16(buf);
    onoff_ctx->tid = net_buf_simple_pull_be16(buf);
    onoff_ctx->dir = net_buf_simple_pull_u8(buf);


    SYS_LOG_DBG("onoff_ctx->target : %X",onoff_ctx->target);
    SYS_LOG_DBG("onoff_ctx->delay : %X",onoff_ctx->delay);
    SYS_LOG_DBG("onoff_ctx->tid : %X",onoff_ctx->tid);

    uint16_t tid = onoff_ctx->tid;
    uint8_t dir = onoff_ctx->dir;

    if(!tid_check(tid,dir))return;

    if(onoff_ctx->target == TARGET_OPEN_LEVEL){
        door_open();
        return;
    }
    door_close(NULL);
}

void bt_ready(int err)
{

#ifdef MESH_TEST
    k_delayed_work_init(&mesh_test.timer, mesh_test_work);
    k_delayed_work_submit(&mesh_test.timer, 3000);
#endif
    struct bt_le_oob oob;

    if (err) {
        SYS_LOG_ERR("Bluetooth init failed (err %d)", err);
        return;
    }
    u32_t opcade=BT_MESH_MODEL_OP_2(0x82, 0x02);
    SYS_LOG_INF("opcode = 0x%08x",opcade);
    SYS_LOG_INF("Bluetooth initialized");

    err = bt_le_oob_get_local(BT_ID_DEFAULT, &oob);

    if (err != 0) {
        SYS_LOG_ERR("get local oob failed with error: %d", err);
        return;
    }

    memset(uuid, 0, sizeof(uuid));
    memcpy(uuid, oob.addr.a.val, 6);

    err = bt_mesh_init(&prov, &comp);

    if (err) {
        SYS_LOG_ERR("Initializing mesh failed (err %d)", err);
        return;
    }

    SYS_LOG_DBG("Mesh initialized\n");

    if (IS_ENABLED(CONFIG_BT_SETTINGS)) {
        SYS_LOG_DBG("Loading stored settings\n");
        setting_loading = true;
        settings_load();
        setting_loading = false;
    }
    u16_t _addr = flash_unicast_get();
      if(0x7FFF<_addr) {
          _addr = 0x0001;
      }
    SYS_LOG_DBG("UNICAST: 0x%04x", _addr);

    err = bt_mesh_provision(net_key, net_idx, flags, iv_index, _addr,
                            dev_key);
    if (err == -EALREADY) {
        printk("Using stored settings\n");
        return;
    } else if (err) {
        printk("Provisioning failed (err %d)\n", err);
        return;
    }
}

