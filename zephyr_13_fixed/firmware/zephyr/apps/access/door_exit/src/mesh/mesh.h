#ifndef MESH_H
#define MESH_H

#include <zephyr.h>

#define BT_MESH_MODEL_OP_VND_CFG_TIMEOUT_STT_SET		BT_MESH_MODEL_OP_2(0x83, 0x04)
#define BT_MESH_MODEL_ID_VND_CFG_STT_SRV				0X2001

void bt_ready(int err);
void status_init();
//extern u16_t addr;
void status_transmit(struct k_work* work);


#endif // MESH_H
