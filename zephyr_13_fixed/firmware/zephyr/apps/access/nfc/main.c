#include <device.h>
#include <uart.h>
#include <gpio.h>
#include <board.h>
 
#include <bluetooth/bluetooth.h>
#include <bluetooth/mesh.h>
#include <crc16.h>
#include <misc/byteorder.h>
#include <watchdog.h>
#include "common/log.h"
#define SYS_LOG_DOMAIN "app"
 
#include <logging/sys_log.h>
#include <app.h>
 
#include <host/mesh/net.h>
#include <host/mesh/adv.h>
#include <host/mesh/access.h>
#include <settings/settings.h>
 
#include "flash_uicr.h"
 
enum {
    TYPE_NFC = 0x00,
    TYPE_STATUS = 0x01,
};
 
 
#define DEVICE_ID 0x1200
#define CID_VNG 0x00FF
#define GATEWAY_GROUP_ADDR 0xC000
#define STATUS_GROUP_ADDR  0xD000
#define STATUS_DST_GROUP_ADDR  0xE000
#define BT_MESH_MODEL_OP_VNG    BT_MESH_MODEL_OP_3(0x04, CID_VNG)
#define VNG_MESH_RENEW_GROUP 0xC002
 
#define STATUS_INTERVAL_MIN  30
#define STATUS_INTERVAL_MAX  300
#define STATUS_RANDOM_RANGE  10
 

#define UART_MESH_TRANSMIT_STACK_SIZE 1024

#define CARD_ID_LEN         6 
#define CACHE_SIZE          1 
 
#ifdef TURNSTILE 
 
    #define SPAM_TIME_OUT       10000 // MS 
#else  
 
    #define SPAM_TIME_OUT       3000 // MS 
#endif 

#define BT_MESH_BUF_SIZE 36
#define BT_MESH_USER_DATA_MIN 4
#if defined(CONFIG_BT_MESH_BUFFERS)
#define MESH_BUF_COUNT CONFIG_BT_MESH_BUFFERS
#else
#define MESH_BUF_COUNT 12
#endif
 
 
#define PUB_PERIOD(seconds) ((0x01<<6 | seconds) & 0xFF)
#define MODELS_BIND(models, vnd, net_idx, app_idx) \
do { \
    size_t size = ARRAY_SIZE(models);\
    size_t idx; \
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    for(idx = 0; idx < size; idx++) { \
        addr = elems[models[idx].elem_idx].addr;\
        if(vnd) {\
            SYS_LOG_DBG("vnd bind addr [0x%04x]", addr);\
            bt_mesh_cfg_mod_app_bind_vnd(net_idx, primary_addr, addr, app_idx, models[idx].vnd.id, \
                models[idx].vnd.company, NULL);\
        } else {\
            SYS_LOG_DBG("bind addr [0x%04x]", addr);\
            bt_mesh_cfg_mod_app_bind(net_idx, primary_addr, addr,\
            app_idx, models[idx].id, NULL);\
        }\
    } \
} while(0)

#define MODEL_PUB(model, vnd, net_idx, pub)\
{\
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    addr = elems[model.elem_idx].addr;\
    if(vnd) {\
        SYS_LOG_DBG("vnd pub addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_pub_set_vnd(net_idx, primary_addr, addr,\
            model.vnd.id, model.vnd.company, pub, NULL);\
    } else {\
        SYS_LOG_DBG("pub addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_pub_set(net_idx, primary_addr, addr,\
            model.id, pub, NULL);\
    }\
}
 
 
 
NET_BUF_POOL_DEFINE(mesh_pool,
                    MESH_BUF_COUNT,
                    BT_MESH_BUF_SIZE,
                    BT_MESH_USER_DATA_MIN,
                    NULL);
 
#define SLIP_END             0xC0    /* indicates end of packet */
#define SLIP_ESC             0xDB    /* indicates byte stuffing */
#define SLIP_ESC_END         0xDC    /* ESC ESC_END means END data byte */
#define SLIP_ESC_ESC         0xDD    /* ESC ESC_ESC means ESC data byte */
#define SLIP_ESC_XON         0xDE /* ESC SLIP_ESC_XON means XON control byte */
#define SLIP_ESC_XOFF        0xDF /* ESC SLIP_ESC_XON means XOFF control byte */
#define XON 0x11 /* indicates XON charater */
#define XOFF 0x13 /* indicates XOFF charater */

#define SUCCESS_NOTIFY  0
#define FAILED_NOTIFY   0
#define SUCCESS_REPEAT  1
#define FAIL_REPEAT     3
 
static struct device *uart;
 
static BT_STACK_NOINIT(tx_thread_stack, UART_MESH_TRANSMIT_STACK_SIZE);
static struct k_thread tx_thread_handle;
static K_FIFO_DEFINE(tx_queue);


 
struct notify_buzzer_set
{
    u8_t success;
    u8_t failed;
    u8_t success_repeat;
    u8_t fail_repeat;

}__packed;


struct _mesh
{
    u16_t tx_crc;
    struct k_fifo *tx_queue;
    struct net_buf* rx_buf;
    struct k_delayed_work uart_timer;
    u8_t last_char;
 
};
 
struct mesh_status
{
    struct k_delayed_work timer;
    u32_t  next_period;
    u32_t  last_status;
    u16_t  period;
};
 
struct checckcard {
    u32_t last_time;
    u16_t tid;
    u8_t tid_cmp; 
    bool is_card_err; 
}; 

struct checkdid { 
    u32_t last_time; 
    u8_t data[CARD_ID_LEN]; 
}; 
 
struct cache_id { 
    struct checkdid ids[CACHE_SIZE]; 
    u8_t index; 
}; 
extern void _prov_complete(u16_t net_idx, u16_t addr);
static void prov_reset(void);
 
static void status_init();
static void status_prepare(u32_t ms_min, u32_t ms_max);
static void status_signal(struct k_work* work);
static void mesh_status_set_func(u16_t time, u16_t main, u16_t duration, u16_t period);
static void mesh_checkcard_set_func(struct net_buf_simple* vng_data, void *user_data);
static void mesh_reader_notify_set_func(u8_t success, u8_t failed, u8_t success_repeat, u8_t fail_repeat, void *user_data);

 
static void nfc_transmit(struct net_buf* buf);
static void nfc_data_fill(struct net_buf_simple* buf);
static void load_setting_reader_notify();
 
static int mesh_uart_init();
static void mesh_uart_decoce(u8_t byte);
static void uart_rx_timeout(struct k_work* work);
static void mesh_uart_isr(struct device *unused);
 
 
static struct _mesh _mesh = {
    .tx_queue = &tx_queue,
    .rx_buf = NULL,
};
 
static struct checckcard checckcard ;

static struct notify_buzzer_set notify_buzzer_set = {
    .success = SUCCESS_NOTIFY,
    .failed  = FAILED_NOTIFY,
    .success_repeat = SUCCESS_REPEAT,
    .fail_repeat    = FAIL_REPEAT
};

static struct cache_id cache_id = {
    .index = 0
};


static uint8_t uuid[16] = {0xFF, 0xFF};
 
static struct bt_mesh_prov prov = {
    .uuid = uuid,
    .output_size = 0,
    .complete = _prov_complete,
    .reset = prov_reset
 
};
 
static const u8_t app_idx_0 = 0;
 
static const u8_t app_key_0[16] = {
   0x4d, 0x48, 0x63, 0x43, 0x41, 0x51, 0x45, 0x45, 
   0x49, 0x4c, 0x79, 0x53, 0x4a, 0x69, 0x4c, 0x2f,
};
 
static const u8_t dev_key[16] = {
    0x55, 0x65, 0x41, 0x58, 0x64, 0x65, 0x78, 0x4d, 
    0x76, 0x74, 0x4c, 0x65, 0x56, 0x31, 0x70, 0x52, 
};
 
static const u8_t net_key[16] = {
    0x7a, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 
    0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b, 
};


static const u8_t app_idx_1 = 1;
 
static const u8_t app_key_1[16] = {
    0x02, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
    0x02, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
};


static u16_t _net_idx = 0;
static const u32_t iv_index = 0;
static u8_t flags;
 
// static u16_t addr = 0x7EB9;//0x7F0E;
static u16_t addr;
 
static bool setting_loading = false;
 
static struct bt_mesh_cfg_srv cfg_srv = {
    .relay = BT_MESH_RELAY_ENABLED,
    .beacon = BT_MESH_BEACON_ENABLED,
#if defined(CONFIG_BT_MESH_FRIEND)
    .frnd = BT_MESH_FRIEND_ENABLED,
#else
    .frnd = BT_MESH_FRIEND_NOT_SUPPORTED,
#endif
#if defined(CONFIG_BT_MESH_GATT_PROXY)
    .gatt_proxy = BT_MESH_GATT_PROXY_ENABLED,
#else
    .gatt_proxy = BT_MESH_GATT_PROXY_NOT_SUPPORTED,
#endif
    .default_ttl = 4,
 
    /* 1 transmissions with 20ms interval */
    .net_transmit = BT_MESH_TRANSMIT(3, 20),
    .relay_retransmit = BT_MESH_TRANSMIT(3, 20),
};
 
 
static struct bt_mesh_health_srv health_srv = {
};

static struct bt_mesh_gen_onoff_srv mesh_checkcard_srv[] = {
    {.set_func = mesh_checkcard_set_func, .get_func = NULL, .user_data = NULL},
};
 
BT_MESH_HEALTH_PUB_DEFINE(health_pub, 0);
 
static struct bt_mesh_cfg_cli cfg_cli = {
};
 
 
static struct bt_mesh_vng_srv vng_srvs[] = {
    {.user_data = NULL},
};
BT_MESH_MODEL_PUB_DEFINE(nfc_srv_pub, NULL, 3 + 15);
 
 
static struct mesh_status mesh_status = {
    .period = STATUS_RANDOM_RANGE,
};
 
static struct bt_mesh_vng_status vng_status[] = {
    {.user_data = &mesh_status, .set_func = mesh_status_set_func},
};
 
static struct bt_mesh_vng_command_srv vng_command_srvs[] = {
    {.notify = mesh_reader_notify_set_func, .user_data = NULL},
};
 
BT_MESH_MODEL_PUB_DEFINE(nfc_status_pub, NULL, 3 + 15);
 
static struct bt_mesh_model vnd_models0[] = {
    BT_MESH_MODEL_VNG_SRV(&vng_srvs[0], &nfc_srv_pub),
    BT_MESH_MODEL_VNG_STATUS(&vng_status[0], &nfc_status_pub),
    BT_MESH_MODEL_VNG_COMMAND_SRV(&vng_command_srvs[0]),
    
};

static struct bt_mesh_model models0[] = {
    BT_MESH_MODEL_CFG_SRV(&cfg_srv),
    BT_MESH_MODEL_CFG_CLI(&cfg_cli),
    BT_MESH_MODEL_HEALTH_SRV(&health_srv, &health_pub),
    BT_MESH_MODEL_GEN_ONOFF_SRV(&mesh_checkcard_srv[0], NULL),
 
};
 
static struct bt_mesh_elem elems[] = {
    BT_MESH_ELEM(0, models0, vnd_models0),
};
 
static const struct bt_mesh_comp comp = {
    .cid = CID_VNG,
    .elem = elems,
    .elem_count = ARRAY_SIZE(elems),
}; 

void _prov_complete(u16_t net_idx, u16_t addr)
{
    _net_idx = net_idx;
    if(setting_loading)
        return;
    
    SYS_LOG_INF("Provisioned completed");
 
    bool vnd = false;
    u16_t primary_addr;
 
     /* Add Application Key */
    bt_mesh_cfg_app_key_add(net_idx, elems[0].addr,
                net_idx, app_idx_0, app_key_0, NULL);
 
    bt_mesh_cfg_app_key_add(net_idx, elems[0].addr,
                net_idx, app_idx_1, app_key_1, NULL);
 
    MODELS_BIND(models0, vnd , net_idx, app_idx_0);

     vnd = true;
    MODELS_BIND(vnd_models0, vnd, net_idx, app_idx_0);
    MODELS_BIND(vnd_models0, vnd, net_idx, app_idx_1);
 
    struct bt_mesh_cfg_mod_pub nfc_srv_pub = {
        .addr = GATEWAY_GROUP_ADDR,
        .app_idx = app_idx_0,
        .ttl = 7,
        .transmit = BT_MESH_PUB_TRANSMIT(0, 50),
    };
 
    MODEL_PUB(vnd_models0[0], vnd, net_idx, &nfc_srv_pub);
 
    struct bt_mesh_cfg_mod_pub nfc_status_pub = {
        .addr = STATUS_DST_GROUP_ADDR,
        .app_idx = app_idx_1,
        .ttl = 7,
        .transmit = BT_MESH_PUB_TRANSMIT(0, 50),
    };
 
    MODEL_PUB(vnd_models0[1], vnd, net_idx, &nfc_status_pub);
 
    primary_addr = bt_mesh_primary_addr();
    /* Add model subscription */
    bt_mesh_cfg_mod_sub_add_vnd(net_idx, primary_addr, primary_addr,
                STATUS_GROUP_ADDR, BT_MESH_MODEL_ID_VNG_STATUS, CID_VNG, NULL);
 
     bt_mesh_cfg_mod_sub_add_vnd(net_idx, primary_addr, primary_addr,
                VNG_MESH_RENEW_GROUP, BT_MESH_MODEL_ID_VNG_COMMAND_SRV, CID_VNG, NULL);
}
 
 
static void prov_reset(void) 
{
    bt_mesh_prov_enable(BT_MESH_PROV_ADV | BT_MESH_PROV_GATT);
}

static int cache_index(u8_t* card_id) 
{ 
    for(int i =0; i< CACHE_SIZE ; i++){ 
 
        if(strncmp(card_id, cache_id.ids[i].data, CARD_ID_LEN) == 0){ 
            return i; 
        } 
    } 
 
    return -1; 
 
} 
 
void cache_add(u8_t* card_id) { 
 
    memcpy(cache_id.ids[cache_id.index].data, card_id, CARD_ID_LEN); 
    cache_id.ids[cache_id.index].last_time = k_uptime_get_32(); 
    cache_id.index = (cache_id.index + 1) % CACHE_SIZE; 
 
} 

bool is_time_out(u8_t cid) { 
    u32_t current; 
    u32_t timeout; 
    current = k_uptime_get_32(); 
 
    if(current < cache_id.ids[cid].last_time) { 
 
        cache_id.ids[cid].last_time = current; 
        return true; 
    }else if(current > cache_id.ids[cid].last_time){ 
 
        timeout = current - cache_id.ids[cid].last_time; 
        if(timeout > SPAM_TIME_OUT) { 
 
            cache_id.ids[cid].last_time = current; 
            return true; 
        } 
        else { 
            return false; 
        }    
    } 
    //same timestamp 
    return false; 
} 

static void nfc_data_fill(struct net_buf_simple* buf)
{
 
    if(buf->len >= 7)
        return;
    u8_t padding = 7 - buf->len;
 
    if(padding == 0) 
        return;
 
    for(int i=0; i<padding; i++) {
        net_buf_simple_push_u8(buf, 0x00);
    }
}
 
 static void nfc_transmit(struct net_buf* buf )
{
    int err;
    u16_t cal_crc, crc;
     int cid; 
    
    if(buf->len <= 3) {
         SYS_LOG_WRN("Data len is too short");
         return;
     }
 
    crc = (u16_t)(buf->data[buf->len-2]) << 8 | buf->data[buf->len-1];
    cal_crc = crc16_ccitt(0, buf->data, buf->len-2);
    if(crc != cal_crc) {
        SYS_LOG_ERR("cal_crc[0x%04x], crc[0x%04x]", cal_crc, crc);
        return;
    }
 
    if (!bt_mesh_is_provisioned()) {
          SYS_LOG_WRN("Device still not provisioned");  
          return;
    }

    bool valid = false; 
 
    cid = cache_index(buf->data); 
 
    if(cid == -1) { 
        //not in cache 
        cache_add(buf->data); 
        valid = true; 
    } else { 
        // check timm out 
        if(is_time_out(cid)){ 
            valid = true; 
        } 
    } 
    if(!valid) 
        return; 
    
    checckcard.tid++;
    if(checckcard.tid > 65535){
        checckcard.tid =0;
    }
 
    NET_BUF_SIMPLE_DEFINE(msg, 15);
    // net_buf_simple_add_mem(&msg, 0x00, buf->len-2);
 
    
    net_buf_simple_add_mem(&msg, buf->data, buf->len-2);
    net_buf_simple_add_be16(&msg, checckcard.tid);
    nfc_data_fill(&msg);
    
    BT_DBG("publish buf [%s]", bt_hex(msg.data, msg.len));
    //err = bt_mesh_nfc_publish(&vnd_models0[0], &msg);
 
    vnd_models0[0].pub->ttl = bt_mesh_default_ttl_get();
    err = bt_mesh_nfc_publish(&vnd_models0[0], &msg);

    if(notify_buzzer_set.success == 1){

                buzzer_notify_set(notify_buzzer_set.success_repeat);
            }
    checckcard.is_card_err = true; 

    if(err) {
 
        return ;
        BT_ERR("NFC publish buf len [%d] failed", msg.len);
    }
    else {
        BT_DBG("NFC publish buf len [%d] successed", msg.len);
    }
 
    nfc_notify_led();  
}

static void mesh_status_set_func(u16_t time, u16_t main, u16_t duration, u16_t period)
{
 
    u16_t primary_addr;
 
 
    primary_addr = bt_mesh_primary_addr();
    
 
    SYS_LOG_DBG("index: %u, mod: %u, period: %u", time, main, duration);
    if(main == 0) {
        SYS_LOG_DBG("mod = 0 is invalid" );
        return;   
    }
    if(primary_addr % main == time) {
       status_prepare(0, K_SECONDS(duration));
    }
   
}

static void mesh_checkcard_set_func (struct net_buf_simple *buf ,void *user_data)
{
    u8_t target;
    u16_t tid_get ; 
    u32_t current = k_uptime_get_32();
    target = net_buf_simple_pull_u8(buf);
    tid_get = net_buf_simple_pull_be16(buf); 

    SYS_LOG_DBG("target: %u",target);
    SYS_LOG_DBG("tid_get: %u",tid_get); 
    SYS_LOG_DBG("tid_get: %u",checckcard.tid); 

    if(checckcard.last_time > current) {
        checckcard.last_time =0;
    }
 
 
    if(current - checckcard.last_time >=1500) {

        checckcard.last_time = current;
        if(target == 1 && (checckcard.tid == tid_get) && (checckcard.is_card_err == true)){

            if(notify_buzzer_set.failed == 1) {

                // buzzer_notify();
                SYS_LOG_DBG("reader failed notify");
                k_sleep(K_MSEC(1000));
                buzzer_notify_set(notify_buzzer_set.fail_repeat);
            }
            
            checckcard.is_card_err = false; 
            SYS_LOG_DBG("checkcard-ok");
        }
        else SYS_LOG_DBG("check_card falied ");
    }
     
    
}

static void mesh_reader_notify_set_func(u8_t success, u8_t failed, u8_t success_repeat, u8_t fail_repeat, void *user_data)
{
    

    SYS_LOG_DBG("success: %u",success);
    SYS_LOG_DBG("success_repeat: %u",success_repeat); 
    SYS_LOG_DBG("failed: %u",failed);
    SYS_LOG_DBG("fail_repeat: %u",fail_repeat); 

    notify_buzzer_set.success = success;
    notify_buzzer_set.success_repeat = success_repeat;
    notify_buzzer_set.failed = failed;
    notify_buzzer_set.fail_repeat = fail_repeat;


    bt_mesh_model_setting_val_update(&vnd_models0[2], (const u8_t*)(&notify_buzzer_set) , sizeof(notify_buzzer_set));
}

static void load_setting_reader_notify()
{

    if (vnd_models0[2].setting_val.val->len < sizeof(notify_buzzer_set)) {
        // load failed.
        SYS_LOG_DBG("false load_setting_reader_notify: ");
        return;
    }

    memcpy(&notify_buzzer_set, vnd_models0[2].setting_val.val->data, sizeof(notify_buzzer_set));
};

static void status_init()
{
    k_delayed_work_init(&mesh_status.timer, status_signal);
    status_prepare(0, K_SECONDS(STATUS_RANDOM_RANGE));
}
 
static void status_signal(struct k_work* work)
{
    struct net_buf* buf;
    buf = net_buf_alloc(&mesh_pool, K_NO_WAIT);
    if(buf == NULL) {
        SYS_LOG_ERR("Out of pool");
        return;
    }
    net_buf_add_u8(buf, TYPE_STATUS);
    net_buf_put(_mesh.tx_queue, buf);
    
}

static void status_prepare(u32_t ms_min, u32_t ms_max)
{
    int err;
 
    err = rand_number_blocking_generate(ms_min, ms_max, 
                                        &mesh_status.next_period);
    if(err) {
        SYS_LOG_ERR("RNG failed\n");
        mesh_status.next_period = ms_max;
    }
 
    SYS_LOG_DBG("Next status: %u", mesh_status.next_period);
    k_delayed_work_cancel(&mesh_status.timer);
    k_delayed_work_submit(&mesh_status.timer, mesh_status.next_period);
}

static void status_transmit(struct net_buf* buf) 
{
    int err;
    struct net_buf_simple *msg = vnd_models0[1].pub->msg;
    u32_t current;
    /*Send status in timeout from last msg*/
    status_prepare(K_SECONDS(STATUS_INTERVAL_MAX), K_SECONDS(STATUS_INTERVAL_MAX*2));
 
    if(!bt_mesh_is_provisioned()) {
        SYS_LOG_ERR("Device is still not provisioned\n");
        return;
    }
 
    if(msg == NULL) {
         SYS_LOG_ERR("No pub ctx\n");
        return;
    }
 
    current = k_uptime_get_32();
    if(mesh_status.last_status > current) {
        // mesh_status.last_status wrapped
        mesh_status.last_status = 0;
    }
    if(mesh_status.last_status != 0 && current > mesh_status.last_status && 
            (current - mesh_status.last_status < K_SECONDS(STATUS_INTERVAL_MIN))) {
        SYS_LOG_WRN("Too early, elapsed: %u ms", current - mesh_status.last_status);
        return;
    }
    mesh_status.last_status = current;
    bt_mesh_model_msg_init(msg, BT_MESH_MODEL_OP_VNG);
    u16_t primary_addr = bt_mesh_primary_addr();
    net_buf_simple_add_u8(msg, 0xFF); // add data vao goi msg
    net_buf_simple_add_be16(msg, DEVICE_ID);
    net_buf_simple_add_be16(msg, primary_addr);
 
    vnd_models0[1].pub->ttl = bt_mesh_default_ttl_get();
    
    err = bt_mesh_model_publish(&vnd_models0[1]);
    if(err) {
        BT_ERR("NFC publish status failed");
    }
 
}

static void mesh_uart_decoce(u8_t byte) {
    if (_mesh.rx_buf == NULL)
        _mesh.rx_buf = net_buf_alloc(&mesh_pool, K_NO_WAIT);
 
    if (_mesh.rx_buf == NULL)
        return;
 
    switch (byte) {
    case SLIP_ESC:
        _mesh.last_char = byte;
        break;
    case SLIP_END:
        _mesh.last_char = byte;
        /* Queue to rx queue */
        net_buf_push_u8(_mesh.rx_buf, TYPE_NFC);
        net_buf_put(_mesh.tx_queue, _mesh.rx_buf);
        _mesh.rx_buf = net_buf_alloc(&mesh_pool, K_NO_WAIT);
        break;
    default:
        if(_mesh.last_char == SLIP_ESC) {
            _mesh.last_char = byte;
            /* Previous read byte was an escape byte, so this byte will be
             * interpreted differently from others. */
            switch(byte) {
            case SLIP_ESC_END:
                byte = SLIP_END;
                break;
            case SLIP_ESC_ESC:
                byte = SLIP_ESC;
                break;
            case SLIP_ESC_XON:
                byte = XON;
                break;
            case SLIP_ESC_XOFF:
                byte = XOFF;
                break;
            }
        } else {
            _mesh.last_char = byte;
        }
 
        if(_mesh.rx_buf->len >= BT_MESH_BUF_SIZE) {
            net_buf_reset(_mesh.rx_buf);
        }
 
        net_buf_add_u8(_mesh.rx_buf, byte);
        k_delayed_work_cancel(&_mesh.uart_timer);
        k_delayed_work_submit(&_mesh.uart_timer, 300);
        break;
    }
}
 
static void uart_rx_timeout(struct k_work* work)
{
    /*re*/
    if(!_mesh.rx_buf)
        return;
    if(_mesh.rx_buf->len == 0)
        return;
    SYS_LOG_INF("reset uart rx buf");
    net_buf_reset(_mesh.rx_buf);
}

static void mesh_uart_isr(struct device *unused)
{
    
    ARG_UNUSED(unused);
    // SYS_LOG_INF("");
    while (uart_irq_update(uart) && uart_irq_is_pending(uart)) {
        int rx;
        u8_t byte;
        if (uart_irq_rx_ready(uart) == 0) {
            if (uart_irq_tx_ready(uart) != 0) {
                // SYS_LOG_INF("transmit ready");
            } else {
                SYS_LOG_INF("spurious interrupt");
            }
            /* Only the UART RX path is interrupt-enabled */
            break;
        }
 
        rx = uart_fifo_read(uart, &byte, sizeof(byte));
 
        if (rx == 0)
            break;
 
        mesh_uart_decoce(byte);
        
    }
}
static int mesh_uart_init()
{
    SYS_LOG_INF("Start");
 
    uart = device_get_binding(CONFIG_UART_0_NAME);
    if (!uart) {
        SYS_LOG_ERR("Failed");
        return -EINVAL;
    }
 
    uart_irq_rx_disable(uart);
    uart_irq_tx_disable(uart);
 
    uart_irq_callback_set(uart, mesh_uart_isr);
 
    uart_irq_rx_enable(uart);
 
    SYS_LOG_INF("Successed, pin_tx %d, pin_rx %d", CONFIG_UART_0_NRF_TX_PIN, CONFIG_UART_0_NRF_RX_PIN);
 
    k_delayed_work_init(&_mesh.uart_timer, uart_rx_timeout);
    
    return 0;
}

static void tx_thread(void *p1, void *p2, void *p3)
{
    while (1) {
        struct net_buf *buf;
        u8_t type;
        /* Wait until a buffer is available */
        buf = net_buf_get(_mesh.tx_queue, K_FOREVER);
        type = net_buf_pull_u8(buf);
        switch(type) {
            case TYPE_NFC:  
                nfc_transmit(buf);
                break;
            case TYPE_STATUS: 
                status_transmit(buf);
                break;
            default:
                break;
        }
       
        net_buf_unref(buf);
 
        /* Give other threads a chance to run if _tx_queue keeps getting
         * new data all the time.
         */
        k_yield();
    }
}

// khoi tao model .
static void model_init()
{
    bt_mesh_vng_srv_init(&vnd_models0[0]); //NFC
    bt_mesh_vng_status_init(&vnd_models0[1]);//STATUS
    bt_mesh_vng_cmd_srv_init(&vnd_models0[2], true);
    bt_mesh_gen_onoff_srv_init(&models0[3],true);//
    BT_MESH_MODEL_SETTING_VAL_DEFINE(ena_setting_reader,
                                     (&vnd_models0[2]),
                                     sizeof(notify_buzzer_set) + 10);
    status_init();

}
 
 
 
static void bt_ready(int err)
{
 
    struct bt_le_oob oob;
 
    if (err) {
        SYS_LOG_ERR("Bluetooth init failed (err %d)", err);
        return;
    }
 
    SYS_LOG_INF("Bluetooth initialized");
 
    err = bt_le_oob_get_local(BT_ID_DEFAULT, &oob);
 
    if (err != 0) {
        SYS_LOG_ERR("get local oob failed with error: %d", err);
        return;
    }
     memset(uuid, 0, sizeof(uuid));
    memcpy(uuid, oob.addr.a.val, 6);
 
    err = bt_mesh_init(&prov, &comp);
 
    if (err) {
        SYS_LOG_ERR("Initializing mesh failed (err %d)", err);
        return;
    }
 
    SYS_LOG_DBG("Mesh initialized\n");
 
 
    if (IS_ENABLED(CONFIG_BT_SETTINGS)) {
        SYS_LOG_DBG("Loading stored settings\n");
        setting_loading = true;
        settings_load();
        setting_loading = false;
    }

    load_setting_reader_notify();

    addr = flash_unicast_get();
 
    SYS_LOG_DBG("UNICAST: 0x%04x", addr);

    err = bt_mesh_provision(net_key, _net_idx, flags, iv_index, addr,
                dev_key);
    if (err == -EALREADY) {
        printk("Using stored settings\n");
    } else if (err) {
        printk("Provisioning failed (err %d)\n", err);
        return;
    }
 
}

static int mesh_devices_init(struct device* unused)
{
    mesh_uart_init();
    return 0;
}
 
 
DEVICE_INIT(mesh_devices,
        "mesh_devices",
        mesh_devices_init,
        NULL,
        NULL,
        APPLICATION,
        CONFIG_KERNEL_INIT_PRIORITY_DEVICE);


void main(void)
{
    THREADS_MONITOR_MAIN_START
    int err;
    SYS_LOG_INF("Start");
 
    /* Initialize the Bluetooth Subsystem */
 
    model_init();
 
    err = bt_enable(bt_ready);
 
    if (err) {
        SYS_LOG_ERR("Bluetooth init failed (err %d)", err);
        return;
    }
 
    /* Spawn the TX thread and start feeding commands and data to the
     * controller
     */
    k_thread_create(&tx_thread_handle,
                    tx_thread_stack,
                    K_THREAD_STACK_SIZEOF(tx_thread_stack),
                    tx_thread,
                    NULL,
                    NULL,
                    NULL,
                    K_PRIO_COOP(7),
                    0,
                    K_NO_WAIT);
 
    THREADS_MONITOR_MAIN_TERMINAL
}