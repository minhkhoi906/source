
#include <zephyr.h>
#include <device.h>
#include <watchdog.h>
#include <misc/printk.h>
#include <gpio.h>
#include <board.h>
#include <logging/sys_log.h>
#include <debug/object_tracing.h>
#include <pwm.h>
#include "app.h"
#include <entropy.h>

#define SYS_LOG_LEVEL 4


#define THREAD_TRACKING_INTERVAL   3000 //ms
#define NFC_NOTIFY_DURATION 	   50
#define NFC_NOTIFY_LED_DURATION 	1000
#define NFC_BUZZER_CYCLES_PER_SECOND (1000000/2700)


struct device* entropy;

static u8_t threads_count;
struct k_delayed_work thread_timer;

#ifdef CONFIG_WDT_NRFX
struct {
	struct k_delayed_work feed_work;
	struct device* dev;
	int channel_id;
} mesh_wdt;

static void mesh_wdt_feed(struct k_work* work)
{
	wdt_feed(mesh_wdt.dev, mesh_wdt.channel_id);
	k_delayed_work_submit(&mesh_wdt.feed_work, 1000);
}

static int mesh_wdt_init() 
{
	SYS_LOG_DBG("");
	mesh_wdt.dev = device_get_binding(CONFIG_WDT_0_NAME);
	if(!mesh_wdt.dev)
		return 1;
	struct wdt_timeout_cfg cfg = {
		.window = {0, 10000},
		.callback = NULL,
		.flags = WDT_FLAG_RESET_SOC
	};

	mesh_wdt.channel_id = wdt_install_timeout(mesh_wdt.dev, &cfg);

	if(mesh_wdt.channel_id < 0)
		return 1;
	wdt_setup(mesh_wdt.dev, 0);

	k_delayed_work_init(&mesh_wdt.feed_work, mesh_wdt_feed);
	k_delayed_work_submit(&mesh_wdt.feed_work, 1000); 
	return 0;
}

#endif 



static inline int threads_count_get(void)
{
    int obj_counter = 0;
    struct k_thread *thread_list = NULL;

    /* wait a bit to allow any initialization-only threads to terminate */
    thread_list   = (struct k_thread *)SYS_THREAD_MONITOR_HEAD;
    while (thread_list != NULL) {
        thread_list =
            (struct k_thread *)SYS_THREAD_MONITOR_NEXT(thread_list);
        obj_counter++;
    } 
    return obj_counter;
}




static void thread_tracker(struct k_work* work)
{	
	static u8_t threads;
    threads = threads_count_get();
    // SYS_LOG_DBG("work threads: %d", threads);
    if(threads < threads_count) {
  		SYS_LOG_DBG("System reset");
    	NVIC_SystemReset();
    }
  	if(threads > threads_count) {
    	threads_count = threads;
    }

    k_delayed_work_submit(&thread_timer, THREAD_TRACKING_INTERVAL);
}

void threads_monitor_main_start()
{	

	threads_count = threads_count_get();
	SYS_LOG_DBG("threads_count: %d", threads_count);
	k_delayed_work_init(&thread_timer, thread_tracker);
	k_delayed_work_submit(&thread_timer, THREAD_TRACKING_INTERVAL);
}

void threads_monitor_main_termial()
{
	/*
	 *this function is called at end of main 
	 *so main thread is terminating
	 */
	threads_count = threads_count_get() - 1;
	SYS_LOG_DBG("threads_count: %d", threads_count);
}






struct nfc_notify_ctx 
{
	struct k_delayed_work timer;

	struct device* buzzer;
	//struct device* led;

	u32_t buzzer_period;
};

struct nfc_notify_led_ctx {

	struct k_delayed_work timer_led;
	struct device* led;
};

static struct nfc_notify_ctx nfc_notify_ctx;
static struct nfc_notify_led_ctx nfc_notify_led_ctx;



static void nfc_buzzer_notify(bool on)
{
	if(on) {
		if(nfc_notify_ctx.buzzer != NULL) {
			pwm_pin_set_usec(nfc_notify_ctx.buzzer, PIN_BUZZER_CTRL, 
					NFC_BUZZER_CYCLES_PER_SECOND, (NFC_BUZZER_CYCLES_PER_SECOND/2));	
		}
	} else {
		if(nfc_notify_ctx.buzzer != NULL) {
			pwm_pin_set_usec(nfc_notify_ctx.buzzer, PIN_BUZZER_CTRL, 
					NFC_BUZZER_CYCLES_PER_SECOND, 0);	
		}
	}
}

static void nfc_led_notify(bool on)
{
	if(on) {
		if(nfc_notify_led_ctx.led != NULL) {
			SYS_LOG_DBG("led on");
			gpio_pin_write(nfc_notify_led_ctx.led, PIN_LED_RED_CTRL, 1);
			gpio_pin_write(nfc_notify_led_ctx.led, PIN_LED_GREEN_CTRL, 0);
		}
	} else {
		if(nfc_notify_led_ctx.led != NULL) {
			SYS_LOG_DBG("led off");
			gpio_pin_write(nfc_notify_led_ctx.led, PIN_LED_RED_CTRL, 0);
			gpio_pin_write(nfc_notify_led_ctx.led, PIN_LED_GREEN_CTRL, 1);
		}
	}
}


static void nfc_notify_buzzer_off(struct k_work* work)
{
	SYS_LOG_DBG("");
	nfc_buzzer_notify(false);
	//nfc_led_notify(false);
}

static void nfc_notify_led_off(struct k_work* work){

	SYS_LOG_DBG("");
	nfc_led_notify(false);
}

static int nfc_notify_init()
{
	SYS_LOG_DBG("");
	k_delayed_work_init(&nfc_notify_ctx.timer, nfc_notify_buzzer_off);
	k_delayed_work_init(&nfc_notify_led_ctx.timer_led, nfc_notify_led_off);
	nfc_notify_ctx.buzzer = device_get_binding(CONFIG_PWM_NRF5_SW_0_DEV_NAME);
	

	// u64_t pwm_cycles_per_sec;
	// pwm_get_cycles_per_sec(nfc_notify_ctx.buzzer, PIN_BUZZER_CTRL, &pwm_cycles_per_sec);
	//nfc_notify_ctx.buzzer_period = 1000000/NFC_BUZZER_CYCLES_PER_SECOND;

	if(nfc_notify_ctx.buzzer == NULL) {
		SYS_LOG_ERR("Not found pwm device");
		return 1;
	}

	// SYS_LOG_DBG("buzzer period: %u", nfc_notify_ctx.buzzer_period);

	nfc_notify_led_ctx.led = device_get_binding(CONFIG_GPIO_P0_DEV_NAME);
	if(nfc_notify_led_ctx.led == NULL) {
		SYS_LOG_ERR("Not found gpio device");
		return 1;
	}
	gpio_pin_configure(nfc_notify_led_ctx.led, PIN_LED_GREEN_CTRL, GPIO_DIR_OUT);
	gpio_pin_configure(nfc_notify_led_ctx.led, PIN_LED_RED_CTRL, GPIO_DIR_OUT);

	nfc_notify_buzzer_off(NULL);
	nfc_notify_led_off(NULL);

	return 0;

}

void nfc_notify()
{
	SYS_LOG_DBG("");
	nfc_buzzer_notify(true);
	//nfc_led_notify(true);
	k_delayed_work_submit(&nfc_notify_ctx.timer, NFC_NOTIFY_DURATION);
}

void nfc_notify_led(){

	nfc_led_notify(true);
	k_delayed_work_submit(&nfc_notify_led_ctx.timer_led, NFC_NOTIFY_LED_DURATION);
}

void buzzer_notify_set(u8_t number_notify){

	// k_sleep(K_MSEC(500));
	for(int i =0;i < number_notify; i++){
		nfc_buzzer_notify(true);
		k_sleep(K_MSEC(50));
		nfc_buzzer_notify(false);
		k_sleep(K_MSEC(50));
	}
}


void buzzer_notify()
{
	k_sleep(K_MSEC(1000));
	for(int i =0 ;i<3 ; i++) {

		nfc_buzzer_notify(true);
		k_sleep(K_MSEC(50));
		nfc_buzzer_notify(false);
		k_sleep(K_MSEC(50));
	}
}

static void entropy_init()
{
    entropy = device_get_binding(CONFIG_ENTROPY_NAME);
    if(entropy == NULL) {
        printk("Entropy eevice not found\n");
        return;
    }
}


int board_init(struct device* unused)
{
	SYS_LOG_DBG("");
	struct device* gpio;
	int err;

#ifdef CONFIG_WDT_NRFX

	mesh_wdt_init();	
#endif
	gpio = device_get_binding(CONFIG_GPIO_P0_DEV_NAME);
	if (!gpio) {
		printk("Cannot find %s!\n", CONFIG_GPIO_P0_DEV_NAME);
		return 1;
	}
#ifdef PIN_BYPASS
	gpio_pin_configure(gpio, PIN_BYPASS, GPIO_DIR_OUT);
	gpio_pin_write(gpio, PIN_BYPASS, 0);
#endif

	entropy_init();

	err = nfc_notify_init();


	return err;
}


DEVICE_INIT(
	mesh_board,
	"mesh_board",
	board_init,
	NULL,
	NULL,
	APPLICATION,
	CONFIG_KERNEL_INIT_PRIORITY_DEVICE);


int rand_number_blocking_generate(u32_t min, u32_t max, u32_t* rn)
{
    int err;
    u32_t rand;

    if(entropy == NULL) {
        return -1;
    }

    err = entropy_get_entropy(entropy, (u8_t*)(&rand), sizeof(rand));
    if(err) {
        SYS_LOG_ERR("Rng failed");
        return err;
    }

    *rn = min + (rand % (max - min));
    return 0;

}