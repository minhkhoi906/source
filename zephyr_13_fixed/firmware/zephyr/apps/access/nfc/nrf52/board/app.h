

#ifndef APP_H
#define APP_H



#define THREADS_MONITOR_MAIN_START 	  threads_monitor_main_start();
#define THREADS_MONITOR_MAIN_TERMINAL threads_monitor_main_termial();
/*
	should be called at start of main function.
*/
void threads_monitor_main_start();

/*
	should be called at end of main function.
*/
void threads_monitor_main_termial();


void nfc_notify();
void buzzer_notify();
void nfc_notify_led();
void buzzer_notify_set(u8_t number_notify);

/*
	it take 1 ms to wait for new random number
*/
int rand_number_blocking_generate(u32_t min, u32_t max, u32_t* rn);

#endif //APP_H