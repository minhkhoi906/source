
#include <zephyr.h>
#include <device.h>
#include <watchdog.h>
#include <misc/printk.h>
#include <gpio.h>
#include <board.h>
#include <logging/sys_log.h>
#include <debug/object_tracing.h>
#include "app.h"

#define SYS_LOG_LEVEL 4



#define THREAD_TRACKING_INTERVAL 3000 //ms
#define NFC_NOTIFY_DURATION		 100


static u8_t threads_count;
struct k_delayed_work thread_timer;

#ifdef CONFIG_WDT_NRFX
struct {
	struct k_delayed_work feed_work;
	struct device* dev;
	int channel_id;
} mesh_wdt;

static void mesh_wdt_feed(struct k_work* work)
{
	wdt_feed(mesh_wdt.dev, mesh_wdt.channel_id);
	k_delayed_work_submit(&mesh_wdt.feed_work, 1000);
}

static int mesh_wdt_init() 
{
	SYS_LOG_DBG("");
	mesh_wdt.dev = device_get_binding(CONFIG_WDT_0_NAME);
	if(!mesh_wdt.dev)
		return 1;
	struct wdt_timeout_cfg cfg = {
		.window = {0, 5000},
		.callback = NULL,
		.flags = WDT_FLAG_RESET_SOC
	};

	mesh_wdt.channel_id = wdt_install_timeout(mesh_wdt.dev, &cfg);

	if(mesh_wdt.channel_id < 0)
		return 1;
	wdt_setup(mesh_wdt.dev, 0);

	k_delayed_work_init(&mesh_wdt.feed_work, mesh_wdt_feed);
	k_delayed_work_submit(&mesh_wdt.feed_work, 1000); 
	return 0;
}

#endif 



static inline int threads_count_get(void)
{
    int obj_counter = 0;
    struct k_thread *thread_list = NULL;

    /* wait a bit to allow any initialization-only threads to terminate */
    thread_list   = (struct k_thread *)SYS_THREAD_MONITOR_HEAD;
    while (thread_list != NULL) {
        thread_list =
            (struct k_thread *)SYS_THREAD_MONITOR_NEXT(thread_list);
        obj_counter++;
    } 
    return obj_counter;
}




static void thread_tracker(struct k_work* work)
{	
	static u8_t threads;
    threads = threads_count_get();
    // SYS_LOG_DBG("work threads: %d", threads);
    if(threads < threads_count) {
  		SYS_LOG_DBG("System reset");
    	NVIC_SystemReset();
    }
  	if(threads > threads_count) {
    	threads_count = threads;
    }

    k_delayed_work_submit(&thread_timer, THREAD_TRACKING_INTERVAL);
}

void threads_monitor_main_start()
{	

	threads_count = threads_count_get();
	SYS_LOG_DBG("threads_count: %d", threads_count);
	k_delayed_work_init(&thread_timer, thread_tracker);
	k_delayed_work_submit(&thread_timer, THREAD_TRACKING_INTERVAL);
}

void threads_monitor_main_termial()
{
	/*
	 *this function is called at end of main 
	 *so main thread is terminating
	 */
	threads_count = threads_count_get() - 1;
	SYS_LOG_DBG("threads_count: %d", threads_count);
}



struct nfc_notify_ctx 
{
	struct k_delayed_work timer;
	struct device* gpio;

};

static struct nfc_notify_ctx nfc_notify_ctx;


static void do_nfc_notify(bool on)
{
	if(on) {
		if(nfc_notify_ctx.gpio != NULL) {
			printk("do_nfc_notify: on\n");
			gpio_pin_write(nfc_notify_ctx.gpio, PIN_LED_RED_CTRL, 0);
			gpio_pin_write(nfc_notify_ctx.gpio, PIN_LED_GREEN_CTRL, 1);
			gpio_pin_write(nfc_notify_ctx.gpio, PIN_BUZZER_CTRL, 1);
		}
	} else {
		if(nfc_notify_ctx.gpio != NULL) {
			printk("do_nfc_notify: off\n");
			gpio_pin_write(nfc_notify_ctx.gpio, PIN_LED_RED_CTRL, 1);
			gpio_pin_write(nfc_notify_ctx.gpio, PIN_LED_GREEN_CTRL, 0);
			gpio_pin_write(nfc_notify_ctx.gpio, PIN_BUZZER_CTRL, 0);
		}
	}
}


static void nfc_notify_off(struct k_work* work)
{
	SYS_LOG_DBG("");
	do_nfc_notify(false);
}

static int nfc_notify_init()
{
	SYS_LOG_DBG("");
	k_delayed_work_init(&nfc_notify_ctx.timer, nfc_notify_off);
	nfc_notify_ctx.gpio = device_get_binding(CONFIG_GPIO_P0_DEV_NAME);


	if(nfc_notify_ctx.gpio == NULL) {
		SYS_LOG_ERR("Not found gpio device");
		return 1;
	}

	gpio_pin_configure(nfc_notify_ctx.gpio, PIN_BUZZER_CTRL, GPIO_DIR_OUT);
	gpio_pin_configure(nfc_notify_ctx.gpio, PIN_LED_GREEN_CTRL, GPIO_DIR_OUT);
	gpio_pin_configure(nfc_notify_ctx.gpio, PIN_LED_RED_CTRL, GPIO_DIR_OUT);

	do_nfc_notify(false);

	return 0;

}

void nfc_notify()
{
	SYS_LOG_DBG("");
	do_nfc_notify(true);
	k_delayed_work_submit(&nfc_notify_ctx.timer, NFC_NOTIFY_DURATION);
}



int board_init(struct device* unused)
{
	SYS_LOG_DBG("");
	struct device* gpio;
	int err;

#ifdef CONFIG_WDT_NRFX
	mesh_wdt_init();	
#endif

	gpio = device_get_binding(CONFIG_GPIO_P0_DEV_NAME);
	if (!gpio) {
		printk("Cannot find %s!\n", CONFIG_GPIO_P0_DEV_NAME);
		return 1;
	}

#ifdef PIN_BYPASS
	gpio_pin_configure(gpio, PIN_BYPASS, GPIO_DIR_OUT);
	gpio_pin_write(gpio, PIN_BYPASS, 0);
#endif

	gpio_pin_configure(gpio, PIN_BLE_LNA_EN, GPIO_DIR_OUT);
	gpio_pin_write(gpio, PIN_BLE_LNA_EN, 1);
	
	SYS_LOG_DBG("Clear UART_CTS: %u", CONFIG_UART_0_NRF_CTS_PIN);
	gpio_pin_configure(gpio, CONFIG_UART_0_NRF_CTS_PIN, GPIO_DIR_OUT);
	gpio_pin_write(gpio, CONFIG_UART_0_NRF_CTS_PIN, 0);	

	err = nfc_notify_init();

	return err;
	
}


DEVICE_INIT(
	mesh_board,
	"mesh_board",
	board_init,
	NULL,
	NULL,
	APPLICATION,
	CONFIG_KERNEL_INIT_PRIORITY_DEVICE);



