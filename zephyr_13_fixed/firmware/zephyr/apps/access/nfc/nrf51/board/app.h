

#ifndef APP_H
#define APP_H



#define THREADS_MONITOR_MAIN_START 	  threads_monitor_main_start();
#define THREADS_MONITOR_MAIN_TERMINAL threads_monitor_main_termial();
/*
	should be called at start of main function.
*/
void threads_monitor_main_start();

/*
	should be called at end of main function.
*/
void threads_monitor_main_termial();



void nfc_notify();

#endif //APP_H