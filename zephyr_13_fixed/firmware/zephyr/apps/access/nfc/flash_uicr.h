
#ifndef FLASH_UICR_H
#define FLASH_UICR_H

#include <host/mesh/mesh.h>

#define MESH_DEPLOY_UNICAST 0x0001

#define FLASH_UNICAST_ADDR 0x70000

static u16_t flash_unicast;

static inline u16_t flash_unicast_get()
{

	return flash_unicast;
}

static int flash_uicr_init(struct device* dev)
{

	u32_t w = * ((u32_t*)(FLASH_UNICAST_ADDR));
	flash_unicast =  (u16_t)w;

	if(!BT_MESH_ADDR_IS_UNICAST(flash_unicast))
		flash_unicast = MESH_DEPLOY_UNICAST;

	return 0;
}


DEVICE_INIT(
	flash_uicr,
	"flash_uicr",
	flash_uicr_init,
	NULL,
	NULL,
	APPLICATION,
	CONFIG_KERNEL_INIT_PRIORITY_DEVICE);


#endif //FLASH_UICR_H