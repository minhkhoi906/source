#ifndef MESH_H
#define MESH_H

#include <zephyr.h>

void bt_ready(int err);
void status_init();
void press_transmit();

#endif // MESH_H
