#include "button.h"
#include "mesh/mesh.h"
#include <board.h>
#include <pwm.h>

#include <entropy.h>
#include <errno.h>
#include <gpio.h>
#include <pwm.h>

#define SYS_LOG_DOMAIN "app"
#include <common/log.h>
#include <logging/sys_log.h>
#include <misc/util.h>
#include <bluetooth/mesh.h>

struct model_button_ctx_st {
  struct device* dev;
  struct k_delayed_work timer;
  u32_t pin_sensor;
  u32_t pin_led_on;
  u32_t pin_led_off;
  struct device* dev_pwm;
  u32_t pin_buzzer;
  struct k_delayed_work timer_buzzer;
  u32_t buzzer_period;
};

#define BUZZER_DURATION_MSEC    50
#define BUZZER_PERIOD_HZ        2700

static struct model_button_ctx_st model_button_ctx[] = {
{.dev = NULL,
    .pin_sensor = SENSOR_GPIO_PIN,
    .pin_led_on = LED_OPEN_GPIO_PIN,
    .pin_led_off = LED_CLOSE_GPIO_PIN,
    .pin_buzzer = BUZZER_PWM_PIN,
    .buzzer_period = (1000000/BUZZER_PERIOD_HZ),
    }
};




static struct gpio_callback gpio_cb;
void led_off(struct k_work* work);

void led_off(struct k_work* work)
{
    if(! model_button_ctx[0].dev) {
        SYS_LOG_ERR("onoff_ctx has no dev gpio");
        return;
    }
    k_delayed_work_cancel(&model_button_ctx[0].timer);
    gpio_pin_write(model_button_ctx[0].dev, model_button_ctx[0].pin_led_on, !LED_OPEN_LEVEL);
    gpio_pin_write(model_button_ctx[0].dev, model_button_ctx[0].pin_led_off, LED_OPEN_LEVEL);
    SYS_LOG_DBG("Off Led");
}
static void buzzer_notify(bool on)
{
    if(on) {
            pwm_pin_set_usec(model_button_ctx[0].dev_pwm, model_button_ctx[0].pin_buzzer,
                    model_button_ctx[0].buzzer_period,model_button_ctx[0].buzzer_period/2);
    } else {
            pwm_pin_set_usec(model_button_ctx[0].dev_pwm, model_button_ctx[0].pin_buzzer,
                    model_button_ctx[0].buzzer_period, 0);
    }
}

static void sensor_cb(struct device *dev, struct gpio_callback *cb,
            u32_t pins)
{
    for(int i=0; i<ARRAY_SIZE(model_button_ctx); i++) {

        if(cb->pin_mask & BIT(model_button_ctx[i].pin_sensor)) {

            SYS_LOG_DBG("sensor pin [%d] change state", model_button_ctx[i].pin_sensor);
            if(! model_button_ctx[0].dev) {
                SYS_LOG_ERR("onoff_ctx has no dev gpio");
                return;
            }
            gpio_pin_write(model_button_ctx[0].dev, model_button_ctx[0].pin_led_on, LED_OPEN_LEVEL);
            gpio_pin_write(model_button_ctx[0].dev, model_button_ctx[0].pin_led_off, !LED_OPEN_LEVEL);
            press_transmit();
            buzzer_notify(true);
            k_delayed_work_submit(&model_button_ctx[0].timer,LED_TIMER_SIGNAL );
            k_delayed_work_submit(&model_button_ctx[0].timer_buzzer, BUZZER_DURATION_MSEC);
            return ;
        }
    }
    SYS_LOG_ERR("not found button pressed");
}




static void notify_off(struct k_work* work)
{
    SYS_LOG_DBG("");
    buzzer_notify(false);
}

int button_init()
{
    struct device* dev = device_get_binding(CONFIG_GPIO_P0_DEV_NAME);
    if(!dev) {
        BT_ERR("no dev gpio");
        return -1;
    }
    int i;
    for(i=0; i<ARRAY_SIZE(model_button_ctx); i++) {
        model_button_ctx[i].dev = dev;
        gpio_pin_configure(model_button_ctx[i].dev, model_button_ctx[i].pin_led_on, GPIO_DIR_OUT);
        gpio_pin_configure(model_button_ctx[i].dev, model_button_ctx[i].pin_led_off, GPIO_DIR_OUT);
        gpio_pin_configure(model_button_ctx[i].dev, model_button_ctx[i].pin_sensor, GPIO_DIR_IN);
        k_delayed_work_init(&model_button_ctx[i].timer, led_off);
        gpio_pin_write(model_button_ctx[i].dev, model_button_ctx[i].pin_led_on, !LED_OPEN_LEVEL);
        gpio_pin_write(model_button_ctx[i].dev, model_button_ctx[i].pin_led_off, LED_OPEN_LEVEL);

//      Init sensor pin
        gpio_pin_configure(model_button_ctx[i].dev, model_button_ctx[i].pin_sensor,
                        GPIO_DIR_IN | GPIO_INT | GPIO_INT_DEBOUNCE | GPIO_INT_EDGE | GPIO_INT_ACTIVE_LOW | GPIO_PUD_NORMAL);
        gpio_init_callback(&gpio_cb, sensor_cb, BIT(model_button_ctx[i].pin_sensor));
        gpio_add_callback(dev, &gpio_cb);
        gpio_pin_enable_callback(model_button_ctx[i].dev, model_button_ctx[i].pin_sensor);

//      Init buzzer pin
        k_delayed_work_init(&model_button_ctx[i].timer_buzzer, notify_off);
        model_button_ctx[i].dev_pwm = device_get_binding(CONFIG_PWM_NRF5_SW_0_DEV_NAME);
        if(model_button_ctx[i].dev_pwm == NULL) {
            SYS_LOG_ERR("Not found pwm device");
            return 1;
        }
        notify_off(NULL);

    }

    return 0;
}

