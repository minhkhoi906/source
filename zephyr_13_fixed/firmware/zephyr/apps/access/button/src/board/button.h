#ifndef _DOOR_H
#define _DOOR_H
#include <zephyr.h>

int button_init();
enum Command {
    VNG_READER = 0x12,
    VNG_BUTTON = 0x14,
    VMG_DEVICE_HEALTH = 0x15,
};
#endif // _DOOR_H
