#ifndef _TUNSTILE_H
#define _TUNSTILE_H
#include <zephyr.h>

int tunstile_init();
void tunstile_open();
void tunstile_close(struct k_work* work);
u8_t get_sensor_state();

struct model_onoff_ctx_st {
    u8_t target;
    u16_t delay;
    u16_t tid;
    u8_t dir;
    u8_t present;
    struct device* dev;
    struct k_delayed_work timer;
    u32_t pin_relay;
    u32_t pin_led_on;
    u32_t pin_led_off;
    u32_t pin_sensor;
};
extern  struct  model_onoff_ctx_st model_onoff_ctx[];
#endif // _TUNSTILE_H
