
#include <zephyr.h>
#include <device.h>
#include <watchdog.h>
#include <misc/printk.h>
#include <gpio.h>
#include <board.h>
#include <logging/sys_log.h>
#include <debug/object_tracing.h>
#include "app.h"

#define THREAD_TRACKING_INTERVAL 3000 //ms

static u8_t threads_count;
struct k_delayed_work thread_timer;

#ifdef CONFIG_WDT_NRFX
struct {
	struct k_delayed_work feed_work;
	struct device* dev;
	int channel_id;
} mesh_wdt;

static void mesh_wdt_feed(struct k_work* work)
{
	wdt_feed(mesh_wdt.dev, mesh_wdt.channel_id);
	k_delayed_work_submit(&mesh_wdt.feed_work, 1000);
}

static int mesh_wdt_init() 
{
	SYS_LOG_DBG("");
	mesh_wdt.dev = device_get_binding(CONFIG_WDT_0_NAME);
	if(!mesh_wdt.dev)
		return 1;
	struct wdt_timeout_cfg cfg = {
		.window = {0, 5000},
		.callback = NULL,
		.flags = WDT_FLAG_RESET_SOC
	};

	mesh_wdt.channel_id = wdt_install_timeout(mesh_wdt.dev, &cfg);

	if(mesh_wdt.channel_id < 0)
		return 1;
	wdt_setup(mesh_wdt.dev, 0);

	k_delayed_work_init(&mesh_wdt.feed_work, mesh_wdt_feed);
	k_delayed_work_submit(&mesh_wdt.feed_work, 1000); 
	return 0;
}

#endif 



static inline int threads_count_get(void)
{
    int obj_counter = 0;
    struct k_thread *thread_list = NULL;

    /* wait a bit to allow any initialization-only threads to terminate */
    thread_list   = (struct k_thread *)SYS_THREAD_MONITOR_HEAD;
    while (thread_list != NULL) {
        thread_list =
            (struct k_thread *)SYS_THREAD_MONITOR_NEXT(thread_list);
        obj_counter++;
    } 
    return obj_counter;
}




static void thread_tracker(struct k_work* work)
{	
	static u8_t threads;
    threads = threads_count_get();
    // SYS_LOG_DBG("work threads: %d", threads);
    if(threads < threads_count) {
  		SYS_LOG_DBG("System reset");
    	NVIC_SystemReset();
    }
    if(threads > threads_count) {
    	threads_count = threads;
    }
    k_delayed_work_submit(&thread_timer, THREAD_TRACKING_INTERVAL);
}

void threads_monitor_main_start()
{	

	threads_count = threads_count_get();
	SYS_LOG_DBG("threads_count: %d", threads_count);
	k_delayed_work_init(&thread_timer, thread_tracker);
	k_delayed_work_submit(&thread_timer, THREAD_TRACKING_INTERVAL);
}

void threads_monitor_main_termial()
{
	/*
	 *this function is called at end of main 
	 *so main thread is terminating
	 */
	threads_count = threads_count_get() - 1;
	SYS_LOG_DBG("threads_count: %d", threads_count);
}



int board_init(struct device* unused)
{
	SYS_LOG_DBG("");
	struct device* gpio;

#ifdef CONFIG_WDT_NRFX
	mesh_wdt_init();	
#endif

#ifdef PIN_BYPASS
	gpio = device_get_binding(CONFIG_GPIO_P0_DEV_NAME);
	if (!gpio) {
		printk("Cannot find %s!\n", CONFIG_GPIO_P0_DEV_NAME);
		return -1;
	}
	gpio_pin_configure(gpio, PIN_BYPASS, GPIO_DIR_OUT);
	gpio_pin_write(gpio, PIN_BYPASS, 0);
#endif
	return 0;
}


DEVICE_INIT(
	mesh_board,
	"mesh_board",
	board_init,
	NULL,
	NULL,
	APPLICATION,
	CONFIG_KERNEL_INIT_PRIORITY_DEVICE);

int rand_number_blocking_generate(u32_t min, u32_t max, u32_t* rn)
{
    int i;
    u32_t rn_32;
    u8_t* rn_8;
    u8_t rn_count;
    u32_t delta;

    rn_8 = (u8_t*)(&rn_32);

    if(max < min) {
        return -EINVAL;
    }
    if(max == min) {
        *rn = min;
        return 0;
    }

    delta = max - min;

    if(delta < 0xFF) {
        rn_count = 1;
    } else if (delta < 0xFFFF) {
        rn_count = 2;
    } else {
        rn_count = 4;
    }

    for(i = 0; i < rn_count; i++) {
        NRF_RNG->TASKS_START = 1;
        k_busy_wait(1000);
        NRF_RNG->TASKS_STOP = 1;
        rn_8[i] = NRF_RNG->VALUE;
    }
    // SYS_LOG_DBG("delta: %u, rn_count: %u, rn_32: %u", delta, rn_count, rn_32);
    *rn = (rn_32 % delta) + min;
    return 0;
}