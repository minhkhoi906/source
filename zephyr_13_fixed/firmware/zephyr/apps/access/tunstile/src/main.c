#include "mesh/mesh.h"
#include "board/tunstile.h"
#include <bluetooth/bluetooth.h>
#include <kernel.h>
#define SYS_LOG_DOMAIN "app"
#include "common/log.h"
#include <logging/sys_log.h>


void main()
{
    int err;


    /* Initialize the Bluetooth Subsystem */

    err = bt_enable(bt_ready);
    if (err) {
        SYS_LOG_ERR("Bluetooth init failed (err %d)", err);
        return;
    }
    status_init();
        // Board Inittialize
    err = tunstile_init();
    if (err != 0)return;

}

