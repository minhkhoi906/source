TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
#DEFINES += CONFIG_ADC_CONFIGURABLE_INPUTS

INCLUDEPATH += \
    ../../../../os/v1.13.0/include \
    ../../../../os/v1.13.0/subsys/bluetooth \
    ../../../../os/v1.13.0/subsys/bluetooth/mesh \
    ../../../../os/v1.13.0/boards/arm/nrf52_vng_door

HEADERS += \
    mesh/mesh.h \
    board/door.h \
    flash_uicr.h

SOURCES += \
    main.c \
    mesh/mesh.c \
    board/door.c \
    board/board.c

