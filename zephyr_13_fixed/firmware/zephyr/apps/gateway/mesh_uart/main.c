
#include <device.h>
#include <uart.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/mesh.h>
#include <crc16.h>
#include <misc/byteorder.h>


#include "common/log.h"
// #define SYS_LOG_DOMAIN "hub/main"
#include <logging/sys_log.h>
#include <app.h>

#include <host/mesh/net.h>
#include <host/mesh/adv.h>
#include <host/mesh/access.h>
#include <settings/settings.h>
#include "flash_uicr.h"

/*
    bt_mesh_rpl_reset();
    command to reset rpl cache
*/



#define MODELS_BIND(models, __vnd, net_idx, app_idx) \
do { \
    size_t size = ARRAY_SIZE(models);\
    size_t idx; \
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    for(idx = 0; idx < size; idx++) { \
        addr = elems[models[idx].elem_idx].addr;\
        if(__vnd) {\
            SYS_LOG_DBG("vnd bind addr [0x%04x]", addr);\
            bt_mesh_cfg_mod_app_bind_vnd(net_idx, primary_addr, addr, app_idx, models[idx].vnd.id, \
                models[idx].vnd.company, NULL);\
        } else {\
            SYS_LOG_DBG("bind addr [0x%04x]", addr);\
            bt_mesh_cfg_mod_app_bind(net_idx, primary_addr, addr,\
            app_idx, models[idx].id, NULL);\
        }\
    } \
} while(0)



#define MODEL_PUB(model, __vnd, net_idx, pub)\
{\
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    addr = elems[model.elem_idx].addr;\
    if(__vnd) {\
        SYS_LOG_DBG("vnd pub addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_pub_set_vnd(net_idx, primary_addr, addr,\
            model.vnd.id, model.vnd.company, pub, NULL);\
    } else {\
        SYS_LOG_DBG("pub addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_pub_set(net_idx, primary_addr, addr,\
            model.id, pub, NULL);\
    }\
}


#define CID_VNG 0x00FF
#define MOD_VND_VNG_GATEWAY 0x0000
#define MOD_VNG_GATEWAY 0x0004
#define OP_VENDOR_EVENT                   BT_MESH_MODEL_OP_3(0x00, CID_VNG)
#define BT_MESH_MODEL_OP_VNG_STATUS_SRV   BT_MESH_MODEL_OP_3(0x04, CID_VNG)
#define BT_MESH_MODEL_OP_GEN_ONOFF_STATUS BT_MESH_MODEL_OP_2(0x82, 0x04)

#define BT_MESH_MODEL_OP_VND_STATUS       BT_MESH_MODEL_OP_2(0x82, 0x07)
#define BT_MESH_MODEL_OP_VND_LEVELS       BT_MESH_MODEL_OP_2(0x82, 0x08)
#define BT_MESH_MODEL_OP_VND_WARNING      BT_MESH_MODEL_OP_2(0x82, 0x09)

#define BT_MESH_MODEL_OP_VND_GROUPS_COUNT   BT_MESH_MODEL_OP_2(0x90, 0x20)
#define BT_MESH_MODEL_OP_VND_GROUPS_LIST    BT_MESH_MODEL_OP_2(0x90, 0x21)

#define VNG_MESH_RENEW_GROUP 0xC002
#define GATEWAY_GROUP_ADDR 0xC000
#define STATUS_GROUP_ADDR 0xD000
#define STATUS_DST_GROUP_ADDR 0xE000
#define UART_MESH_TRANSMIT_STACK_SIZE 512


#define CONFIG_GAP_DELAYED 300 //ms

#define BT_MESH_BUF_SIZE 36
#define BT_MESH_USER_DATA_MIN 4
#if defined(CONFIG_BT_MESH_BUFFERS)
#define MESH_BUF_COUNT CONFIG_BT_MESH_BUFFERS
#else
#define MESH_BUF_COUNT 100
#endif

NET_BUF_POOL_DEFINE(mesh_pool,
                    MESH_BUF_COUNT,
                    BT_MESH_BUF_SIZE,
                    BT_MESH_USER_DATA_MIN,
                    NULL);

#define SLIP_END             0xC0    /* indicates end of packet */
#define SLIP_ESC             0xDB    /* indicates byte stuffing */
#define SLIP_ESC_END         0xDC    /* ESC ESC_END means END data byte */
#define SLIP_ESC_ESC         0xDD    /* ESC ESC_ESC means ESC data byte */
#define SLIP_ESC_XON         0xDE /* ESC SLIP_ESC_XON means XON control byte */
#define SLIP_ESC_XOFF        0xDF /* ESC SLIP_ESC_XON means XOFF control byte */
#define XON 0x11 /* indicates XON charater */
#define XOFF 0x13 /* indicates XOFF charater */

#define STATUS_CMD          0xFF00
#define HUB_DEV_TYPE        0x0021

#define GROUPS_CMD          0xEE00



static struct device *uart;
static BT_STACK_NOINIT(tx_thread_stack, UART_MESH_TRANSMIT_STACK_SIZE);
static struct k_thread tx_thread_handle;
static K_FIFO_DEFINE(tx_queue);
static K_FIFO_DEFINE(rx_queue);

enum hub_info {
    HUB_LEVELS,
    HUB_STATUS,
    HUB_WARNING,
};


enum groups_status_type
{
    GROUPS_COUNT = 0,
    GROUPS_LIST  = 1,
};

struct _mesh
{
    u16_t tx_crc;
    struct k_fifo *tx_queue;
    struct k_fifo *rx_queue;
    struct net_buf *rx_buf;
    u8_t last_char;
};

static struct _mesh _mesh = {
    .tx_queue = &tx_queue,
    .rx_queue = &rx_queue,
    .rx_buf = NULL
};


enum config_tasks
{
    SUBTASK0,
    SUBTASK1,
    SUBTASK2,
    SUBTASK3,
};
struct _mesh_config
{
    u8_t tasks;
    struct k_delayed_work timer;
};

static struct _mesh_config _mesh_config = {
    .tasks = 0,
};

static void _mesh_config_work(struct k_work* work);

extern void _prov_complete(u16_t net_idx, u16_t addr);
static void prov_reset(void);

static uint8_t uuid[16] = {0xFF, 0xFF};

static struct bt_mesh_prov prov = {
    .uuid = uuid,
    .output_size = 0,
    .complete = _prov_complete,
    .reset = prov_reset
};


static const u8_t app_idx_0 = 0;


static const u8_t app_key_0[16] = {
   0x4d, 0x48, 0x63, 0x43, 0x41, 0x51, 0x45, 0x45, 0x49, 0x4c, 0x79, 0x53, 0x4a, 0x69, 0x4c, 0x2f,
};

static const u8_t dev_key[16] = {
    0x55, 0x65, 0x41, 0x58, 0x64, 0x65, 0x78, 0x4d, 0x76, 0x74, 0x4c, 0x65, 0x56, 0x31, 0x70, 0x52, 
};

static const u8_t net_key[16] = {
    0x7a, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b, 
};


static u16_t net_idx;
static const u32_t iv_index;
static u8_t flags;
static u16_t addr;
static bool setting_loading = false;

static struct bt_mesh_cfg_srv cfg_srv = {
    .relay = BT_MESH_RELAY_ENABLED,
    .beacon = BT_MESH_BEACON_ENABLED,
#if defined(CONFIG_BT_MESH_FRIEND)
    .frnd = BT_MESH_FRIEND_ENABLED,
#else
    .frnd = BT_MESH_FRIEND_NOT_SUPPORTED,
#endif
#if defined(CONFIG_BT_MESH_GATT_PROXY)
    .gatt_proxy = BT_MESH_GATT_PROXY_ENABLED,
#else
    .gatt_proxy = BT_MESH_GATT_PROXY_NOT_SUPPORTED,
#endif
    .default_ttl = 5,

    /* 5 transmissions with 20ms interval */
    .net_transmit = BT_MESH_TRANSMIT(5, 20),
    .relay_retransmit = BT_MESH_TRANSMIT(5, 20),
};

static struct bt_mesh_health_srv health_srv = {
};

BT_MESH_HEALTH_PUB_DEFINE(health_pub, 0);

static struct bt_mesh_cfg_cli cfg_cli = {
};



static void mesh_event(struct bt_mesh_model *model,
                       struct bt_mesh_msg_ctx *ctx,
                       struct net_buf_simple *raw)
{

    u32_t modelId = ((u32_t)CID_VNG) << 16 | MOD_VNG_GATEWAY;
    struct net_buf *buf = net_buf_alloc(&mesh_pool, K_NO_WAIT);
    if (buf == NULL) {
        SYS_LOG_ERR("Out of memory");
        return;
    }



    // SYS_LOG_DBG("received, src 0x%04x", ctx->addr);
    net_buf_add_be16(buf, ctx->addr);
    net_buf_add_be32(buf, modelId);
    net_buf_add_be16(buf, (u16_t)(OP_VENDOR_EVENT));

    u16_t cmd;

    // device msg: cmd(1)|data

    cmd = net_buf_simple_pull_u8(raw);
    cmd = sys_cpu_to_be16(cmd);

    net_buf_add_be16(buf, cmd);
    net_buf_add_mem(buf, raw->data, raw->len);
    net_buf_put(_mesh.rx_queue, buf);
}


static void hub_forward(enum hub_info type,
                       struct bt_mesh_model *model,
                       struct bt_mesh_msg_ctx *ctx,
                       struct net_buf_simple *raw) {


    u32_t modelId = ((u32_t)CID_VNG) << 16 | MOD_VNG_GATEWAY;
    u16_t opcode;
    struct net_buf *buf = net_buf_alloc(&mesh_pool, K_NO_WAIT);
    if (buf == NULL) {
        SYS_LOG_ERR("Out of memory");
        return;
    }
    if(type == HUB_LEVELS) {
        opcode = BT_MESH_MODEL_OP_VND_LEVELS;
    } else if(type == HUB_STATUS) {
        opcode = BT_MESH_MODEL_OP_VND_STATUS;
    } else {
        opcode = BT_MESH_MODEL_OP_VND_WARNING;
    }

    // SYS_LOG_DBG("received src: 0x%04x", ctx->addr);
    net_buf_add_be16(buf, ctx->addr);
    net_buf_add_be32(buf, modelId);
    net_buf_add_be16(buf, opcode);
    net_buf_add_be16(buf, STATUS_CMD);
    net_buf_add_be16(buf, HUB_DEV_TYPE);
    net_buf_add_be16(buf, ctx->addr);
    net_buf_add_mem(buf, raw->data, raw->len);
    net_buf_put(_mesh.rx_queue, buf);

}

static void groups_status(enum groups_status_type type,
                       struct bt_mesh_model *model,
                       struct bt_mesh_msg_ctx *ctx,
                       struct net_buf_simple *raw)
{
    
    u32_t modelId = ((u32_t)CID_VNG) << 16 | MOD_VNG_GATEWAY;
    u16_t opcode;
    struct net_buf *buf = net_buf_alloc(&mesh_pool, K_NO_WAIT);
    if (buf == NULL) {
        SYS_LOG_ERR("Out of memory");
        return;
    }

    SYS_LOG_INF("type: %s, buf [%u : %s]", 
                (type == GROUPS_COUNT ? "groups count" : "groups_list"),
                raw->len, bt_hex(raw->data, raw->len));

    opcode = (type == GROUPS_COUNT) ? BT_MESH_MODEL_OP_VND_GROUPS_COUNT: 
                                      BT_MESH_MODEL_OP_VND_GROUPS_LIST;

    net_buf_add_be16(buf, ctx->addr);
    net_buf_add_be32(buf, modelId);
    net_buf_add_be16(buf, opcode);
    net_buf_add_be16(buf, GROUPS_CMD);
    net_buf_add_be16(buf, type);
    net_buf_add_be16(buf, ctx->addr);
    net_buf_add_mem(buf, raw->data, raw->len);
    net_buf_put(_mesh.rx_queue, buf);

}

static void groups_count_status(struct bt_mesh_model *model,
                       struct bt_mesh_msg_ctx *ctx,
                       struct net_buf_simple *buf)
{
    // [groups_count(4bits) | groups_check_sum(4 lsb bits)] x n (lines)
    groups_status(GROUPS_COUNT, model, ctx, buf);

}

static void groups_list_status(struct bt_mesh_model *model,
                       struct bt_mesh_msg_ctx *ctx,
                       struct net_buf_simple *buf)
{
    // [line(4b)[gbits(4b)|pno(4b)|pid (4b)][egroup]x n
    groups_status(GROUPS_LIST, model, ctx, buf);
}

static void hub_status(struct bt_mesh_model *model,
                       struct bt_mesh_msg_ctx *ctx,
                       struct net_buf_simple *raw)
{
    hub_forward(HUB_STATUS, model, ctx, raw);
}

static void hub_levels(struct bt_mesh_model *model,
                       struct bt_mesh_msg_ctx *ctx,
                       struct net_buf_simple *raw)
{

    hub_forward(HUB_LEVELS, model, ctx, raw);
}

static void hub_warning(struct bt_mesh_model *model,
                       struct bt_mesh_msg_ctx *ctx,
                       struct net_buf_simple *raw)
{

    hub_forward(HUB_WARNING, model, ctx, raw);
}

static const struct bt_mesh_model_op vnd_ops[] = {
    { OP_VENDOR_EVENT, 0, mesh_event}, 
    { BT_MESH_MODEL_OP_VNG_STATUS_SRV, 0, mesh_event}, 
    BT_MESH_MODEL_OP_END,
};


static const struct bt_mesh_model_op ops[] = {
    { BT_MESH_MODEL_OP_GEN_ONOFF_STATUS, 0, mesh_event}, 
    { BT_MESH_MODEL_OP_VND_LEVELS, 0, hub_levels},
    { BT_MESH_MODEL_OP_VND_STATUS, 0, hub_status},
    { BT_MESH_MODEL_OP_VND_WARNING, 0, hub_warning},
    { BT_MESH_MODEL_OP_VND_GROUPS_COUNT, 0, groups_count_status},
    { BT_MESH_MODEL_OP_VND_GROUPS_LIST, 0, groups_list_status},
    BT_MESH_MODEL_OP_END,
};


static struct bt_mesh_vng_command_srv vng_command_srvs[] = {
    {.user_data = NULL},
};

static struct bt_mesh_model vnd_models0[] = {
    BT_MESH_MODEL_VND(CID_VNG, MOD_VND_VNG_GATEWAY, vnd_ops, NULL, NULL),
    BT_MESH_MODEL_VNG_COMMAND_SRV(&vng_command_srvs[0]),
};


static struct bt_mesh_model models0[] = {
    BT_MESH_MODEL_CFG_SRV(&cfg_srv),
    BT_MESH_MODEL_CFG_CLI(&cfg_cli),
    BT_MESH_MODEL_HEALTH_SRV(&health_srv, &health_pub),
    BT_MESH_MODEL(MOD_VNG_GATEWAY, 
                  ops, NULL , NULL)

};


static struct bt_mesh_elem elems[] = {
    BT_MESH_ELEM(0, models0, vnd_models0),
};

static const struct bt_mesh_comp comp = {
    .cid = CID_VNG,
    .elem = elems,
    .elem_count = ARRAY_SIZE(elems),
};



static void mesh_uart_decoce(u8_t byte) {

 
    if (_mesh.rx_buf == NULL)
        _mesh.rx_buf = net_buf_alloc(&mesh_pool, K_NO_WAIT);

    if (_mesh.rx_buf == NULL)
        return;

    switch (byte) {
    case SLIP_ESC:
        _mesh.last_char = byte;
        break;
    case SLIP_END:
        _mesh.last_char = byte;

        net_buf_put(_mesh.tx_queue, _mesh.rx_buf);
       
        _mesh.rx_buf = net_buf_alloc(&mesh_pool, K_NO_WAIT);
        break;
    default:
        if(_mesh.last_char == SLIP_ESC) {
            _mesh.last_char = byte;
            /* Previous read byte was an escape byte, so this byte will be
             * interpreted differently from others. */
            switch(byte) {
            case SLIP_ESC_END:
                byte = SLIP_END;
                break;
            case SLIP_ESC_ESC:
                byte = SLIP_ESC;
                break;
            case SLIP_ESC_XON:
                byte = XON;
                break;
            case SLIP_ESC_XOFF:
                byte = XOFF;
                break;
            }
        } else {
            _mesh.last_char = byte;
        }

        if(_mesh.rx_buf->len >= BT_MESH_BUF_SIZE) {
            net_buf_reset(_mesh.rx_buf);
        }
        net_buf_add_u8(_mesh.rx_buf, byte);
        break;
    }
}

static void mesh_uart_isr(struct device *unused)
{

    ARG_UNUSED(unused);

    while (uart_irq_update(uart) && uart_irq_is_pending(uart)) {
        int rx;
        u8_t byte;
        if (uart_irq_rx_ready(uart) == 0) {
            if (uart_irq_tx_ready(uart) != 0) {
                SYS_LOG_INF("transmit ready");
            } else {
                SYS_LOG_INF("spurious interrupt");
            }
            /* Only the UART RX path is interrupt-enabled */
            break;
        }

        rx = uart_fifo_read(uart, &byte, sizeof(byte));

        if (rx == 0)
            break;

        mesh_uart_decoce(byte);
    }
}

static struct bt_mesh_model *find_model(u16_t id)
{
    struct bt_mesh_elem *elem = &elems[0];
    u8_t i;

    for (i = 0; i < elem->vnd_model_count; i++) {

        if (elem->vnd_models[i].vnd.id == id)
            return &elem->vnd_models[i];
    }

    for (i = 0; i < elem->model_count; i++) {
        if (elem->models[i].id == id) {
            return &elem->models[i];
        }
    }

    return NULL;
}

static void mesh_transmit(struct net_buf *buf)
{

    int err;
    struct bt_mesh_model *mod;
    uint16_t len;
    u16_t target;
    u16_t crc;
    u16_t src_crc;
    uint16_t model;
    uint32_t opcode;

    NET_BUF_SIMPLE_DEFINE(msg, 4 + 12 + 4);

    if (buf->len < sizeof(target) +
        sizeof(model) +
        sizeof(opcode) +
        sizeof(crc)) {
        SYS_LOG_ERR("buffer size: %d too small", (int) buf->len);
        return;
    }

    len = buf->len -
          sizeof(target) -
          sizeof(model) -
          sizeof(opcode) -
          sizeof(crc);

    crc = crc16_ccitt(0, buf->data, buf->len - sizeof(u16_t));
    target = net_buf_pull_be16(buf);
    model = net_buf_pull_be16(buf);
    mod = find_model(model);

    if (mod == NULL) {
        SYS_LOG_ERR("model with id: 0x%04x, not found", model);
        return;
    }


    opcode = net_buf_pull_be32(buf);
    bt_mesh_model_msg_init(&msg, opcode);

    SYS_LOG_DBG("target: 0x%04x, model: 0x%04x, opcode: 0x%08x, data: %s",
           target, model, opcode, bt_hex(buf->data, len));

    net_buf_simple_add_mem(&msg, buf->data, len);
    net_buf_pull(buf, len);

    src_crc = net_buf_pull_be16(buf);

    if (crc != src_crc) {
        SYS_LOG_ERR("Crc 0x%04X not matched with source: 0x%04X\r", crc, src_crc);
        return;
    }

    struct bt_mesh_msg_ctx ctx = {
        .net_idx = bt_mesh.sub[0].net_idx,
        .app_idx = 0,
        .addr = target,
        .send_ttl = BT_MESH_TTL_DEFAULT,
    };

    err = bt_mesh_model_send(mod, &ctx, &msg, NULL, NULL);

    if (err != 0) {
        SYS_LOG_ERR("send failed with error: %d", err);
    }
}

static void tx_thread(void *p1, void *p2, void *p3)
{
    while (1) {
        struct net_buf *buf;
        //      int err;

        /* Wait until a buffer is available */
        buf = net_buf_get(_mesh.tx_queue, K_FOREVER);

        mesh_transmit(buf);

        net_buf_unref(buf);

        /* Give other threads a chance to run if _tx_queue keeps getting
         * new data all the time.
         */
        k_yield();
    }
}

static void mesh_send_byte(u8_t byte)
{
    switch (byte) {
    case SLIP_END:
        uart_poll_out(uart, SLIP_ESC);
        uart_poll_out(uart, SLIP_ESC_END);
        break;
    case SLIP_ESC:
        uart_poll_out(uart, SLIP_ESC);
        uart_poll_out(uart, SLIP_ESC_ESC);
        break;
    case XON:
        uart_poll_out(uart, SLIP_ESC);
        uart_poll_out(uart, SLIP_ESC_XON);
        break;
    case XOFF:
        uart_poll_out(uart, SLIP_ESC);
        uart_poll_out(uart, SLIP_ESC_XOFF);
        break;
    default:
        uart_poll_out(uart, byte);
        break;
    }
}

static int mesh_forward(struct net_buf *buf)
{
    // SYS_LOG_INF("buf %p len %u", buf, buf->len);

    u16_t crc = crc16_ccitt(0, buf->data, buf->len);
    u8_t *bytes = (u8_t *) &crc;

    while (buf->len > 0) {

        u8_t byte = net_buf_pull_u8(buf);
        mesh_send_byte(byte);
    }

    crc = sys_cpu_to_be16(crc);
    mesh_send_byte(bytes[0]);
    mesh_send_byte(bytes[1]);
    uart_poll_out(uart, SLIP_END);

    return 0;
}

static int mesh_uart_init(struct device *unused)
{
    SYS_LOG_INF("Start");

    uart = device_get_binding(CONFIG_UART_0_NAME);
    if (!uart) {
        SYS_LOG_ERR("Failed");
        return -EINVAL;
    }

    uart_irq_rx_disable(uart);
    uart_irq_tx_disable(uart);

    uart_irq_callback_set(uart, mesh_uart_isr);

    uart_irq_rx_enable(uart);

    SYS_LOG_INF("Successed, pin_tx %d, pin_rx %d", CONFIG_UART_0_NRF_TX_PIN, CONFIG_UART_0_NRF_RX_PIN);
    return 0;
}


static void _mesh_config_work(struct k_work* work)
{
    u16_t primary_addr = bt_mesh_primary_addr();

    if (_mesh_config.tasks & BIT(SUBTASK0)) {

        SYS_LOG_INF("SUBTASK0");
        _mesh_config.tasks &= ~BIT(SUBTASK0);
        bt_mesh_cfg_app_key_add(net_idx, primary_addr,
                net_idx, app_idx_0, app_key_0, NULL);

        k_delayed_work_submit(&_mesh_config.timer, CONFIG_GAP_DELAYED);

    } else if (_mesh_config.tasks & BIT(SUBTASK1)) {

        SYS_LOG_INF("SUBTASK1");
        _mesh_config.tasks &= ~BIT(SUBTASK1);
        MODELS_BIND(models0, false , net_idx, app_idx_0);
        MODELS_BIND(vnd_models0, true, net_idx, app_idx_0);

        k_delayed_work_submit(&_mesh_config.timer, CONFIG_GAP_DELAYED);

    } else if (_mesh_config.tasks & BIT(SUBTASK2)) {

        SYS_LOG_INF("SUBTASK2");
        _mesh_config.tasks &= ~BIT(SUBTASK2);
        bt_mesh_cfg_mod_sub_add(net_idx, primary_addr, primary_addr,
                            GATEWAY_GROUP_ADDR, 
                            MOD_VNG_GATEWAY, 
                            NULL);
        bt_mesh_cfg_mod_sub_add(net_idx, primary_addr, primary_addr,
                                STATUS_DST_GROUP_ADDR, 
                                MOD_VNG_GATEWAY, 
                                NULL);

        k_delayed_work_submit(&_mesh_config.timer, CONFIG_GAP_DELAYED);

    } else if(_mesh_config.tasks & BIT(SUBTASK3)) {

        SYS_LOG_INF("SUBTASK3");
        _mesh_config.tasks &= ~BIT(SUBTASK3);
        bt_mesh_cfg_mod_sub_add_vnd(net_idx, primary_addr, primary_addr,
                                GATEWAY_GROUP_ADDR, 
                                MOD_VND_VNG_GATEWAY, 
                                CID_VNG, 
                                NULL);

        bt_mesh_cfg_mod_sub_add_vnd(net_idx, primary_addr, primary_addr,
                                    STATUS_DST_GROUP_ADDR, 
                                    MOD_VND_VNG_GATEWAY, 
                                    CID_VNG, 
                                    NULL);

        bt_mesh_cfg_mod_sub_add_vnd(net_idx, primary_addr, primary_addr,
                                    VNG_MESH_RENEW_GROUP, 
                                    BT_MESH_MODEL_ID_VNG_COMMAND_SRV, 
                                    CID_VNG, 
                                    NULL);

    }
}

void _prov_complete(u16_t net_idx_, u16_t addr)
{
    
    if(setting_loading) {
        SYS_LOG_INF("Stored net_idx %u", bt_mesh.sub[0].net_idx);
        return;
    }
    net_idx = net_idx_;

    SYS_LOG_INF("Provisioned completed, net_idx %u", net_idx_);

    _mesh_config.tasks |= BIT(SUBTASK0);
    _mesh_config.tasks |= BIT(SUBTASK1);
    _mesh_config.tasks |= BIT(SUBTASK2);
    _mesh_config.tasks |= BIT(SUBTASK3);

    k_delayed_work_cancel(&_mesh_config.timer);
    k_delayed_work_submit(&_mesh_config.timer, 1000);

}


static void prov_reset(void)
{
    bt_mesh_prov_enable(BT_MESH_PROV_ADV | BT_MESH_PROV_GATT);
}

static void bt_ready(int err)
{

    struct bt_le_oob oob;

    if (err) {
        SYS_LOG_ERR("Bluetooth init failed (err %d)", err);
        return;
    }

    SYS_LOG_INF("Bluetooth initialized");

    err = bt_le_oob_get_local(BT_ID_DEFAULT, &oob);

    if (err != 0) {
        SYS_LOG_ERR("get local oob failed with error: %d", err);
        return;
    }

    memset(uuid, 0, sizeof(uuid));
    memcpy(uuid, oob.addr.a.val, 6);

    err = bt_mesh_init(&prov, &comp);

    if (err) {
        SYS_LOG_ERR("Initializing mesh failed (err %d)", err);
        return;
    }


    SYS_LOG_DBG("Mesh initialized\n");

    k_sleep(3000);
    if (IS_ENABLED(CONFIG_BT_SETTINGS)) {
        SYS_LOG_DBG("Loading stored settings\n");
        setting_loading = true;
        settings_load();
        setting_loading = false;
    }

    addr = flash_unicast_get();
    
    SYS_LOG_INF("Stored net_idx[0] %u, net_idx[1] %u", bt_mesh.sub[0].net_idx, bt_mesh.sub[1].net_idx);
    
    SYS_LOG_INF("do self-provision, net_idx %u", net_idx);
    err = bt_mesh_provision(net_key, net_idx, flags, iv_index, addr,
                dev_key);
    if (err == -EALREADY) {
        printk("Using stored settings\n");
    } else if (err) {
        printk("Provisioning failed (err %d)\n", err);
        return;
    } 

}

DEVICE_INIT(mesh_uart,
            "mesh_uart",
            mesh_uart_init,
            NULL,
            NULL,
            APPLICATION,
            CONFIG_KERNEL_INIT_PRIORITY_DEVICE);

void main(void)
{
    THREADS_MONITOR_MAIN_START
    int err;
    SYS_LOG_INF("Start");

    k_delayed_work_init(&_mesh_config.timer, _mesh_config_work);
    /* Initialize the Bluetooth Subsystem */
    err = bt_enable(bt_ready);
    if (err) {
        SYS_LOG_ERR("Bluetooth init failed (err %d)", err);
        return;
    }

    /* Spawn the TX thread and start feeding commands and data to the
     * controller
     */
    k_thread_create(&tx_thread_handle,
                    tx_thread_stack,
                    K_THREAD_STACK_SIZEOF(tx_thread_stack),
                    tx_thread,
                    NULL,
                    NULL,
                    NULL,
                    K_PRIO_COOP(7),
                    0,
                    K_NO_WAIT);




    while (1) {

        struct net_buf *buf;

        buf = net_buf_get(_mesh.rx_queue, K_FOREVER);

        err = mesh_forward(buf);
        net_buf_unref(buf);
        if (err) {
            SYS_LOG_ERR("Failed to send");
        }

        
        k_yield();
    }

    THREADS_MONITOR_MAIN_TERMINAL

}
