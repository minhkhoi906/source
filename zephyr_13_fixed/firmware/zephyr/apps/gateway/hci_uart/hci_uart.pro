TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
DEFINES += CONFIG_UART_INTERRUPT_DRIVEN

INCLUDEPATH += \
    ../../../os/v1.11.0/include \
    ../../../os/v1.11.0/subsys/bluetooth

SOURCES += \
    src/main.c

HEADERS += \
    src/board.h

DISTFILES += \
    prj_nrf52_vng_gateway.conf \
    CMakeLists.txt
