#include <errno.h>
#include <stddef.h>

#include <zephyr.h>
#include <arch/cpu.h>

#include <init.h>
#include <uart.h>
#include <misc/util.h>
#include <misc/byteorder.h>
#include <string.h>
#include <net/buf.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/l2cap.h>
#include <bluetooth/hci.h>
#include <bluetooth/buf.h>
#include <bluetooth/hci_raw.h>
#include <gpio.h>

#define BT_DBG_ENABLED IS_ENABLED(CONFIG_BT_DEBUG)
#include "common/log.h"

#if defined(CONFIG_BT_NRF51_PM)
#include "../nrf51_pm.h"
#endif

#include <board.h>

#define H4_NONE 0x00
#define H4_CMD  0x01
#define H4_ACL  0x02
#define H4_SCO 0x03
#define H4_EVT 0x04

static struct device* _gpio;
static BT_STACK_NOINIT(rx_thread_stack, CONFIG_BT_RX_STACK_SIZE);
static struct k_thread rx_thread_data;

/* HCI command buffers */
#define CMD_BUF_SIZE BT_BUF_RX_SIZE
NET_BUF_POOL_DEFINE(cmd_tx_pool, CONFIG_BT_HCI_CMD_COUNT, CMD_BUF_SIZE,
            BT_BUF_USER_DATA_MIN, NULL);

static struct {
    struct net_buf *buf;
    struct k_fifo   fifo;

    u16_t    remaining;
    u16_t    discard;

    bool     have_hdr;
    bool     discardable;

    u8_t     hdr_len;

    u8_t     type;
    union {
        struct bt_hci_cmd_hdr cmd;
        struct bt_hci_acl_hdr acl;
        u8_t hdr[4];
    };
} rx = {
    .fifo = _K_FIFO_INITIALIZER(rx.fifo),
};

static struct {
    u8_t type;
    struct net_buf *buf;
    struct k_fifo   fifo;
} tx = {
    .fifo = _K_FIFO_INITIALIZER(tx.fifo),
};

static struct device *h4_dev;

static inline void h4_get_type(void)
{
    /* Get packet type */
    BT_DBG("");
    if (uart_fifo_read(h4_dev, &rx.type, 1) != 1) {
        BT_WARN("Unable to read H:4 packet type");
        rx.type = H4_NONE;
        return;
    }

    switch (rx.type) {
    case H4_CMD:
        rx.remaining = sizeof(rx.cmd);
        rx.hdr_len = rx.remaining;
        break;
    case H4_ACL:
        rx.remaining = sizeof(rx.acl);
        rx.hdr_len = rx.remaining;
        break;
    default:
        BT_ERR("Unknown H:4 type 0x%02x", rx.type);
        rx.type = H4_NONE;
    }
}

static void reset_rx(void)
{
    rx.type = H4_NONE;
    rx.remaining = 0;
    rx.have_hdr = false;
    rx.hdr_len = 0;
    rx.discardable = false;
    rx.buf = NULL;
}

static inline void get_acl_hdr(void)
{
    struct bt_hci_acl_hdr *hdr = &rx.acl;
    int pos = sizeof(*hdr) - rx.remaining;

    BT_DBG("");

    rx.remaining -= uart_fifo_read(h4_dev, (u8_t *)hdr + pos, rx.remaining);
    if (rx.remaining > 0)
        return;

    rx.buf = net_buf_alloc(&cmd_tx_pool, K_NO_WAIT);

    if (rx.buf == NULL) {
        reset_rx();
        BT_ERR("insufficient memory");
        return;
    }

    bt_buf_set_type(rx.buf, BT_BUF_ACL_OUT);
    rx.remaining = sys_le16_to_cpu(hdr->len);
    BT_DBG("Got ACL header. Handle: 0x%04X, Payload %u bytes",
           hdr->handle,
           rx.remaining);
    rx.have_hdr = true;
}

static inline void get_cmd_hdr(void)
{
    struct bt_hci_cmd_hdr *hdr = &rx.cmd;
    int pos = rx.hdr_len - rx.remaining;

    BT_DBG("");
    rx.remaining -= uart_fifo_read(h4_dev, (u8_t *)hdr + pos, rx.remaining);

    if (rx.remaining > 0)
        return;

    rx.buf = net_buf_alloc(&cmd_tx_pool, K_NO_WAIT);

    if (rx.buf == NULL) {
        BT_ERR("insufficient memory");
        reset_rx();
        return;
    }

    bt_buf_set_type(rx.buf, BT_BUF_CMD);
    rx.remaining = hdr->param_len;
    BT_DBG("Got command header. opcode: 0x%04X, Payload %u bytes",
           hdr->opcode,
           hdr->param_len);
    rx.have_hdr = true;
}


static inline void copy_hdr(struct net_buf *buf)
{
    net_buf_add_mem(buf, rx.hdr, rx.hdr_len);
}

static void rx_thread(void *p1, void *p2, void *p3)
{
    ARG_UNUSED(p1);
    ARG_UNUSED(p2);
    ARG_UNUSED(p3);

    while (1) {
        struct net_buf *buf;
        int err;


        /* Wait until a buffer is available */
        buf = net_buf_get(&rx.fifo, K_FOREVER);


        /* Pass buffer to the stack */
        err = bt_send(buf);
        if (err) {
            BT_ERR("Unable to send (err %d)", err);
            net_buf_unref(buf);
        }

        /* Give other threads a chance to run if tx.fifo keeps getting
         * new data all the time.
         */
        k_yield();
    }

}

static size_t h4_discard(struct device *uart, size_t len)
{
    u8_t buf[33];

    return uart_fifo_read(uart, buf, min(len, sizeof(buf)));
}

static inline void read_payload(void)
{
    struct net_buf *buf;
    int read;

    read = uart_fifo_read(h4_dev, net_buf_tail(rx.buf), rx.remaining);
    net_buf_add(rx.buf, read);
    rx.remaining -= read;

    BT_DBG("got %d bytes, remaining %u", read, rx.remaining);
    if (rx.remaining > 0)
        return;

    buf = rx.buf;
    reset_rx();

    net_buf_put(&rx.fifo, buf);
    BT_DBG("Putting buf %p to rx fifo", buf);
}

static inline void read_header(void)
{
    BT_DBG("");
    switch (rx.type) {
    case H4_NONE:
        h4_get_type();
        return;
    case H4_CMD:
        get_cmd_hdr();
        break;
    case H4_ACL:
        get_acl_hdr();
        break;
    default:
        CODE_UNREACHABLE;
        return;
    }

    if (rx.have_hdr && rx.buf) {
        if (rx.remaining > net_buf_tailroom(rx.buf)) {
            BT_ERR("Not enough space in buffer");
            rx.discard = rx.remaining;
            reset_rx();
        } else {
            copy_hdr(rx.buf);
        }
    }
}

static inline void process_tx(void)
{
    int bytes;

    BT_DBG("start");
    if (!tx.buf) {
        tx.buf = net_buf_get(&tx.fifo, K_NO_WAIT);
        if (!tx.buf) {
            BT_ERR("TX interrupt but no pending buffer!");
            uart_irq_tx_disable(h4_dev);
            return;
        }
    }

    if (!tx.type) {
        switch (bt_buf_get_type(tx.buf)) {
        case BT_BUF_ACL_IN:
            tx.type = H4_ACL;
            break;
        case BT_BUF_EVT:
            tx.type = H4_EVT;
            break;
        default:
            BT_ERR("Unknown buffer type");
            goto done;
        }

        bytes = uart_fifo_fill(h4_dev, &tx.type, 1);
        if (bytes != 1) {
            BT_WARN("Unable to send H:4 type");
            tx.type = H4_NONE;
            return;
        }
    }

    bytes = uart_fifo_fill(h4_dev, tx.buf->data, tx.buf->len);
    net_buf_pull(tx.buf, bytes);

    if (tx.buf->len) {
        return;
    }

done:
    tx.type = H4_NONE;
    net_buf_unref(tx.buf);
    tx.buf = net_buf_get(&tx.fifo, K_NO_WAIT);
    if (!tx.buf) {
        uart_irq_tx_disable(h4_dev);
    }
}

static inline void process_rx(void)
{
    struct net_buf *buf;

    if (rx.discard) {
        rx.discard -= h4_discard(h4_dev, rx.discard);
        return;
    }

    if (rx.have_hdr) {
        read_payload();
        return;
    }

    read_header();

    if (rx.have_hdr != true || rx.remaining > 0)
        return;

    buf = rx.buf;
    reset_rx();

    BT_DBG("Putting buf %p to rx fifo", buf);
    net_buf_put(&rx.fifo, buf);
}

static void bt_uart_isr(struct device *unused)
{
    ARG_UNUSED(unused);

    while (uart_irq_update(h4_dev) && uart_irq_is_pending(h4_dev)) {
        if (uart_irq_rx_ready(h4_dev)) {
            process_rx();
        }
    }
}

#if defined(CONFIG_BT_CTLR_ASSERT_HANDLER)
void bt_ctlr_assert_handle(char *file, u32_t line)
{
    u32_t len = 0, pos = 0;

    BT_DBG("");

    /* Disable interrupts, this is unrecoverable */
    (void)irq_lock();

    uart_irq_rx_disable(h4_dev);
    uart_irq_tx_disable(h4_dev);

    if (file) {
        while (file[len] != '\0') {
            if (file[len] == '/') {
                pos = len + 1;
            }
            len++;
        }
        file += pos;
        len -= pos;
    }

    uart_poll_out(h4_dev, H4_EVT);
    /* Vendor-Specific debug event */
    uart_poll_out(h4_dev, 0xff);
    /* 0xAA + strlen + \0 + 32-bit line number */
    uart_poll_out(h4_dev, 1 + len + 1 + 4);
    uart_poll_out(h4_dev, 0xAA);

    if (len) {
        while (*file != '\0') {
            uart_poll_out(h4_dev, *file);
            file++;
        }
        uart_poll_out(h4_dev, 0x00);
    }

    uart_poll_out(h4_dev, line >> 0 & 0xff);
    uart_poll_out(h4_dev, line >> 8 & 0xff);
    uart_poll_out(h4_dev, line >> 16 & 0xff);
    uart_poll_out(h4_dev, line >> 24 & 0xff);

    while (1) {
    }
}
#endif /* CONFIG_BT_CTLR_ASSERT_HANDLER */

static int h4_send(struct net_buf *buf)
{
    BT_DBG("buf %p type %u len %u", buf, bt_buf_get_type(buf),
            buf->len);

    switch (bt_buf_get_type(buf)) {
    case BT_BUF_ACL_IN:
        uart_poll_out(h4_dev, H4_ACL);
        break;
    case BT_BUF_EVT:
        uart_poll_out(h4_dev, H4_EVT);
        break;
    default:
        BT_ERR("Unknown type %u", bt_buf_get_type(buf));
        net_buf_unref(buf);
        return -EINVAL;
    }

    while (buf->len > 0) {
        uart_poll_out(h4_dev, net_buf_pull_u8(buf));
    }

    net_buf_unref(buf);

    return 0;
}


static int _bt_uart_init(struct device *unused)
{
    ARG_UNUSED(unused);

    h4_dev = device_get_binding(CONFIG_BT_CTLR_TO_HOST_UART_DEV_NAME);
    if (!h4_dev) {
        return -EINVAL;
    }

    uart_irq_rx_disable(h4_dev);
    uart_irq_tx_disable(h4_dev);
    h4_discard(h4_dev, 32);

    uart_irq_callback_set(h4_dev, bt_uart_isr);

    return 0;
}

DEVICE_INIT(hci_uart, "hci_uart", _bt_uart_init, NULL, NULL,
        APPLICATION, CONFIG_KERNEL_INIT_PRIORITY_DEVICE);

void main(void)
{
    /* incoming events and data from the controller */
    static K_FIFO_DEFINE(queue);


    BT_DBG("Start");

    _gpio = device_get_binding(CONFIG_GPIO_NRF5_P0_DEV_NAME);
    if (!_gpio) {
        printk("Cannot find %s!\n", CONFIG_GPIO_NRF5_P0_DEV_NAME);
        return;
    }

    gpio_pin_configure(_gpio, PIN_BYPASS, GPIO_DIR_OUT);
    gpio_pin_configure(_gpio, PIN_SWITCH, GPIO_DIR_OUT);
    gpio_pin_write(_gpio, PIN_BYPASS, 0);
    gpio_pin_write(_gpio, PIN_SWITCH, 0);

    /* Enable the raw interface, this will in turn open the HCI driver */
    bt_enable_raw(&queue);

    /* Spawn the TX thread and start feeding commands and data to the
     * controller
     */
    k_thread_create(&rx_thread_data,
                    rx_thread_stack,
                    K_THREAD_STACK_SIZEOF(rx_thread_stack),
                    rx_thread,
                    NULL,
                    NULL,
                    NULL,
                    K_PRIO_COOP(CONFIG_BT_RX_PRIO),
                    0, K_NO_WAIT);

    uart_irq_rx_enable(h4_dev);
    while (1) {
        int err;
        struct net_buf *buf = net_buf_get(&queue, K_FOREVER);
        err = h4_send(buf);
        if (err) {
            BT_ERR("Failed to send");
        }
    }
}

