#include <errno.h>
#include <zephyr.h>
#include <misc/printk.h>
#include <device.h>
#include <app.h>
#include <board.h>
#include <logging/sys_log.h>
#include <debug/object_tracing.h>
#include <watchdog.h>
#include <entropy.h>


#define SYS_LOG_LEVEL 4
#define THREAD_TRACKING_INTERVAL 3000 //ms


static u8_t threads_count;
struct k_delayed_work thread_timer;
struct device* entropy;

#ifdef CONFIG_WDT_NRFX
struct {
	struct k_delayed_work feed_work;
	struct device* dev;
	int channel_id;
} mesh_wdt;

static void mesh_wdt_feed(struct k_work* work)
{
	wdt_feed(mesh_wdt.dev, mesh_wdt.channel_id);
	k_delayed_work_submit(&mesh_wdt.feed_work, 1000);
}

static int mesh_wdt_init() 
{
	SYS_LOG_DBG("");
	mesh_wdt.dev = device_get_binding(CONFIG_WDT_0_NAME);
	if(!mesh_wdt.dev)
		return 1;
	struct wdt_timeout_cfg cfg = {
		.window = {0, 10000},
		.callback = NULL,
		.flags = WDT_FLAG_RESET_SOC
	};

	mesh_wdt.channel_id = wdt_install_timeout(mesh_wdt.dev, &cfg);

	if(mesh_wdt.channel_id < 0)
		return 1;
	wdt_setup(mesh_wdt.dev, 0);

	k_delayed_work_init(&mesh_wdt.feed_work, mesh_wdt_feed);
	k_delayed_work_submit(&mesh_wdt.feed_work, 1000); 
	return 0;
}

#endif 


static inline int threads_count_get(void)
{
    int obj_counter = 0;
    struct k_thread *thread_list = NULL;

    /* wait a bit to allow any initialization-only threads to terminate */
    thread_list   = (struct k_thread *)SYS_THREAD_MONITOR_HEAD;
    while (thread_list != NULL) {
        thread_list =
            (struct k_thread *)SYS_THREAD_MONITOR_NEXT(thread_list);
        obj_counter++;
    } 
    return obj_counter;
}




static void thread_tracker(struct k_work* work)
{	
	static u8_t threads;
    threads = threads_count_get();
    // SYS_LOG_DBG("work threads: %d", threads);
    if(threads < threads_count) {
  		SYS_LOG_DBG("System reset");
    	NVIC_SystemReset();
    }
  	if(threads > threads_count) {
    	threads_count = threads;
    }

    k_delayed_work_submit(&thread_timer, THREAD_TRACKING_INTERVAL);
}


void threads_monitor_main_start()
{	

	threads_count = threads_count_get();
	SYS_LOG_DBG("threads_count: %d", threads_count);
	k_delayed_work_init(&thread_timer, thread_tracker);
	k_delayed_work_submit(&thread_timer, THREAD_TRACKING_INTERVAL);
}

void threads_monitor_main_termial()
{
	/*
	 *this function is called at end of main 
	 *so main thread is terminating
	 */
	threads_count = threads_count_get() - 1;
	SYS_LOG_DBG("threads_count: %d", threads_count);
}


static void entropy_init()
{
    entropy = device_get_binding(CONFIG_ENTROPY_NAME);
    if(entropy == NULL) {
        printk("Entropy eevice not found\n");
        return;
    }
}



int board_init(struct device* unused) {


	SYS_LOG_DBG("");
	struct device* gpio;

	#ifdef CONFIG_WDT_NRFX
		mesh_wdt_init();	
	#endif
	gpio = device_get_binding(CONFIG_GPIO_P0_DEV_NAME);
	
	if (!gpio) {
		printk("Cannot find %s!\n", CONFIG_GPIO_P0_DEV_NAME);
		return 1;
	}
	// gpio_pin_configure(gpio, VDD_SENSOR_CTRL, GPIO_DIR_OUT);
	// gpio_pin_write(gpio, VDD_SENSOR_CTRL, 1);
	k_sleep(20);

	

	// i2c_dev = device_get_binding("I2C_0");
	// if (!i2c_dev) {
	// 	printk("I2C: Device driver not found.\n");
	// 	return;
	// }

	entropy_init();

	return 0;
}

DEVICE_INIT(
	sensor_board,
	"sensor_board",
	board_init,
	NULL,
	NULL,
	APPLICATION,
	CONFIG_KERNEL_INIT_PRIORITY_DEVICE);

int rand_number_blocking_generate(u32_t min, u32_t max, u32_t* rn)
{
    int err;
    u32_t rand;

    if(entropy == NULL) {
        return -1;
    }

    err = entropy_get_entropy(entropy, (u8_t*)(&rand), sizeof(rand));
    if(err) {
        SYS_LOG_ERR("Rng failed");
        return err;
    }

    *rn = min + (rand % (max - min));
    return 0;

}