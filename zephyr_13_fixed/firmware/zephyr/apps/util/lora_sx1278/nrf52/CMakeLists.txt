cmake_minimum_required(VERSION 3.8.2)

set(BOARD nrf52_lora_sx1278)

include($ENV{ZEPHYR_BASE}/cmake/app/boilerplate.cmake NO_POLICY_SCOPE)
project(NONE)

zephyr_include_directories(board)
target_sources(app PRIVATE ../unitest.c)
target_sources(app PRIVATE board/board.c)

