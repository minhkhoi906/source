#include <device.h>
#include <gpio.h>
#include <uart.h>
#include <board.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/mesh.h>
#include <misc/byteorder.h>
#include <watchdog.h>
#include "common/log.h"
#include <watchdog.h>
#include <app.h>
#include <settings/settings.h>



#define SYS_LOG_DOMAIN "app"
#define BT_MESH_BUF_SIZE 36
#define BT_MESH_USER_DATA_MIN 4

#if defined(CONFIG_BT_MESH_BUFFERS)
#define MESH_BUF_COUNT CONFIG_BT_MESH_BUFFERS
#else
#define MESH_BUF_COUNT 12
#endif


NET_BUF_POOL_DEFINE(mesh_pool,
                    MESH_BUF_COUNT,
                    BT_MESH_BUF_SIZE,
                    BT_MESH_USER_DATA_MIN,
                    NULL);

static struct device *uart;
static K_FIFO_DEFINE(tx_queue);

struct _mesh
{
    u16_t tx_crc;
    struct k_fifo *tx_queue;
    struct net_buf* rx_buf;
    struct k_delayed_work uart_timer;
 
};


static int mesh_uart_init();
static void mesh_uart_decoce(u8_t byte);
static void uart_rx_timeout(struct k_work* work);
static void mesh_uart_isr(struct device *unused);

static struct _mesh _mesh = {
    .tx_queue = &tx_queue,
    .rx_buf = NULL,
};

/* isr Uart lora sx1278 */

static void mesh_uart_decoce(u8_t byte) {
    if (_mesh.rx_buf == NULL)
        _mesh.rx_buf = net_buf_alloc(&mesh_pool, K_NO_WAIT);
 
    if (_mesh.rx_buf == NULL)
        return;
 
    
}

static void uart_rx_timeout(struct k_work* work)
{
    /*re*/
    if(!_mesh.rx_buf)
        return;
    if(_mesh.rx_buf->len == 0)
        return;
    SYS_LOG_INF("reset uart rx buf");
    net_buf_reset(_mesh.rx_buf);
}

static void mesh_uart_isr(struct device *unused)
{
    
    ARG_UNUSED(unused);
    // SYS_LOG_INF("");
    while (uart_irq_update(uart) && uart_irq_is_pending(uart)) {
        int rx;
        u8_t byte;
        if (uart_irq_rx_ready(uart) == 0) {
            if (uart_irq_tx_ready(uart) != 0) {
                // SYS_LOG_INF("transmit ready");
            } else {
                SYS_LOG_INF("spurious interrupt");
            }
            /* Only the UART RX path is interrupt-enabled */
            break;
        }
 
        rx = uart_fifo_read(uart, &byte, sizeof(byte));
 
        if (rx == 0)
            break;
 
        mesh_uart_decoce(byte);
        
    }
}

static int mesh_uart_init()
{
    SYS_LOG_INF("Start");
 
    uart = device_get_binding(CONFIG_UART_0_NAME);
    if (!uart) {
        SYS_LOG_ERR("Failed");
        return -EINVAL;
    }
 
    uart_irq_rx_disable(uart);
    uart_irq_tx_disable(uart);
 
    uart_irq_callback_set(uart, mesh_uart_isr);
 
    uart_irq_rx_enable(uart);
 
    SYS_LOG_INF("Successed, pin_tx %d, pin_rx %d", CONFIG_UART_0_NRF_TX_PIN, CONFIG_UART_0_NRF_RX_PIN);
 
    k_delayed_work_init(&_mesh.uart_timer, uart_rx_timeout);
    
    return 0;
}

static int mesh_devices_init(struct device* unused)
{
    mesh_uart_init();
    return 0;
}

DEVICE_INIT(mesh_devices,
        "mesh_devices",
        mesh_devices_init,
        NULL,
        NULL,
        APPLICATION,
        CONFIG_KERNEL_INIT_PRIORITY_DEVICE);


void main(){
	THREADS_MONITOR_MAIN_START

	SYS_LOG_INF("Start");


	THREADS_MONITOR_MAIN_TERMINAL


}