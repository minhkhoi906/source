

#include <device.h>
#include <uart.h>
#include <gpio.h>
#include <board.h>
#include <logging/sys_log.h>
#include <settings/settings.h>


#define BT_SETTINGS_SIZE(in_size) ((((((in_size) - 1) / 3) * 4) + 4) + 1)


#define BUF_SIZE 5
static u8_t foo_val[BUF_SIZE] = {1,2,3,4,5};


static int foo_settings_set(int argc, char **argv, char *val)
{
    int err;
    int len = BUF_SIZE;
    if (argc == 1) {
        if (!strcmp(argv[0], "buf")) {

            err = settings_bytes_from_str(val, foo_val, &len);
            if(err) {
                printk("parse str failed\n");
                return err;
            }
            
        }
    }

    return -ENOENT;
}

static void save_data()
{
   
    char buf[BT_SETTINGS_SIZE(BUF_SIZE)];

    char *str;

    printk("save buf: %s\n", bt_hex(foo_val, BUF_SIZE));
    str = settings_str_from_bytes(foo_val, BUF_SIZE, buf, sizeof(buf));
    if (!str) {
        printk("Unable to encode foo as value\n");
        return;
    }
    settings_save_one("foo/buf", str);


} 


struct settings_handler my_conf = {
    .name = "foo",
    .h_set = foo_settings_set
};



int my_settings_init(void)
{
    int err;

    err = settings_subsys_init();
    if (err) {
        printk("settings_subsys_init failed (err %d)\n", err);
        return err;
    }

    err = settings_register(&my_conf);
    if (err) {
        printk("settings_register failed (err %d)\n", err);
        return err;
    }

    return 0;
}


void main()
{
	int err;

    my_settings_init();

    err = settings_load();
    if(err) {
        printk("Setting load err\n");
        return;
    }

    printk("load buf: %s\n", bt_hex(foo_val, BUF_SIZE));
    foo_val[0]++;
    save_data();

    printk("press reset to check setting...\n");

}

