
#include <device.h>
#include <uart.h>
#include <gpio.h>
// #include <board.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/mesh.h>
#include <crc16.h>
#include <misc/byteorder.h>
#include <watchdog.h>
#include "common/log.h"
// #include <logging/sys_log.h>
#include <settings/settings.h>
#include "flash_uicr.h"

// #define LOG_LEVEL CONFIG_LOG_DEFAULT_LEVEL
#include <logging/log.h>
// LOG_MODULE_REGISTER(main);

#define SYS_LOG_DBG LOG_DBG
#define SYS_LOG_INF LOG_INF
#define SYS_LOG_ERR LOG_ERR
#define SYS_LOG_WARN LOG_WARN

extern u16_t bt_mesh_primary_addr(void);
#define CID_VNG 0x00FF

#define MODELS_BIND(models, vnd, net_idx, app_idx) \
do { \
    size_t size = ARRAY_SIZE(models);\
    size_t idx; \
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    for(idx = 0; idx < size; idx++) { \
        addr = elems[models[idx].elem_idx].addr;\
        if(vnd) {\
            SYS_LOG_DBG("vnd bind addr [0x%04x]", addr);\
            bt_mesh_cfg_mod_app_bind_vnd(net_idx, primary_addr, addr, app_idx, models[idx].vnd.id, \
                models[idx].vnd.company, NULL);\
        } else {\
            SYS_LOG_DBG("bind addr [0x%04x]", addr);\
            bt_mesh_cfg_mod_app_bind(net_idx, primary_addr, addr,\
            app_idx, models[idx].id, NULL);\
        }\
    } \
} while(0)



#define MODEL_PUB(model, vnd, net_idx, pub)\
{\
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    addr = elems[model.elem_idx].addr;\
    if(vnd) {\
        SYS_LOG_DBG("vnd pub addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_pub_set_vnd(net_idx, primary_addr, addr,\
            model.vnd.id, model.vnd.company, pub, NULL);\
    } else {\
        SYS_LOG_DBG("pub addr [0x%04x]", addr);\
        bt_mesh_cfg_mod_pub_set(net_idx, primary_addr, addr,\
            model.id, pub, NULL);\
    }\
}



static void prov_complete(u16_t net_idx, u16_t addr);
static void prov_reset(void);

static uint8_t uuid[16] = {0xFF, 0xFF};

static struct bt_mesh_prov prov = {
    .uuid = uuid,
    .output_size = 0,
    .complete = prov_complete,
    .reset = prov_reset

};



static const u8_t app_idx = 0;

// NEW KEY

static const u8_t app_key[16] = {
   0x4d, 0x48, 0x63, 0x43, 0x41, 0x51, 0x45, 0x45, 0x49, 0x4c, 0x79, 0x53, 0x4a, 0x69, 0x4c, 0x2f,
};

static const u8_t dev_key[16] = {
    0x55, 0x65, 0x41, 0x58, 0x64, 0x65, 0x78, 0x4d, 0x76, 0x74, 0x4c, 0x65, 0x56, 0x31, 0x70, 0x52, 
};

static const u8_t net_key[16] = {
    0x7a, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b, 
};




static struct bt_mesh_cfg_srv cfg_srv = {
    .relay = BT_MESH_RELAY_ENABLED,
    .beacon = BT_MESH_BEACON_ENABLED,
#if defined(CONFIG_BT_MESH_FRIEND)
    .frnd = BT_MESH_FRIEND_ENABLED,
#else
    .frnd = BT_MESH_FRIEND_NOT_SUPPORTED,
#endif
#if defined(CONFIG_BT_MESH_GATT_PROXY)
    .gatt_proxy = BT_MESH_GATT_PROXY_ENABLED,
#else
    .gatt_proxy = BT_MESH_GATT_PROXY_NOT_SUPPORTED,
#endif
    .default_ttl = 7,

    /* 3 transmissions with 20ms interval */
    .net_transmit = BT_MESH_TRANSMIT(2, 20),
    .relay_retransmit = BT_MESH_TRANSMIT(2, 20),
};


static struct bt_mesh_health_srv health_srv = {
};

BT_MESH_HEALTH_PUB_DEFINE(health_pub, 0);

static struct bt_mesh_cfg_cli cfg_cli = {
};

static struct bt_mesh_model models0[] = {
    BT_MESH_MODEL_CFG_SRV(&cfg_srv),
    BT_MESH_MODEL_CFG_CLI(&cfg_cli),
    BT_MESH_MODEL_HEALTH_SRV(&health_srv, &health_pub),

};


static struct bt_mesh_elem elems[] = {
    BT_MESH_ELEM(0, models0, BT_MESH_MODEL_NONE),
};

static const struct bt_mesh_comp comp = {
    .cid = CID_VNG,
    .elem = elems,
    .elem_count = ARRAY_SIZE(elems),
};

static bool settings_loading = false;
static void prov_complete(u16_t net_idx, u16_t addr)
{
    SYS_LOG_INF("Provisioned completed");

    if (settings_loading) {
        SYS_LOG_INF("Settings loaded no need to config");
        return;
    }
    SYS_LOG_INF("Configuring mesh");

    bool vnd = false;

     /* Add Application Key */
    bt_mesh_cfg_app_key_add(net_idx, elems[0].addr,
                net_idx, app_idx, app_key, NULL);

    MODELS_BIND(models0, vnd , net_idx, app_idx);

    SYS_LOG_INF("Done");

}


static void prov_reset(void) 
{
    bt_mesh_prov_enable(BT_MESH_PROV_ADV | BT_MESH_PROV_GATT);
}


static void bt_ready(int err)
{

    struct bt_le_oob oob;

    if (err) {
        SYS_LOG_ERR("Bluetooth init failed (err %d)", err);
        return;
    }

    SYS_LOG_DBG("Bluetooth initialized");

    err = bt_le_oob_get_local(BT_ID_DEFAULT, &oob);

    if (err != 0) {
        SYS_LOG_ERR("Get local oob failed with error: %d", err);
        return;
    }

    memset(uuid, 0, sizeof(uuid));
    memcpy(uuid, oob.addr.a.val, 6);

    err = bt_mesh_init(&prov, &comp);

    if (err) {
        SYS_LOG_ERR("Mesh initialized failed (err %d)", err);
        return;
    }

    SYS_LOG_DBG("Mesh initialized");


    if (IS_ENABLED(CONFIG_SETTINGS)) {
        settings_loading = true;
        err = settings_load();
        settings_loading = false;
    }

    if (bt_mesh_is_provisioned()) {
        SYS_LOG_DBG("Mesh settings loaded");
        return;
    }

    u16_t addr = flash_unicast_get();

    err = bt_mesh_provision(net_key,
                                0,
                                0,
                                0,
                                addr,
                                dev_key);

    

}

void main(void)
{
    int err;

    SYS_LOG_DBG("Start");

    /* Initialize the Bluetooth Subsystem */

    err = bt_enable(bt_ready);

    if (err) {
        SYS_LOG_ERR("Bluetooth init failed (err %d)", err);
        return;
    }

}

