cmake_minimum_required(VERSION 3.8.2)


set(BOARD nrf51_vng_forwarder)

include($ENV{ZEPHYR_BASE}/cmake/app/boilerplate.cmake NO_POLICY_SCOPE)
project(NONE)


zephyr_include_directories(board)
target_link_libraries(app subsys__bluetooth)

target_sources(app PRIVATE ../main.c)
target_sources(app PRIVATE board/board.c)