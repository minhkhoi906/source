TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

DISTFILES += \
    nrf51/prj_nrf51_vng_forwarder.conf \
    nrf52/prj_nrf52_vng_forwarder.conf \
    nrf51/CMakeLists.txt \
    nrf52/CMakeLists.txt

HEADERS += \
    nrf51/board/app.h \
    nrf52/board/app.h

SOURCES += \
    nrf51/board/board.c \
    nrf52/board/board.c \
    main.c
