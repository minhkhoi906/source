

#include <zephyr.h>
#include <misc/printk.h>
#include <device.h>
#include <pwm.h>
#include <board.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/mesh.h>
#include "irsend.h"
#include "ir_remote.h"
#include <watchdog.h>
#include <debug/object_tracing.h>
#include <logging/sys_log.h>

/* config port pwm */

#define SYS_LOG_LEVEL 4
#define THREAD_TRACKING_INTERVAL 3000 //ms
static u8_t threads_count;
struct k_delayed_work thread_timer;

NET_BUF_SIMPLE_DEFINE(rx_buf, 800);

struct pwm_dev_ctx {

	struct device* pwm_dev;
	u32_t period;
	u32_t pulse_with;
  int status;
};

struct change_pwm {
  uint16_t duration ;
  uint8_t status;
  uint8_t active;
  struct k_work send_work;
};

#ifdef CONFIG_WDT_NRFX
struct {
  struct k_delayed_work feed_work;
  struct device* dev;
  int channel_id;
} mesh_wdt;

static void mesh_wdt_feed(struct k_work* work)
{
  wdt_feed(mesh_wdt.dev, mesh_wdt.channel_id);
  k_delayed_work_submit(&mesh_wdt.feed_work, 1000);
}

static int mesh_wdt_init() 
{
  SYS_LOG_DBG("");
  mesh_wdt.dev = device_get_binding(CONFIG_WDT_0_NAME);
  if(!mesh_wdt.dev)
    return 1;
  struct wdt_timeout_cfg cfg = {
    .window = {0, 10000},
    .callback = NULL,
    .flags = WDT_FLAG_RESET_SOC
  };

  mesh_wdt.channel_id = wdt_install_timeout(mesh_wdt.dev, &cfg);

  if(mesh_wdt.channel_id < 0)
    return 1;
  wdt_setup(mesh_wdt.dev, 0);

  k_delayed_work_init(&mesh_wdt.feed_work, mesh_wdt_feed);
  k_delayed_work_submit(&mesh_wdt.feed_work, 1000); 
  return 0;
}

#endif 
static struct change_pwm change_pwm;

static struct pwm_dev_ctx pwm_dev_ctx;

static inline int threads_count_get(void)
{
    int obj_counter = 0;
    struct k_thread *thread_list = NULL;

    /* wait a bit to allow any initialization-only threads to terminate */
    thread_list   = (struct k_thread *)SYS_THREAD_MONITOR_HEAD;
    while (thread_list != NULL) {
        thread_list =
            (struct k_thread *)SYS_THREAD_MONITOR_NEXT(thread_list);
        obj_counter++;
    } 
    return obj_counter;
}

static void thread_tracker(struct k_work* work)
{ 
  static u8_t threads;
    threads = threads_count_get();
    // SYS_LOG_DBG("work threads: %d", threads);
    if(threads < threads_count) {
      SYS_LOG_DBG("System reset");
      NVIC_SystemReset();
    }
    if(threads > threads_count) {
      threads_count = threads;
    }

    k_delayed_work_submit(&thread_timer, THREAD_TRACKING_INTERVAL);
}

void threads_monitor_main_start()
{ 

  threads_count = threads_count_get();
  SYS_LOG_DBG("threads_count: %d", threads_count);
  k_delayed_work_init(&thread_timer, thread_tracker);
  k_delayed_work_submit(&thread_timer, THREAD_TRACKING_INTERVAL);
}

void threads_monitor_main_termial()
{
  /*
   *this function is called at end of main 
   *so main thread is terminating
   */
  threads_count = threads_count_get() - 1;
  SYS_LOG_DBG("threads_count: %d", threads_count);
}

/**************************************************test **************************/



void timer2_isr()
{
  if ((NRF_TIMER2->EVENTS_COMPARE[0] != 0) && ((NRF_TIMER2->INTENSET & TIMER_INTENSET_COMPARE0_Msk) != 0))
  {
    NRF_TIMER2->EVENTS_COMPARE[0] = 0;         //Clear compare register 0 event 
    NRF_TIMER2->TASKS_STOP = 1;

    if(change_pwm.active == 0) {
      pwm_pin_set_usec(pwm_dev_ctx.pwm_dev ,IR_TRANSMIT_PWM ,0 ,0);
      return;
    }

    if(change_pwm.status == 1) {

      pwm_pin_set_usec(pwm_dev_ctx.pwm_dev ,IR_TRANSMIT_PWM ,26 ,13);
      NRF_TIMER2->CC[0] = (change_pwm.duration /16);
      NRF_TIMER2->TASKS_START = 1;
    }

    else if (change_pwm.status == 0){

      pwm_pin_set_usec(pwm_dev_ctx.pwm_dev ,IR_TRANSMIT_PWM ,0 ,0);
      NRF_TIMER2->CC[0] = (change_pwm.duration /16);
      NRF_TIMER2->TASKS_START = 1;
      
    }

    if(rx_buf.len == 0){
        NRF_RADIO->TASKS_TXEN = 1;
        NRF_RADIO->TASKS_RXEN = 1;
        change_pwm.active = 0;
        return;
    }
    
    change_pwm.duration = net_buf_simple_pull_le16(&rx_buf);
    change_pwm.status = net_buf_simple_pull_u8(&rx_buf);    
  }
}


static void timer2_init() {

    NVIC_ClearPendingIRQ(NRF5_IRQ_TIMER2_IRQn);
    IRQ_CONNECT(NRF5_IRQ_TIMER2_IRQn, 0, timer2_isr, 0, 0);
    irq_enable(NRF5_IRQ_TIMER2_IRQn);

    NRF_TIMER2->MODE = TIMER_MODE_MODE_Timer;              // Set the timer in Counter Mode
    NRF_TIMER2->TASKS_CLEAR = 1;
    NRF_TIMER2->PRESCALER = 8;                             // clear the task first to be usable for later
    NRF_TIMER2->BITMODE = TIMER_BITMODE_BITMODE_16Bit;       //Set counter to 16 bit resolution
                               //Set value for TIMER2 compare register 0                                  //Set value for TIMER2 compare register 1
    NRF_TIMER2->SHORTS = TIMER_SHORTS_COMPARE0_CLEAR_Msk;
 //  // Enable interrupt on Timer 2, both for CC[0] and CC[1] compare match events
    NRF_TIMER2->INTENSET = (TIMER_INTENSET_COMPARE0_Enabled << TIMER_INTENSET_COMPARE0_Pos);
  
    NRF_TIMER2->TASKS_STOP = 1;
    

};

static void dim_pwm_timer() {

  change_pwm.duration = net_buf_simple_pull_le16(&rx_buf);
  change_pwm.status = net_buf_simple_pull_u8(&rx_buf);
  NRF_TIMER2->CC[0] = 1;  
  NRF_RADIO->TASKS_DISABLE = 1;
  NRF_TIMER2->TASKS_START = 1;
  change_pwm.active =1;

}

static void send_work_ir(struct k_work* work) {

    dim_pwm_timer();
    
};
/**********send data ir  ************/ 
void send_data_ir(const uint16_t headermark, const uint16_t headerspace,
                    const uint16_t onemark , const uint16_t onespace,
                    const uint16_t zeromark, const uint16_t zerospace,
                    const uint16_t gapmark, const uint16_t gapspace,
                    unsigned long data , uint8_t repeat , uint8_t nbits) {

  int i;
  net_buf_simple_reset(&rx_buf);

  for(i = 0; i< repeat ;i++) {

    net_buf_simple_add_le16(&rx_buf, headermark);
    net_buf_simple_add_u8(&rx_buf,1);
    net_buf_simple_add_le16(&rx_buf, headerspace);
    net_buf_simple_add_u8(&rx_buf,0);
      for (unsigned long  mask = 1UL << (nbits - 1);  mask;  mask >>= 1) {
        if (data & mask) {
          net_buf_simple_add_le16(&rx_buf, onemark);
          net_buf_simple_add_u8(&rx_buf,1);
          net_buf_simple_add_le16(&rx_buf, onespace);
          net_buf_simple_add_u8(&rx_buf,0);
        } else {
          net_buf_simple_add_le16(&rx_buf, zeromark);
          net_buf_simple_add_u8(&rx_buf,1);
          net_buf_simple_add_le16(&rx_buf, zerospace);
          net_buf_simple_add_u8(&rx_buf,0);
          }
        }

        if(gapmark != 0) {
          net_buf_simple_add_le16(&rx_buf, gapmark);
          net_buf_simple_add_u8(&rx_buf,1);
        }
        
        net_buf_simple_add_le16(&rx_buf,gapspace);
        net_buf_simple_add_u8(&rx_buf,0);
        
   }

  // printk("pulse: [%s]", bt_hex(rx_buf.data, rx_buf.len));
  k_work_submit(&change_pwm.send_work);
  
}

/****************** select device remote *************

para: type : thong tin device select vd: Sony, Samsung, Hatachi ...
      vd: type = 1 -> Hatachi ,2->Sony ,3-> Samsung
para: command: data  : data(32bit, 8 bit, 28 bit ...) -> power (0xffffffff)


**/

void send_generic(uint8_t type , unsigned long command) {

    switch (type) {

        case 1: //HATACHI
          printk("Hatachi \n");
          send_data_ir(NEC_HDR_MARK, NEC_HDR_SPACE,
                    NEC_BITS_MARK, NEC_ONESPACE,
                    NEC_BITS_MARK, NEC_ZEROSPACE,
                    NEC_BITS_MARK, NEC_MIN_GAP,
                    command , 1 ,32);
          break;

        case 2: //SONY
          printk("SONY \n");
          send_data_ir(SONY_HDR_MARK, SONY_SPACE,
                    SONY_ONE_MARK, SONY_SPACE,
                    SONY_ZERO_MARK, SONY_SPACE,
                    0, SONY_MIN_GAP,
                    command , 1 ,12);
          break;

        case 3: //SAMSUNG
          printk("SAMSUNG \n");
          send_data_ir(SAMSUNG_HDR_MARK, SAMSUNG_HDR_SPACE,
                    SAMSUNG_BIT_MARK, SAMSUNG_ONE_SPACE,
                    SAMSUNG_BIT_MARK, SAMSUNG_ZERO_SPACE,
                    SAMSUNG_BIT_MARK, SAMSUNG_MIN_GAP,
                    command , 1 ,32);
          break;

  }

}

int init_pwm(struct device* unused) {
  
	pwm_dev_ctx.pwm_dev = device_get_binding(CONFIG_PWM_NRF5_SW_0_DEV_NAME);
	if(!pwm_dev_ctx.pwm_dev) {

		printk("Cannot find %s!\n", CONFIG_PWM_NRF5_SW_0_DEV_NAME);
        return 1;
	}

  timer2_init();
  #ifdef CONFIG_WDT_NRFX
  mesh_wdt_init();  
  #endif

  k_work_init(&change_pwm.send_work ,send_work_ir);
	return 0;
}

DEVICE_INIT(
	ir_board,
	"ir_board",
	init_pwm,
	NULL,
	NULL,
	APPLICATION,
	CONFIG_KERNEL_INIT_PRIORITY_DEVICE);


int rand_number_blocking_generate(u32_t min, u32_t max, u32_t* rn)
{
    int i;
    u32_t rn_32;
    u8_t* rn_8;
    u8_t rn_count;
    u32_t delta;

    rn_8 = (u8_t*)(&rn_32);

    if(max < min) {
        return -EINVAL;
    }
    if(max == min) {
        *rn = min;
        return 0;
    }

    delta = max - min;

    if(delta < 0xFF) {
        rn_count = 1;
    } else if (delta < 0xFFFF) {
        rn_count = 2;
    } else {
        rn_count = 4;
    }

    for(i = 0; i < rn_count; i++) {
        NRF_RNG->TASKS_START = 1;
        k_sleep(1);
        rn_8[i] = NRF_RNG->VALUE;
    }
    // SYS_LOG_DBG("delta: %u, rn_count: %u, rn_32: %u", delta, rn_count, rn_32);
    *rn = (rn_32 % delta) + min;
    return 0;
}


