

#ifndef IRSEND_H_
#define IRSEND_H_


// #define IR_TRANSMIT_EN	13
#define IR_TRANSMIT_PWM	19
// #define IR_LED_CTR		24
// #define TEST_LED		21
#define THREADS_MONITOR_MAIN_START 	  threads_monitor_main_start();
#define THREADS_MONITOR_MAIN_TERMINAL threads_monitor_main_termial();


void send_generic(uint8_t type , unsigned long command);
int rand_number_blocking_generate(u32_t min, u32_t max, u32_t* rn);
void threads_monitor_main_start();
void send_data_ir(const uint16_t headermark, const uint16_t headerspace,
                    const uint16_t onemark , const uint16_t onespace,
                    const uint16_t zeromark, const uint16_t zerospace,
                    const uint16_t gapmark, const uint16_t gapspace,
                    unsigned long data , uint8_t repeat , uint8_t nbits);
/*
	should be called at end of main function.
*/
void threads_monitor_main_termial();

#endif


