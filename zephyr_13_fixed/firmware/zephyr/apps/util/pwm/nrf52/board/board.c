#include <errno.h>
#include <zephyr.h>
#include <misc/printk.h>
#include <device.h>
#include <app.h>
#include <board.h>
#include <logging/sys_log.h>
#include <debug/object_tracing.h>
#include <watchdog.h>
#include <entropy.h>


#define SYS_LOG_LEVEL 4
#define THREAD_TRACKING_INTERVAL 3000 //ms
#define NFC_NOTIFY_DURATION		 100
#define HUMIDITY_INTERVAL		3000


static u8_t threads_count;
struct k_delayed_work thread_timer;
struct k_delayed_work humidity_work;
struct device* entropy;

#ifdef CONFIG_WDT_NRFX
struct {
	struct k_delayed_work feed_work;
	struct device* dev;
	int channel_id;
} mesh_wdt;

static void mesh_wdt_feed(struct k_work* work)
{
	wdt_feed(mesh_wdt.dev, mesh_wdt.channel_id);
	k_delayed_work_submit(&mesh_wdt.feed_work, 1000);
}

static int mesh_wdt_init() 
{
	SYS_LOG_DBG("");
	mesh_wdt.dev = device_get_binding(CONFIG_WDT_0_NAME);
	if(!mesh_wdt.dev)
		return 1;
	struct wdt_timeout_cfg cfg = {
		.window = {0, 10000},
		.callback = NULL,
		.flags = WDT_FLAG_RESET_SOC
	};

	mesh_wdt.channel_id = wdt_install_timeout(mesh_wdt.dev, &cfg);

	if(mesh_wdt.channel_id < 0)
		return 1;
	wdt_setup(mesh_wdt.dev, 0);

	k_delayed_work_init(&mesh_wdt.feed_work, mesh_wdt_feed);
	k_delayed_work_submit(&mesh_wdt.feed_work, 1000); 
	return 0;
}

#endif 


static inline int threads_count_get(void)
{
    int obj_counter = 0;
    struct k_thread *thread_list = NULL;

    /* wait a bit to allow any initialization-only threads to terminate */
    thread_list   = (struct k_thread *)SYS_THREAD_MONITOR_HEAD;
    while (thread_list != NULL) {
        thread_list =
            (struct k_thread *)SYS_THREAD_MONITOR_NEXT(thread_list);
        obj_counter++;
    } 
    return obj_counter;
}




static void thread_tracker(struct k_work* work)
{	
	static u8_t threads;
    threads = threads_count_get();
    // SYS_LOG_DBG("work threads: %d", threads);
    if(threads < threads_count) {
  		SYS_LOG_DBG("System reset");
    	NVIC_SystemReset();
    }
  	if(threads > threads_count) {
    	threads_count = threads;
    }

    k_delayed_work_submit(&thread_timer, THREAD_TRACKING_INTERVAL);
}


void threads_monitor_main_start()
{	

	threads_count = threads_count_get();
	SYS_LOG_DBG("threads_count: %d", threads_count);
	k_delayed_work_init(&thread_timer, thread_tracker);
	k_delayed_work_submit(&thread_timer, THREAD_TRACKING_INTERVAL);
}

void threads_monitor_main_termial()
{
	/*
	 *this function is called at end of main 
	 *so main thread is terminating
	 */
	threads_count = threads_count_get() - 1;
	SYS_LOG_DBG("threads_count: %d", threads_count);
}

/* mo pho*/
static int read_reg16(struct device *i2c_dev, u8_t reg_addr,
           u8_t *data)
{
    u8_t wr_addr;
    struct i2c_msg msgs[2];

    /* Register address */
    wr_addr = reg_addr;

    /* Setup I2C messages */

    /* Send the address to read */
    msgs[0].buf = &wr_addr;
    msgs[0].len = 1;
    msgs[0].flags = I2C_MSG_WRITE;

    /* Read from device. RESTART as neededm and STOP after this. */
    msgs[1].buf = data;
    msgs[1].len = 2;
    msgs[1].flags = I2C_MSG_READ | I2C_MSG_RESTART | I2C_MSG_STOP;

    return i2c_transfer(i2c_dev, &msgs[0], 2, SI7021_DEFAULT_ADDRESS);
}

static int write_reg16(struct device *i2c_dev, u8_t reg_addr,
        u8_t *data)
{
    u8_t wr_addr;
    struct i2c_msg msgs[2];

    /* Register address */
    wr_addr = reg_addr;

    /* Setup I2C messages */

    /* Send the address to read */
    msgs[0].buf = &wr_addr;
    msgs[0].len = 1;
    msgs[0].flags = I2C_MSG_WRITE;

    /* Read from device. RESTART as neededm and STOP after this. */
    msgs[1].buf = data;
    msgs[1].len = 2;
    msgs[1].flags = I2C_MSG_WRITE | I2C_MSG_STOP;

    return i2c_transfer(i2c_dev, &msgs[0], 2, SI7021_DEFAULT_ADDRESS);
}

uint16_t read_huminity(struct device *dev ,u8_t start_addr) {

	u8_t data[2] = {0,0};
	uint16_t hum;
	float humidity =0;
	int ret;

	ret = read_reg16(dev ,start_addr ,data);
	if(ret) {
		printk("Error read_reg16 %s!\n", ret);
		return ;
	}

	hum = data[0] << 8 | data[1];
	humidity = hum;
	humidity *= 125;
    humidity /= 65536;
    humidity -= 6;

	return humidity;
}

uint16_t read_temp(struct device *dev ,u8_t start_addr) {

	u8_t data[2] = {0,0};
	uint16_t temp;
	float temperature = 0;
	int ret;

	ret = read_reg16(dev ,start_addr ,data);
	if(ret) {
		printk("Error read_reg16 %s!\n", ret);
		return ;
	}

	temp = data[0] << 8 | data[1];
	temperature = temp;
	temperature *= 175.72;
    temperature /= 65536;
    temperature -= 46.85;
    return temperature;

}

static void entropy_init()
{
    entropy = device_get_binding(CONFIG_ENTROPY_NAME);
    if(entropy == NULL) {
        printk("Entropy eevice not found\n");
        return;
    }
}



int board_init(struct device* unused) {


	SYS_LOG_DBG("");
	struct device* gpio;
	
	int ret;
	u8_t temp;
	u8_t data[2] = {0,0};

	#ifdef CONFIG_WDT_NRFX
		mesh_wdt_init();	
	#endif
	gpio = device_get_binding(CONFIG_GPIO_P0_DEV_NAME);
	
	if (!gpio) {
		printk("Cannot find %s!\n", CONFIG_GPIO_P0_DEV_NAME);
		return 1;
	}
	gpio_pin_configure(gpio, VDD_SENSOR_CTRL, GPIO_DIR_OUT);
	gpio_pin_write(gpio, VDD_SENSOR_CTRL, 1);
	k_sleep(20);

	

	i2c_dev = device_get_binding("I2C_0");
	if (!i2c_dev) {
		printk("I2C: Device driver not found.\n");
		return;
	}

	entropy_init();

	return 0;
}

DEVICE_INIT(
	sensor_board,
	"sensor_board",
	board_init,
	NULL,
	NULL,
	APPLICATION,
	CONFIG_KERNEL_INIT_PRIORITY_DEVICE);

int rand_number_blocking_generate(u32_t min, u32_t max, u32_t* rn)
{
    int err;
    u32_t rand;

    if(entropy == NULL) {
        return -1;
    }

    err = entropy_get_entropy(entropy, (u8_t*)(&rand), sizeof(rand));
    if(err) {
        SYS_LOG_ERR("Rng failed");
        return err;
    }

    *rn = min + (rand % (max - min));
    return 0;

}