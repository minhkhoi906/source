/* main.c - Application main entry point */

/*
 * Copyright (c) 2015-2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/types.h>
#include <stddef.h>
#include <misc/printk.h>
#include <misc/util.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>

static u8_t mfg_data[] = { 0xff, 0xff, 0x00 };

#define DEVICE_NAME CONFIG_BT_DEVICE_NAME
#define DEVICE_NAME_LEN (sizeof(DEVICE_NAME) - 1)

static const struct bt_data ad[] = {
	BT_DATA(BT_DATA_NAME_COMPLETE, DEVICE_NAME, DEVICE_NAME_LEN),
	BT_DATA(BT_DATA_MANUFACTURER_DATA, mfg_data, 3),
};

static struct bt_le_adv_param param_adv_nconn;
struct bt_le_oob addr_local;

static void set_param_advertise_nconn(void)
{
	/* Lay dia chi local cua thiet bi */
    bt_le_oob_get_local(&addr_local);

	param_adv_nconn.options = 0;
	param_adv_nconn.interval_min = BT_GAP_ADV_FAST_INT_MIN_1;
	param_adv_nconn.interval_max = BT_GAP_ADV_FAST_INT_MAX_1;
	param_adv_nconn.own_addr = &addr_local.addr.a;
}

static void scan_cb(const bt_addr_le_t *addr, s8_t rssi, u8_t adv_type,
		    struct net_buf_simple *buf)
{
	// mfg_data[2]++;
	printk("scan_cb\n");
}

void main(void)
{
	struct bt_le_scan_param scan_param = {
		.type       = BT_HCI_LE_SCAN_PASSIVE,
		.filter_dup = BT_HCI_LE_SCAN_FILTER_DUP_DISABLE,
		.interval   = 0x0010,
		.window     = 0x0010,
	};
	int err;

	printk("Starting Scanner/Advertiser Demo\n");

	/* Initialize the Bluetooth Subsystem */
	err = bt_enable(NULL);
	if (err) {
		printk("Bluetooth init failed (err %d)\n", err);
		return;
	}

	printk("Bluetooth initialized\n");

	set_param_advertise_nconn();

	err = bt_le_scan_start(&scan_param, scan_cb);
	if (err) {
		printk("Starting scanning failed (err %d)\n", err);
		return;
	}
	// k_sleep(K_MSEC(400));

	while(1) {
		// k_sleep(K_MSEC(400));

		/* Start advertising */
		err = bt_le_adv_start(&param_adv_nconn, ad, ARRAY_SIZE(ad),
				      NULL, 0);
		if (err) {
			printk("Advertising failed to start (err %d)\n", err);
			return;
		}

		k_sleep(K_MSEC(50));

		err = bt_le_adv_stop();
		if (err) {
			printk("Advertising failed to stop (err %d)\n", err);
			return;
		}

		printk("Advertise successful \n");
	} 
}
