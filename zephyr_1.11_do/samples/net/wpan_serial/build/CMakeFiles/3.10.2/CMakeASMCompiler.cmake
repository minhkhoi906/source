set(CMAKE_ASM_COMPILER "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/bin/i586-zephyr-elfiamcu/i586-zephyr-elfiamcu-gcc")
set(CMAKE_ASM_COMPILER_ARG1 "")
set(CMAKE_AR "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/bin/i586-zephyr-elfiamcu/i586-zephyr-elfiamcu-ar")
set(CMAKE_ASM_COMPILER_AR "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/bin/i586-zephyr-elfiamcu/i586-zephyr-elfiamcu-gcc-ar")
set(CMAKE_RANLIB "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/bin/i586-zephyr-elfiamcu/i586-zephyr-elfiamcu-ranlib")
set(CMAKE_ASM_COMPILER_RANLIB "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/bin/i586-zephyr-elfiamcu/i586-zephyr-elfiamcu-gcc-ranlib")
set(CMAKE_LINKER "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/bin/i586-zephyr-elfiamcu/i586-zephyr-elfiamcu-ld")
set(CMAKE_ASM_COMPILER_LOADED 1)
set(CMAKE_ASM_COMPILER_ID "GNU")
set(CMAKE_ASM_COMPILER_VERSION "")
set(CMAKE_ASM_COMPILER_ENV_VAR "ASM")


set(CMAKE_ASM_IGNORE_EXTENSIONS h;H;o;O;obj;OBJ;def;DEF;rc;RC)
set(CMAKE_ASM_LINKER_PREFERENCE 0)


