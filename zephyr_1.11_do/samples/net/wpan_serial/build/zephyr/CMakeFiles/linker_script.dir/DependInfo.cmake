# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/x86/soc/intel_quark/quark_se/linker.ld" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/linker.cmd"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../../kernel/include"
  "../../../../arch/x86/include"
  "../../../../arch/x86/soc/intel_quark/quark_se"
  "../../../../arch/x86/soc/intel_quark/quark_se/include"
  "../../../../arch/x86/soc/intel_quark/include"
  "../../../../boards/x86/quark_se_c1000_devboard"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/i586-zephyr-elfiamcu/gcc/i586-zephyr-elfiamcu/6.2.0/include"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/i586-zephyr-elfiamcu/gcc/i586-zephyr-elfiamcu/6.2.0/include-fixed"
  "../../../../lib/libc/minimal/include"
  "../../../../ext/hal/qmsi/include"
  "../../../../ext/hal/qmsi/drivers/include"
  "../../../../ext/hal/qmsi/soc/quark_se/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
