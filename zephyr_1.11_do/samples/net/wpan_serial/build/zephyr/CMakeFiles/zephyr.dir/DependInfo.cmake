# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "ASM"
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_ASM
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/x86/core/cache_s.S" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/arch/x86/core/cache_s.S.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/x86/core/crt0.S" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/arch/x86/core/crt0.S.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/x86/core/excstub.S" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/arch/x86/core/excstub.S.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/x86/core/intstub.S" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/arch/x86/core/intstub.S.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/x86/core/swap.S" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/arch/x86/core/swap.S.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/x86/soc/intel_quark/quark_se/soc_power.S" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/arch/x86/soc/intel_quark/quark_se/soc_power.S.obj"
  )
set(CMAKE_ASM_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_ASM
  "ENABLE_EXTERNAL_ISR_HANDLING"
  "KERNEL"
  "QM_LAKEMONT"
  "SOC_SERIES=quark_se"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_ASM_TARGET_INCLUDE_PATH
  "../../../../drivers"
  "../../../../kernel/include"
  "../../../../arch/x86/include"
  "../../../../arch/x86/soc/intel_quark/quark_se"
  "../../../../arch/x86/soc/intel_quark/quark_se/include"
  "../../../../arch/x86/soc/intel_quark/include"
  "../../../../boards/x86/quark_se_c1000_devboard"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/i586-zephyr-elfiamcu/gcc/i586-zephyr-elfiamcu/6.2.0/include"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/i586-zephyr-elfiamcu/gcc/i586-zephyr-elfiamcu/6.2.0/include-fixed"
  "../../../../lib/libc/minimal/include"
  "../../../../ext/hal/qmsi/include"
  "../../../../ext/hal/qmsi/drivers/include"
  "../../../../ext/hal/qmsi/soc/quark_se/include"
  )
set(CMAKE_DEPENDS_CHECK_C
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/x86/core/cache.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/arch/x86/core/cache.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/x86/core/cpuhalt.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/arch/x86/core/cpuhalt.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/x86/core/fatal.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/arch/x86/core/fatal.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/x86/core/irq_manage.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/arch/x86/core/irq_manage.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/x86/core/msr.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/arch/x86/core/msr.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/x86/core/sys_fatal_error_handler.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/arch/x86/core/sys_fatal_error_handler.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/x86/core/thread.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/arch/x86/core/thread.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/x86/soc/intel_quark/quark_se/eoi.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/arch/x86/soc/intel_quark/quark_se/eoi.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/x86/soc/intel_quark/quark_se/power.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/arch/x86/soc/intel_quark/quark_se/power.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/x86/soc/intel_quark/quark_se/soc.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/arch/x86/soc/intel_quark/quark_se/soc.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/x86/soc/intel_quark/quark_se/soc_config.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/arch/x86/soc/intel_quark/quark_se/soc_config.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/drivers/console/uart_console.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/drivers/console/uart_console.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/drivers/gpio/gpio_qmsi.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/drivers/gpio/gpio_qmsi.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/drivers/ieee802154/ieee802154_cc2520.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/drivers/ieee802154/ieee802154_cc2520.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/drivers/interrupt_controller/ioapic_intr.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/drivers/interrupt_controller/ioapic_intr.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/drivers/interrupt_controller/loapic_intr.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/drivers/interrupt_controller/loapic_intr.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/drivers/interrupt_controller/system_apic.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/drivers/interrupt_controller/system_apic.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/drivers/pinmux/pinmux_qmsi.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/drivers/pinmux/pinmux_qmsi.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/drivers/serial/uart_qmsi.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/drivers/serial/uart_qmsi.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/drivers/spi/spi_qmsi.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/drivers/spi/spi_qmsi.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/drivers/timer/loapic_timer.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/drivers/timer/loapic_timer.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/drivers/timer/sys_clock_init.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/drivers/timer/sys_clock_init.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/drivers/usb/device/usb_dc_dw.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/drivers/usb/device/usb_dc_dw.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/lib/crc/crc16_sw.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/lib/crc/crc16_sw.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/lib/crc/crc8_sw.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/lib/crc/crc8_sw.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/misc/generated/configs.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/misc/generated/configs.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/misc/printk.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/misc/printk.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/subsys/logging/sys_log.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/subsys/logging/sys_log.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/subsys/random/rand32_timestamp.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/subsys/random/rand32_timestamp.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/subsys/usb/class/cdc_acm.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/subsys/usb/class/cdc_acm.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/subsys/usb/usb_descriptor.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/subsys/usb/usb_descriptor.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/subsys/usb/usb_device.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/CMakeFiles/zephyr.dir/subsys/usb/usb_device.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "ENABLE_EXTERNAL_ISR_HANDLING"
  "KERNEL"
  "QM_LAKEMONT"
  "SOC_SERIES=quark_se"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../../drivers"
  "../../../../kernel/include"
  "../../../../arch/x86/include"
  "../../../../arch/x86/soc/intel_quark/quark_se"
  "../../../../arch/x86/soc/intel_quark/quark_se/include"
  "../../../../arch/x86/soc/intel_quark/include"
  "../../../../boards/x86/quark_se_c1000_devboard"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/i586-zephyr-elfiamcu/gcc/i586-zephyr-elfiamcu/6.2.0/include"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/i586-zephyr-elfiamcu/gcc/i586-zephyr-elfiamcu/6.2.0/include-fixed"
  "../../../../lib/libc/minimal/include"
  "../../../../ext/hal/qmsi/include"
  "../../../../ext/hal/qmsi/drivers/include"
  "../../../../ext/hal/qmsi/soc/quark_se/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
