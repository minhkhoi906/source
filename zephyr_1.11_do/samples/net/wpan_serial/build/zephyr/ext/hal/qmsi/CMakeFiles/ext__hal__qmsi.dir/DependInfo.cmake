# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/hal/qmsi/drivers/flash/qm_flash.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/ext/hal/qmsi/CMakeFiles/ext__hal__qmsi.dir/drivers/flash/qm_flash.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/hal/qmsi/drivers/gpio/qm_gpio.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/ext/hal/qmsi/CMakeFiles/ext__hal__qmsi.dir/drivers/gpio/qm_gpio.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/hal/qmsi/drivers/pinmux/qm_pinmux.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/ext/hal/qmsi/CMakeFiles/ext__hal__qmsi.dir/drivers/pinmux/qm_pinmux.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/hal/qmsi/drivers/spi/qm_spi.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/ext/hal/qmsi/CMakeFiles/ext__hal__qmsi.dir/drivers/spi/qm_spi.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/hal/qmsi/drivers/uart/qm_uart.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/ext/hal/qmsi/CMakeFiles/ext__hal__qmsi.dir/drivers/uart/qm_uart.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/hal/qmsi/soc/quark_se/drivers/clk.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/ext/hal/qmsi/CMakeFiles/ext__hal__qmsi.dir/soc/quark_se/drivers/clk.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/hal/qmsi/soc/quark_se/drivers/power_states.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/ext/hal/qmsi/CMakeFiles/ext__hal__qmsi.dir/soc/quark_se/drivers/power_states.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/hal/qmsi/soc/quark_se/drivers/vreg.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/net/wpan_serial/build/zephyr/ext/hal/qmsi/CMakeFiles/ext__hal__qmsi.dir/soc/quark_se/drivers/vreg.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "ENABLE_EXTERNAL_ISR_HANDLING"
  "KERNEL"
  "QM_LAKEMONT"
  "SOC_SERIES=quark_se"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../../kernel/include"
  "../../../../arch/x86/include"
  "../../../../arch/x86/soc/intel_quark/quark_se"
  "../../../../arch/x86/soc/intel_quark/quark_se/include"
  "../../../../arch/x86/soc/intel_quark/include"
  "../../../../boards/x86/quark_se_c1000_devboard"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/i586-zephyr-elfiamcu/gcc/i586-zephyr-elfiamcu/6.2.0/include"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/i586-zephyr-elfiamcu/gcc/i586-zephyr-elfiamcu/6.2.0/include-fixed"
  "../../../../lib/libc/minimal/include"
  "../../../../ext/hal/qmsi/include"
  "../../../../ext/hal/qmsi/drivers/include"
  "../../../../ext/hal/qmsi/soc/quark_se/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
