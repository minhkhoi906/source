# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/soc/nordic_nrf5/nrf51/linker.ld" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/hello_world/build/zephyr/linker_pass_final.cmd"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../kernel/include"
  "../../../arch/arm/include"
  "../../../arch/arm/soc/nordic_nrf5/nrf51"
  "../../../arch/arm/soc/nordic_nrf5/nrf51/include"
  "../../../arch/arm/soc/nordic_nrf5/include"
  "../../../boards/arm/nrf51_pca10028"
  "../../../include"
  "../../../include/drivers"
  "zephyr/include/generated"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include-fixed"
  "../../../lib/libc/minimal/include"
  "../../../ext/hal/cmsis/Include"
  "../../../ext/hal/nordic/nrfx"
  "../../../ext/hal/nordic/nrfx/drivers/include"
  "../../../ext/hal/nordic/nrfx/hal"
  "../../../ext/hal/nordic/nrfx/mdk"
  "../../../ext/hal/nordic/."
  "../../../ext/debug/segger/."
  "../../../subsys/bluetooth"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
