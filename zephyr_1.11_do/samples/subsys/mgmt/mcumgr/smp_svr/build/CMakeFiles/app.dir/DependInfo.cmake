# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/subsys/mgmt/mcumgr/smp_svr/src/main.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/subsys/mgmt/mcumgr/smp_svr/build/CMakeFiles/app.dir/src/main.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "KERNEL"
  "MBEDTLS_CONFIG_FILE=\"config-mini-tls1_2.h\""
  "NRF52832_XXAA"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/kernel/include"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/include"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/soc/nordic_nrf5/nrf52"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/soc/nordic_nrf5/nrf52/include"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/soc/nordic_nrf5/include"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/boards/arm/nrf52_pca10040"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/include"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/include/drivers"
  "zephyr/include/generated"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include-fixed"
  "/opt/zephyr-sdk/sysroots/armv5-zephyr-eabi/usr/include"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/lib/crypto/tinycrypt/include"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/hal/cmsis/Include"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/hal/nordic/nrfx"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/hal/nordic/nrfx/drivers/include"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/hal/nordic/nrfx/hal"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/hal/nordic/nrfx/mdk"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/hal/nordic/."
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/subsys/bluetooth"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/fs/nffs/include"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/lib/crypto/mbedtls/include"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/lib/crypto/mbedtls/configs"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/lib/mgmt/mcumgr/cborattr/include"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/lib/mgmt/mcumgr/cmd/fs_mgmt/include"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/lib/mgmt/mcumgr/cmd/img_mgmt/include"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/lib/mgmt/mcumgr/cmd/os_mgmt/include"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/lib/mgmt/mcumgr/cmd/stat_mgmt/include"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/lib/mgmt/mcumgr/mgmt/include"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/lib/mgmt/mcumgr/mgmt/port/zephyr/include"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/lib/mgmt/mcumgr/smp/include"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/lib/mgmt/mcumgr/util/include"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/lib/encoding/tinycbor/src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
