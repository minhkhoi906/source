# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/subsys/mgmt/mcumgr/smp_svr/build/zephyr/isr_tables.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/subsys/mgmt/mcumgr/smp_svr/build/zephyr/CMakeFiles/kernel_elf.dir/isr_tables.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/misc/empty_file.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/subsys/mgmt/mcumgr/smp_svr/build/zephyr/CMakeFiles/kernel_elf.dir/misc/empty_file.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "KERNEL"
  "MBEDTLS_CONFIG_FILE=\"config-mini-tls1_2.h\""
  "NRF52832_XXAA"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../../../../kernel/include"
  "../../../../../../arch/arm/include"
  "../../../../../../arch/arm/soc/nordic_nrf5/nrf52"
  "../../../../../../arch/arm/soc/nordic_nrf5/nrf52/include"
  "../../../../../../arch/arm/soc/nordic_nrf5/include"
  "../../../../../../boards/arm/nrf52_pca10040"
  "../../../../../../include"
  "../../../../../../include/drivers"
  "zephyr/include/generated"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include-fixed"
  "/opt/zephyr-sdk/sysroots/armv5-zephyr-eabi/usr/include"
  "../../../../../../ext/lib/crypto/tinycrypt/include"
  "../../../../../../ext/hal/cmsis/Include"
  "../../../../../../ext/hal/nordic/nrfx"
  "../../../../../../ext/hal/nordic/nrfx/drivers/include"
  "../../../../../../ext/hal/nordic/nrfx/hal"
  "../../../../../../ext/hal/nordic/nrfx/mdk"
  "../../../../../../ext/hal/nordic/."
  "../../../../../../subsys/bluetooth"
  "../../../../../../ext/fs/nffs/include"
  "../../../../../../ext/lib/crypto/mbedtls/include"
  "../../../../../../ext/lib/crypto/mbedtls/configs"
  "../../../../../../ext/lib/mgmt/mcumgr/cborattr/include"
  "../../../../../../ext/lib/mgmt/mcumgr/cmd/fs_mgmt/include"
  "../../../../../../ext/lib/mgmt/mcumgr/cmd/img_mgmt/include"
  "../../../../../../ext/lib/mgmt/mcumgr/cmd/os_mgmt/include"
  "../../../../../../ext/lib/mgmt/mcumgr/cmd/stat_mgmt/include"
  "../../../../../../ext/lib/mgmt/mcumgr/mgmt/include"
  "../../../../../../ext/lib/mgmt/mcumgr/mgmt/port/zephyr/include"
  "../../../../../../ext/lib/mgmt/mcumgr/smp/include"
  "../../../../../../ext/lib/mgmt/mcumgr/util/include"
  "../../../../../../ext/lib/encoding/tinycbor/src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/subsys/mgmt/mcumgr/smp_svr/build/CMakeFiles/app.dir/DependInfo.cmake"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/subsys/mgmt/mcumgr/smp_svr/build/zephyr/CMakeFiles/zephyr.dir/DependInfo.cmake"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/subsys/mgmt/mcumgr/smp_svr/build/zephyr/lib/libc/newlib/CMakeFiles/lib__libc__newlib.dir/DependInfo.cmake"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/subsys/mgmt/mcumgr/smp_svr/build/zephyr/ext/lib/crypto/mbedtls/CMakeFiles/ext__lib__crypto__mbedtls.dir/DependInfo.cmake"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/subsys/mgmt/mcumgr/smp_svr/build/zephyr/ext/lib/encoding/tinycbor/CMakeFiles/ext__lib__encoding__tinycbor.dir/DependInfo.cmake"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/subsys/mgmt/mcumgr/smp_svr/build/zephyr/ext/lib/mgmt/mcumgr/CMakeFiles/ext__lib__mgmt__mcumgr.dir/DependInfo.cmake"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/subsys/mgmt/mcumgr/smp_svr/build/zephyr/ext/fs/nffs/CMakeFiles/ext__fs__nffs.dir/DependInfo.cmake"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/subsys/mgmt/mcumgr/smp_svr/build/zephyr/subsys/bluetooth/common/CMakeFiles/subsys__bluetooth__common.dir/DependInfo.cmake"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/subsys/mgmt/mcumgr/smp_svr/build/zephyr/subsys/bluetooth/host/CMakeFiles/subsys__bluetooth__host.dir/DependInfo.cmake"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/subsys/mgmt/mcumgr/smp_svr/build/zephyr/subsys/bluetooth/controller/CMakeFiles/subsys__bluetooth__controller.dir/DependInfo.cmake"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/subsys/mgmt/mcumgr/smp_svr/build/zephyr/subsys/fs/CMakeFiles/subsys__fs.dir/DependInfo.cmake"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/subsys/mgmt/mcumgr/smp_svr/build/zephyr/subsys/mgmt/CMakeFiles/subsys__mgmt.dir/DependInfo.cmake"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/subsys/mgmt/mcumgr/smp_svr/build/zephyr/subsys/net/CMakeFiles/subsys__net.dir/DependInfo.cmake"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/subsys/mgmt/mcumgr/smp_svr/build/zephyr/kernel/CMakeFiles/kernel.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
