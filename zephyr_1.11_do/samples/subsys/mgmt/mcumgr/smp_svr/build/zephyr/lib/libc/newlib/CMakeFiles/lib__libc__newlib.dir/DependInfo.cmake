# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/lib/libc/newlib/libc-hooks.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/subsys/mgmt/mcumgr/smp_svr/build/zephyr/lib/libc/newlib/CMakeFiles/lib__libc__newlib.dir/libc-hooks.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "KERNEL"
  "MBEDTLS_CONFIG_FILE=\"config-mini-tls1_2.h\""
  "NRF52832_XXAA"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../../../../kernel/include"
  "../../../../../../arch/arm/include"
  "../../../../../../arch/arm/soc/nordic_nrf5/nrf52"
  "../../../../../../arch/arm/soc/nordic_nrf5/nrf52/include"
  "../../../../../../arch/arm/soc/nordic_nrf5/include"
  "../../../../../../boards/arm/nrf52_pca10040"
  "../../../../../../include"
  "../../../../../../include/drivers"
  "zephyr/include/generated"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include-fixed"
  "/opt/zephyr-sdk/sysroots/armv5-zephyr-eabi/usr/include"
  "../../../../../../ext/lib/crypto/tinycrypt/include"
  "../../../../../../ext/hal/cmsis/Include"
  "../../../../../../ext/hal/nordic/nrfx"
  "../../../../../../ext/hal/nordic/nrfx/drivers/include"
  "../../../../../../ext/hal/nordic/nrfx/hal"
  "../../../../../../ext/hal/nordic/nrfx/mdk"
  "../../../../../../ext/hal/nordic/."
  "../../../../../../subsys/bluetooth"
  "../../../../../../ext/fs/nffs/include"
  "../../../../../../ext/lib/crypto/mbedtls/include"
  "../../../../../../ext/lib/crypto/mbedtls/configs"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
