/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <misc/printk.h>
#include <nrfx_spi.h>


#include "header/spi_m.h"


#define SPI_INSTANCE 0
#define CS_PIN 13
#define MISO_PIN 11
#define MOSI_PIN 10
#define SCK_PIN 12


static const nrfx_spi_t _spi = NRFX_SPI_INSTANCE(SPI_INSTANCE);

int spi_init(){
	printk("[SPI] spi_init... \r\n");

	nrfx_err_t err;
	nrfx_spi_config_t spi_config = NRFX_SPI_DEFAULT_CONFIG;

	spi_config.ss_pin = CS_PIN;
	spi_config.miso_pin = MISO_PIN;
	spi_config.mosi_pin = MOSI_PIN;
	spi_config.sck_pin = SCK_PIN;

	err = nrfx_spi_init(&_spi,&spi_config,NULL, NULL);

	if(err != NRFX_SUCCESS){
		printk("[SPI] spi_init failed\r\n");
		return -1;
	}else{
		printk("[SPI] spi_init OK\r\n");
		return 0;
	}
}

nrfx_err_t spi_transfer(nrfx_spi_t const * const p_instance,
					uint8_t const * p_tx_buffer,
					uint8_t tx_buffer_length,
					uint8_t * p_rx_buffer,
					uint8_t rx_buffer_length){

	nrfx_spi_xfer_desc_t xfer_desc;

	xfer_desc.p_tx_buffer = p_tx_buffer;
	xfer_desc.p_rx_buffer = p_rx_buffer;
	xfer_desc.tx_length = tx_buffer_length;
	xfer_desc.rx_length = rx_buffer_length;

	return nrfx_spi_xfer(p_instance, &xfer_desc, 0);
	
}