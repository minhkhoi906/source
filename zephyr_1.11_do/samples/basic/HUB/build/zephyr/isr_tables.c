
/* AUTO-GENERATED by gen_isr_tables.py, do not edit! */

#include <toolchain.h>
#include <linker/sections.h>
#include <sw_isr_table.h>

u32_t __irq_vector_table _irq_vector_table[39] = {
	0x19fd,
	0x102a3,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
	0x19fd,
};
struct _isr_table_entry __sw_isr_table _sw_isr_table[39] = {
	{(void *)0x0, (void *)0x210d},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x20004400, (void *)0x2483},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x200043e8, (void *)0x22f1},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x10265},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1029f},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x289d},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x10299},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
	{(void *)0x0, (void *)0x1849},
};
