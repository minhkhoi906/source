/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <misc/printk.h>
#include <device.h>
#include <gpio.h>
#include <nrf_pwm.h>
#include <nrf52832_peripherals.h>
#include <nrf_gpio.h>
#include <nrf52.h>
#include <board.h>

#include "header/pwm_hw.h"
#include "header/pwm_sw.h"
#include "header/pwm.h"

static u32_t pwmN_gpiote_ch[]    = PWMN_GPIOTE_CH;
static u32_t pwmN_ppi_ch_a[]     = PWMN_PPI_CH_A;      
static u32_t pwmN_ppi_ch_b[]     = PWMN_PPI_CH_B;   
static u32_t pwmN_timer_cc_num[] = PWMN_TIMER_CC_NUM;

/* Init timer 3 */

static void timer3_init(void)
{
	NRF_TIMER3->BITMODE                 = TIMER_BITMODE_BITMODE_24Bit << TIMER_BITMODE_BITMODE_Pos;
    NRF_TIMER3->PRESCALER               = 0;
    NRF_TIMER3->SHORTS                  = TIMER_SHORTS_COMPARE0_CLEAR_Msk << TIMER_RELOAD_CC_NUM;
    NRF_TIMER3->MODE                    = TIMER_MODE_MODE_Timer << TIMER_MODE_MODE_Pos;
    NRF_TIMER3->CC[TIMER_RELOAD_CC_NUM] = TIMER_RELOAD;    
}

static void timer3_start(void)
{
    NRF_TIMER3->TASKS_START = 1;
}

/* Init timer 4 */

static void timer4_init(void)
{
	NRF_TIMER4->BITMODE                 = TIMER_BITMODE_BITMODE_24Bit << TIMER_BITMODE_BITMODE_Pos;
    NRF_TIMER4->PRESCALER               = 0;
    NRF_TIMER4->SHORTS                  = TIMER_SHORTS_COMPARE0_CLEAR_Msk << TIMER_RELOAD_CC_NUM;
    NRF_TIMER4->MODE                    = TIMER_MODE_MODE_Timer << TIMER_MODE_MODE_Pos;
    NRF_TIMER4->CC[TIMER_RELOAD_CC_NUM] = TIMER_RELOAD;    
}

static void timer4_start(void)
{
    NRF_TIMER4->TASKS_START = 1;
}

/* PWM dung TIMER 3 */

// static void pwm0_init(u32_t pinselect)
// {  
//     NRF_GPIOTE->CONFIG[PWM0_GPIOTE_CH] = GPIOTE_CONFIG_MODE_Task << GPIOTE_CONFIG_MODE_Pos | 
//                                          GPIOTE_CONFIG_POLARITY_Toggle << GPIOTE_CONFIG_POLARITY_Pos | 
//                                          pinselect << GPIOTE_CONFIG_PSEL_Pos | 
//                                          GPIOTE_CONFIG_OUTINIT_High << GPIOTE_CONFIG_OUTINIT_Pos;

//     NRF_PPI->CH[PWM0_PPI_CH_A].EEP = (u32_t)&NRF_TIMER4->EVENTS_COMPARE[PWM0_TIMER_CC_NUM];
//     NRF_PPI->CH[PWM0_PPI_CH_A].TEP = (u32_t)&NRF_GPIOTE->TASKS_CLR[PWM0_GPIOTE_CH];
//     NRF_PPI->CH[PWM0_PPI_CH_B].EEP = (u32_t)&NRF_TIMER4->EVENTS_COMPARE[TIMER_RELOAD_CC_NUM];
//     NRF_PPI->CH[PWM0_PPI_CH_B].TEP = (u32_t)&NRF_GPIOTE->TASKS_SET[PWM0_GPIOTE_CH];
    
//     NRF_PPI->CHENSET               = (1 << PWM0_PPI_CH_A) | (1 << PWM0_PPI_CH_B);
// }

static void pwmN_init(u32_t N, u32_t pinselect)
{    
    if(N < 4)
    {
        NRF_GPIO->DIRSET = 1 << pinselect;
        
        NRF_GPIOTE->CONFIG[pwmN_gpiote_ch[N]] = GPIOTE_CONFIG_MODE_Task << GPIOTE_CONFIG_MODE_Pos | 
                                                GPIOTE_CONFIG_POLARITY_Toggle << GPIOTE_CONFIG_POLARITY_Pos | 
                                                pinselect << GPIOTE_CONFIG_PSEL_Pos | 
                                                GPIOTE_CONFIG_OUTINIT_High << GPIOTE_CONFIG_OUTINIT_Pos;

        NRF_PPI->CH[pwmN_ppi_ch_a[N]].EEP     = (u32_t)&NRF_TIMER3->EVENTS_COMPARE[pwmN_timer_cc_num[N]];
        NRF_PPI->CH[pwmN_ppi_ch_a[N]].TEP     = (u32_t)&NRF_GPIOTE->TASKS_CLR[pwmN_gpiote_ch[N]];
        if((N % 2) == 0)
        {
            NRF_PPI->CH[pwmN_ppi_ch_b[N]].EEP = (u32_t)&NRF_TIMER3->EVENTS_COMPARE[TIMER_RELOAD_CC_NUM];
            NRF_PPI->CH[pwmN_ppi_ch_b[N]].TEP = (u32_t)&NRF_GPIOTE->TASKS_SET[pwmN_gpiote_ch[N]];
        }
        else
        {
            NRF_PPI->FORK[pwmN_ppi_ch_b[N-1]].TEP = (u32_t)&NRF_GPIOTE->TASKS_SET[pwmN_gpiote_ch[N]];
        }
        NRF_PPI->CHENSET                      = (1 << pwmN_ppi_ch_a[N]) | (1 << pwmN_ppi_ch_b[N]); 
    }        
}

static void pwmN_set_duty_cycle(u32_t N, u32_t value)
{
    if(N < 4)
    {
        u32_t pwmN_pin_assignment = (NRF_GPIOTE->CONFIG[pwmN_gpiote_ch[N]] & GPIOTE_CONFIG_PSEL_Msk) >> GPIOTE_CONFIG_PSEL_Pos;
        if(value == 0)
        {
            NRF_GPIOTE->CONFIG[pwmN_gpiote_ch[N]] &= ~GPIOTE_CONFIG_MODE_Msk;
            NRF_GPIOTE->CONFIG[pwmN_gpiote_ch[N]] |= GPIOTE_CONFIG_MODE_Disabled << GPIOTE_CONFIG_MODE_Pos;
            NRF_GPIO->OUTCLR = (1 << pwmN_pin_assignment);
        }
        else if(value >= TIMER_RELOAD)
        {
            NRF_GPIOTE->CONFIG[pwmN_gpiote_ch[N]] &= ~GPIOTE_CONFIG_MODE_Msk;
            NRF_GPIOTE->CONFIG[pwmN_gpiote_ch[N]] |= GPIOTE_CONFIG_MODE_Disabled << GPIOTE_CONFIG_MODE_Pos;
            NRF_GPIO->OUTSET = (1 << pwmN_pin_assignment);
        }
        else
        {
            NRF_GPIOTE->CONFIG[pwmN_gpiote_ch[N]] &= ~GPIOTE_CONFIG_MODE_Msk;
            NRF_GPIOTE->CONFIG[pwmN_gpiote_ch[N]] |= GPIOTE_CONFIG_MODE_Task << GPIOTE_CONFIG_MODE_Pos;
            NRF_TIMER3->CC[pwmN_timer_cc_num[N]] = value;  
        }            
    }
}

u32_t convert_percent_to_value_sw(u16_t percent)
{
	u32_t value;

	value = (u32_t)percent * PWM_1_PERCENT_TIMER_RELOAD;

	return (u32_t)value;
}

int set_percent_pwm_sw(u32_t channel_pin, u16_t percent)
{
	u32_t value = convert_percent_to_value_sw(percent);

	switch(channel_pin)
	{
		case 12:
            pwmN_set_duty_cycle(0, value);
			return 0;
		case 13:
            pwmN_set_duty_cycle(1, value);
			return 0;
		case 14:
            pwmN_set_duty_cycle(2, value);
			return 0;
		case 15:
            pwmN_set_duty_cycle(3, value);
			return 0;		
		default:
			break;
	}
    printk("[PWM] Error set_percent_pin_pwm_sw\n");
	return 1;
}

void pwm_init_sw(void)
{
    timer3_init();
    // timer4_init();
    
    pwmN_init(0, PWM_CHANNEL_12);
    pwmN_init(1, PWM_CHANNEL_13);    
    pwmN_init(2, PWM_CHANNEL_14);
    pwmN_init(3, PWM_CHANNEL_15);

    set_percent_pwm_sw(12,0);
    set_percent_pwm_sw(13,0);
    set_percent_pwm_sw(14,0);
    set_percent_pwm_sw(15,0);

    timer3_start();
    // timer4_start();
}