/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */


#include <zephyr.h>
#include <misc/printk.h>
#include <device.h>
#include <nrf52832_peripherals.h>
#include <board.h>
#include <gpio.h>
#include <nrf_gpio.h>
#include <nrf52.h>

#include "header/74LV165.h"

void shift_reg_init(void)
{
	nrf_gpio_pin_dir_set(SR_LOAD, GPIO_PIN_CNF_DIR_Output);
	nrf_gpio_pin_dir_set(SR_CLK, GPIO_PIN_CNF_DIR_Output);
	nrf_gpio_pin_dir_set(SR_OUT, GPIO_PIN_CNF_DIR_Input);

	nrf_gpio_pin_set(SR_LOAD);
	nrf_gpio_pin_clear(SR_CLK);
}

static void print_led_fail(int bit_led_fail)
{
	switch(bit_led_fail)
	{
		case 0:
			printk("[Fail] LED_FAIL_LIGHT_9 \n");
			break;
		case 1:
			printk("[Fail] LED_FAIL_LIGHT_10 \n");
			break;
		case 2:
			printk("[Fail] LED_FAIL_LIGHT_11 \n");
			break;
		case 3:
			printk("[Fail] LED_FAIL_LIGHT_12 \n");
			break;
		case 4:	
			printk("[Fail] LED_FAIL_LIGHT_13 \n");
			break;
		case 5:
			printk("[Fail] LED_FAIL_LIGHT_14 \n");
			break;
		case 6:
			printk("[Fail] LED_FAIL_LIGHT_15 \n");
			break;
		case 7:
			printk("[Fail] LED_FAIL_LIGHT_16 \n");
			break;
		case 8:
			printk("[Fail] LED_FAIL_LIGHT_1 \n");
			break;
		case 9:
			printk("[Fail] LED_FAIL_LIGHT_2 \n");
			break;
		case 10:
			printk("[Fail] LED_FAIL_LIGHT_3 \n");
			break;
		case 11:
			printk("[Fail] LED_FAIL_LIGHT_4 \n");
			break;
		case 12:
			printk("[Fail] LED_FAIL_LIGHT_5 \n");
			break;
		case 13:
			printk("[Fail] LED_FAIL_LIGHT_6 \n");
			break;
		case 14:
			printk("[Fail] LED_FAIL_LIGHT_7 \n");
			break;
		case 15:
			printk("[Fail] LED_FAIL_LIGHT_8 \n");
			break;
		default:
			break;
	}
}

u16_t read_state(void)
{ 
	nrf_gpio_pin_clear(SR_LOAD);
	k_sleep(20);
	nrf_gpio_pin_set(SR_LOAD);

	int i;
  	u32_t temp = 0;
  	int pin_state;
  	u16_t data = 0;

	for (i = 15; i >= 0; i--)
	{
		if (i == 15)
		{
			temp = nrf_gpio_pin_read(SR_OUT);
		    if (temp) {
		    	pin_state = 1;
		      	
		      	data = data | (1 << i);

		      	// print_led_fail(i);
		    }
		    else {
		      	pin_state = 0;
		    }
		}
		else
		{
			nrf_gpio_pin_toggle(SR_CLK);
		    k_sleep(2);

		    temp = nrf_gpio_pin_read(SR_OUT);
		    if (temp) {
		    	pin_state = 1;
		      	
		      	data = data | (1 << i);

		      	// print_led_fail(i);
		    }
		    else {
		      	pin_state = 0;
		    }

		    nrf_gpio_pin_toggle(SR_CLK);
		}
	}  

	return (u16_t)data;
}

#define K_DATA   	7
#define K_CLK 		9
#define K_LATCH  	8

void enable_74hc595(void)
{
	nrf_gpio_pin_dir_set(K_DATA, GPIO_PIN_CNF_DIR_Output);
	nrf_gpio_pin_dir_set(K_CLK, GPIO_PIN_CNF_DIR_Output);
	nrf_gpio_pin_dir_set(K_LATCH, GPIO_PIN_CNF_DIR_Output);

	// nrf_gpio_pin_set(K_DATA);
	nrf_gpio_pin_clear(K_CLK);
	nrf_gpio_pin_clear(K_LATCH);

	for (int i = 0; i < 16; ++i)
	{
		nrf_gpio_pin_set(K_DATA);
		nrf_gpio_pin_set(K_CLK);
		k_sleep(20);
		nrf_gpio_pin_clear(K_CLK);
	}

	nrf_gpio_pin_set(K_LATCH);
	k_sleep(20);
	nrf_gpio_pin_clear(K_LATCH);
}