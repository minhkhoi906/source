#ifndef __PWM_HW__
#define __PWM_HW__

/**
 * 
 * 
 * 
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif

#define PWM_COUNTER_TOP     16000
#define PWM_1_PERCENT       160
#define PWM_1_THOUSANDS     16

static nrf_pwm_values_individual_t m_seq_values;

static nrf_pwm_sequence_t const seq_0 =
{
    .values.p_individual = &m_seq_values,
    .length              = NRF_PWM_VALUES_LENGTH(m_seq_values),
    .repeats             = 1,
    .end_delay           = 0
};

static nrf_pwm_sequence_t const seq_1 =
{
    .values.p_individual = &m_seq_values,
    .length              = NRF_PWM_VALUES_LENGTH(m_seq_values),
    .repeats             = 1,
    .end_delay           = 0
};

static nrf_pwm_sequence_t const seq_2 =
{
    .values.p_individual = &m_seq_values,
    .length              = NRF_PWM_VALUES_LENGTH(m_seq_values),
    .repeats             = 1,
    .end_delay           = 0
};

void pwm_init_hw(void);

/* 0..11 */
int set_percent_pwm_hw(u32_t channel_pin, u16_t percent);

u16_t convert_percent_to_duty_circle_hw(u16_t percent);

int set_thousands_pwm_hw(u32_t channel_pin, u16_t thousands);

u16_t convert_thousands_to_duty_circle_hw(u16_t thousands);

#ifdef __cplusplus
}
#endif

/**
 * @}
 */

#endif /* __PWM_HW__ */