#include <nrfx_spi.h>


#include "header/spim_0.h"


#define SPI_INSTANCE 0
#define CS_PIN 13
#define MISO_PIN 11
#define MOSI_PIN 10
#define SCK_PIN 12

const unsigned int CRC_Table[]=
{
    0x00,0x07,0x0E,0x09,0x1C,0x1B,0x12,0x15,
    0x38,0x3F,0x36,0x31,0x24,0x23,0x2A,0x2D,
    0x70,0x77,0x7E,0x79,0x6C,0x6B,0x62,0x65,
    0x48,0x4F,0x46,0x41,0x54,0x53,0x5A,0x5D,
    0xE0,0xE7,0xEE,0xE9,0xFC,0xFB,0xF2,0xF5,
    0xD8,0xDF,0xD6,0xD1,0xC4,0xC3,0xCA,0xCD,
    0x90,0x97,0x9E,0x99,0x8C,0x8B,0x82,0x85,
    0xA8,0xAF,0xA6,0xA1,0xB4,0xB3,0xBA,0xBD,
    0xC7,0xC0,0xC9,0xCE,0xDB,0xDC,0xD5,0xD2,
    0xFF,0xF8,0xF1,0xF6,0xE3,0xE4,0xED,0xEA,
    0xB7,0xB0,0xB9,0xBE,0xAB,0xAC,0xA5,0xA2,
    0x8F,0x88,0x81,0x86,0x93,0x94,0x9D,0x9A,
    0x27,0x20,0x29,0x2E,0x3B,0x3C,0x35,0x32,
    0x1F,0x18,0x11,0x16,0x03,0x04,0x0D,0x0A,
    0x57,0x50,0x59,0x5E,0x4B,0x4C,0x45,0x42,
    0x6F,0x68,0x61,0x66,0x73,0x74,0x7D,0x7A,
    0x89,0x8E,0x87,0x80,0x95,0x92,0x9B,0x9C,
    0xB1,0xB6,0xBF,0xB8,0xAD,0xAA,0xA3,0xA4,
    0xF9,0xFE,0xF7,0xF0,0xE5,0xE2,0xEB,0xEC,
    0xC1,0xC6,0xCF,0xC8,0xDD,0xDA,0xD3,0xD4,
    0x69,0x6E,0x67,0x60,0x75,0x72,0x7B,0x7C,
    0x51,0x56,0x5F,0x58,0x4D,0x4A,0x43,0x44,
    0x19,0x1E,0x17,0x10,0x05,0x02,0x0B,0x0C,
    0x21,0x26,0x2F,0x28,0x3D,0x3A,0x33,0x34,
    0x4E,0x49,0x40,0x47,0x52,0x55,0x5C,0x5B,
    0x76,0x71,0x78,0x7F,0x6A,0x6D,0x64,0x63,
    0x3E,0x39,0x30,0x37,0x22,0x25,0x2C,0x2B,
    0x06,0x01,0x08,0x0F,0x1A,0x1D,0x14,0x13,
    0xAE,0xA9,0xA0,0xA7,0xB2,0xB5,0xBC,0xBB,
    0x96,0x91,0x98,0x9F,0x8A,0x8D,0x84,0x83,
    0xDE,0xD9,0xD0,0xD7,0xC2,0xC5,0xCC,0xCB,
    0xE6,0xE1,0xE8,0xEF,0xFA,0xFD,0xF4,0xF3
};

static inline uint8_t FW_CRC (uint8_t temp_buf, uint8_t LastCRC){

    // printk("*spim_0* FW_CRC temp_buf= 0x%X, LastCRC = 0x%X,LastCRC ^ temp_buf= 0x%X, LastCRC= 0x%X\r\n",temp_buf,
    //        LastCRC,LastCRC ^ temp_buf, CRC_Table[LastCRC ^ temp_buf]);

    LastCRC = CRC_Table[LastCRC ^ temp_buf];

    return LastCRC;
}


void spi_event_handler(nrfx_spi_evt_handler_t const *p_event){
	printk("[SPI] spi_event_handler... \r\n");
}

static const nrfx_spi_t _spi = NRFX_SPI_INSTANCE(SPI_INSTANCE);

int spi_init(){
	printk("[SPI] spi_init... \r\n");

	nrfx_err_t err;
	nrfx_spi_config_t spi_config = NRFX_SPI_DEFAULT_CONFIG;

	spi_config.ss_pin = CS_PIN;
	spi_config.miso_pin = MISO_PIN;
	spi_config.mosi_pin = MOSI_PIN;
	spi_config.sck_pin = SCK_PIN;

	err = nrfx_spi_init(&_spi,&spi_config,NULL, NULL);

	if(err != NRFX_SUCCESS){
		printk("[SPI] spi_init failed\r\n");
		return -1;
	}else{
		printk("[SPI] spi_init OK\r\n");
		return 0;
	}
}

nrfx_err_t spi_transfer(nrfx_spi_t const * const p_instance,
					uint8_t const * p_tx_buffer,
					uint8_t tx_buffer_length,
					uint8_t * p_rx_buffer,
					uint8_t rx_buffer_length){

	nrfx_spi_xfer_desc_t xfer_desc;

	xfer_desc.p_tx_buffer = p_tx_buffer;
	xfer_desc.p_rx_buffer = p_rx_buffer;
	xfer_desc.tx_length = tx_buffer_length;
	xfer_desc.rx_length = rx_buffer_length;

	return nrfx_spi_xfer(p_instance, &xfer_desc, 0);
	
}


int get_data_consumption_t(uint8_t *raw_data)
{
	// uint8_t tx_buf[2];
	uint8_t tx_buf;
	uint8_t rx_buf[2];
	int index = 0;
	// tx_buf[0] = 0xE5;

	tx_buf = 0xE5;

	nrfx_err_t err_code;

	err_code = spi_transfer(&_spi, &tx_buf, 1, rx_buf, 1);

	printk("[SPI] recv rx_buf[0] = %02X rx_buf[1] = %02X \r\n",rx_buf[0]
												  			  ,rx_buf[1]);

	// printk("[SPI] recv rx_buf[0] = %02X \n",rx_buf[0]);

	if (err_code != NRFX_SUCCESS)
	{
		printk("[ERR] get data : NRFX_ERROR_BUSY \n");
		return -1;
	}

		// k_sleep(100);
		// if((rx_buf[0] == 0x68)){
		// 	raw_data[index] = rx_buf[1] ;
		// 	index++;
			// printk("[SPI] recv rx_buf[0] = %02X  \r\n",rx_buf[0]);

		// 	for(int i = 0; i < 11; i++){

		// 		spi_transfer(&_spi,tx_buf,1,rx_buf,2);
	 //            raw_data[index] = rx_buf[1];


		// 		// printk("[SPI] recv rx_buf[0] = %02X  \r\n",rx_buf[0]);

	 //            if(index >= 11){ //checksum

	 //                raw_data[10] = 0;
	 //                raw_data[11] = 0;
	 //                uint8_t cks = 0;
	 //                for (int cc = 0 ; cc < 11 ; cc++){
	 //                    cks = FW_CRC(raw_data[cc], cks) ;
	 //                }

	 //                if(cks == rx_buf[1]){ //data OK
	 //                    // printk("*SPI* Checksum Cks = 0x%X OK OK OK OK OK OK \r\n ",rx_buf[0]);
	 //                    raw_data[11] = rx_buf[1];
	 //                }
	                
	 //                index = 0;
	 //            }

	 //            index ++;
		//         k_sleep(100);
		// 	} //end for
		// 	return 0;

		// }else{
		// 	printk("\r\n[SPI] got wrong data... \r\n");
		// 	return -1;
		// }

}

// int get_data_consumption_t(uint8_t *raw_data, u8_t cmd){


// 	uint8_t *tx_buf;
// 	uint8_t rx_buf[13];
// 	int index = 0;
// 	// tx_buf[0] = 0xE5;


// 	switch(cmd){
// 		case 0xE5:
// 			*tx_buf = cmd;
// 			spi_transfer(&_spi, tx_buf, 1, NULL, 0);
// 			break;
// 		case 0xE4:
// 			*tx_buf = cmd;
// 			spi_transfer(&_spi, NULL, 0, rx_buf, 13);
// 			break;
// 		default:
// 			break;
// 	}

// 		spi_transfer(&_spi,tx_buf, 1, rx_buf, 2);
// 		// printk("\r\n[SPI] recv rx_buf[0] = %02X  \r\n",rx_buf[0]);

// 		k_sleep(100);
// 		if((rx_buf[0] == 0x68)){
// 			raw_data[index] = rx_buf[1] ;
// 			index++;
// 			// printk("[SPI] recv rx_buf[0] = %02X  \r\n",rx_buf[0]);

// 			for(int i = 0; i < 11; i++){

// 				spi_transfer(&_spi,tx_buf,1,rx_buf,2);
// 	            raw_data[index] = rx_buf[1];


// 				// printk("[SPI] recv rx_buf[0] = %02X  \r\n",rx_buf[0]);

// 	            if(index >= 11){ //checksum

// 	                raw_data[10] = 0;
// 	                raw_data[11] = 0;
// 	                uint8_t cks = 0;
// 	                for (int cc = 0 ; cc < 11 ; cc++){
// 	                    cks = FW_CRC(raw_data[cc], cks) ;
// 	                }

// 	                if(cks == rx_buf[1]){ //data OK
// 	                    // printk("*SPI* Checksum Cks = 0x%X OK OK OK OK OK OK \r\n ",rx_buf[0]);
// 	                    raw_data[11] = rx_buf[1];
// 	                }
	                
// 	                index = 0;
// 	            }

// 	            index ++;
// 		        k_sleep(100);
// 			} //end for
// 			return 0;

// 		}else{
// 			printk("\r\n[SPI] got wrong data... \r\n");
// 			return -1;
// 		}

// }













