#ifndef WATCHDOG_H_
#define WATCHDOG_H_

#include <zephyr.h>
#include <nrfx_wdt.h>

nrfx_wdt_channel_id _channel_id;

int watchdog_reload();
int watchdog_init(uint32_t timeout_ms);




#endif //WATCHDOG_H_