#include <zephyr.h>
#include <board.h>
#include <device.h>
#include <gpio.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>



/* Change this if you have an LED connected to a custom port */
#define LED_PORT	LED0_GPIO_PORT

/* Change this if you have an LED connected to a custom pin */
#define LED	12

/* 1000 msec = 1 sec */
#define SLEEP_TIME 	1000

/* size of stack area used by each thread */
#define STACKSIZE 1024

/* scheduling priority used by each thread */
#define PRIORITY 7
/* scheduling priority used by each thread */
#define PRIORITY_ADV 6
/* scheduling priority used by each thread */
#define PRIORITY_ADC 8

#define SIZE_RAWDATA 12

/* declare struct*/

struct package_t {
	uint8_t length;
	uint8_t manufacturer;
	uint16_t company_ID;
	uint16_t sequence_ID;
	uint16_t command_ID;
	uint8_t gateway;
	uint8_t add[6];
	uint8_t hub_add;
	uint8_t state;
	uint16_t request_ID;

};

/* Global variable*/

volatile bool is_bt_ready = false;

uint8_t _address[6];

int _sets[] = {15,16,17,20};


/* BLE declaration */

#define ADV_RESPONSE 1
#define ADV_REPORT 2
#define ADV_STATUS 3

#define EVENT_CONTROL_HUB 0X26
#define EVENT_ACTIVE_POWER 0X71


#define DEVICE_NAME CONFIG_BT_DEVICE_NAME
#define DEVICE_NAME_LEN (sizeof(DEVICE_NAME) - 1)
#define DEVICE_NAME_LEN_MAX 6



#define DEVICE_NAME_POWER "POWER"
#define DEVICE_NAME_POWER_LEN (sizeof(DEVICE_NAME_POWER) - 1)






static struct bt_le_adv_param param_adv_nconn;
static struct bt_le_oob addr_local;
static uint16_t _sequence_Id;
static uint16_t _sequence_Id_res;


struct bt_le_scan_param scan_param = {
	.type       = BT_HCI_LE_SCAN_PASSIVE,
	.filter_dup = BT_HCI_LE_SCAN_FILTER_DUP_DISABLE,
	.interval   = 0x0010,
	.window     = 0x0010,
};

#define COMPANY_OFSET 0
#define SEQUENCE_OFSET 2
#define COMMAND_OFSET 4

#define VOLTAGE_OFSET 7
#define AMPE_OFSET 9


#define ENERGY_OFSET 11

static uint8_t _response[] = {
				  0xFF,0x0F, 					/*CompanyID*/
				  0x00,0x00, 					/*SequenceID*/
				  0xFF,0x00, 					/*CommandID*/
				  0x00,							/*Send BLE device*/
				  0x00,0x26,					/*Type of device*/
				  0x11,0x22,0x33,0x44,			/*Token*/
				  0x00,0x00,					/*Request ID*/
				  0x00,							/*Reset time*/
				  0x00 							/*Status of device*/
				};

static uint8_t _status[] = {
				  0xFF,0x0F, 					/*CompanyID*/
				  0x00,0x00, 					/*SequenceID*/
				  0x00,0x27, 					/*CommandID*/
				  0x00,							/*Send BLE device*/
				  0x00,0x00,0x00,0x00,			/**/
				  0x00,0x00 					/*Value status all port*/
				};


static uint8_t _consumpt[] = {
				  0xFF,0x0F, 					/*CompanyID*/
				  0x00,0x00, 					/*SequenceID*/
				  0x71,0x00, 					/*CommandID*/
				  0x00,							/*Send BLE device*/
				  0x00,0x00,0x00,0x00,			/*Voltage value*/
				  0x00,0x00,0x00,0x00,			/*Ampe value*/
				};














