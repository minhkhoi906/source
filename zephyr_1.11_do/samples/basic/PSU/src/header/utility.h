#ifndef UTILITY_H_
#define UTILITY_H_

#include <zephyr.h>

/* Global function*/
uint16_t to_litle(uint16_t data);
uint16_t to_big(uint16_t data);
uint32_t swap_uint32(uint8_t *val);
void swap_6byte( uint8_t *val);
uint16_t extract_16(uint8_t _first, uint8_t _last);
#endif //UTILITY_H_