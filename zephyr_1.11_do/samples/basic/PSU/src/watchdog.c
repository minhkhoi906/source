#include "header/watchdog.h"




static void wdt_event_handler(void){

}

int watchdog_reload(){
	nrfx_wdt_channel_feed(_channel_id);	
}



int watchdog_init(uint32_t timeout_ms){

	uint32_t err_code = NRFX_SUCCESS;


	nrfx_wdt_config_t config = NRFX_WDT_DEAFULT_CONFIG;
	config.reload_value = timeout_ms; //defaut 2000ms

	err_code = nrfx_wdt_init(&config,wdt_event_handler);

	err_code = nrfx_wdt_channel_alloc(&_channel_id);

	nrfx_wdt_enable();

	return err_code;

}