/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <board.h>
#include <device.h>
#include <gpio.h>
#include <uart.h>
#include <stdio.h>
#include <net/buf.h>
#include <net/net_pkt.h>
#include <nrf_gpio.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>

#include "header/crc16.h"
#include "header/watch_dog.h"

#define BLE_PAYLOAD_SIZE 	31
#define BLE_ADDRESS_SIZE 	6

#define BLE_COMPANYID_VNG 		0xFF0F
#define BLE_SEND_GATEWAY		0x00

#define ADDR_SIZE   		6
#define RSSI_SIZE			1
#define LEN_SIZE			1
#define TYPE_SIZE			1
#define CRC_SIZE			2

#define HEADER_BUF_SIZE		ADDR_SIZE + RSSI_SIZE + LEN_SIZE + TYPE_SIZE

#define SLIP_END            0xC0   /* indicates end of packet */
#define SLIP_ESC            0xDB   /* indicates byte stuffing */
#define SLIP_ESC_END        0xDC   /* ESC ESC_END means END data byte */
#define SLIP_ESC_ESC        0xDD   /* ESC ESC_ESC means ESC data byte */
#define SLIP_ESC_XON        0xDE   /* ESC SLIP_ESC_XON means XON control byte */
#define SLIP_ESC_XOFF       0xDF   /* ESC SLIP_ESC_XON means XOFF control byte */
#define XON                 0x11   /* indicates XON charater */
#define XOFF                0x13   /* indicates XOFF charater */

/* TX queue */
static struct k_sem tx_sem;
static struct k_fifo tx_queue;
static K_THREAD_STACK_DEFINE(tx_stack, 1024);
static struct k_thread tx_thread_data;

struct bt_le_scan_param scan_param = {
	.type       = BT_HCI_LE_SCAN_PASSIVE,
	.filter_dup = BT_HCI_LE_SCAN_FILTER_DUP_DISABLE,
	.interval   = 0x0010,
	.window     = 0x0010,
}; 

struct scan_packet
{
	u8_t address[6];
	u16_t company_ID;
	u8_t gateway;
	u16_t crc;
};

static struct device *uart_dev;

/* 
 * Tach goi tin scan thanh cac frame de xu ly
 *
 */

static void ad_parse(struct net_buf_simple *ad, 
					 const bt_addr_le_t *addr,
					 s8_t rssi,
				     bool (*func)(const bt_addr_le_t *addr, 
				     			  s8_t rssi, 
				     			  u8_t type, 
				     			  const u8_t *data,
		  		     			  u8_t data_len))
{
	while (ad->len > 1) {
		u8_t len = net_buf_simple_pull_u8(ad);
		u8_t type;

		/* Check for early termination */
		if (len == 0) {
			return;
		}

		if (len > ad->len || ad->len < 1) {
			/*  printk("AD malformed\n");  */
			return;
		}

		type = net_buf_simple_pull_u8(ad);

		if (!func(addr, rssi, type, ad->data, len - 1))
		{
			return;
		}

		net_buf_simple_pull(ad, len - 1);
	}
}

/** 
 *	Ham cb xu ly goi tin 
 */

static bool process_node(const bt_addr_le_t *addr, 
						 s8_t rssi,
						 u8_t type, 
						 const u8_t *data, 
						 u8_t data_len)
{
	struct scan_packet packet;

	struct net_buf *buf;

	switch(type)
	{
		case BT_DATA_MANUFACTURER_DATA:

			packet.company_ID = (u16_t)data[0] << 8 
							    | (u16_t)data[1] << 0;

			if (packet.company_ID != BLE_COMPANYID_VNG)
			{
				return false;
			}

			packet.gateway = data[6];

			if (packet.gateway != BLE_SEND_GATEWAY)
			{
				return false;
			}

			size_t size = HEADER_BUF_SIZE + data_len + CRC_SIZE;

			u8_t *data_buf = k_malloc(size);

			u8_t len = data_len + 1;

			data_buf[0] = addr->a.val[5];
			data_buf[1] = addr->a.val[4];
			data_buf[2] = addr->a.val[3];
			data_buf[3] = addr->a.val[2];
			data_buf[4] = addr->a.val[1];
			data_buf[5] = addr->a.val[0];

			data_buf[6] = rssi;
			data_buf[7] = len;
			data_buf[8] = type;

			for (int i = 0; i < data_len; ++i)
			{
				data_buf[9 + i] = data[i];
			}

			len = HEADER_BUF_SIZE + data_len;

			packet.crc = calculate(data_buf, len);

			data_buf[len] = (u8_t)(packet.crc & 0x00ff);
			data_buf[len + 1] = (u8_t)(packet.crc >> 8);

			size_t buf_size = sizeof(struct net_buf);
			struct net_buf *mem_ptr = k_malloc(buf_size);
			mem_ptr->data = data_buf;
			mem_ptr->len = size;

			k_fifo_put(&tx_queue, mem_ptr);

			return false;
		default:
			return true;
	}	
}

/** 
 *	Ham cb khi scan duoc goi tin
 */

static void device_found(const bt_addr_le_t *addr, s8_t rssi, u8_t type,
			 struct net_buf_simple *ad)
{
	if (type == BT_LE_ADV_NONCONN_IND)
	{
		ad_parse(ad, addr, rssi, process_node);
	}
}

static void init_bt(void){

	int err;

	printk("[BT] BT_Scan_init...\n");

	/* Initialize the Bluetooth Subsystem */
	err = bt_enable(NULL);

	if (err) {
        printk("[BT] Bluetooth init failed (err %d)\n", err);
		return;
	}
}

/**
 * 	Encode buf and send by uart
 */

static u8_t slip_send(struct net_buf *buf)
{
	size_t len = buf->len;

	u8_t slip_size = 0;

	for (int i = 0; i < len; i++) {
		u8_t byte = buf->data[i];

		switch (byte) {
			case SLIP_END:
				uart_poll_out(uart_dev, SLIP_ESC);
				uart_poll_out(uart_dev, SLIP_ESC_END);
				slip_size += 2;
				break;
			case SLIP_ESC:
				uart_poll_out(uart_dev, SLIP_ESC);
				uart_poll_out(uart_dev, SLIP_ESC_ESC);
				slip_size += 2;
				break;
			case XON:
				uart_poll_out(uart_dev, SLIP_ESC);
				uart_poll_out(uart_dev, SLIP_ESC_XON);
				slip_size += 2;
				break;
			case XOFF:
				uart_poll_out(uart_dev, SLIP_ESC);
				uart_poll_out(uart_dev, SLIP_ESC_XOFF);
				slip_size += 2;
				break;
			default:
				uart_poll_out(uart_dev, byte);
				slip_size++;
		}
	}

	uart_poll_out(uart_dev, SLIP_END);
	slip_size++;

	return slip_size;
}

/**
 * TX - transmit to SLIP interface
 */

static void tx_thread(void)
{
	printk("[THREAD] TX thread started\n");

	/* Allow to send one TX */
	// k_sem_give(&tx_sem);

	while (1) {
		struct net_buf *buf;
		u8_t len;

		/* Wait 2s to a buffer is available */
		buf = k_fifo_get(&tx_queue, K_MSEC(2000));
		
		if (buf)
		{	
			/* SLIP encode and send */
			len = slip_send(buf);
		}

		k_free(buf->data);
		k_free(buf);

		feed_dog(0);
	}
}

static void init_tx_queue(void)
{
	// k_sem_init(&tx_sem, 1, 1);
	k_fifo_init(&tx_queue);

	k_thread_create(&tx_thread_data, tx_stack,
			K_THREAD_STACK_SIZEOF(tx_stack),
			(k_thread_entry_t)tx_thread,
			NULL, NULL, NULL, K_PRIO_COOP(8), 0, K_NO_WAIT);
}

void main(void)
{
	/* Configure watch dog after 5s */
	watchdog_config(5000, 1);

	/* Initialize TX queue */
	init_tx_queue();

	uart_dev = device_get_binding(CONFIG_UART_NRF5_NAME);

    if (!uart_dev) {
    	printk("[UART] Cannot get UART device\n");
        return;
    }

	init_bt();

	int err;

	err = bt_le_scan_start(&scan_param, device_found);

	if (err) {
		printk("Starting scanning failed (err %d)\n", err);
		return;
	}
}
