/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <board.h>
#include <device.h>

#include "header/nfc_conf.h"
#include "header/nfc_ndef_msg.h"
#include "header/nrf_error_nfc.h"



/**
 * @brief Resolve the value of record location flags of the NFC NDEF record within an NFC NDEF message.
 */
__STATIC_INLINE nfc_ndef_record_location_t record_location_get(uint32_t index,
                                                               uint32_t record_count)
{
    nfc_ndef_record_location_t record_location;

    if (index == 0)
    {
        if (record_count == 1)
        {
            record_location = NDEF_LONE_RECORD;
        }
        else
        {
            record_location = NDEF_FIRST_RECORD;
        }
    }
    else if (record_count == index + 1)
    {
        record_location = NDEF_LAST_RECORD;
    }
    else
    {
        record_location = NDEF_MIDDLE_RECORD;
    }

    return record_location;
}

#define NDEF_RECORD_BASE_LONG_SIZE          (2 + NDEF_RECORD_PAYLOAD_LEN_LONG_SIZE)


__STATIC_INLINE uint32_t record_header_size_calc(nfc_ndef_record_desc_t const * p_ndef_record_desc)
{
    uint32_t len = NDEF_RECORD_BASE_LONG_SIZE;

    len += p_ndef_record_desc->id_length + p_ndef_record_desc->type_length;

    if (p_ndef_record_desc->id_length > 0)
    {
        len++;
    }

    return len;
}

ret_code_t nfc_ndef_record_encode(nfc_ndef_record_desc_t const * p_ndef_record_desc,
                                  nfc_ndef_record_location_t     record_location,
                                  uint8_t                      * p_record_buffer,
                                  uint32_t                     * p_record_len)
{
    uint8_t * p_flags;              // use as pointer to TNF + flags field
    uint8_t * p_payload_len = NULL; // use as pointer to payload length field
    uint32_t  record_payload_len;

    if (p_ndef_record_desc == NULL)
    {
        return NRF_ERROR_NULL;
    }

    // count record length without payload
    uint32_t record_header_len = record_header_size_calc(p_ndef_record_desc);
    uint32_t err_code          = NRF_SUCCESS;

    if (p_record_buffer != NULL)
    {
        /* verify location range */
        if ((record_location & (~NDEF_RECORD_LOCATION_MASK)) != 0x00)
        {
            return NRF_ERROR_INVALID_PARAM;
        }

        /* verify if there is enough available memory */
        if (record_header_len > *p_record_len)
        {
            return NRF_ERROR_NO_MEM;
        }

        p_flags = p_record_buffer;
        p_record_buffer++;

        // set location bits and clear other bits in 1st byte.
        *p_flags = record_location;

        *p_flags |= p_ndef_record_desc->tnf;

        /* TYPE LENGTH */
        *(p_record_buffer++) = p_ndef_record_desc->type_length;

        // use always long record and remember payload len field memory offset.
        p_payload_len    = p_record_buffer;
        p_record_buffer += NDEF_RECORD_PAYLOAD_LEN_LONG_SIZE;

        /* ID LENGTH - option */
        if (p_ndef_record_desc->id_length > 0)
        {
            *(p_record_buffer++) = p_ndef_record_desc->id_length;

            /* IL flag */
            *p_flags |= NDEF_RECORD_IL_MASK;
        }

        /* TYPE */
        memcpy(p_record_buffer, p_ndef_record_desc->p_type, p_ndef_record_desc->type_length);
        p_record_buffer += p_ndef_record_desc->type_length;

        /* ID */
        if (p_ndef_record_desc->id_length > 0)
        {
            memcpy(p_record_buffer, p_ndef_record_desc->p_id, p_ndef_record_desc->id_length);
            p_record_buffer += p_ndef_record_desc->id_length;
        }

        // count how much memory is left in record buffer for payload field.
        record_payload_len = (*p_record_len - record_header_len);
    }

    /* PAYLOAD */
    if (p_ndef_record_desc->payload_constructor != NULL)
    {
        err_code =
            p_ndef_record_desc->payload_constructor(p_ndef_record_desc->p_payload_descriptor,
                                                    p_record_buffer,
                                                    &record_payload_len);

        if (err_code != NRF_SUCCESS)
        {
            return err_code;
        }
    }
    else
    {
        return NRF_ERROR_NULL;
    }

    if (p_record_buffer != NULL)
    {
        /* PAYLOAD LENGTH */
        // (void) uint32_big_encode(record_payload_len, p_payload_len);
    }

    *p_record_len = record_header_len + record_payload_len;

    return NRF_SUCCESS;
}


ret_code_t nfc_ndef_msg_encode(nfc_ndef_msg_desc_t const * p_ndef_msg_desc,
                               uint8_t                   * p_msg_buffer,
                               uint32_t * const            p_msg_len)
{
    nfc_ndef_record_location_t record_location;
    uint32_t                   temp_len;
    uint32_t                   i;
    uint32_t                   err_code;

    uint32_t sum_of_len = 0;

    if ((p_ndef_msg_desc == NULL) || p_msg_len == NULL)
    {
        return NRF_ERROR_NULL;
    }

    nfc_ndef_record_desc_t * * pp_record_rec_desc = p_ndef_msg_desc->pp_record;

    if (p_ndef_msg_desc->pp_record == NULL)
    {
        return NRF_ERROR_NULL;
    }

#if NFC_NDEF_MSG_TAG_TYPE == TYPE_4_TAG
    uint8_t * p_root_msg_buffer = p_msg_buffer;

    if (p_msg_buffer != NULL)
    {
        if (*p_msg_len < NLEN_FIELD_SIZE)
        {
            return NRF_ERROR_NO_MEM;
        }

        p_msg_buffer += NLEN_FIELD_SIZE;
    }
    sum_of_len += NLEN_FIELD_SIZE;
#endif

    for (i = 0; i < p_ndef_msg_desc->record_count; i++)
    {
        record_location = record_location_get(i, p_ndef_msg_desc->record_count);

        temp_len = *p_msg_len - sum_of_len;

        err_code = nfc_ndef_record_encode(*pp_record_rec_desc,
                                          record_location,
                                          p_msg_buffer,
                                          &temp_len);

        if (err_code != NRF_SUCCESS)
        {
            return err_code;
        }

        sum_of_len += temp_len;
        if (p_msg_buffer != NULL)
        {
            p_msg_buffer += temp_len;
        }

        /* next record */
        pp_record_rec_desc++;
    }

    if (p_msg_buffer != NULL)
    {
        if (sum_of_len - NLEN_FIELD_SIZE > UINT16_MAX)
        {
            return NRF_ERROR_NOT_SUPPORTED;
        }

    }

    *p_msg_len = sum_of_len;

    return NRF_SUCCESS;
}

ret_code_t nfc_ndef_msg_record_add(nfc_ndef_msg_desc_t * const    p_msg,
                                   nfc_ndef_record_desc_t * const p_record)
{
    if (p_msg->record_count >= p_msg->max_record_count)
    {
        return NRF_ERROR_NO_MEM;
    }

    p_msg->pp_record[p_msg->record_count] = p_record;
    p_msg->record_count++;

    return NRF_SUCCESS;
}

ret_code_t nfc_uri_msg_encode( nfc_uri_id_t          uri_id_code,
                               uint8_t const * const p_uri_data,
                               uint8_t               uri_data_len,
                               uint8_t       *       p_buf,
                               uint32_t      *       p_len)
{
    ret_code_t err_code;

    /* Create NFC NDEF message description with URI record */
    NFC_NDEF_MSG_DEF(nfc_uri_msg, 1);
    NFC_NDEF_URI_RECORD_DESC_DEF(nfc_uri_rec, uri_id_code, p_uri_data, uri_data_len);

    err_code = nfc_ndef_msg_record_add(&NFC_NDEF_MSG(nfc_uri_msg),
                                       &NFC_NDEF_URI_RECORD_DESC(nfc_uri_rec));

    /* Encode whole message into buffer */
    err_code = nfc_ndef_msg_encode(&NFC_NDEF_MSG(nfc_uri_msg),
                                   p_buf,
                                   p_len);

    return err_code;
}

ret_code_t nfc_uri_payload_constructor( uri_payload_desc_t * p_input,
                                        uint8_t * p_buff,
                                        uint32_t * p_len)
{
    if (p_buff != NULL)
    {
        /* Verify if there is enough available memory */
        if (p_input->uri_data_len >= *p_len)
        {
            return NRF_ERROR_NO_MEM;
        }

        /* Copy descriptor content into the buffer */
        *(p_buff++) = p_input->uri_id_code;
        memcpy(p_buff, p_input->p_uri_data, p_input->uri_data_len );
    }

    *p_len      = p_input->uri_data_len + 1;

    return NRF_SUCCESS;
}