#ifndef __NFC_CONF_H__
#define __NFC_CONF_H__s

/**
 * 
 * 
 * 
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif

#include "nrf_error_nfc.h"

/** @defgroup NRF_ERRORS_BASE Error Codes Base number definitions
 * @{ */
#define NRF_ERROR_BASE_NUM      (0x0)       ///< Global error base
#define NRF_ERROR_SDM_BASE_NUM  (0x1000)    ///< SDM error base
#define NRF_ERROR_SOC_BASE_NUM  (0x2000)    ///< SoC error base
#define NRF_ERROR_STK_BASE_NUM  (0x3000)    ///< STK error base
/** @} */

#define NRF_SUCCESS                           (NRF_ERROR_BASE_NUM + 0)  ///< Successful command
#define NRF_ERROR_SVC_HANDLER_MISSING         (NRF_ERROR_BASE_NUM + 1)  ///< SVC handler is missing
#define NRF_ERROR_SOFTDEVICE_NOT_ENABLED      (NRF_ERROR_BASE_NUM + 2)  ///< SoftDevice has not been enabled
#define NRF_ERROR_INTERNAL                    (NRF_ERROR_BASE_NUM + 3)  ///< Internal Error
#define NRF_ERROR_NO_MEM                      (NRF_ERROR_BASE_NUM + 4)  ///< No Memory for operation
#define NRF_ERROR_NOT_FOUND                   (NRF_ERROR_BASE_NUM + 5)  ///< Not found
#define NRF_ERROR_NOT_SUPPORTED               (NRF_ERROR_BASE_NUM + 6)  ///< Not supported
#define NRF_ERROR_INVALID_PARAM               (NRF_ERROR_BASE_NUM + 7)  ///< Invalid Parameter
#define NRF_ERROR_INVALID_STATE               (NRF_ERROR_BASE_NUM + 8)  ///< Invalid state, operation disallowed in this state
#define NRF_ERROR_INVALID_LENGTH              (NRF_ERROR_BASE_NUM + 9)  ///< Invalid Length
#define NRF_ERROR_INVALID_FLAGS               (NRF_ERROR_BASE_NUM + 10) ///< Invalid Flags
#define NRF_ERROR_INVALID_DATA                (NRF_ERROR_BASE_NUM + 11) ///< Invalid Data
#define NRF_ERROR_DATA_SIZE                   (NRF_ERROR_BASE_NUM + 12) ///< Data size exceeds limit
#define NRF_ERROR_TIMEOUT                     (NRF_ERROR_BASE_NUM + 13) ///< Operation timed out
#define NRF_ERROR_NULL                        (NRF_ERROR_BASE_NUM + 14) ///< Null Pointer
#define NRF_ERROR_FORBIDDEN                   (NRF_ERROR_BASE_NUM + 15) ///< Forbidden Operation
#define NRF_ERROR_INVALID_ADDR                (NRF_ERROR_BASE_NUM + 16) ///< Bad Memory Address
#define NRF_ERROR_BUSY                        (NRF_ERROR_BASE_NUM + 17) ///< Busy

typedef enum
{
    NFC_FIELD_STATE_NONE,   /**< Initial value indicating no NFCT Field events. */
    NFC_FIELD_STATE_OFF,    /**< NFCT FIELDLOST Event has been set. */
    NFC_FIELD_STATE_ON,     /**< NFCT FIELDDETECTED Event has been set. */
    NFC_FIELD_STATE_UNKNOWN /**< Both NFCT Field Events have been set - ambiguous state. */
}nfct_field_sense_state_t;


typedef enum {
    HAL_NFC_EVENT_FIELD_ON,           ///< Field is detected.
    HAL_NFC_EVENT_FIELD_OFF,          ///< Field is lost.
    HAL_NFC_EVENT_DATA_RECEIVED,      ///< Data is received.
    HAL_NFC_EVENT_DATA_TRANSMITTED    ///< Data is transmitted.
} hal_nfc_event_t;

/** @brief Parameter IDs for the set/get function. */
typedef enum {
    HAL_NFC_PARAM_ID_TESTING,   ///< Used for unit tests.
    HAL_NFC_PARAM_FWI,          ///< Frame Wait Time parameter.
    HAL_NFC_PARAM_SELRES,       ///< Parameter for setting the 'Protocol' bits for SEL_RES packet.
    HAL_NFC_PARAM_NFCID1,       /**< NFCID1 value, data can be 4, 7, or 10 bytes long (simple, double, or triple size).
                                     To use default NFCID1 of specific length pass one byte containing requested length.
                                     Default 7-byte NFCID1 will be used if this parameter was not set before nfc_t4t_setup(). */
    HAL_NFC_PARAM_DID,          ///< Parameter for DID field management.
    HAL_NFC_PARAM_ID_UNKNOWN
} hal_nfc_param_id_t;

#define NRF_NFCT_AUTOCOLRESCONFIG     (*(uint32_t volatile *)(0x4000559C))
#define NRF_NFCT_AUTOCOLRESCONFIG_Pos 0

#define NRF_NFCT_DEFAULTSTATESLEEP     (*(uint32_t volatile *)(0x40005420))     /**< The default state of NFCT. */
#define NRF_NFCT_DEFAULTSTATESLEEP_MSK 0x1UL                                    /**< Mask for checking the default state of NFCT. */

/**< Length of double size NFCID1 */
#define NFCID1_DOUBLE_SIZE          7u   

/**< Length of NFCID1 if user does not provide one */
#define NFCID1_DEFAULT_LENGHT       NFCID1_DOUBLE_SIZE 

/** The lower 8 bits (of a 32 bit value) */
#define LSB_32(a) ((a) & 0x000000FF)                         

#define NFC_T4T_FWI_MAX         4u                 /**< Maximum FWI parameter value */

typedef void (* hal_nfc_callback_t)(void          * p_context,
                                    hal_nfc_event_t event,
                                    const uint8_t * p_data,
                                    size_t          data_length);

void hal_nfc_setup(hal_nfc_callback_t callback, void * p_context);

ret_code_t hal_nfc_send(const uint8_t * p_data, size_t data_length);

ret_code_t hal_nfc_start(void);

ret_code_t hal_nfc_stop(void);

ret_code_t hal_nfc_done(void);

#ifdef __cplusplus
}
#endif

/**
 * @}
 */

#endif /* __NFC_CONF_H__ */