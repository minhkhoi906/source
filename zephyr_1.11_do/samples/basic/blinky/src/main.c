/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <board.h>
#include <device.h>
#include <gpio.h>
#include <misc/printk.h>
// #include <nrf52832_peripherals.h>
// #include <nrf_gpio.h>
// #include <nrf52.h>
#include <uart.h>
#include <stdio.h>
#include <kernel.h>
#include <linker/sections.h>

static struct device *uart_dev;

/* TX queue */
static K_THREAD_STACK_DEFINE(tx_stack, 1024);
static struct k_thread tx_thread_data;

static const char fifo_data[] = "This is a FIFO test.\r\n";
static const char *banner2 = "Characters read:\r\n";

static volatile bool data_transmitted;

static void interrupt_handler(struct device *dev){

	uart_irq_update(dev);

	if (uart_irq_tx_ready(dev)) {
		// printk("[ISR] uart_isr \n");
		data_transmitted = true;
	}
}

static void write_data(struct device *dev, const char *buf, int len)
{
	uart_irq_tx_enable(dev);

	data_transmitted = false;
	uart_fifo_fill(dev, (const u8_t *)buf, len);
	while (data_transmitted == false)
		;

	printk("[THREAD] write_data\n");
	uart_irq_tx_disable(dev);
}

// static int init_uart(void){

// 	// struct device *uart_dev;

//     uart_dev = device_get_binding(CONFIG_UART_NRF5_NAME);

//     if (!uart_dev) {
//     	printk("[UART] Uart was not found\n");
//         return -1;
//     } else {
//     	printk("[UART] Uart was found\n");
//     }

//     uart_irq_callback_set(uart_dev, uart_isr);

// 	/* Enable Tx interrupt before using fifo */
// 	/* Verify uart_irq_tx_enable() */
// 	uart_irq_tx_enable(uart_dev);

// 	// k_sleep(500);

// 	/* Verify uart_irq_tx_disable() */
// 	// uart_irq_tx_disable(uart_dev);

//     return 0;
// }



static void tx_thread(void)
{
	printk("[THREAD] TX thread started\n");

	int fill;

	while(1){
		fill = uart_fifo_fill(uart_dev, (u8_t *)&fifo_data, sizeof(fifo_data));
		printk("fill: %d\n", fill);
		k_sleep(1500);
	}
}

static void init_tx_queue(void)
{
	k_thread_create(&tx_thread_data, tx_stack,
			K_THREAD_STACK_SIZEOF(tx_stack),
			(k_thread_entry_t)tx_thread,
			NULL, NULL, NULL, K_PRIO_COOP(8), 0, K_NO_WAIT);
}

void main(void)
{
	uart_dev = device_get_binding(CONFIG_UART_NRF5_NAME);

    if (!uart_dev) {
    	printk("[UART] Uart was not found\n");
        return;
    }

    uart_irq_callback_set(uart_dev, interrupt_handler);
    write_data(uart_dev, banner2, strlen(banner2));

	// init_tx_queue();

	// int tx_ready;

	// while(1){
	// 	tx_ready = uart_irq_tx_ready(uart_dev);
	// 	printk("TX: %d \n", tx_ready);
	// 	k_sleep(1000);
	// }

	// printk("[Main]\n");


}
