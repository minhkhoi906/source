# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.10

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/local/bin/cmake

# The command to remove a file.
RM = /usr/local/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/blinky

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/blinky/build

# Utility rule file for flash.

# Include the progress variables for this target.
include zephyr/cmake/flash/CMakeFiles/flash.dir/progress.make

zephyr/cmake/flash/CMakeFiles/flash: zephyr/zephyr.elf
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/blinky/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Flashing nrf51_vbluno51"
	/usr/local/bin/cmake -E env /usr/local/bin/python3 /home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/scripts/support/zephyr_flash_debug.py pyocd flash --board-dir=/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/boards/arm/nrf51_vbluno51 --kernel-elf=/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/blinky/build/zephyr/zephyr.elf --kernel-hex=/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/blinky/build/zephyr/zephyr.hex --kernel-bin=/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/blinky/build/zephyr/zephyr.bin --gdb=/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/bin/arm-zephyr-eabi/arm-zephyr-eabi-gdb --openocd=/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/bin/openocd --openocd-search=/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/share/openocd/scripts --dt-flash=y --target=nrf51

flash: zephyr/cmake/flash/CMakeFiles/flash
flash: zephyr/cmake/flash/CMakeFiles/flash.dir/build.make

.PHONY : flash

# Rule to build all files generated by this target.
zephyr/cmake/flash/CMakeFiles/flash.dir/build: flash

.PHONY : zephyr/cmake/flash/CMakeFiles/flash.dir/build

zephyr/cmake/flash/CMakeFiles/flash.dir/clean:
	cd /home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/blinky/build/zephyr/cmake/flash && $(CMAKE_COMMAND) -P CMakeFiles/flash.dir/cmake_clean.cmake
.PHONY : zephyr/cmake/flash/CMakeFiles/flash.dir/clean

zephyr/cmake/flash/CMakeFiles/flash.dir/depend:
	cd /home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/blinky/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/blinky /home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/cmake/flash /home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/blinky/build /home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/blinky/build/zephyr/cmake/flash /home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/blinky/build/zephyr/cmake/flash/CMakeFiles/flash.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : zephyr/cmake/flash/CMakeFiles/flash.dir/depend

