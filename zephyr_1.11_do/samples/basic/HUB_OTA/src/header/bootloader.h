#ifndef __BOOTLOADER_H__
#define __BOOTLOADER_H__

/**
 * 
 * 
 * 
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif


#define BOOT_TIMER_SECOND			5
#define BOOTLOADER_RUNNING			0
#define APP_RUNNING					1

void bootloader_init(void);

#ifdef __cplusplus
}
#endif

/**
 * @}
 */

#endif /* __BOOTLOADER_H__ */