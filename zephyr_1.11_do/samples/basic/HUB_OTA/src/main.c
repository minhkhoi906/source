/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <device.h>
#include <nrf_pwm.h>

#include "header/pwm_hw.h"
#include "header/pwm_sw.h"
#include "header/pwm.h"
#include "header/bluetooth.h"
#include "header/74LV165.h"
#include "header/Si7021.h"
#include "header/thread.h"
#include "header/watch_dog.h"
#include "header/bootloader.h"

void main(void)
{
	printk("[Main] ---->>>_____HUB_____<<<---- \n");

	bootloader_init();
}
