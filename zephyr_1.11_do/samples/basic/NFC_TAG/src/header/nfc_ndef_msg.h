#ifndef __NFC_NDEF_MSG_H__
#define __NFC_NDEF_MSG_H__

/**
 * 
 * 
 * 
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif

#include "nrf_error_nfc.h"

typedef ret_code_t (* p_payload_constructor_t)(void     * p_payload_descriptor,
                                               uint8_t  * p_buffer,
                                               uint32_t * p_len);


typedef enum
{
    NFC_URI_NONE          = 0x00,  /**< No prepending is done. */
    NFC_URI_HTTP_WWW      = 0x01,  /**< "http://www." */
    NFC_URI_HTTPS_WWW     = 0x02,  /**< "https://www." */
    NFC_URI_HTTP          = 0x03,  /**< "http:" */
    NFC_URI_HTTPS         = 0x04,  /**< "https:" */
    NFC_URI_TEL           = 0x05,  /**< "tel:" */
    NFC_URI_MAILTO        = 0x06,  /**< "mailto:" */
    NFC_URI_FTP_ANONYMOUS = 0x07,  /**< "ftp://anonymous:anonymous@" */
    NFC_URI_FTP_FTP       = 0x08,  /**< "ftp://ftp." */
    NFC_URI_FTPS          = 0x09,  /**< "ftps://" */
    NFC_URI_SFTP          = 0x0A,  /**< "sftp://" */
    NFC_URI_SMB           = 0x0B,  /**< "smb://" */
    NFC_URI_NFS           = 0x0C,  /**< "nfs://" */
    NFC_URI_FTP           = 0x0D,  /**< "ftp://" */
    NFC_URI_DAV           = 0x0E,  /**< "dav://" */
    NFC_URI_NEWS          = 0x0F,  /**< "news:" */
    NFC_URI_TELNET        = 0x10,  /**< "telnet://" */
    NFC_URI_IMAP          = 0x11,  /**< "imap:" */
    NFC_URI_RTSP          = 0x12,  /**< "rtsp://" */
    NFC_URI_URN           = 0x13,  /**< "urn:" */
    NFC_URI_POP           = 0x14,  /**< "pop:" */
    NFC_URI_SIP           = 0x15,  /**< "sip:" */
    NFC_URI_SIPS          = 0x16,  /**< "sips:" */
    NFC_URI_TFTP          = 0x17,  /**< "tftp:" */
    NFC_URI_BTSPP         = 0x18,  /**< "btspp://" */
    NFC_URI_BTL2CAP       = 0x19,  /**< "btl2cap://" */
    NFC_URI_BTGOEP        = 0x1A,  /**< "btgoep://" */
    NFC_URI_TCPOBEX       = 0x1B,  /**< "tcpobex://" */
    NFC_URI_IRDAOBEX      = 0x1C,  /**< "irdaobex://" */
    NFC_URI_FILE          = 0x1D,  /**< "file://" */
    NFC_URI_URN_EPC_ID    = 0x1E,  /**< "urn:epc:id:" */
    NFC_URI_URN_EPC_TAG   = 0x1F,  /**< "urn:epc:tag:" */
    NFC_URI_URN_EPC_PAT   = 0x20,  /**< "urn:epc:pat:" */
    NFC_URI_URN_EPC_RAW   = 0x21,  /**< "urn:epc:raw:" */
    NFC_URI_URN_EPC       = 0x22,  /**< "urn:epc:" */
    NFC_URI_URN_NFC       = 0x23,  /**< "urn:nfc:" */
    NFC_URI_RFU           = 0xFF   /**< No prepending is done. Reserved for future use. */
} nfc_uri_id_t;

typedef struct
{
    nfc_uri_id_t    uri_id_code;  /**< URI identifier code. */
    uint8_t const * p_uri_data;   /**< Pointer to a URI string. */
    uint8_t         uri_data_len; /**< Length of the URI string. */
} uri_payload_desc_t;

static const uint8_t ndef_uri_record_type = 'U'; ///< URI Record type.

ret_code_t nfc_uri_payload_constructor( uri_payload_desc_t * p_input,
                                        uint8_t            * p_buff,
                                        uint32_t           * p_len);

/**
 * @brief Type Name Format (TNF) Field Values.
 *
 * Values to specify the TNF of a record.
 */
typedef enum
{
    TNF_EMPTY         = 0x00, ///< The value indicates that there is no type or payload associated with this record.
    TNF_WELL_KNOWN    = 0x01, ///< NFC Forum well-known type [NFC RTD].
    TNF_MEDIA_TYPE    = 0x02, ///< Media-type as defined in RFC 2046 [RFC 2046].
    TNF_ABSOLUTE_URI  = 0x03, ///< Absolute URI as defined in RFC 3986 [RFC 3986].
    TNF_EXTERNAL_TYPE = 0x04, ///< NFC Forum external type [NFC RTD].
    TNF_UNKNOWN_TYPE  = 0x05, ///< The value indicates that there is no type associated with this record.
    TNF_UNCHANGED     = 0x06, ///< The value is used for the record chunks used in chunked payload.
    TNF_RESERVED      = 0x07, ///< The value is reserved for future use.
} nfc_ndef_record_tnf_t;

typedef enum
{
    NDEF_FIRST_RECORD  = 0x80, ///< First record.
    NDEF_MIDDLE_RECORD = 0x00, ///< Middle record.
    NDEF_LAST_RECORD   = 0x40, ///< Last record.
    NDEF_LONE_RECORD   = 0xC0  ///< Only one record in the message.
} nfc_ndef_record_location_t;

#define NDEF_RECORD_LOCATION_MASK (NDEF_LONE_RECORD) ///< Mask of the Record Location bits in the NDEF record's flags byte.

/**
 * @brief NDEF record descriptor.
 */
typedef struct
{
    nfc_ndef_record_tnf_t tnf;                    ///< Value of the Type Name Format (TNF) field.

    uint8_t         id_length;                    ///< Length of the ID field. If 0, a record format without ID field is assumed.
    uint8_t const * p_id;                         ///< Pointer to the ID field data. Not relevant if id_length is 0.

    uint8_t         type_length;                  ///< Length of the type field.
    uint8_t const * p_type;                       ///< Pointer to the type field data. Not relevant if type_length is 0.

    p_payload_constructor_t payload_constructor;  ///< Pointer to the payload constructor function.
    void                  * p_payload_descriptor; ///< Pointer to the data for the payload constructor function.

} nfc_ndef_record_desc_t;




/**
  * @brief NDEF message descriptor.
  */
 typedef struct {
     nfc_ndef_record_desc_t ** pp_record;        ///< Pointer to an array of pointers to NDEF record descriptors.
     uint32_t                  max_record_count; ///< Number of elements in the allocated pp_record array, which defines the maximum number of records within the NDEF message.
     uint32_t                  record_count;     ///< Number of records in the NDEF message.
 } nfc_ndef_msg_desc_t;

#define NFC_NDEF_MSG_DEF(NAME, MAX_RECORD_CNT)                                             \
    nfc_ndef_record_desc_t   * NAME##_nfc_ndef_p_record_desc_array[MAX_RECORD_CNT];        \
    nfc_ndef_msg_desc_t  NAME##_nfc_ndef_msg_desc =                                        \
        {                                                                                  \
            .pp_record = NAME##_nfc_ndef_p_record_desc_array,                              \
            .max_record_count = MAX_RECORD_CNT,                                            \
            .record_count = 0                                                              \
        }

#define NFC_NDEF_MSG(NAME) (NAME##_nfc_ndef_msg_desc)

#define NFC_NDEF_GENERIC_RECORD_DESC_DEF(NAME,                                  \
                                         TNF,                                   \
                                         P_ID,                                  \
                                         ID_LEN,                                \
                                         P_TYPE,                                \
                                         TYPE_LEN,                              \
                                         P_PAYLOAD_CONSTRUCTOR,                 \
                                         P_PAYLOAD_DESCRIPTOR)                  \
    nfc_ndef_record_desc_t NAME##_ndef_generic_record_desc =                    \
    {                                                                           \
        .tnf = TNF,                                                             \
                                                                                \
        .id_length = ID_LEN,                                                    \
        .p_id      = P_ID,                                                      \
                                                                                \
        .type_length = TYPE_LEN,                                                \
        .p_type      = P_TYPE,                                                  \
                                                                                \
        .payload_constructor  = (p_payload_constructor_t)P_PAYLOAD_CONSTRUCTOR, \
        .p_payload_descriptor = (void *) P_PAYLOAD_DESCRIPTOR                   \
    }

#define NFC_NDEF_URI_RECORD_DESC_DEF(NAME,                                   \
                                     URI_ID_CODE,                            \
                                     P_URI_DATA,                             \
                                     URI_DATA_LEN)                           \
    uri_payload_desc_t NAME##_ndef_uri_record_payload_desc =                 \
    {                                                                        \
        .uri_id_code  = (URI_ID_CODE),                                       \
        .p_uri_data   = (P_URI_DATA),                                        \
        .uri_data_len = (URI_DATA_LEN)                                       \
    };                                                                       \
                                                                             \
    NFC_NDEF_GENERIC_RECORD_DESC_DEF( NAME,                                  \
                                      TNF_WELL_KNOWN,                        \
                                      NULL,                                  \
                                      0,                                     \
                                      &ndef_uri_record_type,                 \
                                      sizeof(ndef_uri_record_type),          \
                                      nfc_uri_payload_constructor,           \
                                      &NAME##_ndef_uri_record_payload_desc)  \

#define NDEF_RECORD_IL_MASK                0x08 ///< Mask of the ID field presence bit in the flags byte of an NDEF record.
#define NDEF_RECORD_PAYLOAD_LEN_LONG_SIZE  4    ///< Size of the Payload Length field in a long NDEF record.

#define NLEN_FIELD_SIZE 2U                       ///< Size of NLEN field, used to encode NDEF message for Type 4 Tag.


#define NFC_NDEF_URI_RECORD_DESC(NAME) NFC_NDEF_GENERIC_RECORD_DESC(NAME)
#define NFC_NDEF_GENERIC_RECORD_DESC(NAME) (NAME##_ndef_generic_record_desc)

ret_code_t nfc_ndef_msg_encode(nfc_ndef_msg_desc_t const * p_ndef_msg_desc,
                               uint8_t                   * p_msg_buffer,
                               uint32_t * const            p_msg_len);

ret_code_t nfc_uri_msg_encode( nfc_uri_id_t          uri_id_code,
                               uint8_t const * const p_uri_data,
                               uint8_t               uri_data_len,
                               uint8_t       *       p_buf,
                               uint32_t      *       p_len);

#ifdef __cplusplus
}
#endif

/**
 * @}
 */

#endif /* __NFC_NDEF_MSG_H__ */