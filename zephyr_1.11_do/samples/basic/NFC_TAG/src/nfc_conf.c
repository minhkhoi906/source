/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <board.h>
#include <device.h>
#include <gpio.h>


#include <nrf52.h>

#include "header/nfc_conf.h"
#include "header/nrf_error_nfc.h"

#define NFC_LIB_VERSION             0x00u              /**< Internal: current NFC lib. version  */

#define CASCADE_TAG_BYTE            0x88u              /**< Constant defined by ISO/EIC 14443-3 */
#define NFCID1_3RD_LAST_BYTE2_SHIFT 16u                /**< Shift value for NFC ID byte 2 */
#define NFCID1_3RD_LAST_BYTE1_SHIFT 8u                 /**< Shift value for NFC ID byte 1 */
#define NFCID1_3RD_LAST_BYTE0_SHIFT 0u                 /**< Shift value for NFC ID byte 0 */
#define NFCID1_2ND_LAST_BYTE2_SHIFT 16u                /**< Shift value for NFC ID byte 2 */
#define NFCID1_2ND_LAST_BYTE1_SHIFT 8u                 /**< Shift value for NFC ID byte 1 */
#define NFCID1_2ND_LAST_BYTE0_SHIFT 0u                 /**< Shift value for NFC ID byte 0 */
#define NFCID1_LAST_BYTE3_SHIFT     24u                /**< Shift value for NFC ID byte 3 */
#define NFCID1_LAST_BYTE2_SHIFT     16u                /**< Shift value for NFC ID byte 2 */
#define NFCID1_LAST_BYTE1_SHIFT     8u                 /**< Shift value for NFC ID byte 1 */
#define NFCID1_LAST_BYTE0_SHIFT     0u                 /**< Shift value for NFC ID byte 0 */
#define NFCID1_SINGLE_SIZE          4u                 /**< Length of single size NFCID1 */
#define NFCID1_DOUBLE_SIZE          7u                 /**< Length of double size NFCID1 */
#define NFCID1_TRIPLE_SIZE          10u                /**< Length of triple size NFCID1 */
#define NFCID1_DEFAULT_LENGHT       NFCID1_DOUBLE_SIZE /**< Length of NFCID1 if user does not provide one */
#define NFCID1_MAX_LENGHT           NFCID1_TRIPLE_SIZE /**< Maximum length of NFCID1 */
#define NFC_RX_BUFFER_SIZE          256u               /**< NFC Rx data buffer size */
#define NFC_SLP_REQ_CMD             0x50u              /**< NFC SLP_REQ command identifier */
#define NFC_CRC_SIZE                2u                 /**< CRC size in bytes */
#define NFC_T4T_SELRES_PROTOCOL     1u                 /**< Type 4A Tag PROTOCOL bit setup (b7:b6) for SEL_RES Response frame */
#define NFC_T4T_SELRES_PROTOCOL_MSK 0x03u              /**< PROTOCOL bits mask for SEL_RES Response frame */


#define NFC_T4T_FWI_52840S_MAX      4u                 /**< Maximum FWI parameter value for first sample of 52840 */
#define NFCT_FRAMEDELAYMAX_52840S   (0xFFFFUL)         /**< Bit mask of FRAMEDELAYMAX field for first sample of 52840 */
#define NFC_T4T_FWI_DEFAULT         4u                 /**< Default FWI parameter value */
#define NRF_T4T_FWI_LISTEN_MAX      8u                 /**< Maxiumum FWI parameter value in Listen Mode */
#define NFC_T4T_RATS_CMD            0xE0u              /**< RATS Command Byte */
#define NFC_T4T_RATS_DID_MASK       0x0Fu              /**< Mask of DID field inside RATS Parameter Byte. */
#define NFC_T4T_RATS_DID_RFU        0x0Fu              /**< Invalid value of DID - RFU. */
#define NFC_T4T_S_DESELECT          0xC2u              /**< S(DESELECT) Block identifier */
#define NFC_T4T_S_WTX               0xF2u              /**< S(WTX)Block identifier */
#define NFC_T4T_S_BLOCK_MSK         0xF7u              /**< S-Block Mask */
#define NFC_T4T_I_BLOCK             0x02u              /**< I-Block identifier */
#define NFC_T4T_BLOCK_MSK           0xE6u              /**< I/R- block mask (NAD not supported, expect this bit equal 0) */
#define NFC_T4T_ISO_DEP_MSK         0x02u              /**< ISO-DEP block mask */
#define NFC_T4T_R_BLOCK             0xA2u              /**< R- Block identifier (static bits) */
#define NFC_T4T_WTX_NO_DID_SIZE     0x02               /**< WTX data buffer size without DID field. */
#define NFC_T4T_WTX_DID_SIZE        0x03               /**< WTX data buffer size with DID field. */
#define NFC_T4T_WTXM_MAX_VALUE      0x3B               /**< WTXM max value, according to 'NFC Forum Digital Protocol Technical Specification 2.0, 16.2.2 */
#define NFC_T4T_DID_BIT             0x08               /**< Indicates if DID present in ISO-DEP block. */
#define NFC_T4T_DID_MASK            0x0F               /**< DID field mask */
#define NFCT_INTEN_MSK              0x1C5CFFu          /**< Mask for all NFCT interrupts */

#define NRF_NFCT_ERRORSTATUS_ALL (NFCT_ERRORSTATUS_NFCFIELDTOOWEAK_Msk   | \
                                      NFCT_ERRORSTATUS_NFCFIELDTOOSTRONG_Msk | \
                                      NFCT_ERRORSTATUS_FRAMEDELAYTIMEOUT_Msk)   /**< Mask for clearing all error flags in NFCT_ERRORSTATUS register */


#define NRF_NFCT_FRAMESTATUS_RX_MSK (NFCT_FRAMESTATUS_RX_OVERRUN_Msk      | \
                                     NFCT_FRAMESTATUS_RX_PARITYSTATUS_Msk | \
                                     NFCT_FRAMESTATUS_RX_CRCERROR_Msk)          /**< Mask for clearing all flags in NFCT_FRAMESTATUS_RX register */
#define NFC_FIELD_ON_MASK            NFCT_FIELDPRESENT_LOCKDETECT_Msk           /**< Mask for checking FIELDPRESENT register for state: FIELD ON. */
#define NFC_FIELD_OFF_MASK           NFCT_FIELDPRESENT_FIELDPRESENT_Msk         /**< Mask for checking FIELDPRESENT register for state: FIELD OFF. */


#define NFC_T4T_FWI_TO_FWT(FWI)      (256u * 16u * (1 << (FWI)))                /**< Macro for calculating FWT (in number of NFC carrier periods) from FWI parameter. */
#define NFC_T4T_WTXM_MAX(FWI)        ( 1 << (NRF_T4T_FWI_LISTEN_MAX - (FWI)))   /**< Macro for calculating WTXM based on 'NFC Forum Digital Protocol Specification Version 1.1, Requirement 15.2.2.9', and FRAMEDELAYMAX maximum register setting */


/* Static data */
static hal_nfc_callback_t           m_nfc_lib_callback = (hal_nfc_callback_t) NULL;                     /**< Callback to nfc_lib layer */
static void *                       m_nfc_lib_context;                                                  /**< Callback execution context */
static volatile uint8_t             m_nfc_rx_buffer[NFC_RX_BUFFER_SIZE]   = {0};                        /**< Buffer for NFC Rx data */
static volatile bool                m_slp_req_received                    = false;                      /**< Flag indicating that SLP_REQ Command was received */
static volatile bool                m_field_on                            = false;                      /**< Flag indicating that NFC Tag field is present */
static volatile uint8_t             m_fwi;                                                              /**< FWI parameter */
static volatile uint8_t             m_wtxm;                                                             /**< WTXM maximum value */
static volatile uint8_t             m_wtx_data[3];                                                      /**< Tx buffer for an S(WTX) block */
static volatile bool                m_deselect                            = false;                      /**< Flag indicating reception of DESELECT command */
static volatile bool                m_swtx_sent                           = false;                      /**< Flag indicating that SWTX command has been sended. */
static volatile bool                m_pending_msg                         = false;                      /**< Flag signaling pending message during SWTX command execution. */
static volatile const uint8_t *     m_pending_msg_ptr                     = NULL;                       /**< Pointer to pending message buffer. */
static volatile size_t              m_pending_data_length                 = 0;                          /**< Length of pending message data. */
static volatile bool                m_t4t_tx_waiting                      = false;                      /**< Indicates if HAL is waiting for upper layer response to received command */
static volatile uint8_t             m_t4t_selres                          = NFC_T4T_SELRES_PROTOCOL;    /**< Protocol bits setup in SEL_RES frame - can be modified using the library API */
static volatile uint8_t             m_did                                 = 0;                          /**< DID field value. */
static uint8_t                      m_nfcid1_length                       = 0;                          /**< Length of NFCID1 provided by user or 0 if not initialized yet */
static uint8_t                      m_nfcid1_data[NFCID1_MAX_LENGHT]      = {0};                        /**< Content of NFCID1 */
static volatile uint8_t             m_t4t_active                          = false;                      /**< Indicates if NFC Tag is in 4A state (on reception of correct RATS command). */

static volatile uint32_t            m_nfc_fieldpresent_mask               = NFC_FIELD_OFF_MASK;   /**< Mask used for NFC Field polling in NFCT_FIELDPRESENT register */


static void hal_nfc_nfcid1_default_bytes(void)
{
    uint32_t nfc_tag_header0 = NRF_FICR->NFC.TAGHEADER0;
    uint32_t nfc_tag_header1 = NRF_FICR->NFC.TAGHEADER1;
    uint32_t nfc_tag_header2 = NRF_FICR->NFC.TAGHEADER2;

    m_nfcid1_data[0] = (uint8_t) LSB_32(nfc_tag_header0 >> 0);
    m_nfcid1_data[1] = (uint8_t) LSB_32(nfc_tag_header0 >> 8);
    m_nfcid1_data[2] = (uint8_t) LSB_32(nfc_tag_header0 >> 16);
    m_nfcid1_data[3] = (uint8_t) LSB_32(nfc_tag_header1 >> 0);
    m_nfcid1_data[4] = (uint8_t) LSB_32(nfc_tag_header1 >> 8);
    m_nfcid1_data[5] = (uint8_t) LSB_32(nfc_tag_header1 >> 16);
    m_nfcid1_data[6] = (uint8_t) LSB_32(nfc_tag_header1 >> 24);
    m_nfcid1_data[7] = (uint8_t) LSB_32(nfc_tag_header2 >> 0);
    m_nfcid1_data[8] = (uint8_t) LSB_32(nfc_tag_header2 >> 8);
    m_nfcid1_data[9] = (uint8_t) LSB_32(nfc_tag_header2 >> 16);
}

static void hal_nfc_nfcid1_registers_setup(void)
{
    uint32_t sens_res_size;         // Value that will be written to NRF_NFCT->SENSRES
    uint8_t* p_nfcid_remaining_data;  // Points to the first byte of m_nfcid1_data remaining to write to NRF_NFCT->NFCID1 registers

    p_nfcid_remaining_data = m_nfcid1_data;

    if (m_nfcid1_length == NFCID1_SINGLE_SIZE)
    {
        sens_res_size = NFCT_SENSRES_NFCIDSIZE_NFCID1Single;
    }
    else
    {
        if (m_nfcid1_length == NFCID1_DOUBLE_SIZE)
        {
            sens_res_size = NFCT_SENSRES_NFCIDSIZE_NFCID1Double;
        }
        else // then m_nfcid1_length == NFCID1_TRIPLE_SIZE
        {
            /* MSB of NFCID1_3RD_LAST register is not used - always 0 */
            NRF_NFCT->NFCID1_3RD_LAST =
                ((uint32_t) p_nfcid_remaining_data[0] << NFCID1_3RD_LAST_BYTE2_SHIFT) |
                ((uint32_t) p_nfcid_remaining_data[1] << NFCID1_3RD_LAST_BYTE1_SHIFT) |
                ((uint32_t) p_nfcid_remaining_data[2] << NFCID1_3RD_LAST_BYTE0_SHIFT);
            p_nfcid_remaining_data += 3;
            sens_res_size = NFCT_SENSRES_NFCIDSIZE_NFCID1Triple;
        }
        /* MSB of NFCID1_2ND_LAST register is not used - always 0 */
        NRF_NFCT->NFCID1_2ND_LAST =
            ((uint32_t) p_nfcid_remaining_data[0] << NFCID1_2ND_LAST_BYTE2_SHIFT) |
            ((uint32_t) p_nfcid_remaining_data[1] << NFCID1_2ND_LAST_BYTE1_SHIFT) |
            ((uint32_t) p_nfcid_remaining_data[2] << NFCID1_2ND_LAST_BYTE0_SHIFT);
        p_nfcid_remaining_data += 3;
    }

    NRF_NFCT->NFCID1_LAST =
        ((uint32_t) p_nfcid_remaining_data[0] << NFCID1_LAST_BYTE3_SHIFT) |
        ((uint32_t) p_nfcid_remaining_data[1] << NFCID1_LAST_BYTE2_SHIFT) |
        ((uint32_t) p_nfcid_remaining_data[2] << NFCID1_LAST_BYTE1_SHIFT) |
        ((uint32_t) p_nfcid_remaining_data[3] << NFCID1_LAST_BYTE0_SHIFT);

    /* Begin: Bugfix for FTPAN-25 (IC-9929) */
    /* Workaround for wrong SENSRES values require using SDD00001, but here SDD00100 is used
       because it's required to operate with Windows Phone */
    NRF_NFCT->SENSRES =
            (sens_res_size << NFCT_SENSRES_NFCIDSIZE_Pos) |
            (NFCT_SENSRES_BITFRAMESDD_SDD00100   << NFCT_SENSRES_BITFRAMESDD_Pos);
    /* End:   Bugfix for FTPAN-25 (IC-9929)*/

    m_t4t_active          = false;
    m_swtx_sent           = false;
    m_pending_msg         = false;
    m_pending_msg_ptr     = NULL;
    m_pending_data_length = 0;
}

void hal_nfc_setup(hal_nfc_callback_t callback, void * p_context)
{
    m_nfc_lib_callback = callback;
    m_nfc_lib_context  = p_context;

    if (m_nfcid1_length == 0)
    {
        m_nfcid1_length = NFCID1_DEFAULT_LENGHT;
        hal_nfc_nfcid1_default_bytes();
    }

	NRF_NFCT->INTENSET = (NFCT_INTENSET_FIELDDETECTED_Enabled << NFCT_INTENSET_FIELDDETECTED_Pos) |
                         (NFCT_INTENSET_FIELDLOST_Enabled     << NFCT_INTENSET_FIELDLOST_Pos) |
                         (NFCT_INTENSET_TXFRAMESTART_Enabled << NFCT_INTENSET_TXFRAMESTART_Pos) |
                         (NFCT_INTENSET_ERROR_Enabled    << NFCT_INTENSET_ERROR_Pos) |
                         (NFCT_INTENSET_SELECTED_Enabled << NFCT_INTENSET_SELECTED_Pos);

    hal_nfc_nfcid1_registers_setup();

    NRF_NFCT->SELRES =
        (m_t4t_selres << NFCT_SELRES_PROTOCOL_Pos) & NFCT_SELRES_PROTOCOL_Msk;

    m_swtx_sent           = false;
    m_pending_msg         = false;
    m_pending_msg_ptr     = NULL;
    m_pending_data_length = 0;

    printk("[NFC] nfc_conf: setup \n");
    printk("[NRF] NRF_INTEN: %x \n", NRF_NFCT->INTEN);
}



/**@brief Function for clearing an event flag in NRF_NFCT registers.
 *
 * @param[in]  p_event  Pointer to event register.
 *
 */
static inline void nrf_nfct_event_clear(volatile uint32_t * p_event)
{
    *p_event = 0;

    /* Perform read to ensure clearing is effective */
    volatile uint32_t dummy = *p_event;
    (void)dummy;
}

static inline void nrf_nfct_default_state_reset(void)
{
    if (NRF_NFCT_DEFAULTSTATESLEEP & NRF_NFCT_DEFAULTSTATESLEEP_MSK) // Default state is SLEEP_A
    {
        NRF_NFCT->TASKS_GOSLEEP = 1;
    }
    else // Default state is IDLE
    {
        NRF_NFCT->TASKS_GOIDLE = 1;
    }

    /* Disable RX here (will be enabled at SELECTED) */
    NRF_NFCT->INTENCLR = NFCT_INTENCLR_RXFRAMEEND_Clear <<
                         NFCT_INTENCLR_RXFRAMEEND_Pos;
}

static inline void nrf_nfct_field_event_handler(volatile nfct_field_sense_state_t field_state)
{
    if (field_state == NFC_FIELD_STATE_UNKNOWN)
    {
        /* Probe NFC field */
        uint32_t field_present = NRF_NFCT->FIELDPRESENT;

        if (field_present & m_nfc_fieldpresent_mask)
        {
            field_state = NFC_FIELD_STATE_ON;
        }
        else
        {
            field_state = NFC_FIELD_STATE_OFF;
        }
    }

    /* Field event service */
    switch (field_state)
    {
        case NFC_FIELD_STATE_ON:
            if (!m_field_on)
            {
                // HAL_NFC_DEBUG_PIN_SET(HAL_NFC_HCLOCK_ON_DEBUG_PIN); // DEBUG!
                // nrf_drv_clock_hfclk_request(&m_clock_handler_item);

                // HAL_NFC_DEBUG_PIN_CLEAR(HAL_NFC_HCLOCK_ON_DEBUG_PIN); // DEBUG!
            }
            m_field_on = true;
            break;

        case NFC_FIELD_STATE_OFF:
            // HAL_NFC_DEBUG_PIN_SET(HAL_NFC_HCLOCK_OFF_DEBUG_PIN); // DEBUG!

            NRF_NFCT->TASKS_SENSE = 1;
            // nrf_drv_clock_hfclk_release();
            m_field_on = false;

            NRF_NFCT->INTENCLR =
                (NFCT_INTENCLR_RXFRAMEEND_Clear << NFCT_INTENCLR_RXFRAMEEND_Pos) |
                (NFCT_INTENCLR_RXERROR_Clear    << NFCT_INTENCLR_RXERROR_Pos);

            /* Change mask to FIELD_OFF state - trigger FIELD_ON even if HW has not locked to the field */
            m_nfc_fieldpresent_mask = NFC_FIELD_OFF_MASK;

            if ((m_nfc_lib_callback != NULL) )
            {
                m_nfc_lib_callback(m_nfc_lib_context, HAL_NFC_EVENT_FIELD_OFF, 0, 0);
            }

            /* Re-enable Auto Collision Resolution */
            NRF_NFCT_AUTOCOLRESCONFIG = NRF_NFCT_AUTOCOLRESCONFIG &
                                        ~(1u << NRF_NFCT_AUTOCOLRESCONFIG_Pos);
            m_t4t_active = false;

            /* Go back to default frame delay mode, bugfix for tag locking. */
            NRF_NFCT->FRAMEDELAYMODE = NFCT_FRAMEDELAYMODE_FRAMEDELAYMODE_WindowGrid <<
                                       NFCT_FRAMEDELAYMODE_FRAMEDELAYMODE_Pos;

            // HAL_NFC_DEBUG_PIN_CLEAR(HAL_NFC_HCLOCK_OFF_DEBUG_PIN); // DEBUG!
            break;

        default:
            /* No implementation required */
            break;
    }
}

ret_code_t hal_nfc_parameter_set(hal_nfc_param_id_t id, void * p_data, size_t data_length)
{
    /* Parameter validation is done in upper-layer */

    if (id == HAL_NFC_PARAM_FWI)
    {
        /* Update Frame Wait Time setting; possible settings are limited by NFCT hardware */
        m_fwi = *((uint8_t *)p_data);

        if (data_length != sizeof(uint8_t))
        {
            return NRF_ERROR_DATA_SIZE;
        }
        
        if (m_fwi > NFC_T4T_FWI_MAX)
        {
            return NRF_ERROR_INVALID_PARAM;
        }

        /* Set FRAMEDELAYTIME */
        if (m_fwi == NFC_T4T_FWI_MAX)
        {
            NRF_NFCT->FRAMEDELAYMAX = NFCT_FRAMEDELAYMAX_FRAMEDELAYMAX_Msk;
        }
        else
        {
            NRF_NFCT->FRAMEDELAYMAX = NFC_T4T_FWI_TO_FWT(m_fwi);
        }

        if (NFC_T4T_WTXM_MAX(m_fwi) > NFC_T4T_WTXM_MAX_VALUE)
        {
            m_wtxm = NFC_T4T_WTXM_MAX_VALUE;
        }
        else
        {
            m_wtxm = NFC_T4T_WTXM_MAX(m_fwi);
        }
    }
    else if (id == HAL_NFC_PARAM_SELRES)
    {
        /* Update SEL_RES 'Protocol' bits setting */
        uint8_t sel_res = *((uint8_t *)p_data);

        if (data_length != sizeof(uint8_t))
        {
            return NRF_ERROR_DATA_SIZE;
        }
        if (sel_res > NFC_T4T_SELRES_PROTOCOL_MSK)
        {
            return NRF_ERROR_INVALID_PARAM;
        }

        m_t4t_selres     = sel_res;
        NRF_NFCT->SELRES =
            (m_t4t_selres << NFCT_SELRES_PROTOCOL_Pos) & NFCT_SELRES_PROTOCOL_Msk;
    }
    else if (id == HAL_NFC_PARAM_DID)
    {
        if (data_length > sizeof(uint8_t))
        {
            return NRF_ERROR_DATA_SIZE;
        }

        m_did = (data_length == sizeof(m_did)) ? *((uint8_t *)p_data) : 0;
    }
    else if (id == HAL_NFC_PARAM_NFCID1)
    {
        if (data_length == 1)
        {
            uint8_t id_length = *((uint8_t *) p_data);
            if (id_length == NFCID1_SINGLE_SIZE || id_length == NFCID1_DOUBLE_SIZE ||
                id_length == NFCID1_TRIPLE_SIZE)
            {
                m_nfcid1_length = id_length;
                hal_nfc_nfcid1_default_bytes();
            }
            else
            {
                return NRF_ERROR_INVALID_LENGTH;
            }
        }
        else if (data_length == NFCID1_SINGLE_SIZE || data_length == NFCID1_DOUBLE_SIZE ||
            data_length == NFCID1_TRIPLE_SIZE)
        {
            m_nfcid1_length = (uint8_t) data_length;
            memcpy(m_nfcid1_data, p_data, data_length);
        }
        else
        {
            return NRF_ERROR_INVALID_LENGTH;
        }
        hal_nfc_nfcid1_registers_setup();
    }
    else
    {
        /* No implementation needed */
    }

    return NRF_SUCCESS;
}

__STATIC_INLINE void hal_nfc_wtx_data_set(bool did_present)
{
    uint32_t wtx_data_size;

    m_wtx_data[0] = NFC_T4T_S_WTX;
    if (did_present)
    {
        m_wtx_data[0] |= NFC_T4T_DID_BIT;
        m_wtx_data[1]  = m_did;
        m_wtx_data[2]  = m_wtxm;
        wtx_data_size  = NFC_T4T_WTX_DID_SIZE;
    }
    else
    {
        m_wtx_data[1] = m_wtxm;
        wtx_data_size = NFC_T4T_WTX_NO_DID_SIZE;
    }

    NRF_NFCT->PACKETPTR      = (uint32_t) m_wtx_data;
    NRF_NFCT->TXD.AMOUNT     = (wtx_data_size << NFCT_TXD_AMOUNT_TXDATABYTES_Pos) &
                               NFCT_TXD_AMOUNT_TXDATABYTES_Msk;
    NRF_NFCT->FRAMEDELAYMODE = NFCT_FRAMEDELAYMODE_FRAMEDELAYMODE_ExactVal <<
                               NFCT_FRAMEDELAYMODE_FRAMEDELAYMODE_Pos;
    NRF_NFCT->TASKS_STARTTX  = 1;
    m_t4t_tx_waiting         = true;
}

ret_code_t hal_nfc_send(const uint8_t * p_data, size_t data_length)
{
    if (data_length == 0)
    {
        return NRF_ERROR_DATA_SIZE;
    }

    if(m_swtx_sent)
    {
        m_pending_msg_ptr     = p_data;
        m_pending_data_length = data_length;
        m_pending_msg         = true;

        // NRF_LOG_DEBUG("Pending message.");
        return NRF_SUCCESS;
    }

    m_t4t_tx_waiting   = false;

    /* Ignore previous TX END events, SW takes care only for data frames which tranmission is triggered in this function */
    nrf_nfct_event_clear(&NRF_NFCT->EVENTS_TXFRAMEEND);

    NRF_NFCT->INTENSET       = (NFCT_INTENSET_TXFRAMEEND_Enabled << NFCT_INTENSET_TXFRAMEEND_Pos); //Moved to the end in T4T to avoid delaying TASKS_STARTX
    NRF_NFCT->PACKETPTR      = (uint32_t) p_data;
    NRF_NFCT->TXD.AMOUNT     = (data_length << NFCT_TXD_AMOUNT_TXDATABYTES_Pos) &
                                NFCT_TXD_AMOUNT_TXDATABYTES_Msk;
    NRF_NFCT->FRAMEDELAYMODE = NFCT_FRAMEDELAYMODE_FRAMEDELAYMODE_WindowGrid <<
                               NFCT_FRAMEDELAYMODE_FRAMEDELAYMODE_Pos;
    NRF_NFCT->TASKS_STARTTX  = 1;

    printk("Send\n");
    return NRF_SUCCESS;
}

void my_isr(void *arg)
{
    nfct_field_sense_state_t current_field = NFC_FIELD_STATE_NONE;

    if (NRF_NFCT->EVENTS_FIELDDETECTED && (NRF_NFCT->INTEN & NFCT_INTEN_FIELDDETECTED_Msk))
    {
        nrf_nfct_event_clear(&NRF_NFCT->EVENTS_FIELDDETECTED);
        current_field = NFC_FIELD_STATE_ON;

        printk("[NFC] Field detected\n");
    }

    if (NRF_NFCT->EVENTS_FIELDLOST && (NRF_NFCT->INTEN & NFCT_INTEN_FIELDLOST_Msk))
    {
        nrf_nfct_event_clear(&NRF_NFCT->EVENTS_FIELDLOST);
        current_field =
           (current_field == NFC_FIELD_STATE_NONE) ? NFC_FIELD_STATE_OFF : NFC_FIELD_STATE_UNKNOWN;

        printk("[NFC] Field lost\n");
        // NRF_LOG_DEBUG("Field lost");
    }

    /* Perform actions if any FIELD event is active */
    if (current_field != NFC_FIELD_STATE_NONE)
    {
        nrf_nfct_field_event_handler(current_field);
    }

    if (NRF_NFCT->EVENTS_RXFRAMEEND && (NRF_NFCT->INTEN & NFCT_INTEN_RXFRAMEEND_Msk))
    {
        /* Take into account only number of whole bytes */
        uint32_t rx_status    = 0;
        uint32_t rx_data_size = ((NRF_NFCT->RXD.AMOUNT & NFCT_RXD_AMOUNT_RXDATABYTES_Msk) >>
                                 NFCT_RXD_AMOUNT_RXDATABYTES_Pos) - NFC_CRC_SIZE;
        nrf_nfct_event_clear(&NRF_NFCT->EVENTS_RXFRAMEEND);

        if (NRF_NFCT->EVENTS_RXERROR && (NRF_NFCT->INTEN & NFCT_INTEN_RXERROR_Msk))
        {
            rx_status = (NRF_NFCT->FRAMESTATUS.RX & NRF_NFCT_FRAMESTATUS_RX_MSK);
            nrf_nfct_event_clear(&NRF_NFCT->EVENTS_RXERROR);


            printk("[NFC] Rx error\n");
            // NRF_LOG_DEBUG("Rx error (0x%x)", (unsigned int) rx_status);

            /* Clear rx frame status */
            NRF_NFCT->FRAMESTATUS.RX = NRF_NFCT_FRAMESTATUS_RX_MSK;
        }

        /* Ignore all NFC-A data frames with Transmission Error */
        if (rx_status)
        {
            /* Go back to idle state if currently in the ACTIVE_A state */
            if (!m_t4t_active)
            {
                nrf_nfct_default_state_reset();
            }
            /* Stay in the CARD_EMULATOR_4A state */
            else
            {
                /* Command with Transmission Error, so wait for next frame reception */
                NRF_NFCT->TASKS_ENABLERXDATA = 1;
            }
        }
        else
        {
            /* Look for NFC-A Commands */
            if (!m_t4t_active)
            {
                // 'NFC Forum Digital Protocol Technical Specification 2.0, 14.6.1.13' */
                if ((m_nfc_rx_buffer[0] == NFC_T4T_RATS_CMD) &&
                    ((m_nfc_rx_buffer[1] & NFC_T4T_RATS_DID_MASK) != NFC_T4T_RATS_DID_RFU))
                {
                    /* Disable Auto Collision Resolution */
                    NRF_NFCT_AUTOCOLRESCONFIG = NRF_NFCT_AUTOCOLRESCONFIG |
                                                (1u << NRF_NFCT_AUTOCOLRESCONFIG_Pos);
                    m_t4t_active = true;
                    // NRF_LOG_DEBUG("RX: T4T Activate");
                }
                /* Indicate that SLP_REQ was received - this will cause FRAMEDELAYTIMEOUT error */
                else if (m_nfc_rx_buffer[0] == NFC_SLP_REQ_CMD)
                {
                    // disable RX here (will enable at SELECTED)
                    m_slp_req_received = true;
                    NRF_NFCT->INTENCLR = NFCT_INTENCLR_RXFRAMEEND_Clear << NFCT_INTENCLR_RXFRAMEEND_Pos;
                }
                else
                {
                    nrf_nfct_default_state_reset();
                }
            }
            /* Look for Tag 4 Type Commands */
            else
            {
                bool    did_present;
                uint8_t did;

                did_present = (m_nfc_rx_buffer[0] & NFC_T4T_DID_BIT) != 0;
                did         = m_did;

                // React only to the ISO-DEP blocks that are directed to our tag.
                if ((!did_present) && (did > 0)) // 'NFC Forum Digital Protocol Technical Specification 2.0, 16.1.2.12' */
                {
                    /* Not our ISO-DEP block, so wait for next frame reception */
                    NRF_NFCT->TASKS_ENABLERXDATA = 1;
                }
                else if ((!did_present) || (did == (m_nfc_rx_buffer[1] & NFC_T4T_DID_MASK)))
                {
                    if ((m_nfc_rx_buffer[0] & NFC_T4T_S_BLOCK_MSK) == NFC_T4T_S_DESELECT)
                    {
                        m_deselect = true;
                        // NRF_LOG_DEBUG("RX: T4T Deselect");
                    }
                    else if (m_swtx_sent && ((m_nfc_rx_buffer[0] & NFC_T4T_S_BLOCK_MSK) == NFC_T4T_S_WTX))
                    {
                        m_swtx_sent = false;

                        // NRF_LOG_DEBUG("RX: S(WTX) response");

                        if (m_pending_msg)
                        {
                            m_pending_msg = false;

                            nrf_nfct_event_clear(&NRF_NFCT->EVENTS_TXFRAMEEND);

                            NRF_NFCT->INTENSET       = (NFCT_INTENSET_TXFRAMEEND_Enabled <<
                                                        NFCT_INTENSET_TXFRAMEEND_Pos);
                            NRF_NFCT->PACKETPTR      = (uint32_t) m_pending_msg_ptr;
                            NRF_NFCT->TXD.AMOUNT     = (m_pending_data_length <<
                                                        NFCT_TXD_AMOUNT_TXDATABYTES_Pos) &
                                                        NFCT_TXD_AMOUNT_TXDATABYTES_Msk;
                            NRF_NFCT->FRAMEDELAYMODE = NFCT_FRAMEDELAYMODE_FRAMEDELAYMODE_WindowGrid <<
                                                       NFCT_FRAMEDELAYMODE_FRAMEDELAYMODE_Pos;
                            NRF_NFCT->TASKS_STARTTX  = 1;

                            m_t4t_tx_waiting   = false;
                            // NRF_LOG_DEBUG("Sending pending message!");
                        }
                        else
                        {
                            hal_nfc_wtx_data_set(did_present);
                        }
                    }
                    else if ((m_nfc_rx_buffer[0] & NFC_T4T_BLOCK_MSK) == NFC_T4T_I_BLOCK)
                    {
                        /* Set up default transmission of S(WTX) block. Tx will be executed only if FDT timer
                        * expires (FrameDelayMode-ExactVal) before hal_nfc_send is called */
                        hal_nfc_wtx_data_set(did_present);

                        NRF_NFCT->INTENSET = (NFCT_INTENSET_TXFRAMEEND_Enabled <<
                                            NFCT_INTENSET_TXFRAMEEND_Pos);
                    }
                    else
                    {
                        /* Not a valid ISO-DEP block, so wait for next frame reception */
                        NRF_NFCT->TASKS_ENABLERXDATA = 1;
                    }
                }
                else
                {
                    /* Not our ISO-DEP block, so wait for next frame reception */
                    NRF_NFCT->TASKS_ENABLERXDATA = 1;
                }
            }

            if (m_nfc_lib_callback != NULL)
            {
                /* This callback should trigger transmission of READ Response */
                m_nfc_lib_callback(m_nfc_lib_context,
                                   HAL_NFC_EVENT_DATA_RECEIVED,
                                   (void *)m_nfc_rx_buffer,
                                   rx_data_size);
            }
            /* Clear TXFRAMESTART EVENT so it can be checked in hal_nfc_send */
            nrf_nfct_event_clear(&NRF_NFCT->EVENTS_TXFRAMESTART);
        }
        // NRF_LOG_DEBUG("Rx fend");
    }

    if (NRF_NFCT->EVENTS_TXFRAMEEND && (NRF_NFCT->INTEN & NFCT_INTEN_TXFRAMEEND_Msk))
    {
        nrf_nfct_event_clear(&NRF_NFCT->EVENTS_TXFRAMEEND);

        /* Disable TX END event to ignore frame transmission other than READ response */
        NRF_NFCT->INTENCLR = (NFCT_INTENCLR_TXFRAMEEND_Clear << NFCT_INTENCLR_TXFRAMEEND_Pos);

        if (m_deselect)
        {
            /* Re-enable Auto Collision Resolution */
            NRF_NFCT_AUTOCOLRESCONFIG = NRF_NFCT_AUTOCOLRESCONFIG &
                                        ~(1u << NRF_NFCT_AUTOCOLRESCONFIG_Pos);
            NRF_NFCT->TASKS_GOSLEEP   = 1;
            /* Disable RX here (will be enabled at SELECTED) */
            NRF_NFCT->INTENCLR        = NFCT_INTENCLR_RXFRAMEEND_Clear <<
                                        NFCT_INTENCLR_RXFRAMEEND_Pos;
            m_deselect   = false;
            m_t4t_active = false;
        }
        else
        {
            /* Set up for reception */
            NRF_NFCT->PACKETPTR          = (uint32_t) m_nfc_rx_buffer;
            NRF_NFCT->MAXLEN             = NFC_RX_BUFFER_SIZE;
            NRF_NFCT->TASKS_ENABLERXDATA = 1;
            NRF_NFCT->INTENSET           =
                        (NFCT_INTENSET_RXFRAMEEND_Enabled << NFCT_INTENSET_RXFRAMEEND_Pos) |
                        (NFCT_INTENSET_RXERROR_Enabled    << NFCT_INTENSET_RXERROR_Pos);
        }

        if (m_nfc_lib_callback != NULL)
        {
            m_nfc_lib_callback(m_nfc_lib_context, HAL_NFC_EVENT_DATA_TRANSMITTED, 0, 0);
        }

        printk("[NFC] Tx fend\n");

        // NRF_LOG_DEBUG("Tx fend");
    }

    if (NRF_NFCT->EVENTS_SELECTED && (NRF_NFCT->INTEN & NFCT_INTEN_SELECTED_Msk))
    {
        nrf_nfct_event_clear(&NRF_NFCT->EVENTS_SELECTED);

        /* Clear also RX END and RXERROR events because SW does not take care of commands which were received before selecting the tag */
        nrf_nfct_event_clear(&NRF_NFCT->EVENTS_RXFRAMEEND);
        nrf_nfct_event_clear(&NRF_NFCT->EVENTS_RXERROR);

        /* Set up registers for EasyDMA and start receiving packets */
        NRF_NFCT->PACKETPTR          = (uint32_t) m_nfc_rx_buffer;
        NRF_NFCT->MAXLEN             = NFC_RX_BUFFER_SIZE;
        NRF_NFCT->TASKS_ENABLERXDATA = 1;

        NRF_NFCT->INTENSET = (NFCT_INTENSET_RXFRAMEEND_Enabled << NFCT_INTENSET_RXFRAMEEND_Pos) |
                             (NFCT_INTENSET_RXERROR_Enabled    << NFCT_INTENSET_RXERROR_Pos);

        /* At this point any previous error status can be ignored */
        NRF_NFCT->FRAMESTATUS.RX = NRF_NFCT_FRAMESTATUS_RX_MSK;
        NRF_NFCT->ERRORSTATUS    = NRF_NFCT_ERRORSTATUS_ALL;

        /* Change mask to FIELD_ON state - trigger FIELD_ON only if HW has locked to the field */
        m_nfc_fieldpresent_mask = NFC_FIELD_ON_MASK;

        if (m_nfc_lib_callback != NULL)
        {
            m_nfc_lib_callback(m_nfc_lib_context, HAL_NFC_EVENT_FIELD_ON, 0, 0);
        }

        m_t4t_active          = false;
        m_swtx_sent           = false;
        m_pending_msg         = false;
        m_pending_msg_ptr     = NULL;
        m_pending_data_length = 0;

        printk("[NFC] Selected \n");
    }

    if (NRF_NFCT->EVENTS_ERROR && (NRF_NFCT->INTEN & NFCT_INTEN_ERROR_Msk))
    {
        uint32_t err_status = NRF_NFCT->ERRORSTATUS;
        nrf_nfct_event_clear(&NRF_NFCT->EVENTS_ERROR);

        /* Clear FRAMEDELAYTIMEOUT error (expected HW behaviour) when SLP_REQ command was received */
        if ((err_status & NFCT_ERRORSTATUS_FRAMEDELAYTIMEOUT_Msk) && m_slp_req_received)
        {
            NRF_NFCT->ERRORSTATUS = NFCT_ERRORSTATUS_FRAMEDELAYTIMEOUT_Msk;
            m_slp_req_received    = false;
        }
        /* Report any other error */
        err_status &= ~NFCT_ERRORSTATUS_FRAMEDELAYTIMEOUT_Msk;

        if (err_status)
        {
        }

        /* Clear error status */
        NRF_NFCT->ERRORSTATUS = NRF_NFCT_ERRORSTATUS_ALL;
    }

    if (NRF_NFCT->EVENTS_TXFRAMESTART && (NRF_NFCT->INTEN & NFCT_INTEN_TXFRAMESTART_Msk))
    {
        nrf_nfct_event_clear(&NRF_NFCT->EVENTS_TXFRAMESTART);

        if (m_t4t_tx_waiting)
        {
            m_t4t_tx_waiting = false;
            m_swtx_sent      = true;
        }
    }
}

ret_code_t hal_nfc_start(void)
{
    NRF_NFCT->ERRORSTATUS = NRF_NFCT_ERRORSTATUS_ALL;
    NRF_NFCT->TASKS_SENSE = 1;

    IRQ_CONNECT(NFCT_IRQn, 0, my_isr, NULL, 0);
    irq_enable(NFCT_IRQn);

    printk("[NFC] Start\n");
    return NRF_SUCCESS;
}

ret_code_t hal_nfc_stop(void)
{
    NRF_NFCT->TASKS_DISABLE = 1;

    printk("Stop\n");
    return NRF_SUCCESS;
}

ret_code_t hal_nfc_done(void)
{
    m_nfc_lib_callback = (hal_nfc_callback_t) NULL;

    return NRF_SUCCESS;
}
