/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <board.h>
#include <device.h>

#include "header/nfc_conf.h"
#include "header/nfc_ndef_msg.h"
#include "header/nrf_error_nfc.h"

static const uint8_t m_url[] =
    {'g', 'o', 'o', 'g', 'l', 'e', '.', 'c', 'o', 'm'}; // URL "nordicsemi.com"

uint8_t       m_ndef_msg_buf[1024];                               // Buffer for NDEF file
volatile bool m_update_state;                                               // Flag indicating that Type 4 Tag performs NDEF message update procedure.

static void nfc_callback(void          * context,
                         hal_nfc_event_t event,
                         const uint8_t * data,
                         size_t          dataLength)
{
    printk("nfc_callback \n");
}

void main(void)
{
	ret_code_t err_code;

	hal_nfc_setup(nfc_callback, NULL);

	/* Provide information about available buffer size to encoding function */
    uint32_t len = sizeof(m_ndef_msg_buf);

	/* Encode URI message into buffer */
    err_code = nfc_uri_msg_encode(NFC_URI_HTTP_WWW,
                                  m_url,
                                  sizeof(m_url),
                                  m_ndef_msg_buf,
                                  &len);

    err_code = hal_nfc_start();

    while(1)
    {
    	// printk("I am here\n");
    	// err_code = hal_nfc_send(m_ndef_msg_buf, sizeof(m_ndef_msg_buf));
    	// k_sleep(500);
    }
}


