/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */


#include <zephyr.h>
#include <misc/printk.h>
#include <device.h>
#include <nrf52832_peripherals.h>
#include <board.h>
#include <gpio.h>
#include <nrf_gpio.h>
#include <nrf52.h>
#include <i2c.h>

#include "header/Si7021.h"

static int write_bytes(struct device *i2c_dev,
		       u8_t *data, u32_t num_bytes)
{
	struct i2c_msg msgs;

	/* Setup I2C messages */

	/* Data to be written*/
	msgs.buf = data;
	msgs.len = num_bytes;
	msgs.flags = I2C_MSG_WRITE;

	return i2c_transfer(i2c_dev, &msgs, 1, SENSOR_I2C_ADDR);
}

static int read_bytes(struct device *i2c_dev,
		      u8_t *data, u32_t num_bytes)
{
	struct i2c_msg msgs;

	/* Read from device. STOP after this. */
	msgs.buf = data;
	msgs.len = num_bytes;
	msgs.flags = I2C_MSG_RESTART | I2C_MSG_READ | I2C_MSG_STOP;;

	return i2c_transfer(i2c_dev, &msgs, 1, SENSOR_I2C_ADDR);
}

u32_t read_data_Si7021(struct device *i2c_dev)
{
	u8_t command_i2c;

	int ret;

	command_i2c = I2C_COMMAND;

	ret = write_bytes(i2c_dev, &command_i2c, sizeof(command_i2c));
	if (ret) {
		printk("[I2C] Error writing to device_i2c! error code (%d)\n", ret);
		return;
	}

	u8_t temp_byte[2];

	ret = read_bytes(i2c_dev, &temp_byte, sizeof(temp_byte));
	if (ret) {
		printk("[I2C] Error reading to device_i2c! error code (%d)\n", ret);
		return;
	} 
	// printk("[I2C] temp_byte 0: %02x, 1: %02x \n", temp_byte[0], temp_byte[1]);

	u32_t temp;

	temp = (u32_t)temp_byte[0] << 8 | (u32_t)temp_byte[1];

	temp = ((175.72 * temp) / 65536) - 46.85; 

	return temp;
}

void read_firmware_version(struct device *i2c_dev)
{
	u8_t command_i2c[2];

	int ret;

	command_i2c[0] = 0x84;
	command_i2c[1] = 0xb8;

	ret = write_bytes(i2c_dev, &command_i2c, sizeof(command_i2c));
	if (ret) {
		printk("[I2C] Error writing to device_i2c! error code (%d)\n", ret);
		return;
	}

	k_sleep(2);

	u8_t version_byte;

	ret = i2c_read(i2c_dev, &version_byte, sizeof(version_byte), SENSOR_I2C_ADDR);
	if (ret) {
		printk("[I2C] Error reading to device_i2c! error code (%d)\n", ret);
		return;
	}

	// printk("[I2C] temp_byte 0: %02x, 1: %02x \n", temp_byte[0], temp_byte[1]);

	printk("[I2C] firmware_version: %02x \n", version_byte);
}