/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <net/buf.h>
#include <net/net_pkt.h>

#include <bluetooth/hci.h>

#include "header/thread.h"
#include "header/bluetooth.h"

static bool process_node(u8_t type, const u8_t *data, u8_t data_len)
{
    switch (type)
    {
        case BT_DATA_NAME_COMPLETE:
            
            ad_data[0].type = BT_DATA_NAME_COMPLETE;
            ad_data[0].data_len = data_len;
            ad_data[0].data = data;

            printk("DEVICE_NAME [ ");
            for(int i = 0 ;  i < data_len; i++){
                printf("%02X ", data[i]);
            }
            printk("] - ");

            advertise();

            return false;

        case BT_DATA_MANUFACTURER_DATA:
            
            ad_data[1].type = BT_DATA_MANUFACTURER_DATA;
            ad_data[1].data_len = data_len;
            ad_data[1].data = data;

            printk("DATA [ ");
            for(int i = 0 ;  i < data_len; i++){
                printf("%02X ", data[i]);
            }
            printk("] - ");
            
            return true;

        default:
            return true;
    }

    return true;
}

static void ad_parse(struct net_buf *ad, 
                           bool (*func)(u8_t type, const u8_t *data,
                           u8_t data_len))
{
    while (ad->len > 1) {
        u8_t len = net_buf_pull_u8(ad);
        u8_t type;

        /* Check for early termination */
        if (len == 0) {
            return;
        }

        if (len > ad->len || ad->len < 1) {
            printk("AD malformed\n");
            return;
        }

        type = net_buf_pull_u8(ad);

        if (!func(type, ad->data, len - 1))
        {
            return;
        }

        net_buf_pull(ad, len - 1);
    }
}

static void rx_thread(void)
{
    printk("[THREAD] RX thread started\n");

    while (1) {
        struct net_pkt *pkt;
        struct net_buf *buf;
        
        pkt = k_fifo_get(&rx_queue, K_MSEC(2000));

        if (!pkt)
        {
            feed_dog(0);
            continue;
        }

        buf = net_buf_frag_last(pkt->frags);

        if (check_crc(buf))
        {
            ad_parse(buf, process_node);
        }

        net_pkt_unref(pkt);

        feed_dog(0);
     
        k_yield();
    }
}

void init_rx_queue(void)
{
    k_fifo_init(&rx_queue);

    k_thread_create(&rx_thread_data, rx_stack,
            K_THREAD_STACK_SIZEOF(rx_stack),
            (k_thread_entry_t)rx_thread,
            NULL, NULL, NULL, K_PRIO_COOP(8), 0, K_NO_WAIT);
}