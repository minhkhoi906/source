/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <board.h>
#include <device.h>
#include <gpio.h>
#include <uart.h>
#include <stdio.h>
#include <net/buf.h>
#include <net/net_pkt.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>

#include "header/watch_dog.h"
#include "header/crc16.h"

#define SLIP_END            0xC0   /* indicates end of packet */
#define SLIP_ESC            0xDB   /* indicates byte stuffing */
#define SLIP_ESC_END        0xDC   /* ESC ESC_END means END data byte */
#define SLIP_ESC_ESC        0xDD   /* ESC ESC_ESC means ESC data byte */
#define SLIP_ESC_XON        0xDE   /* ESC SLIP_ESC_XON means XON control byte */
#define SLIP_ESC_XOFF       0xDF   /* ESC SLIP_ESC_XON means XOFF control byte */
#define XON                 0x11   /* indicates XON charater */
#define XOFF                0x13   /* indicates XOFF charater */
        
static struct bt_data ad_data[2];

enum slip_state {
    STATE_GARBAGE,
    STATE_OK,
    STATE_ESC,
};

/* RX queue */
static struct k_fifo rx_queue;
static K_THREAD_STACK_DEFINE(rx_stack, 1024);
static struct k_thread rx_thread_data;

/* UART device */
static struct device *uart_dev;

/* SLIP state machine */
static u8_t slip_state = STATE_OK;

static struct net_pkt *pkt_curr;

u8_t _address[6];

static struct bt_le_adv_param param_adv_nconn;
static struct bt_le_oob addr_local;

static void set_param_advertise_nconn(void)
{
    bt_le_oob_get_local(&addr_local);
    
    param_adv_nconn.options = 0;
    param_adv_nconn.interval_min = BT_GAP_ADV_FAST_INT_MIN_1;
    param_adv_nconn.interval_max = BT_GAP_ADV_FAST_INT_MAX_1;
    param_adv_nconn.own_addr = &addr_local.addr.a;

	memcpy(_address, &addr_local.addr.a, sizeof(bt_addr_t));

}

static void advertise(void){

    int err;

	err = bt_le_adv_start(&param_adv_nconn, ad_data, ARRAY_SIZE(ad_data),
							NULL, 0);

    k_sleep(K_MSEC(30));

    err = bt_le_adv_stop();
    if (err) 
    {
        printk("Advertising failed to stop (err %d)\n", err);
        return;
    }

    printk("[BT] Advertise successful \n");

    return ;
}

static int slip_process_byte(unsigned char c)
{
    struct net_buf *buf;

    switch (slip_state) {
        case STATE_GARBAGE:
            if (c == SLIP_END) {
                slip_state = STATE_OK;
            }
            printk("garbage: discard byte %x \n", c);
            return 0;

        case STATE_ESC:
            if (c == SLIP_ESC_END) {
                c = SLIP_END;
            } else if (c == SLIP_ESC_ESC) {
                c = SLIP_ESC;
            } else if (c == SLIP_ESC_XON) {
                c = XON;
            } else if (c == SLIP_ESC_XOFF) {
                c = XOFF;
            } else {
                slip_state = STATE_GARBAGE;
                return 0;
            }
            slip_state = STATE_OK;
            break;

        case STATE_OK:
            if (c == SLIP_ESC) {
                slip_state = STATE_ESC;
                return 0;
            } else if (c == SLIP_END) {
                return 1;
            }
            break;
    }

    if (!pkt_curr) {
        pkt_curr = net_pkt_get_reserve_rx(0, K_NO_WAIT);
        if (!pkt_curr) {
            printk("No more buffers\n");
            return 0;
        }
        buf = net_pkt_get_frag(pkt_curr, K_NO_WAIT);
        if (!buf) {
            printk("No more buffers\n");
            net_pkt_unref(pkt_curr);
            return 0;
        }
        net_pkt_frag_insert(pkt_curr, buf);
    } else {
        buf = net_buf_frag_last(pkt_curr->frags);
    }

    if (!net_buf_tailroom(buf)) {
        printk("No more buf space: buf %p len %u\n", buf, buf->len);

        net_pkt_unref(pkt_curr);
        pkt_curr = NULL;
        return 0;
    }

    net_buf_add_u8(buf, c);

    return 0;
}

static bool process_node(u8_t type, const u8_t *data, u8_t data_len)
{
    switch (type)
    {
        case BT_DATA_NAME_COMPLETE:
            
            ad_data[0].type = BT_DATA_NAME_COMPLETE;
            ad_data[0].data_len = data_len;
            ad_data[0].data = data;

            // printk("DEVICE_NAME [ ");
            // for(int i = 0 ;  i < data_len; i++){
            //     printf("%02X ", data[i]);
            // }
            // printk("] - ");

            for (int i = 0; i < 3; ++i)
            {
                advertise();
            }

            return false;

        case BT_DATA_MANUFACTURER_DATA:
            
            ad_data[1].type = BT_DATA_MANUFACTURER_DATA;
            ad_data[1].data_len = data_len;
            ad_data[1].data = data;

            // printk("DATA [ ");
            // for(int i = 0 ;  i < data_len; i++){
            //     printf("%02X ", data[i]);
            // }
            // printk("] - ");
            
            return true;

        default:
            return true;
    }

    return true;
}

static void ad_parse(struct net_buf *ad, 
                           bool (*func)(u8_t type, const u8_t *data,
                           u8_t data_len))
{
    while (ad->len > 1) {
        u8_t len = net_buf_pull_u8(ad);
        u8_t type;

        /* Check for early termination */
        if (len == 0) {
            return;
        }

        if (len > ad->len || ad->len < 1) {
            printk("AD malformed\n");
            return;
        }

        type = net_buf_pull_u8(ad);

        if (!func(type, ad->data, len - 1))
        {
            return;
        }

        net_buf_pull(ad, len - 1);
    }
}

/**
 *  Compare check_sum
 */

static bool check_crc(struct net_buf *buf)
{
    size_t size = buf->len - 2;

    u16_t calculate_crc = calculate(buf->data, size);

    // u16_t temp = (buf->data[size] << 8) + buf->data[size + 1];
    u16_t src_crc = buf->data[size] + (buf->data[size + 1] << 8);

    if (!calculate_crc) {
        return false;
    }       
    else if (calculate_crc == src_crc) {
        return true;
    }
    else {
        return false;
    }
}

/**
 *  RX - receive UART
 */

static void rx_thread(void)
{
    printk("[THREAD] RX thread started\n");

    while (1) {
        struct net_pkt *pkt;
        struct net_buf *buf;
        
        pkt = k_fifo_get(&rx_queue, K_MSEC(2000));

        if (!pkt)
        {
            feed_dog(0);
            continue;
        }

        buf = net_buf_frag_last(pkt->frags);

        if (check_crc(buf))
        {
            ad_parse(buf, process_node);
        }

        net_pkt_unref(pkt);

        feed_dog(0);
     
        k_yield();
    }
}

/**
 *  Interrupt - callback handle isr
 */

static void uart_isr(struct device *dev){

    while (uart_irq_update(dev) && uart_irq_is_pending(dev)) 
    {
        if (uart_irq_rx_ready(dev)) 
        {
            unsigned char byte;

            while (uart_fifo_read(dev, &byte, sizeof(byte))) 
            {
                if (slip_process_byte(byte)) 
                {
                    /**
                     * slip_process_byte() returns 1 on
                     * SLIP_END, even after receiving full
                     * packet
                     */
                    if (!pkt_curr) 
                    {
                        printk("Skip SLIP_END\n");
                        continue;
                    }

                    k_fifo_put(&rx_queue, pkt_curr);
                    pkt_curr = NULL;
                }
            }
        }
    }
}

static void init_rx_queue(void)
{
    k_fifo_init(&rx_queue);

    k_thread_create(&rx_thread_data, rx_stack,
            K_THREAD_STACK_SIZEOF(rx_stack),
            (k_thread_entry_t)rx_thread,
            NULL, NULL, NULL, K_PRIO_COOP(8), 0, K_NO_WAIT);
}

static int init_uart(void)
{
    uart_dev = device_get_binding(CONFIG_UART_NRF5_NAME);
    if (!uart_dev) 
    {
    	printk("[UART] Cannot get UART device\n");
        return -1;
    }

    u8_t c;

    /* Drain the fifo */
    while (uart_fifo_read(uart_dev, &c, 1)) {
        continue;
    }

    uart_irq_callback_set(uart_dev, uart_isr);

    uart_irq_rx_enable(uart_dev);

    return 0;
}

static void init_bt(void)
{
    int err;

    err = bt_enable(NULL);
    if (err) 
    {
        printk("[BT] Bluetooth init failed (err %d)\n", err);
        return;
    }

    set_param_advertise_nconn();
}

void main(void)
{
    /* Configure watch dog after 5s */
    watchdog_config(5000, 1);

    /* Initialize UART */
	if (init_uart()) {
        return;
    }
    
	init_bt();

    /* Initialize RX queue */
    init_rx_queue();
}
