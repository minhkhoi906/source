#ifndef __UART_GW_H__
#define __UART_GW_H__

/**
 * 
 * 
 * 
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif

#define SLIP_END            0xC0   /* indicates end of packet */
#define SLIP_ESC            0xDB   /* indicates byte stuffing */
#define SLIP_ESC_END        0xDC   /* ESC ESC_END means END data byte */
#define SLIP_ESC_ESC        0xDD   /* ESC ESC_ESC means ESC data byte */
#define SLIP_ESC_XON        0xDE   /* ESC SLIP_ESC_XON means XON control byte */
#define SLIP_ESC_XOFF       0xDF   /* ESC SLIP_ESC_XON means XOFF control byte */
#define XON                 0x11   /* indicates XON charater */
#define XOFF                0x13   /* indicates XOFF charater */

enum slip_state {
    STATE_GARBAGE,
    STATE_OK,
    STATE_ESC,
};

/* UART device */
static struct device *uart_dev;

/* SLIP state machine */
static u8_t slip_state = STATE_OK;

static struct net_pkt *pkt_curr;

int init_uart(void);

#ifdef __cplusplus
}
#endif

/**
 * @}
 */

#endif /* __UART_GW_H__ */