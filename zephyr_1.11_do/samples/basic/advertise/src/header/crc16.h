#ifndef CRC16_H_
#define CRC16_H_

/**
 * 
 * 
 * 
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <gpio.h>
#include <zephyr.h>

#define SHIFTED 0x00FF

typedef uint16_t CRC;
typedef uint8_t Payload;
typedef uint32_t Size;

uint16_t calculate(const uint8_t *payload, uint32_t size_);

#ifdef __cplusplus
}
#endif

/**
 * @}
 */
#endif //CRC16_H_