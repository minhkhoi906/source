#ifndef __THREAD_GW_H__
#define __THREAD_GW_H__

/**
 * 
 * 
 * 
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <zephyr.h>

/* RX queue */
static struct k_fifo rx_queue;
static K_THREAD_STACK_DEFINE(rx_stack, 1024);
static struct k_thread rx_thread_data;

void init_rx_queue(void);

#ifdef __cplusplus
}
#endif

/**
 * @}
 */

#endif /* __THREAD_GW_H__ */