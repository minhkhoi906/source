#ifndef __WATCHTDOG_H__
#define __WATCHTDOG_H__

/**
 * 
 * 
 * 
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <zephyr.h>
#include <nrfx_wdt.h>

nrfx_wdt_channel_id _channel_id[8];

void feed_dog(u8_t id_channel);

nrfx_err_t watchdog_config(u32_t timeoutn, u8_t channels);

#ifdef __cplusplus
}
#endif

/**
 * @}
 */

#endif /* __WATCHTDOG_H__ */