#ifndef __BLUETOOTH_GW_H__
#define __BLUETOOTH_GW_H__

/**
 * 
 * 
 * 
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <bluetooth/conn.h>

/* Bluetooth */
static struct bt_data ad_data[2];
static struct bt_le_adv_param param_adv_nconn;
static struct bt_le_oob addr_local;

static u8_t _address[6];

void init_bt(void);

void advertise(void);

#ifdef __cplusplus
}
#endif

/**
 * @}
 */

#endif /* __BLUETOOTH_GW_H__ */