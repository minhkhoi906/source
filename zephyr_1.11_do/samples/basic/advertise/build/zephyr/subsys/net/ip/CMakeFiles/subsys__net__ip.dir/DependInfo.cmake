# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/subsys/net/ip/connection.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/advertise/build/zephyr/subsys/net/ip/CMakeFiles/subsys__net__ip.dir/connection.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/subsys/net/ip/icmpv6.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/advertise/build/zephyr/subsys/net/ip/CMakeFiles/subsys__net__ip.dir/icmpv6.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/subsys/net/ip/ipv6.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/advertise/build/zephyr/subsys/net/ip/CMakeFiles/subsys__net__ip.dir/ipv6.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/subsys/net/ip/nbr.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/advertise/build/zephyr/subsys/net/ip/CMakeFiles/subsys__net__ip.dir/nbr.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/subsys/net/ip/net_context.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/advertise/build/zephyr/subsys/net/ip/CMakeFiles/subsys__net__ip.dir/net_context.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/subsys/net/ip/net_core.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/advertise/build/zephyr/subsys/net/ip/CMakeFiles/subsys__net__ip.dir/net_core.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/subsys/net/ip/net_if.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/advertise/build/zephyr/subsys/net/ip/CMakeFiles/subsys__net__ip.dir/net_if.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/subsys/net/ip/net_mgmt.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/advertise/build/zephyr/subsys/net/ip/CMakeFiles/subsys__net__ip.dir/net_mgmt.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/subsys/net/ip/net_pkt.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/advertise/build/zephyr/subsys/net/ip/CMakeFiles/subsys__net__ip.dir/net_pkt.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/subsys/net/ip/route.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/advertise/build/zephyr/subsys/net/ip/CMakeFiles/subsys__net__ip.dir/route.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/subsys/net/ip/udp.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/advertise/build/zephyr/subsys/net/ip/CMakeFiles/subsys__net__ip.dir/udp.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/subsys/net/ip/utils.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/samples/basic/advertise/build/zephyr/subsys/net/ip/CMakeFiles/subsys__net__ip.dir/utils.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "KERNEL"
  "NRF52832_XXAA"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../../subsys/net/ip/."
  "../../../../kernel/include"
  "../../../../arch/arm/include"
  "../../../../arch/arm/soc/nordic_nrf5/nrf52"
  "../../../../arch/arm/soc/nordic_nrf5/nrf52/include"
  "../../../../arch/arm/soc/nordic_nrf5/include"
  "../../../../boards/arm/nrf52_pca10040"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include-fixed"
  "../../../../lib/libc/minimal/include"
  "../../../../ext/lib/crypto/tinycrypt/include"
  "../../../../ext/hal/cmsis/Include"
  "../../../../ext/hal/nordic/nrfx"
  "../../../../ext/hal/nordic/nrfx/drivers/include"
  "../../../../ext/hal/nordic/nrfx/hal"
  "../../../../ext/hal/nordic/nrfx/mdk"
  "../../../../ext/hal/nordic/."
  "../../../../subsys/bluetooth"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
