#ifndef __PWM_HUB__
#define __PWM_HUB__

/**
 * 
 * 
 * 
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif

/* pin HUB */

/* HW_PIN 12 Channel */
#define PWM_CHANNEL_0 	28
#define PWM_CHANNEL_1 	29
#define PWM_CHANNEL_2 	31
#define PWM_CHANNEL_3 	26
#define PWM_CHANNEL_4 	19
#define PWM_CHANNEL_5 	13	
#define PWM_CHANNEL_6 	16
#define PWM_CHANNEL_7 	20

#define ENABLE_PIN_1    27
#define ENABLE_PIN_2    30
#define ENABLE_PIN_3    2
#define ENABLE_PIN_4    25
#define ENABLE_PIN_5    18
#define ENABLE_PIN_6    14
#define ENABLE_PIN_7    15
#define ENABLE_PIN_8    17

static u32_t enable_port[8] = {
                        ENABLE_PIN_1,
                        ENABLE_PIN_2,
                        ENABLE_PIN_3,
                        ENABLE_PIN_4,
                        ENABLE_PIN_5,
                        ENABLE_PIN_6,
                        ENABLE_PIN_7,
                        ENABLE_PIN_8
};

static u8_t enable_flag[8] = {0,0,0,0,0,0,0,0};

static u16_t pre_value[16];

void pwm_hub_init(void);

/* thiet lap gia tri phan tram cho cac port[0..15] */
int set_percent_pwm(u32_t channel_pin, u16_t percent_period);

/* Dieu khien cac port led tren hub */
void control_port_hub(u8_t detail[16]); 

#ifdef __cplusplus
}
#endif

/**
 * @}
 */

#endif /* __PWM_HUB__ */