/*
 * Copyright (c) 2017 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef _BLUETOOTH_H_
#define _BLUETOOTH_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <zephyr/types.h>
#include <stddef.h>
#include <errno.h>
#include <zephyr.h>
#include <misc/printk.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>
#include <misc/byteorder.h>


#define ADV_RESPONSE			0x1111
#define ADV_REPORT				0x3333
#define ADV_SEND_HUB_STATUS		0x2222

/* Du lieu data scan cua dim packet 8 byte dau 
 */

#define BYTE_DATA_SCAN_COMPANY_ID_HIGH			0
#define BYTE_DATA_SCAN_COMPANY_ID_LOW			1
#define BYTE_DATA_SCAN_SEQUENCE_ID_HIGH			2
#define BYTE_DATA_SCAN_SEQUENCE_ID_LOW			3
#define BYTE_DATA_SCAN_COMMAND_ID_HIGH			4
#define BYTE_DATA_SCAN_COMMAND_ID_LOW			5
#define BYTE_DATA_SCAN_GATEWAY					6
#define BYTE_DATA_SCAN_ADDRESS					7

/* Du lieu data scan cua dim packet command id 0021 
 */

#define BYTE_DATA_21_SCAN_DETAIL_PERCENT			13
#define BYTE_DATA_21_SCAN_TARGET_PORT_HIGH			14
#define BYTE_DATA_21_SCAN_TARGET_PORT_LOW			15
#define BYTE_DATA_21_SCAN_REQUEST_ID_HIGH			16
#define BYTE_DATA_21_SCAN_REQUEST_ID_LOW			17

/* Du lieu data scan cua dim packet command id 0025 
 */

#define BYTE_DATA_25_SCAN_REQUEST_ID_HIGH			13
#define BYTE_DATA_25_SCAN_REQUEST_ID_LOW			14
#define BYTE_DATA_25_SCAN_VALUE_PORT				15

/* Du lieu goi tin advertise response 
 */

#define BYTE_DATA_PAYLOAD_ADV_SEQUENCE_HIGH		2
#define BYTE_DATA_PAYLOAD_ADV_SEQUENCE_LOW		3

#define BYTE_DATA_PAYLOAD_ADV_REQUEST_ID_HIGH	13
#define BYTE_DATA_PAYLOAD_ADV_REQUEST_ID_LOW	14

/* Du lieu goi tin advertise hub send status 
 */
	
#define BYTE_DATA_PAYLOAD_ADV_TEMP_0			7
#define BYTE_DATA_PAYLOAD_ADV_TEMP_1			8
#define BYTE_DATA_PAYLOAD_ADV_TEMP_2			9
#define BYTE_DATA_PAYLOAD_ADV_TEMP_3			10

#define BYTE_DATA_PAYLOAD_ADV_STATUS_HIGH		11
#define BYTE_DATA_PAYLOAD_ADV_STATUS_LOW		12

#define PERIODICAL_TIME						19

#define NORMAL_PORT_STATUS					0xff

#define EQUAL			0
#define DIFFERENT		1

#define DEVICE_NAME CONFIG_BT_DEVICE_NAME
#define DEVICE_NAME_LEN (sizeof(DEVICE_NAME) - 1)

#define STACKSIZE 		512
#define PRIORITY 		7
#define PRIORITY_ADV	8

struct data_hub 
{
	u16_t	company_id;
	u16_t 	sequence_id;
	u16_t	command_id;
	u8_t	gateway;
	u8_t 	addr_ad[6];
	u16_t	request_id;

	// u8_t 	detail[14]; /* 0025 */

	u8_t 	detail;		  /* 0021 */
	u16_t 	target_port;  /* 0021 */
};

// struct data_status
// {
// 	int 	_unused;
// 	u32_t 	temp_status;
// 	u16_t 	port_hub_status;
// };

struct data_dimming
{
	int 	_unused;
	u8_t 	value[16];
};

struct data_hub pre_packet;

struct data_dimming dimming_info;

struct bt_le_oob addr_local;

void bluetooth_init(void);

void process_node(int op_adv);

void advertise(int options, struct k_sem *my_sem);

void print_addr(void);

#ifdef __cplusplus
}
#endif

#endif /*_BLUETOOTH_H_*/
