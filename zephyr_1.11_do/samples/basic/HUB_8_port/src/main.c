/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <device.h>
#include <nrf_pwm.h>

#include "header/pwm_hw.h"
#include "header/pwm.h"
#include "header/bluetooth.h"
#include "header/thread.h"
#include "header/watch_dog.h"

void sys_init(void)
{
	watchdog_config(6000, 3);

	enable_pin();

	pwm_hub_init();

	poll_event_init();

	thread_init();
	
	bluetooth_init();
}

void main(void)
{
	printk("[Main] ---->>>_____HUB_____<<<---- \n");

	sys_init();
}
