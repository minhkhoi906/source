
/* AUTO-GENERATED by gen_isr_tables.py, do not edit! */

#include <toolchain.h>
#include <linker/sections.h>
#include <sw_isr_table.h>

u32_t __irq_vector_table _irq_vector_table[39] = {
	0x18e5,
	0x9523,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
	0x18e5,
};
struct _isr_table_entry __sw_isr_table _sw_isr_table[39] = {
	{(void *)0x0, (void *)0x2005},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x20003688, (void *)0x21e9},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x94e1},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x951f},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x24d1},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x9519},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
	{(void *)0x0, (void *)0x170d},
};
