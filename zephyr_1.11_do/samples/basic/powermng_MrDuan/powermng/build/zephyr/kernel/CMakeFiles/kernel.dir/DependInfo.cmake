# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/Users/duandn/zephyr/kernel/alert.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/alert.c.obj"
  "/Users/duandn/zephyr/kernel/device.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/device.c.obj"
  "/Users/duandn/zephyr/kernel/errno.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/errno.c.obj"
  "/Users/duandn/zephyr/kernel/idle.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/idle.c.obj"
  "/Users/duandn/zephyr/kernel/init.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/init.c.obj"
  "/Users/duandn/zephyr/kernel/mailbox.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/mailbox.c.obj"
  "/Users/duandn/zephyr/kernel/mem_slab.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/mem_slab.c.obj"
  "/Users/duandn/zephyr/kernel/mempool.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/mempool.c.obj"
  "/Users/duandn/zephyr/kernel/msg_q.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/msg_q.c.obj"
  "/Users/duandn/zephyr/kernel/mutex.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/mutex.c.obj"
  "/Users/duandn/zephyr/kernel/pipes.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/pipes.c.obj"
  "/Users/duandn/zephyr/kernel/poll.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/poll.c.obj"
  "/Users/duandn/zephyr/kernel/queue.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/queue.c.obj"
  "/Users/duandn/zephyr/kernel/sched.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/sched.c.obj"
  "/Users/duandn/zephyr/kernel/sem.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/sem.c.obj"
  "/Users/duandn/zephyr/kernel/smp.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/smp.c.obj"
  "/Users/duandn/zephyr/kernel/stack.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/stack.c.obj"
  "/Users/duandn/zephyr/kernel/sys_clock.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/sys_clock.c.obj"
  "/Users/duandn/zephyr/kernel/system_work_q.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/system_work_q.c.obj"
  "/Users/duandn/zephyr/kernel/thread.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/thread.c.obj"
  "/Users/duandn/zephyr/kernel/thread_abort.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/thread_abort.c.obj"
  "/Users/duandn/zephyr/kernel/timer.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/timer.c.obj"
  "/Users/duandn/zephyr/kernel/version.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/version.c.obj"
  "/Users/duandn/zephyr/kernel/work_q.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/kernel/CMakeFiles/kernel.dir/work_q.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "KERNEL"
  "NRF52832_XXAA"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../../kernel/include"
  "../../../../arch/arm/include"
  "../../../../arch/arm/soc/nordic_nrf5/nrf52"
  "../../../../arch/arm/soc/nordic_nrf5/nrf52/include"
  "../../../../arch/arm/soc/nordic_nrf5/include"
  "../../../../boards/arm/nrf52_pca10040"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "/Users/duandn/gcc-arm-none-eabi-5_3-2016q1/bin/../lib/gcc/arm-none-eabi/5.3.1/include"
  "/Users/duandn/gcc-arm-none-eabi-5_3-2016q1/bin/../lib/gcc/arm-none-eabi/5.3.1/include-fixed"
  "../../../../lib/libc/minimal/include"
  "../../../../ext/hal/cmsis/Include"
  "../../../../ext/hal/nordic/nrfx"
  "../../../../ext/hal/nordic/nrfx/drivers/include"
  "../../../../ext/hal/nordic/nrfx/hal"
  "../../../../ext/hal/nordic/nrfx/mdk"
  "../../../../ext/hal/nordic/."
  "../../../../subsys/bluetooth"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
