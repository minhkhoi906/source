# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "ASM"
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_ASM
  "/Users/duandn/zephyr/arch/arm/core/cortex_m/nmi_on_reset.S" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/cortex_m/nmi_on_reset.S.obj"
  "/Users/duandn/zephyr/arch/arm/core/cortex_m/reset.S" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/cortex_m/reset.S.obj"
  "/Users/duandn/zephyr/arch/arm/core/cortex_m/vector_table.S" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/cortex_m/vector_table.S.obj"
  "/Users/duandn/zephyr/arch/arm/core/cpu_idle.S" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/cpu_idle.S.obj"
  "/Users/duandn/zephyr/arch/arm/core/exc_exit.S" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/exc_exit.S.obj"
  "/Users/duandn/zephyr/arch/arm/core/fault_s.S" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/fault_s.S.obj"
  "/Users/duandn/zephyr/arch/arm/core/isr_wrapper.S" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/isr_wrapper.S.obj"
  "/Users/duandn/zephyr/arch/arm/core/swap.S" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/swap.S.obj"
  )
set(CMAKE_ASM_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_ASM
  "KERNEL"
  "NRF52832_XXAA"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_ASM_TARGET_INCLUDE_PATH
  "../../../../kernel/include"
  "../../../../arch/arm/include"
  "../../../../arch/arm/soc/nordic_nrf5/nrf52"
  "../../../../arch/arm/soc/nordic_nrf5/nrf52/include"
  "../../../../arch/arm/soc/nordic_nrf5/include"
  "../../../../boards/arm/nrf52_pca10040"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "/Users/duandn/gcc-arm-none-eabi-5_3-2016q1/bin/../lib/gcc/arm-none-eabi/5.3.1/include"
  "/Users/duandn/gcc-arm-none-eabi-5_3-2016q1/bin/../lib/gcc/arm-none-eabi/5.3.1/include-fixed"
  "../../../../lib/libc/minimal/include"
  "../../../../ext/hal/cmsis/Include"
  "../../../../ext/hal/nordic/nrfx"
  "../../../../ext/hal/nordic/nrfx/drivers/include"
  "../../../../ext/hal/nordic/nrfx/hal"
  "../../../../ext/hal/nordic/nrfx/mdk"
  "../../../../ext/hal/nordic/."
  "../../../../subsys/bluetooth"
  )
set(CMAKE_DEPENDS_CHECK_C
  "/Users/duandn/zephyr/arch/arm/core/cortex_m/exc_manage.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/cortex_m/exc_manage.c.obj"
  "/Users/duandn/zephyr/arch/arm/core/cortex_m/mpu/arm_core_mpu.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/cortex_m/mpu/arm_core_mpu.c.obj"
  "/Users/duandn/zephyr/arch/arm/core/cortex_m/mpu/arm_mpu.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/cortex_m/mpu/arm_mpu.c.obj"
  "/Users/duandn/zephyr/arch/arm/core/cortex_m/nmi.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/cortex_m/nmi.c.obj"
  "/Users/duandn/zephyr/arch/arm/core/cortex_m/prep_c.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/cortex_m/prep_c.c.obj"
  "/Users/duandn/zephyr/arch/arm/core/cortex_m/scb.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/cortex_m/scb.c.obj"
  "/Users/duandn/zephyr/arch/arm/core/fatal.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/fatal.c.obj"
  "/Users/duandn/zephyr/arch/arm/core/fault.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/fault.c.obj"
  "/Users/duandn/zephyr/arch/arm/core/irq_init.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/irq_init.c.obj"
  "/Users/duandn/zephyr/arch/arm/core/irq_manage.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/irq_manage.c.obj"
  "/Users/duandn/zephyr/arch/arm/core/sys_fatal_error_handler.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/sys_fatal_error_handler.c.obj"
  "/Users/duandn/zephyr/arch/arm/core/thread.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/thread.c.obj"
  "/Users/duandn/zephyr/arch/arm/core/thread_abort.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/thread_abort.c.obj"
  "/Users/duandn/zephyr/arch/arm/soc/nordic_nrf5/nrf52/mpu_regions.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/soc/nordic_nrf5/nrf52/mpu_regions.c.obj"
  "/Users/duandn/zephyr/arch/arm/soc/nordic_nrf5/nrf52/power.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/soc/nordic_nrf5/nrf52/power.c.obj"
  "/Users/duandn/zephyr/arch/arm/soc/nordic_nrf5/nrf52/soc.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/soc/nordic_nrf5/nrf52/soc.c.obj"
  "/Users/duandn/zephyr/arch/common/isr_tables.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/arch/common/isr_tables.c.obj"
  "/Users/duandn/zephyr/drivers/clock_control/nrf5_power_clock.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/drivers/clock_control/nrf5_power_clock.c.obj"
  "/Users/duandn/zephyr/drivers/console/uart_console.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/drivers/console/uart_console.c.obj"
  "/Users/duandn/zephyr/drivers/gpio/gpio_nrf5.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/drivers/gpio/gpio_nrf5.c.obj"
  "/Users/duandn/zephyr/drivers/serial/uart_nrf5.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/drivers/serial/uart_nrf5.c.obj"
  "/Users/duandn/zephyr/drivers/timer/nrf_rtc_timer.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/drivers/timer/nrf_rtc_timer.c.obj"
  "/Users/duandn/zephyr/drivers/timer/sys_clock_init.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/drivers/timer/sys_clock_init.c.obj"
  "/Users/duandn/zephyr/lib/crc/crc16_sw.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/lib/crc/crc16_sw.c.obj"
  "/Users/duandn/zephyr/lib/crc/crc8_sw.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/lib/crc/crc8_sw.c.obj"
  "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/misc/generated/configs.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/misc/generated/configs.c.obj"
  "/Users/duandn/zephyr/misc/printk.c" "/Users/duandn/zephyr/samples/basic/powermng/build/zephyr/CMakeFiles/zephyr.dir/misc/printk.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "KERNEL"
  "NRF52832_XXAA"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../../kernel/include"
  "../../../../arch/arm/include"
  "../../../../arch/arm/soc/nordic_nrf5/nrf52"
  "../../../../arch/arm/soc/nordic_nrf5/nrf52/include"
  "../../../../arch/arm/soc/nordic_nrf5/include"
  "../../../../boards/arm/nrf52_pca10040"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "/Users/duandn/gcc-arm-none-eabi-5_3-2016q1/bin/../lib/gcc/arm-none-eabi/5.3.1/include"
  "/Users/duandn/gcc-arm-none-eabi-5_3-2016q1/bin/../lib/gcc/arm-none-eabi/5.3.1/include-fixed"
  "../../../../lib/libc/minimal/include"
  "../../../../ext/hal/cmsis/Include"
  "../../../../ext/hal/nordic/nrfx"
  "../../../../ext/hal/nordic/nrfx/drivers/include"
  "../../../../ext/hal/nordic/nrfx/hal"
  "../../../../ext/hal/nordic/nrfx/mdk"
  "../../../../ext/hal/nordic/."
  "../../../../subsys/bluetooth"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
