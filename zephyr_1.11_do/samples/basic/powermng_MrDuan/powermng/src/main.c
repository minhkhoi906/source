/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#include "header/define.h"
#include "header/utility.h"
#include "header/relay.h"
#include "header/spim_0.h"
#include "header/watchdog.h"

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>
#include <misc/byteorder.h>
#include <debug/object_tracing.h>


struct relay_data {
	void *fifo_reserved; /* 1st word reserved for use by fifo */
	uint8_t hub_add;
	uint8_t state;
};

struct adv_data {
	void *fifo_reserved; /* 1st word reserved for use by fifo */
	uint8_t adv_type;
};

struct device *gpio_dev;

struct relay_f _relay[MAX_RELAY];
struct k_timer watchdog_timer;

struct k_timer relay_timer;

static struct k_work relay_work;
struct relay_data relay_data_t; 


K_FIFO_DEFINE(relay_fifo);
K_FIFO_DEFINE(adv_fifo);
K_SEM_DEFINE(thread_sem, 1, 100);	



struct k_poll_event event_t;
struct k_poll_event adv_event = 
	K_POLL_EVENT_STATIC_INITIALIZER(K_POLL_TYPE_FIFO_DATA_AVAILABLE,
                                    K_POLL_MODE_NOTIFY_ONLY,
                                    &adv_fifo, 0);



struct k_poll_signal signal;

struct k_poll_event events = 
        K_POLL_EVENT_INITIALIZER(K_POLL_TYPE_SIGNAL,
                                 K_POLL_MODE_NOTIFY_ONLY,
                                 &signal);




static inline int thread_monitoring(void)
{
	int obj_counter = 0;
	int obj_main_counter = 0;
	struct k_thread *thread_list = NULL;

	/* wait a bit to allow any initialization-only threads to terminate */

	thread_list   = (struct k_thread *)SYS_THREAD_MONITOR_HEAD;
	while (thread_list != NULL) {

		if(thread_list->base.prio == PRIORITY_ADC || thread_list->base.prio == PRIORITY){
			obj_main_counter++;
		}

		thread_list =
			(struct k_thread *)SYS_THREAD_MONITOR_NEXT(thread_list);
		obj_counter++;
	}
	// printk("THREAD QUANTITY: %d,QUANTITY_MAIN: %d\n", obj_counter,obj_main_counter);


	return obj_main_counter;
}



static void set_param_advertise_nconn(void)
{
    bt_le_oob_get_local(&addr_local);
    
    param_adv_nconn.options = 0;
    param_adv_nconn.interval_min = BT_GAP_ADV_FAST_INT_MIN_1;
    param_adv_nconn.interval_max = BT_GAP_ADV_FAST_INT_MAX_1;
    param_adv_nconn.own_addr = &addr_local.addr.a;

	memcpy(_address, &addr_local.addr.a, sizeof(bt_addr_t));

	printk("[DEVICE ADDRESS] ");
	for(int i = 0; i < 6; i++){
		printk("%02X: ",_address[i]);
	}
	printk("\n");
}



static void ad_parse(struct net_buf_simple *ad)
{
	while (ad->len > 1) {
		u8_t len = net_buf_simple_pull_u8(ad);
		u8_t type;

		/* Check for early termination */
		if (len == 0) {
			return;
		}

		if (len > ad->len) {
			return;
		}

		type = net_buf_simple_pull_u8(ad);


		net_buf_simple_pull(ad, len - 1);
	}
}


void relay_control(struct relay_data relay_t){

	bool state = relay_t.state;
	uint8_t hub_add = relay_t.hub_add;

	for(int i = 0; i < MAX_RELAY; i++){
		if((hub_add >> i) & 0x01){
			relay_enable(_relay[i],state,gpio_dev);
		}
	}

}

static void scan_cb(const bt_addr_le_t *addr, s8_t rssi, u8_t type,
		    struct net_buf_simple *ad)
{


	static uint16_t sequence_ID = 0;
	static uint16_t request_ID = 0;

	if (type !=  BT_LE_ADV_NONCONN_IND) {
		return;
	}

	if(ad->len > 1) {

		char *buff = k_malloc(ad->len); 
		memcpy(buff, ad->data, ad->len);
		ad_parse(ad);

		struct package_t *package = (struct package_t*)buff;

		struct package_backup *package_bk = (struct package_backup*)buff;

		if(package->manufacturer != 0xFF){
			// printk("manufacturer not match\r\n");
			goto exit;
		}

		if(to_litle(package->company_ID) != 0xFF0F){
			// printk("company_ID not match\r\n");
			goto exit;
		}


		uint8_t add_tmp[6];
		memcpy(add_tmp, package->add, 6);
		swap_6byte(add_tmp);
		

		// if(to_litle(package_bk->command_ID) == EVENT_BACKUP_POWER){
		// 	printk("command_ID = EVENT_BACKUP_POWER  \r\n");


		// if(memcmp(_address,add_tmp,6) != 0 ){
		// 		printk("address not match\r\n");
		// 	goto exit;
		// }

		// 	is_getting_pw = false;
		// 	_energy = package_bk->energy;
		// 	goto exit;
		// }



		if(to_litle(package->command_ID) != EVENT_CONTROL_HUB){
			// printk("command_ID not match: %02X\r\n",package->command_ID);
			goto exit;
		}

		if(memcmp(_address,add_tmp,6) != 0 ){
				printk("address not match: \r\naddress 1:\r\n");
				for(int i = 0; i < 6; i++){
					printk("%02X ",add_tmp[i]);
				}
			goto exit;
		}



		if(request_ID != package->request_ID){

			if(sequence_ID != package->sequence_ID){

				relay_cnt = DELAY_TMP;

				// k_work_submit(&relay_work);

				// printk("\r\n[SCAN CB]\nhub_add= %02X state = %d", package->hub_add, package->state);


	   			k_poll_signal(&signal, ADV_RESPONSE);
		    		

				relay_data_t.hub_add = package->hub_add;
				relay_data_t.state = package->state;

				_sequence_Id_res = sequence_ID;
				_rq_Id_res = package->request_ID;

				relay_control(relay_data_t);

			}

		}



		sequence_ID = package->sequence_ID;	
		request_ID = package->request_ID;	



exit:
		k_free(buff);

	}
}


static void bt_ready(int err)
{
	if (err) {
		printk("Bluetooth init failed (err %d)\n", err);
		return;
	}
	is_bt_ready = true;

}


int BT_Scan_init(void){
	int err;
	printk("BT_Scan_init ...\n");
	_sequence_Id = 0;
	_sequence_Id_stt = 0;

	/* Initialize the Bluetooth Subsystem */
	err = bt_enable(bt_ready);
	if (err) {
		return err;
	}
	return 0;

}

int BT_advertise_start(uint16_t sequence_Id, uint8_t type){

    uint8_t *ad_buff;
    size_t size = 0;
    int err;
    char *device_name = "NULL";



    switch(type){
    	case ADV_RESPONSE:

    		device_name = "RESPONSE";
    		size = sizeof(_response);
            ad_buff = k_malloc(size);

	        _response[SEQUENCE_OFSET] = _sequence_Id_res>>8;
	        _response[SEQUENCE_OFSET + 1] = _sequence_Id_res;

	        _response[REQUEST_OFSET] = _rq_Id_res>>8;
	        _response[REQUEST_OFSET + 1] = _rq_Id_res;

	        memcpy(ad_buff,_response,size);
    		break;

    	case ADV_REPORT:

    		device_name = "CONSUMP";
    		size = sizeof(_consumpt);
            ad_buff = k_malloc(size);

		    _consumpt[SEQUENCE_OFSET] = sequence_Id>>8;
    		_consumpt[SEQUENCE_OFSET + 1] = sequence_Id;

	        memcpy(ad_buff,_consumpt,size);

    		break;

    	case ADV_STATUS:

    		device_name = "STATUS";
    		size = sizeof(_status);
            ad_buff = k_malloc(size);

    	    _status[SEQUENCE_OFSET] = _sequence_Id_stt>>8;
    		_status[SEQUENCE_OFSET + 1] = _sequence_Id_stt;
	        memcpy(ad_buff,_status,size);




    		break;
    }


	struct bt_data ad_data[] = {
		BT_DATA(BT_DATA_FLAGS, BT_LE_AD_NO_BREDR, 1),
		BT_DATA(BT_DATA_NAME_COMPLETE, device_name, DEVICE_NAME_LEN_MAX),
		BT_DATA(BT_DATA_MANUFACTURER_DATA, ad_buff, size),
	};


	err = bt_le_adv_start(&param_adv_nconn, ad_data, ARRAY_SIZE(ad_data),
							NULL, 0);

	if(ad_buff != NULL){
	    k_free(ad_buff);
	}

    return 0;

}




static void relay_handler(struct k_work *work)
{

	printk("\n k_work handler \n");

	bool state = relay_data_t.state;
	uint8_t hub_add = relay_data_t.hub_add;

	for(int i = 0; i < MAX_RELAY; i++){
		if((hub_add >> i) & 0x01){
			relay_enable(_relay[i],state,gpio_dev);
		}
	}

}



void relay_timer_handler(struct k_timer *dummy){

	static bool toggle_relay= false;

	for(int i = 0; i < MAX_RELAY; i++){
		relay_enable(_relay[i],toggle_relay,gpio_dev);	
	}

	toggle_relay = !toggle_relay;

}

void test_relay(){

	k_timer_start(&relay_timer, K_MSEC(500), K_MSEC(500));

}


int relay_all_init(){

	gpio_dev = device_get_binding(LED_PORT);
	for(int i = 0; i < MAX_RELAY; i++){
		relay_config(&_relay[i],_sets[i]);

		printk("_relay[%d] .set = %d\n", i
				,_relay[i].set);

		relay_init(_relay[i],gpio_dev);
	}

	return 0;

}



void advertising_loop(void){

	printk("[START ADVERTISING LOOP]\n");

	int err;

	uint8_t _startup_cnt = 0;

	bool is_started = false;
	err = BT_Scan_init();
	if(err != 0){
		printk("Bluetooth init failed (err %d)\n", err);
	}


	while(!is_bt_ready){
		k_sleep(K_MSEC(400));
	}


	err = bt_le_scan_start(&scan_param, scan_cb);  
	if (err) {
		printk("Starting scanning failed (err %d)\n", err);
		return;
	}

	set_param_advertise_nconn();

	k_sleep(K_MSEC(400));

	printk("Bluetooth initialized successfully\n");


    k_poll_signal_init(&signal);

	while(1){

	    err = k_poll(&events, 1, K_FOREVER);

	    if (err == 0) {

		    if (events.signal->result == ADV_RESPONSE) {

		        // printk("\nGot a k_signal ADV_RESPONSE\n");
	        	BT_advertise_start(_sequence_Id_res,ADV_RESPONSE);

		    }else if(events.signal->result == ADV_REPORT){

		        // printk("\nGot a k_signal ADV_REPORT\n");
		    	_sequence_Id++;
	        	BT_advertise_start(_sequence_Id,ADV_REPORT);

		    }else if(events.signal->result == ADV_STATUS){
		        // printk("\nGot a k_signal ADV_STATUS\n");

		        if(_startup_cnt < 3){
					is_started = !is_started;
					_sequence_Id_stt = is_started;
			        _startup_cnt++;
		        }

	        	BT_advertise_start(_sequence_Id_stt,ADV_STATUS);

	        	_sequence_Id_stt++;
	        	if(_sequence_Id_stt == 0){
	        		_sequence_Id_stt++;
	        	}


		    }else{
		        // printk("\nGot a wrong k_signal \n");
		    }

			if (err) {
				printk("Advertising failed to start (err %d)\n", err);
				return;
			}


			k_sleep(K_MSEC(400));

			err = bt_le_adv_stop();
			if (err) {
				printk("Advertising failed to stop (err %d)\n", err);
				return;
			}
	        
	    }
        events.signal->signaled = 0;
        events.state = K_POLL_STATE_NOT_READY;

		k_yield();

	}
}



void watchdog_timer_handler(struct k_timer *dummy)
{
	watchdog_reload();
}

void main(void)
{
	printk("\n[POWER MANAGERMENT]\n");	

	uint32_t timeout_ms = 6000;
	uint8_t thread_num = 0;
	uint8_t *raw_data;
	int cnt = 0;
	int err = 0;
	uint8_t time_t = 0;

	watchdog_init(timeout_ms);
	
	relay_all_init();

	k_work_init(&relay_work, relay_handler);

	//waiting for adversting loop
	k_sleep(1000);

	// test_relay();
	raw_data = k_malloc(SIZE_RAWDATA);

	// init SPI 0
	err = spi_init();
	if(err != 0){
		return;
	}

	while(1){

		thread_num = thread_monitoring();

		if(thread_num < MAX_THREAD){
			printk("\n[POWER MANAGERMENT] dumped core... \nstop reloading watchdog\n");	
		}else{
			watchdog_reload();
		}

		if(relay_cnt > 0) {
			relay_cnt--;
		} else {

			cnt++;

			if(cnt % 2 == 0){

				err = get_data_consumption_t(raw_data);

				if(err == 0)
				{
					memcpy(_consumpt + VOLTAGE_OFSET, raw_data + 1, 2);
					swap_uint16(_consumpt + VOLTAGE_OFSET);

					memcpy(_consumpt + AMPE_OFSET, raw_data + 3, 2);
					swap_uint16(_consumpt + AMPE_OFSET);

					memcpy(_consumpt + ENERGY_OFSET, raw_data + 5, 4);
					swap_uint32(_consumpt + ENERGY_OFSET);

					k_poll_signal(&signal, ADV_REPORT);

				}	


			}else{


				k_poll_signal(&signal, ADV_STATUS);
			}
		}

		k_sleep(4000);
	}

	k_free(raw_data);

}







K_TIMER_DEFINE(relay_timer, relay_timer_handler, NULL);

K_TIMER_DEFINE(watchdog_timer, watchdog_timer_handler, NULL);

K_THREAD_DEFINE(advertising_id, STACKSIZE, advertising_loop, NULL, NULL, NULL,
		PRIORITY, 0, K_NO_WAIT);
