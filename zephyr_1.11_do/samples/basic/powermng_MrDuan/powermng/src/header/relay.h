#ifndef RELAY_H_
#define RELAY_H_

#include <stdbool.h>
#include <gpio.h>
#include <zephyr.h>

#define MAX_RELAY 4


struct relay_f{
	int set;
	int reset;
};


void relay_init(struct relay_f relay, struct device *dev);
void relay_config(struct relay_f *relay, int set);
void relay_enable(struct relay_f relay,bool enable, struct device *dev);
void relay_disable(struct relay_f relay,struct device *dev);

#endif //RELAY_H_