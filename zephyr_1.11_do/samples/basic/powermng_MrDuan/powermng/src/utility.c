#include "header/utility.h"

/* Global variable*/



uint16_t to_litle(uint16_t data){

	uint16_t tmp  =  (data>>8) | (data<<8);
	return tmp;
}


uint16_t to_big(uint16_t data){

	uint16_t tmp  =  (data<<8) | (data>>8);
	return tmp;
}

uint32_t swap_uint16( uint8_t *val)
{
	uint8_t tmp[2];

	tmp[1] = val[0];
	tmp[0] = val[1];

	memcpy(val,tmp,2);
}

uint32_t swap_uint32( uint8_t *val)
{
	uint8_t tmp[4];

	for(int i = 0; i < 4; i++){
		tmp[3 - i] = val[i];
	}
	memcpy(val,tmp,4);
}


void swap_6byte( uint8_t *val)
{
	uint8_t tmp[6];

	for(int i = 0; i < 6; i++){
		tmp[5 - i] = val[i];
	}
	memcpy(val,tmp,6);
}


uint16_t extract_16(uint8_t _first, uint8_t _last){

	uint16_t tmp = ((uint16_t)_first << 8) | _last;
	return tmp; 

}

