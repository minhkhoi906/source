/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include "header/watch_dog.h"

static void wdt_event_handler(void)
{}

void feed_dog(u8_t id_channel)
{
    switch(id_channel)
    {
        case 0:
            nrfx_wdt_channel_feed(_channel_id[0]);
            break;
        case 1:
            nrfx_wdt_channel_feed(_channel_id[1]);
            break;
        case 2:
            nrfx_wdt_channel_feed(_channel_id[2]);
            break;
        case 3:
            nrfx_wdt_channel_feed(_channel_id[3]);
            break;
        case 4:
            nrfx_wdt_channel_feed(_channel_id[4]);
            break;
        case 5:
            nrfx_wdt_channel_feed(_channel_id[5]);
            break;
        case 6:
            nrfx_wdt_channel_feed(_channel_id[6]);
            break;
        case 7:
            nrfx_wdt_channel_feed(_channel_id[7]);
            break;
        default:
            break;
    }  
}

nrfx_err_t watchdog_config(u32_t timeout, u8_t channels)
{
    nrfx_err_t err_code = NRFX_SUCCESS;

    nrfx_wdt_config_t config = NRFX_WDT_DEAFULT_CONFIG;

    config.reload_value = timeout; //defaut 2000ms

    err_code = nrfx_wdt_init(&config, wdt_event_handler);

    if (err_code != NRFX_SUCCESS)
    {
        return err_code;
    }

    for (int i = 0; i < channels; ++i)
    {
        err_code = nrfx_wdt_channel_alloc(&_channel_id[i]);

        if (err_code != NRFX_SUCCESS)
        {
            return err_code;
        }
    }

    nrfx_wdt_enable();

    printk("[WDT] Config watchdog successful \n");

    return err_code;
}


