/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#include "header/define.h"
#include "header/utility.h"
#include "header/relay.h"
#include "header/spim_0.h"
#include "header/watch_dog.h"

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>
#include <misc/byteorder.h>
#include <debug/object_tracing.h>

#define MAX_THREAD 1
struct relay_data {
	void *fifo_reserved; /* 1st word reserved for use by fifo */
	uint8_t hub_add;
	uint8_t state;
};

struct device *gpio_dev;

struct relay_f _relay[MAX_RELAY];

struct relay_data relay_data_t; 

K_SEM_DEFINE(thread_sem, 1, 100);	

struct k_poll_event event_t;

struct k_poll_signal signal;

struct k_poll_event events = 
        K_POLL_EVENT_INITIALIZER(K_POLL_TYPE_SIGNAL,
                                 K_POLL_MODE_NOTIFY_ONLY,
                                 &signal);

static void set_param_advertise_nconn(void)
{
    bt_le_oob_get_local(&addr_local);
    
    param_adv_nconn.options = 0;
    param_adv_nconn.interval_min = BT_GAP_ADV_FAST_INT_MIN_1;
    param_adv_nconn.interval_max = BT_GAP_ADV_FAST_INT_MAX_1;
    param_adv_nconn.own_addr = &addr_local.addr.a;

	memcpy(_address, &addr_local.addr.a, sizeof(bt_addr_t));

	printk("[DEVICE ADDRESS] ");
	for(int i = 0; i < 6; i++){
		printk("%02X: ",_address[5 - i]);
	}
	printk("\n");
}

static void ad_parse(struct net_buf_simple *ad)
{
	while (ad->len > 1) {
		u8_t len = net_buf_simple_pull_u8(ad);
		u8_t type;

		/* Check for early termination */
		if (len == 0) {
			return;
		}

		if (len > ad->len) {
			return;
		}

		type = net_buf_simple_pull_u8(ad);

		net_buf_simple_pull(ad, len - 1);
	}
}

void relay_control(struct relay_data relay_t){

	bool state = relay_t.state;
	uint8_t hub_add = relay_t.hub_add;

	for(int i = 0; i < MAX_RELAY; i++){
		if((hub_add >> i) & 0x01){
			relay_enable(_relay[i], state, gpio_dev);
		}
	}
}

static void scan_cb(const bt_addr_le_t *addr, s8_t rssi, u8_t type,
		    struct net_buf_simple *ad)
{

	static uint16_t sequence_ID = 0;
	static uint16_t request_ID = 0;

	if (type !=  BT_LE_ADV_NONCONN_IND) {
		return;
	}

	if(ad->len > 1) {

		char *buff = k_malloc(ad->len); 
		memcpy(buff, ad->data, ad->len);
		ad_parse(ad);

		struct package_t *package = (struct package_t*)buff;

		if(package->manufacturer != 0xFF){
			// printk("manufacturer not match \n");
			goto exit;
		}

		if(to_litle(package->company_ID) != 0xFF0F){
			// printk("company_ID not match \n");
			goto exit;
		}

		if(to_litle(package->command_ID) != EVENT_CONTROL_HUB){
			// printk("command_ID not match: %04X \n",package->command_ID);
			goto exit;
		}

		uint8_t add_tmp[6];
		memcpy(add_tmp, package->add, 6);
		swap_6byte(add_tmp);
		if(memcmp(_address,add_tmp,6) != 0 ){
			printk("address not match: \n");
			// for(int i = 0; i < 6; i++){
			// 	printk("%02X ",_address[i]);
			// }
			goto exit;
		}

		if(request_ID != package->request_ID){

			if(sequence_ID != package->sequence_ID){

				printk("[SCAN CB] hub_add = %02X state = %d \n", package->hub_add, package->state);
		    		
				relay_data_t.hub_add = package->hub_add;
				relay_data_t.state = package->state;

				_sequence_Id_res = sequence_ID;

				k_poll_signal(&signal, ADV_RESPONSE);

				relay_control(relay_data_t);
			}
		}
		sequence_ID = package->sequence_ID;	
		request_ID = package->request_ID;	

exit:
		k_free(buff);
	}
}

void BT_advertise(uint16_t sequence_Id, uint8_t type){

    uint8_t *ad_buff;
    size_t size;
    int err;
    char *device_name = "NULL";

    switch(type){
    	case ADV_RESPONSE:
    		device_name = "RESPONSE";
    		size = sizeof(_response);
            ad_buff = k_malloc(size);

	        _response[SEQUENCE_OFSET] = _sequence_Id_res >> 8;
	        _response[SEQUENCE_OFSET + 1] = _sequence_Id_res;

	        _response[REQUEST_OFSET] = _rq_Id_res >> 8;
	        _response[REQUEST_OFSET + 1] = _rq_Id_res;

	        memcpy(ad_buff,_response,size);
	        printk("[ADV] ADV_RESPONSE \n");

    		break;

    	case ADV_REPORT:
    		device_name = "CONSUMP";
    		size = sizeof(_consumpt);
            ad_buff = k_malloc(size);

		    _consumpt[SEQUENCE_OFSET] = sequence_Id >> 8;
    		_consumpt[SEQUENCE_OFSET + 1] = sequence_Id;

	        memcpy(ad_buff,_consumpt,size);
		    printk("[ADV] ADV_REPORT \n");

    		break;

    	case ADV_STATUS:
    		device_name = "STATUS";
    		size = sizeof(_status);
            ad_buff = k_malloc(size);
	        memcpy(ad_buff,_status,size);
	        printk("[ADV] ADV_STATUS \n");

    		break;
    }

	struct bt_data ad_data[] = {
		BT_DATA(BT_DATA_FLAGS, BT_LE_AD_NO_BREDR, 1),
		BT_DATA(BT_DATA_NAME_COMPLETE, device_name, DEVICE_NAME_LEN_MAX),
		BT_DATA(BT_DATA_MANUFACTURER_DATA, ad_buff, size),
	};

	err = bt_le_adv_start(&param_adv_nconn, ad_data, ARRAY_SIZE(ad_data),
							NULL, 0);
	if(err) 
	{
		printk("[BT] Advertising report failed to start (err %d)\n", err);
		return;
	}

	k_sleep(K_MSEC(400));

	err = bt_le_adv_stop();
	if (err) {
		printk("Advertising failed to stop (err %d)\n", err);
		return;
	}

	if(ad_buff != NULL){
	    k_free(ad_buff);
	}

    return;
}

int relay_all_init()
{
	gpio_dev = device_get_binding(LED_PORT);

	for(int i = 0; i < MAX_RELAY; i++)
	{
		relay_config(&_relay[i],_sets[i]);

		// printk("_relay[%d] .set = %d\n", i
		// 		,_relay[i].set);

		relay_init(_relay[i],gpio_dev);

		relay_enable(_relay[i], 1, gpio_dev);
	}

	return 0;
}

void bluetooth_init(void)
{
	_sequence_Id = 0;
	_sequence_Id_stt = 0;
	
	int err;

	/* Initialize the Bluetooth Subsystem */
	err = bt_enable(NULL);
	if (err) {
		printk("[BT] Bluetooth init failed (err %d)\n", err);
		return;
	}

	/* Start scanning */
	err = bt_le_scan_start(&scan_param, scan_cb);  
	if (err) {
		printk("Starting scanning failed (err %d)\n", err);
		return;
	}

	set_param_advertise_nconn();

 	BT_advertise(_sequence_Id_stt, ADV_STATUS);

	printk("[BT] Bluetooth initialized successfully\n");
}

void advertising_loop(void)
{
	printk("[START ADVERTISING LOOP]\n");

	int err;

    k_poll_signal_init(&signal);

	while(1)
	{
	    err = k_poll(&events, 1, K_MSEC(1000));

	    if (err == 0) {
		    if (events.signal->result == ADV_RESPONSE) {
	        	BT_advertise(_sequence_Id_res, ADV_RESPONSE);
		    } else if(events.signal->result == ADV_REPORT) {
		    	_sequence_Id++;
	        	BT_advertise(_sequence_Id, ADV_REPORT);
		    } else if(events.signal->result == ADV_STATUS) {
	        	BT_advertise(_sequence_Id_stt, ADV_STATUS);

	        	_sequence_Id_stt++;
	        	if(_sequence_Id_stt == 0){
	        		_sequence_Id_stt++;
	        	}
		    } else {
		        // printk("\nGot a wrong k_signal \n");
		        continue;
		    }

			// printk("[BT] successfully advertised\n");
	        
	    }
        events.signal->signaled = 0;
        events.state = K_POLL_STATE_NOT_READY;

        feed_dog(1);

		k_yield();
	}
}

K_THREAD_STACK_DEFINE(thread_advertise_report_area, STACKSIZE);
K_THREAD_STACK_DEFINE(thread_advertise_loop_area, STACKSIZE);

static struct k_thread report_thread;
static struct k_thread advertise_loop_thread;

static void thread_send_report(void)
{
	printk("[THREAD SEND REPORT] \n");

	// uint32_t timeout_ms = 6000;
	// uint8_t thread_num = 0;
	// uint8_t *raw_data;
	// int cnt = 0;
	// int err;
	
	// raw_data = k_malloc(SIZE_RAWDATA);

	// k_sleep(1000);

	u8_t count = 1; // report sau 2s init

	while(1)
	{
		// cnt++;

		// if(cnt % 1 == 0)
		// {
  //           err = get_data_consumption_t(raw_data);

  //           if(err == 0)
  //           {
  //               memcpy(_consumpt + VOLTAGE_OFSET, raw_data + 1, 2);
  //               swap_uint16(_consumpt + VOLTAGE_OFSET);

  //               memcpy(_consumpt + AMPE_OFSET, raw_data + 3, 2);
  //               swap_uint16(_consumpt + AMPE_OFSET);

  //               memcpy(_consumpt + ENERGY_OFSET, raw_data + 5, 4);
		// 		swap_uint32(_consumpt + ENERGY_OFSET);

  //               k_poll_signal(&signal, ADV_REPORT);
  //           }

		// 	k_sleep(1000);

		// } else {
			// k_poll_signal(&signal, ADV_STATUS);
			// k_sleep(3000);
		// }

		if (!count)
		{
			k_poll_signal(&signal, ADV_STATUS);
			count = 5;
		}
		k_sleep(K_MSEC(1000));
		count--;
		feed_dog(0);

	}

	// k_free(raw_data);
}

void thread_report_init(void)
{
	k_thread_create(&advertise_loop_thread, 
					thread_advertise_loop_area, 
					K_THREAD_STACK_SIZEOF(thread_advertise_loop_area), 
					advertising_loop,
					NULL, NULL, NULL,
					K_PRIO_COOP(8), 0, K_NO_WAIT);

	k_thread_create(&report_thread, 
					thread_advertise_report_area, 
					K_THREAD_STACK_SIZEOF(thread_advertise_report_area), 
					thread_send_report,
					NULL, NULL, NULL,
					K_PRIO_COOP(8), 0, K_NO_WAIT);
}

void main(void)
{	
	watchdog_config(7000, 2);
	
	relay_all_init();

	/* init SPI 0 */ 
	// int err = spi_init();
	// if(err != 0){
	// 	return;
	// }

	thread_report_init();

	bluetooth_init();
}


