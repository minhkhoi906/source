include($ENV{ZEPHYR_BASE}/cmake/app/boilerplate.cmake NO_POLICY_SCOPE)
project(NONE)

target_sources(app PRIVATE src/main.c)
target_sources(app PRIVATE src/crc16.c)
target_sources(app PRIVATE src/watch_dog.c)

target_sources(app PRIVATE ../../../ext/hal/nordic/nrfx/drivers/src/nrfx_wdt.c)