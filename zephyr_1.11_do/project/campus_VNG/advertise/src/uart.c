#include <uart.h>
#include <net/buf.h>
#include <net/net_pkt.h>

#include "header/uart.h"
#include "header/thread.h"

static int slip_process_byte(unsigned char c)
{
    struct net_buf *buf;

    switch (slip_state) {
        case STATE_GARBAGE:
            if (c == SLIP_END) {
                slip_state = STATE_OK;
            }
            printk("garbage: discard byte %x \n", c);
            return 0;

        case STATE_ESC:
            if (c == SLIP_ESC_END) {
                c = SLIP_END;
            } else if (c == SLIP_ESC_ESC) {
                c = SLIP_ESC;
            } else if (c == SLIP_ESC_XON) {
                c = XON;
            } else if (c == SLIP_ESC_XOFF) {
                c = XOFF;
            } else {
                slip_state = STATE_GARBAGE;
                return 0;
            }
            slip_state = STATE_OK;
            break;

        case STATE_OK:
            if (c == SLIP_ESC) {
                slip_state = STATE_ESC;
                return 0;
            } else if (c == SLIP_END) {
                return 1;
            }
            break;
    }

    if (!pkt_curr) {
        pkt_curr = net_pkt_get_reserve_rx(0, K_NO_WAIT);
        if (!pkt_curr) {
            printk("No more buffers\n");
            return 0;
        }
        buf = net_pkt_get_frag(pkt_curr, K_NO_WAIT);
        if (!buf) {
            printk("No more buffers\n");
            net_pkt_unref(pkt_curr);
            return 0;
        }
        net_pkt_frag_insert(pkt_curr, buf);
    } else {
        buf = net_buf_frag_last(pkt_curr->frags);
    }

    if (!net_buf_tailroom(buf)) {
        printk("No more buf space: buf %p len %u\n", buf, buf->len);

        net_pkt_unref(pkt_curr);
        pkt_curr = NULL;
        return 0;
    }

    net_buf_add_u8(buf, c);

    return 0;
}

static void uart_isr(struct device *dev){

    while (uart_irq_update(dev) && uart_irq_is_pending(dev)) 
    {
        if (uart_irq_rx_ready(dev)) 
        {
            unsigned char byte;

            while (uart_fifo_read(dev, &byte, sizeof(byte))) 
            {
                if (slip_process_byte(byte)) 
                {
                    /**
                     * slip_process_byte() returns 1 on
                     * SLIP_END, even after receiving full
                     * packet
                     */
                    if (!pkt_curr) 
                    {
                        printk("Skip SLIP_END\n");
                        continue;
                    }

                    k_fifo_put(&rx_queue, pkt_curr);
                    pkt_curr = NULL;
                }
            }
        }
    }
}

int init_uart(void)
{
    uart_dev = device_get_binding(CONFIG_UART_NRF5_NAME);
    if (!uart_dev) 
    {
        printk("[UART] Uart was not found\n");
        return -1;
    } else {
        printk("[UART] Uart was found\n");
    }

    u8_t c;

    /* Drain the fifo */
    while (uart_fifo_read(uart_dev, &c, 1)) {
        continue;
    }

    uart_irq_callback_set(uart_dev, uart_isr);

    uart_irq_rx_enable(uart_dev);

    return 0;
}