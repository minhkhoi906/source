#include "header/bluetooth.h"

void advertise(void){

    int err;

    err = bt_le_adv_start(&param_adv_nconn, ad_data, ARRAY_SIZE(ad_data),
                            NULL, 0);

    k_sleep(K_MSEC(30));

    err = bt_le_adv_stop();
    if (err) 
    {
        printk("Advertising failed to stop (err %d)\n", err);
        return;
    }

    printk("Advertise successful \n");

    return;
}

static void set_param_advertise_nconn(void)
{
    bt_le_oob_get_local(&addr_local);
    
    param_adv_nconn.options = 0;
    param_adv_nconn.interval_min = BT_GAP_ADV_FAST_INT_MIN_1;
    param_adv_nconn.interval_max = BT_GAP_ADV_FAST_INT_MAX_1;
    param_adv_nconn.own_addr = &addr_local.addr.a;

    memcpy(_address, &addr_local.addr.a, sizeof(bt_addr_t));

    // printk("[DEVICE ADDRESS] ");
    // for(int i = 0; i < 6; i++)
    // {
    //  printk("%02X: ",_address[5 - i]);
    // }
    // printk("\n");
}

void init_bt(void)
{
    int err;

    err = bt_enable(NULL);
    if (err) 
    {
        printk("[BT] Bluetooth init failed (err %d)\n", err);
        return;
    }

    set_param_advertise_nconn();
}


