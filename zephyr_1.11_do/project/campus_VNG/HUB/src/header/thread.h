#ifndef __thread__
#define __thread__

/**
 * 
 * 
 * 
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif

struct k_fifo dimming_fifo;

struct k_poll_signal signal_ad;

struct k_poll_event event_dim;
struct k_poll_event event_ad;

void poll_event_init(void);

void thread_init(void);

#ifdef __cplusplus
}
#endif

/**
 * @}
 */

#endif /* __thread__ */