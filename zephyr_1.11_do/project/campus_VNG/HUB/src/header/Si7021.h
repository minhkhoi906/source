#ifndef __Si7021__
#define __Si7021__

/**
 * 
 * 
 * 
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif

#define SENSOR_I2C_ADDR 	0x40
#define I2C_COMMAND 		0xE3

u32_t read_data_Si7021(struct device *i2c_dev);

void read_firmware_version(struct device *i2c_dev);

#ifdef __cplusplus
}
#endif

/**
 * @}
 */

#endif /* __Si7021__ */