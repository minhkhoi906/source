#include <zephyr.h>
#include <device.h>
#include <nrf_pwm.h>

#include "header/pwm_hw.h"
#include "header/pwm_sw.h"
#include "header/pwm.h"
#include "header/bluetooth.h"
#include "header/74LV165.h"
#include "header/Si7021.h"
#include "header/thread.h"
#include "header/watch_dog.h"

void sys_init(void)
{
	watchdog_config(4000, 3);

	enable_74hc595();

	pwm_hub_init();

	poll_event_init();

	thread_init();
	
	bluetooth_init();

	// shift_reg_init();

	// setup_fan_hub(); // hub_rev04
}

void main(void)
{
	printk("[Main] ---->>>_____HUB_____<<<---- \n");

	sys_init();
}
