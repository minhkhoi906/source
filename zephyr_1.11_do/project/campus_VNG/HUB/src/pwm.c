#include <zephyr.h>
#include <misc/printk.h>
#include <device.h>
#include <gpio.h>
#include <nrf_pwm.h>
#include <nrf52832_peripherals.h>
#include <nrf_gpio.h>
#include <nrf52.h>
#include <board.h>

#include "header/pwm_hw.h"
#include "header/pwm_sw.h"
#include "header/pwm.h"

void pwm_hub_init(void)
{
    pwm_init_hw();
    pwm_init_sw();

    printk("[PWM] Init PWM successful \n");
}

int set_percent_pwm(u32_t channel_pin, u16_t percent_period)
{
    if (channel_pin < 12)
    {
        if (set_percent_pwm_hw(channel_pin, percent_period))
        {
            printk("[PWM] Error set_percent_pwm HW \n");
            return 1;
        }
    }
    else if (channel_pin > 11 && channel_pin < 16)
    {
        if (set_percent_pwm_sw(channel_pin, percent_period))
        {
            printk("[PWM] Error set_percent_pwm SW \n");
            return 1;
        }     
    }
    return 0;
}

void control_port_hub(u8_t detail[16])
{
    u16_t value_percent;

    for (u32_t i = 0; i < 16; ++i)
    {
        value_percent = detail[i];
        if (value_percent > 100 || value_percent < 0)
        {
            continue;
        }

        // printk("[PWM] NO.%d: %d \n", i + 1, detail[i]);
        
        if (set_percent_pwm(i, value_percent))
        {
            printk("[PWM] Error channel: %d, percent %d \n", i, value_percent);
        }     
    }
}

#define GPIO_DRV_NAME   "GPIO_0"
#define GPIO_OUT_PIN    25
#define GPIO_NAME       "GPIO_"

void setup_fan_hub(void)
{
    struct device *gpio_dev;
    int ret;
    int running_mode = 1;

    gpio_dev = device_get_binding(GPIO_DRV_NAME);
    if (!gpio_dev) {
        printk("[GPIO] Cannot find %s!\n", GPIO_DRV_NAME);
        return;
    }

    /*
     * Setup GPIO output
     */  
          
    ret = gpio_pin_configure(gpio_dev, GPIO_OUT_PIN, (GPIO_DIR_OUT));
    if (ret) {
        printk("[GPIO] Error configuring " GPIO_NAME "%d!\n", GPIO_OUT_PIN);
    }

    ret = gpio_pin_write(gpio_dev, GPIO_OUT_PIN, running_mode);
    if (ret) {
        printk("[GPIO] Error set " GPIO_NAME "%d!\n", GPIO_OUT_PIN);
    }
}