#include "header/relay.h"

void relay_config(struct relay_f *relay, int set){
	relay->set = set;
}


void relay_init(struct relay_f relay, struct device *dev){

	gpio_pin_configure(dev, relay.set, GPIO_DIR_OUT);

}

void relay_enable(struct relay_f relay, bool enable, struct device *dev)
{
	if(enable){
		gpio_pin_write(dev, relay.set, 1);
	} else {
		gpio_pin_write(dev, relay.set, 0);
	}

}
void relay_disable(struct relay_f relay,struct device *dev) {
	gpio_pin_write(dev, relay.set, 0);
}