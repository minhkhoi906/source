#ifndef __PWM_SW__
#define __PWM_SW__

/**
 * 
 * 
 * 
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif

#define PWM0_GPIOTE_CH      0
#define PWM0_PPI_CH_A       16
#define PWM0_PPI_CH_B       17
#define PWM0_TIMER_CC_NUM   0

#define PWM1_GPIOTE_CH      1
#define PWM1_PPI_CH_A       18
#define PWM1_TIMER_CC_NUM   1


#define PWM2_GPIOTE_CH      2
#define PWM2_PPI_CH_A       13
#define PWM2_PPI_CH_B       3
#define PWM2_TIMER_CC_NUM   2

#define PWM3_GPIOTE_CH      4
#define PWM3_PPI_CH_A       19								
#define PWM3_TIMER_CC_NUM   3

#define PWMN_GPIOTE_CH      {PWM0_GPIOTE_CH, PWM1_GPIOTE_CH, PWM2_GPIOTE_CH, PWM3_GPIOTE_CH}
#define PWMN_PPI_CH_A       {PWM0_PPI_CH_A, PWM1_PPI_CH_A, PWM2_PPI_CH_A, PWM3_PPI_CH_A}
#define PWMN_PPI_CH_B       {PWM0_PPI_CH_B, PWM0_PPI_CH_B, PWM2_PPI_CH_B, PWM2_PPI_CH_B}
#define PWMN_TIMER_CC_NUM   {PWM0_TIMER_CC_NUM, PWM1_TIMER_CC_NUM, PWM2_TIMER_CC_NUM, PWM3_TIMER_CC_NUM}

// TIMER3 reload value. The PWM frequency equals '16000000 / TIMER_RELOAD'
#define TIMER_RELOAD        64000
// The timer CC register used to reset the timer. Be aware that not all timers in the nRF52 have 6 CC registers.
#define TIMER_RELOAD_CC_NUM 5

#define PWM_1_PERCENT_TIMER_RELOAD 640

void pwm_init_sw(void);

/* 0..3 */
int set_percent_pwm_sw(u32_t channel_pin, u16_t percent);

u32_t convert_percent_to_value_sw(u16_t percent);

#ifdef __cplusplus
}
#endif

/**
 * @}
 */

#endif /* __PWM_SW__ */