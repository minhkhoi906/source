#ifndef __PWM_HUB__
#define __PWM_HUB__

/**
 * 
 * 
 * 
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif

/* pin HUB */

/* HW_PIN 12 Channel */
// #define PWM_CHANNEL_0 	12
// #define PWM_CHANNEL_1 	8
// #define PWM_CHANNEL_2 	9
// #define PWM_CHANNEL_3 	7
// #define PWM_CHANNEL_4 	31
// #define PWM_CHANNEL_5 	6	
// #define PWM_CHANNEL_6 	2
// #define PWM_CHANNEL_7 	5
// #define PWM_CHANNEL_8 	16
// #define PWM_CHANNEL_9 	15
// #define PWM_CHANNEL_10 	17
// #define PWM_CHANNEL_11  14

// /* SW_PIN 4 Channel*/
// #define PWM_CHANNEL_12   20
// #define PWM_CHANNEL_13   21
// #define PWM_CHANNEL_14   19
// #define PWM_CHANNEL_15   18


/* pin HUB hub rev 4 */

// /* HW_PIN 12 Channel */
// #define PWM_CHANNEL_0 	29
// #define PWM_CHANNEL_1 	30
// #define PWM_CHANNEL_2 	28
// #define PWM_CHANNEL_3 	27
// #define PWM_CHANNEL_4 	26
// #define PWM_CHANNEL_5 	31	
// #define PWM_CHANNEL_6 	2
// #define PWM_CHANNEL_7 	3
// #define PWM_CHANNEL_8 	11
// #define PWM_CHANNEL_9 	10
// #define PWM_CHANNEL_10 	9
// #define PWM_CHANNEL_11   8

// /* SW_PIN 4 Channel*/
// #define PWM_CHANNEL_12   7
// #define PWM_CHANNEL_13   6
// #define PWM_CHANNEL_14   5
// #define PWM_CHANNEL_15   4

/* pin HUB 1608 rev 1*/

/* HW_PIN 12 Channel */
#define PWM_CHANNEL_0 	18
#define PWM_CHANNEL_1 	29
#define PWM_CHANNEL_2 	19
#define PWM_CHANNEL_3 	30
#define PWM_CHANNEL_4 	20
#define PWM_CHANNEL_5 	31	
#define PWM_CHANNEL_6 	17
#define PWM_CHANNEL_7 	2
#define PWM_CHANNEL_8 	16
#define PWM_CHANNEL_9 	3
#define PWM_CHANNEL_10 	15
#define PWM_CHANNEL_11   4

/* SW_PIN 4 Channel*/
#define PWM_CHANNEL_12   14
#define PWM_CHANNEL_13   5
#define PWM_CHANNEL_14   13
#define PWM_CHANNEL_15   6

void pwm_hub_init(void);

/* thiet lap gia tri phan tram cho cac port[0..15] */
int set_percent_pwm(u32_t channel_pin, u16_t percent_period);

/* Dieu khien cac port led tren hub */
void control_port_hub(u8_t detail[16]); 

/* Dieu khien quat */
void setup_fan_hub(void);

#ifdef __cplusplus
}
#endif

/**
 * @}
 */

#endif /* __PWM_HUB__ */