#ifndef __74LV165__
#define __74LV165__

/**
 * 
 * 
 * 
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif

#define SR_LOAD 4
#define SR_OUT	13
#define SR_CLK	3

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0') 

void shift_reg_init(void);

u16_t read_state(void);

void enable_74hc595(void);

#ifdef __cplusplus
}
#endif

/**
 * @}
 */

#endif /* __74LV165__ */