#include <zephyr.h>
#include <stddef.h>
#include <device.h>
#include <nrf_gpio.h>

#include "header/pwm.h"
#include "header/bluetooth.h"
#include "header/thread.h"

static struct bt_le_adv_param param_adv_nconn;

static u16_t sequence_report = 1;

static u8_t start_up;

static struct bt_le_scan_param scan_param = {
	.type       = BT_HCI_LE_SCAN_PASSIVE,
	.filter_dup = BT_HCI_LE_SCAN_FILTER_DUP_DISABLE,
	.interval   = 0x0010,
	.window     = 0x0010,
};

/* payload cho goi tin response */
static u8_t payload_response[] = {
				  0xff,0x0f, 					/*CompanyID*/
				  0x00,0x00, 					/*SequenceID*/
				  0xff,0x00, 					/*CommandID*/
				  0x00,							/*Send to gateway*/
				  0x00,0x21,					/*Type of device*/
				  0x11,0x22,0x33,0x44,			/*Token*/
				  0x00,0x00,					/*Request ID*/
				  0x00,							/*Reset time*/
				  0x00 							/*Status of device*/
				};


/* payload cho goi tin status fail port, temperature */
static u8_t payload_hub_send_status[] = {
				  0xff,0x0f, 					/*CompanyID*/
				  0x00,0x00, 					/*SequenceID*/
				  0x00,0x27, 					/*CommandID*/
				  0x00,							/*Send to gateway*/
				  0x00,0x00,0x00,0x00,			/*Temperature*/
				  0x00,0x00 					/*Value status all port*/
};

/* payload cho goi tin diem danh */
static u8_t payload_report[] = {
				  0xff,0x0f, 					/*CompanyID*/
				  0x00,0x01, 					/*SequenceID*/
				  0xff,0x00, 					/*CommandID*/
				  0x00,							/*Send to gateway*/
				  0x00,0x21,					/*Type of device*/
				  0x11,0x22,0x33,0x44,			/*Token*/
				  0x00,0x00,					/*Request ID*/
				  0x00,							/*Reset time*/
				  0x00 							/*Status of device*/
				};


/* Du lieu goi tin advertise response */
static struct bt_data ad_res[] = {
	BT_DATA(BT_DATA_FLAGS, BT_LE_AD_NO_BREDR, 1),
	BT_DATA(BT_DATA_NAME_COMPLETE, DEVICE_NAME, DEVICE_NAME_LEN),
	BT_DATA(BT_DATA_MANUFACTURER_DATA, payload_response, 17),
};

/* Du lieu goi tin advertise hub send status(fail port, temperature)  */
static struct bt_data ad_hub[] = {
	BT_DATA(BT_DATA_FLAGS, BT_LE_AD_NO_BREDR, 1),
	BT_DATA(BT_DATA_NAME_COMPLETE, DEVICE_NAME, DEVICE_NAME_LEN),
	BT_DATA(BT_DATA_MANUFACTURER_DATA, payload_hub_send_status, 13),
};

/* Du lieu goi tin advertise report */
static struct bt_data ad_report[] = {
	BT_DATA(BT_DATA_FLAGS, BT_LE_AD_NO_BREDR, 1),
	BT_DATA(BT_DATA_NAME_COMPLETE, DEVICE_NAME, DEVICE_NAME_LEN),
	BT_DATA(BT_DATA_MANUFACTURER_DATA, payload_report, 17),
};

static int compare_address(struct bt_le_oob addr_local,u8_t *addr_ad)
{
	for (int i = 0; i < 6; ++i)
	{
		if (addr_local.addr.a.val[i] != addr_ad[5 - i])
		{
			return DIFFERENT;
		}
	}
	return EQUAL;
}

/* 
 * So sanh cac goi tin giong nhau gui cung luc command 0021
 *
 */

static u8_t filter_scan_packet(struct data_hub cur_packet, struct data_hub pre_packet)
{
	if (pre_packet.sequence_id == 0)
	{
		return DIFFERENT;
	}

	if (pre_packet.sequence_id != cur_packet.sequence_id)
	{
		if (pre_packet.request_id != cur_packet.request_id)
		{
			return DIFFERENT;
		}
	}

	return EQUAL;
}

/*
 * Xu ly phan data trong goi tin scan command 0021
 */

static bool process_found(u8_t type, const u8_t *data, u8_t data_len)
{
	u8_t check_equal_packet;

	struct data_hub hub_packet;

	switch (type)
	{
		case BT_DATA_MANUFACTURER_DATA:

			hub_packet.company_id = (u16_t)data[BYTE_DATA_SCAN_COMPANY_ID_HIGH] << 8 
								   	| (u16_t)data[BYTE_DATA_SCAN_COMPANY_ID_LOW] << 0;

			if (hub_packet.company_id != 0xff0f)
			{
				return false;
			}

			hub_packet.sequence_id = (u16_t)data[BYTE_DATA_SCAN_SEQUENCE_ID_HIGH] << 8 
									| (u16_t)data[BYTE_DATA_SCAN_SEQUENCE_ID_LOW] << 0;
			hub_packet.command_id = (u16_t)data[BYTE_DATA_SCAN_COMMAND_ID_HIGH] << 8 
									| (u16_t)data[BYTE_DATA_SCAN_COMMAND_ID_LOW] << 0;


			if (hub_packet.command_id != 0x0021)
			{
				return false;
			}

			hub_packet.target_port = (u16_t)data[BYTE_DATA_21_SCAN_TARGET_PORT_HIGH] << 8 
									| (u16_t)data[BYTE_DATA_21_SCAN_TARGET_PORT_LOW] << 0;

			hub_packet.request_id = (u16_t)data[BYTE_DATA_21_SCAN_REQUEST_ID_HIGH] << 8 
									| (u16_t)data[BYTE_DATA_21_SCAN_REQUEST_ID_LOW] << 0;

			check_equal_packet = filter_scan_packet(hub_packet, pre_packet);

			if (check_equal_packet == EQUAL)
			{
				return false;
			}

			hub_packet.gateway = data[BYTE_DATA_SCAN_GATEWAY];

			if (hub_packet.gateway != 0x01)
			{
				return false;
			}

			for (int i = 0; i < 7; ++i)
			{
				hub_packet.addr_ad[i] = data[BYTE_DATA_SCAN_ADDRESS + i];
			}

			if (compare_address(addr_local, &hub_packet.addr_ad))
			{
				return false;
			}

			for (int i = 0; i < 16; ++i)
			{
				if ((hub_packet.target_port >> i) & 1)
				{
					dimming_info.value[i] =  data[BYTE_DATA_21_SCAN_DETAIL_PERCENT];
				}
				else
				{
					dimming_info.value[i] =  101; // gia tri khong dimming
				}				
			}

			k_poll_signal(&signal_ad, ADV_RESPONSE);

			size_t size = sizeof(struct data_dimming);
			struct data_dimming *mem_ptr = k_malloc(size);
			memcpy(mem_ptr, &dimming_info, size);
			k_fifo_put(&dimming_fifo, mem_ptr);
			
			pre_packet.request_id = hub_packet.request_id;
			pre_packet.sequence_id = hub_packet.sequence_id;

			break;
		default:
			break;
	}

	return true;
}

/*
 * Xu ly phan data trong goi tin scan command 0025
 */

// static bool process_found(u8_t type, const u8_t *data, u8_t data_len)
// {
// 	u8_t check_equal_packet;

// 	struct data_hub hub_packet;

// 	// printk("type %02x Data [", type);
// 	// for (int i = 0; i < data_len; ++i)
// 	// {
// 	// 	printk("%02x ", data[i]);
// 	// }
// 	// printk("] \n");

// 	switch (type)
// 	{
// 		case BT_DATA_MANUFACTURER_DATA:

// 			hub_packet.company_id = (u16_t)data[BYTE_DATA_SCAN_COMPANY_ID_HIGH] << 8 
// 								   	| (u16_t)data[BYTE_DATA_SCAN_COMPANY_ID_LOW] << 0;

// 			if (hub_packet.company_id != 0xff0f)
// 			{
// 				return false;
// 			}

// 			hub_packet.sequence_id = (u16_t)data[BYTE_DATA_SCAN_SEQUENCE_ID_HIGH] << 8 
// 									| (u16_t)data[BYTE_DATA_SCAN_SEQUENCE_ID_LOW] << 0;
// 			hub_packet.command_id = (u16_t)data[BYTE_DATA_SCAN_COMMAND_ID_HIGH] << 8 
// 									| (u16_t)data[BYTE_DATA_SCAN_COMMAND_ID_LOW] << 0;


// 			if (hub_packet.command_id != 0x0025)
// 			{
// 				return false;
// 			}

// 			hub_packet.request_id = (u16_t)data[BYTE_DATA_25_SCAN_REQUEST_ID_HIGH] << 8 
// 									| (u16_t)data[BYTE_DATA_25_SCAN_REQUEST_ID_LOW] << 0;

// 			check_equal_packet = filter_scan_packet(hub_packet, pre_packet);

// 			if (check_equal_packet == EQUAL)
// 			{
// 				return false;
// 			}

// 			hub_packet.gateway = data[BYTE_DATA_SCAN_GATEWAY];

// 			if (hub_packet.gateway != 0x01)
// 			{
// 				return false;
// 			}

// 			for (int i = 0; i < 7; ++i)
// 			{
// 				hub_packet.addr_ad[i] = data[BYTE_DATA_SCAN_ADDRESS + i];
// 			}

// 			if (compare_address(addr_local, &hub_packet.addr_ad))
// 			{
// 				return false;
// 			}

// 			for (int i = 0; i < 14; ++i)
// 			{
// 				hub_packet.detail[i] = data[BYTE_DATA_25_SCAN_VALUE_PORT + i];
// 			}
			
// 			for (int i = 0; i < 16; ++i)
// 			{
// 				if (i == 0)
// 				{
// 					dimming_info.value[i] =  (hub_packet.detail[i] >> (i + 1)) & 0x7f;					
// 				}
// 				else if (i < 8 && i > 0)
// 				{
// 					dimming_info.value[i] = ((hub_packet.detail[i - 1] << (7 - i)) 
// 											| (hub_packet.detail[i] >> (i + 1))) 
// 											& 0x7f;
// 				}
// 				else if (i > 7 && i < 15)
// 				{
// 					dimming_info.value[i] = ((hub_packet.detail[i - 2] << (15 - i)) 
// 											| (hub_packet.detail[i - 1] >> (i - 7))) 
// 											& 0x7f;									 									
// 				}
// 				else if (i == 15)
// 				{
// 					dimming_info.value[i] = (hub_packet.detail[i - 2] << (15 - i)) & 0x7f;						 
// 				}
// 			}

// 			k_poll_signal(&signal_ad, ADV_RESPONSE);

// 			size_t size = sizeof(struct data_dimming);
// 			struct data_dimming *mem_ptr = k_malloc(size);
// 			memcpy(mem_ptr, &dimming_info, size);
// 			k_fifo_put(&dimming_fifo, mem_ptr);
			
// 			pre_packet.request_id = hub_packet.request_id;
// 			pre_packet.sequence_id = hub_packet.sequence_id;

// 			break;
// 		default:
// 			break;
// 	}

// 	return true;
// }

/* 
 * Tach goi tin scan thanh cac frame de xu ly
 *
 */

static void ad_parse(struct net_buf_simple *ad, 
						   bool (*func)(u8_t type, const u8_t *data,
				  		   u8_t data_len))
{
	while (ad->len > 1) {
		u8_t len = net_buf_simple_pull_u8(ad);
		u8_t type;

		/* Check for early termination */
		if (len == 0) {
			return;
		}

		if (len > ad->len || ad->len < 1) {
			printk("AD malformed\n");
			return;
		}

		type = net_buf_simple_pull_u8(ad);

		if (!func(type, ad->data, len - 1))
		{
			return;
		}

		net_buf_simple_pull(ad, len - 1);
	}
}

/* Ham cb khi scan duoc goi tin
 */

static void device_found(const bt_addr_le_t *addr, s8_t rssi, u8_t type,
			 struct net_buf_simple *ad)
{
	if (type == BT_LE_ADV_NONCONN_IND)
	{
		ad_parse(ad, process_found);
	}
}

/* 
 * thiet lap payload cho goi adv tuy vao option duoc dua vao
 * (op_adv):
 * 		+ ADV_RESPONSE: goi tin response
 *		+ ADV_SEND_HUB_STATUS: goi tin status fail port, temperature
 *
 */

void process_node(int op_adv)
{
	switch(op_adv)
	{
		case ADV_RESPONSE:
			/* set sequence_id cho goi advertise response */
			payload_response[BYTE_DATA_PAYLOAD_ADV_SEQUENCE_HIGH] = (u8_t)(pre_packet.sequence_id >> 8) & 0xff;
			payload_response[BYTE_DATA_PAYLOAD_ADV_SEQUENCE_LOW] = (u8_t)(pre_packet.sequence_id >> 0) & 0xff;

			/* set request_id cho goi advertise */
			payload_response[BYTE_DATA_PAYLOAD_ADV_REQUEST_ID_HIGH] = (u8_t)(pre_packet.request_id >> 8) & 0xff;
			payload_response[BYTE_DATA_PAYLOAD_ADV_REQUEST_ID_LOW] = (u8_t)(pre_packet.request_id >> 0) & 0xff;

			return;
		case ADV_REPORT:

			/* set sequence_id cho goi advertise report */
			payload_report[BYTE_DATA_PAYLOAD_ADV_SEQUENCE_HIGH] = (u8_t)(sequence_report >> 8) & 0xff;
			payload_report[BYTE_DATA_PAYLOAD_ADV_SEQUENCE_LOW] = (u8_t)(sequence_report >> 0) & 0xff;

			sequence_report++;
			return;

		default:
			return;
	}
}

static void set_param_advertise_nconn(void)
{
	/* Lay dia chi local cua thiet bi */
    bt_le_oob_get_local(&addr_local);

	param_adv_nconn.options = 0;
	param_adv_nconn.interval_min = BT_GAP_ADV_FAST_INT_MIN_1;
	param_adv_nconn.interval_max = BT_GAP_ADV_FAST_INT_MAX_1;
	param_adv_nconn.own_addr = &addr_local.addr.a;
}

void print_addr(void)
{
	printk("[BT] Address local: [%02X:%02X:%02X:%02X:%02X:%02X] \n", addr_local.addr.a.val[5],
																	addr_local.addr.a.val[4],
																	addr_local.addr.a.val[3],
																	addr_local.addr.a.val[2],
																	addr_local.addr.a.val[1],
																	addr_local.addr.a.val[0]);
}

/*
 * thuc hien viec advertise theo option
 * options:
 *		+ ADV_RESPONSE: goi tin response
 *		+ ADV_REPORT: goi tin report
 *
 */

void advertise(int options, struct k_sem *my_sem)
{
	k_sem_take(my_sem, K_FOREVER);

	int err;

	if (options == ADV_RESPONSE)
	{
		if (!start_up)
		{
			k_sleep(400);
			start_up = 1;
			printk("[BT] Start up response \n");
		}

		err = bt_le_adv_start(&param_adv_nconn, 
							  ad_res, 
							  ARRAY_SIZE(ad_res), NULL, 
							  0);
	
		if(err) 
		{
			printk("[BT] Advertising response failed to start (err %d)\n", err);
			k_sem_give(my_sem);
			return;
		}
	}
	else if (options == ADV_REPORT)
	{
		err = bt_le_adv_start(&param_adv_nconn, 
							  ad_report, 
							  ARRAY_SIZE(ad_report), NULL, 
							  0);
	
		if(err) 
		{
			printk("[BT] Advertising report failed to start (err %d)\n", err);
			k_sem_give(my_sem);
			return;
		}
	}

	k_sleep(K_MSEC(400));

	err = bt_le_adv_stop();

	printk("[BT] successfully advertised\n");

	if(err) 
	{
		printk("[BT] Advertising failed to stop (err %d)\n", err);
		k_sem_give(my_sem);
		return;
	}

	k_sem_give(my_sem);
}

static u16_t scale_value(u16_t value_percent)
{
    return (0.8 * value_percent) + 15;
}

void control_port_hub(u8_t detail[16])
{
	int err = bt_le_scan_stop();
	
    u16_t value_percent;

    for (u32_t i = 0; i < 16; ++i)
    {
        value_percent = detail[i];
        if (value_percent > 100 || value_percent < 0) {
            continue;
        } else {
        	/* Dim xuong */
	        if (pre_value[i] > value_percent)
	        {
	        	if (!enable_flag[i])
			    {
			        enable_flag[i] = 1;
			        nrf_gpio_pin_set(enable_port[i]);
			    }
	    		do {
	                pre_value[i] -= 5;
	                if (set_percent_pwm(i, scale_value(pre_value[i]))) {
	                    printk("[PWM] Error channel: %d, percent %d \n", i, value_percent);
	                }
	                // printk("[PWM] NO.%d: %d cur_value: %d\n", i + 1, pre_value[i], value_percent);
	                // k_sleep(5);
	            } while(value_percent < pre_value[i]);
	        } else if (pre_value[i] < value_percent) { /* Dim len */
	    		if (!enable_flag[i])
			    {
			        enable_flag[i] = 1;
			        nrf_gpio_pin_set(enable_port[i]);
			    }
	    		do {
	                pre_value[i] += 5;
	                if (set_percent_pwm(i, scale_value(pre_value[i]))) {
	                    printk("[PWM] Error channel: %d, percent %d \n", i, value_percent);
	                }
	                // printk("[PWM] NO.%d: %d cur_value: %d\n", i + 1, pre_value[i], value_percent);
	                // k_sleep(5);
	            } while(value_percent > pre_value[i]);
	        }
	        else {
	            if (set_percent_pwm(i, scale_value(pre_value[i]))) {
	                printk("[PWM] Error channel: %d, percent %d \n", i, value_percent);
	            }
	        }

	        if (value_percent == 0) {
	            enable_flag[i] = 0;
	            nrf_gpio_pin_clear(enable_port[i]);
	        }
	        printk("[GPIO] enable_flag[%d]: %d\n", i, enable_flag[i]);

	        pre_value[i] = value_percent;
        }
    }

    err = bt_le_scan_start(&scan_param, device_found);

	if (err) {
		printk("Scanning failed to start control hub (err %d)\n", err);
		return;
	}
}

void bluetooth_init(void)
{
	pre_packet.sequence_id = 0x0000;
	pre_packet.request_id = 0x0000;
	start_up = 0;

	int err;

	err = bt_enable(NULL);

	if(err) 
	{
		printk("[BT] Bluetooth init failed (err %d)\n", err);
		return;
	}

	/* Start scanning */
	err = bt_le_scan_start(&scan_param, device_found);

	if (err) {
		printk("Scanning failed to start (err %d)\n", err);
		return;
	}

	set_param_advertise_nconn();

	printk("[BT] Bluetooth initialized\n");

	/* Start advertising */
	err = bt_le_adv_start(&param_adv_nconn, ad_res, ARRAY_SIZE(ad_res), NULL, 0);

	if(err) 
	{
		printk("[BT] Advertising failed to start (err %d)\n", err);
		return;
	}

	k_sleep(K_MSEC(400));

	err = bt_le_adv_stop();
	if(err) 
	{
		printk("[BT] Advertising failed to stop (err %d)\n", err);
		return;
	}

	k_sleep(K_MSEC(5000));
	
	if (!start_up)
	{
		for (int i = 0; i < 16; ++i)
		{
			dimming_info.value[i] =  70; // gia tri khong dimming	
		}

		size_t size = sizeof(struct data_dimming);
		struct data_dimming *mem_ptr = k_malloc(size);
		memcpy(mem_ptr, &dimming_info, size);
		k_fifo_put(&dimming_fifo, mem_ptr);
	}
}