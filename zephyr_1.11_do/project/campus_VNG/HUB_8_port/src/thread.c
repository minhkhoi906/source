#include <zephyr.h>
#include <stddef.h>
#include <device.h>

#include "header/bluetooth.h"
#include "header/thread.h"
#include "header/watch_dog.h"

K_THREAD_STACK_DEFINE(thread_control_port_stact_area, STACKSIZE);

K_THREAD_STACK_DEFINE(thread_advertise_stact_area, STACKSIZE);

K_THREAD_STACK_DEFINE(thread_advertise_report_area, STACKSIZE);

static struct k_thread adv_thread;
static struct k_thread control_port_thread;
static struct k_thread report_thread;

K_SEM_DEFINE(adv_sem, 1, 1);

/* Thread xu ly 
 * advertise
 */

static void thread_advertise(struct k_poll_event *ad_evt, 
							 struct k_sem *caller_sem, 
							 void *unused)
{
	int err;

	int result = 1;

	while(1)
	{

		err = k_poll(ad_evt, 1, K_MSEC(1000));
		if (err == 0)
		{
			result = ad_evt->signal->result;
			if (ad_evt->signal->result == ADV_RESPONSE || ad_evt->signal->result == ADV_REPORT)
			{
				process_node(result);
				advertise(result, caller_sem);
			}
		}

		/* Reinitializing for next call */
		ad_evt->signal->signaled = 0;
		ad_evt->state = K_POLL_STATE_NOT_READY;
		feed_dog(0);
	}
}

/* Thread xu ly 
 * control_port trong dimming_fifo
 */

static void thread_control_hub(struct k_poll_event *dim_evt,
							   void *unused_1,
							   void *unused_2)
{
	int err;
	struct data_dimming *rx;

	while(1)
	{
		err = k_poll(dim_evt, 1, K_MSEC(1000));

		if (err == 0)
		{
			if (dim_evt->state == K_POLL_STATE_FIFO_DATA_AVAILABLE)
			{
				rx = k_fifo_get(&dimming_fifo, 0);
				if (rx)
				{
					control_port_hub(rx->value);
				}
				k_free(rx);
			}
		}
		dim_evt->state = K_POLL_STATE_NOT_READY;
		feed_dog(1);
	}
}

/* Thread xu ly
 * gui goi tin dinh thoi
 */

static void thread_send_report(void)
{
	u8_t count = 2; // report sau 2s init

	while(1)
	{
		if (!count)
		{
			k_poll_signal(&signal_ad, ADV_REPORT);
			count = PERIODICAL_TIME;
		}
		k_sleep(K_MSEC(1000));
		count--;
		feed_dog(2);
	}
}

/* Thread xu ly
 * read shift register, sensor temperature de advertise
 */

// static void thread_send_hub_status(void)
// {
// 	struct device *i2c_dev;

// 	i2c_dev = device_get_binding(CONFIG_I2C_0_NAME);
// 	if (!i2c_dev) {
// 		printk("[Main] Device driver not found.\n");
// 		return;
// 	}

// 	u16_t cout = PERIODICAL_TIME;
// 	u16_t sequence = 0;

// 	k_sleep(K_MSEC(1000));

// 	while(1)
// 	{
// 		control_packet.temp_status = read_data_Si7021(i2c_dev);
// 		control_packet.port_hub_status = read_state();

// 		printk("[BT] temp: %d C, port_hub_status: "BYTE_TO_BINARY_PATTERN""BYTE_TO_BINARY_PATTERN" \n", control_packet.temp_status, 		
// 																										BYTE_TO_BINARY(control_packet.port_hub_status >> 8),
// 																										BYTE_TO_BINARY(control_packet.port_hub_status));

// 		if (control_packet.port_hub_status != NORMAL_PORT_STATUS)
// 		{
// 			control_packet.op_adv = ADV_SEND_HUB_STATUS;

// 			k_sem_take(&fifo_sem, K_FOREVER);
// 			k_fifo_put(&ad_fifo, &control_packet);
// 			k_sem_give(&fifo_sem);
// 			sequence++;
// 			control_packet.sequence_id = sequence;
// 			printk("[BT] ADV_SEND_HUB_STATUS 1\n");
// 		}

// 		if (!cout)
// 		{
// 			k_sleep(K_MSEC(500));
// 			control_packet.op_adv = ADV_SEND_HUB_STATUS;

// 			k_sem_take(&fifo_sem, K_FOREVER);
// 			k_fifo_put(&ad_fifo, &control_packet);
// 			k_sem_give(&fifo_sem);

// 			sequence++;
// 			control_packet.sequence_id = sequence;
// 			printk("[BT] ADV_SEND_HUB_STATUS 2\n");
// 			cout = PERIODICAL_TIME;
// 		}

// 		k_sleep(K_MSEC(1000));
// 		cout--;

// 	}
// }

void poll_event_init(void)
{
	/* FIFO data control port hub */
 	k_fifo_init(&dimming_fifo);

 	k_poll_signal_init(&signal_ad);

	k_poll_event_init(&event_dim,
                      K_POLL_TYPE_FIFO_DATA_AVAILABLE,
                      K_POLL_MODE_NOTIFY_ONLY,
                      &dimming_fifo);

	k_poll_event_init(&event_ad,
                      K_POLL_TYPE_SIGNAL,
                      K_POLL_MODE_NOTIFY_ONLY,
                      &signal_ad);

	printk("[THREAD] Init poll_event_init successful\n");
}

void thread_init(void)
{
	k_thread_create(&adv_thread, 
					thread_advertise_stact_area, 
					K_THREAD_STACK_SIZEOF(thread_advertise_stact_area), 
					thread_advertise,
					&event_ad, &adv_sem, NULL,
					K_PRIO_COOP(8), 0, K_NO_WAIT);

	k_thread_create(&control_port_thread, 
					thread_control_port_stact_area, 
					K_THREAD_STACK_SIZEOF(thread_control_port_stact_area), 
					thread_control_hub,
					&event_dim, NULL, NULL,
					PRIORITY, 0, K_NO_WAIT);

	k_thread_create(&report_thread, 
					thread_advertise_report_area, 
					K_THREAD_STACK_SIZEOF(thread_advertise_report_area), 
					thread_send_report,
					NULL, NULL, NULL,
					K_PRIO_COOP(8), 0, K_NO_WAIT);

	printk("[THREAD] Init thread_init successful\n");
}