#include <zephyr.h>
#include <misc/printk.h>
#include <device.h>
#include <gpio.h>
#include <nrf_pwm.h>
#include <nrf52832_peripherals.h>
#include <nrf_gpio.h>
#include <nrf52.h>
#include <board.h>

#include "header/pwm_hw.h"
#include "header/pwm.h"

void pwm_hub_init(void)
{
    for (int i = 0; i < 16; ++i)
    {
        pre_value[i] = 0;
    }

    pwm_init_hw();

    printk("[PWM] Init PWM successful \n");
}

int set_percent_pwm(u32_t channel_pin, u16_t percent_period)
{    
    if (channel_pin < 8)
    {
        if (set_percent_pwm_hw(channel_pin, percent_period))
        {
            printk("[PWM] Error set_percent_pwm HW \n");
            return 1;
        }
    }

    return 0;
}

void enable_pin(void)
{
    for (int i = 0; i < 8; ++i)
    {
        nrf_gpio_pin_dir_set(enable_port[i], GPIO_PIN_CNF_DIR_Output);
    }
}