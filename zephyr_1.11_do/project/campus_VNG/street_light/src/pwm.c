/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <misc/printk.h>
#include <device.h>
#include <gpio.h>
#include <nrf_pwm.h>
#include <nrf52832_peripherals.h>
#include <nrf_gpio.h>
#include <nrf52.h>
#include <board.h>

#include "header/pwm_hw.h"
#include "header/pwm.h"

#define ENABLE_PIN_1    27
#define ENABLE_PIN_2    30
#define ENABLE_PIN_3    2
#define ENABLE_PIN_4    25
#define ENABLE_PIN_5    18
#define ENABLE_PIN_6    14
#define ENABLE_PIN_7    15
#define ENABLE_PIN_8    17

// u32_t enable_port[8] = {
//                             ENABLE_PIN_1,
//                             ENABLE_PIN_2,
//                             ENABLE_PIN_3,
//                             ENABLE_PIN_4,
//                             ENABLE_PIN_5,
//                             ENABLE_PIN_6,
//                             ENABLE_PIN_7,
//                             ENABLE_PIN_8
// };

u32_t enable_port = 2;

static u8_t enable_flag = 0;

// static u8_t enable_flag[8] = {0,0,0,0,0,0,0,0};

void pwm_hub_init(void)
{
    pwm_init_hw();

    printk("[PWM] Init PWM successful \n");
}

int set_percent_pwm(u32_t channel_pin, u16_t percent_period)
{
    if (!enable_flag)
    {
        enable_flag = 1;
        nrf_gpio_pin_set(enable_port);
    }
    
    if (channel_pin < 8)
    {
        if (set_percent_pwm_hw(channel_pin, percent_period))
        {
            printk("[PWM] Error set_percent_pwm HW \n");
            return 1;
        }
    }

    return 0;
}

static u16_t scale_value(u16_t value_percent)
{
    return (0.8 * value_percent) + 15;
}

void control_port_hub(u8_t detail[16])
{
    u16_t value_percent;

    for (u32_t i = 0; i < 16; ++i)
    {
        value_percent = detail[i];
        if (value_percent > 100 || value_percent < 0) 
        {
            continue;
        }

        printk("[PWM] Control value: %d \n", value_percent);

        if (value_percent == 0)
        {
            enable_flag = 0;
            nrf_gpio_pin_clear(enable_port);
        } else if (set_percent_pwm(i, value_percent)) {
            printk("[PWM] Error channel: %d, percent %d \n", i, value_percent);
        }     
    }
}

void enable_pin(void)
{
    // for (int i = 0; i < 8; ++i)
    // {
    //     nrf_gpio_pin_dir_set(enable_port[i], GPIO_PIN_CNF_DIR_Output);
    // }

    nrf_gpio_pin_dir_set(enable_port, GPIO_PIN_CNF_DIR_Output);
}