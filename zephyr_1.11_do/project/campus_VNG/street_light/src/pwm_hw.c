/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <misc/printk.h>
#include <device.h>
#include <gpio.h>
#include <nrf_pwm.h>
#include <nrf52832_peripherals.h>
#include <nrf_gpio.h>
#include <nrf52.h>
#include <board.h>

#include "header/pwm_hw.h"
#include "header/pwm.h"

u32_t out_pins[8] = {
						PWM_CHANNEL_0,
						PWM_CHANNEL_1,
						PWM_CHANNEL_2,
						PWM_CHANNEL_3,
						PWM_CHANNEL_4,
						PWM_CHANNEL_5,
						PWM_CHANNEL_6,
						PWM_CHANNEL_7
};

static void set_duty_cycle_init(void)
{
	m_seq_values.channel_0 = PWM_COUNTER_TOP;
    m_seq_values.channel_1 = PWM_COUNTER_TOP;
    m_seq_values.channel_2 = PWM_COUNTER_TOP;
    m_seq_values.channel_3 = PWM_COUNTER_TOP;
}

static void pwm_config_gpio(void)
{
	for (int i = 0; i < 8; ++i)
	{
		nrf_gpio_cfg_output(out_pins[i]);
		nrf_gpio_pin_set(out_pins[i]);
	}
}

static void pwm_0_init(void)
{
	/* Assign pins to NRF_PWM0 output channels */
	NRF_PWM0->PSEL.OUT[0] = (out_pins[0] << PWM_PSEL_OUT_PIN_Pos) | (PWM_PSEL_OUT_CONNECT_Connected << PWM_PSEL_OUT_CONNECT_Pos);
	NRF_PWM0->PSEL.OUT[1] = (out_pins[1] << PWM_PSEL_OUT_PIN_Pos) | (PWM_PSEL_OUT_CONNECT_Connected << PWM_PSEL_OUT_CONNECT_Pos);
	NRF_PWM0->PSEL.OUT[2] = (out_pins[2] << PWM_PSEL_OUT_PIN_Pos) | (PWM_PSEL_OUT_CONNECT_Connected << PWM_PSEL_OUT_CONNECT_Pos);
	NRF_PWM0->PSEL.OUT[3] = (out_pins[3] << PWM_PSEL_OUT_PIN_Pos) | (PWM_PSEL_OUT_CONNECT_Connected << PWM_PSEL_OUT_CONNECT_Pos);

	/* Enable NRF_PWM0 */
	NRF_PWM0->ENABLE = (PWM_ENABLE_ENABLE_Enabled << PWM_ENABLE_ENABLE_Pos);
	
	/* Selects operating mode of the wave counter. */
	NRF_PWM0->MODE = (PWM_MODE_UPDOWN_Up << PWM_MODE_UPDOWN_Pos);

	/* Prescaler of PWM_CLK */
	NRF_PWM0->PRESCALER = (PWM_PRESCALER_PRESCALER_DIV_4 << PWM_PRESCALER_PRESCALER_Pos);
	
	/* Value up to which the pulse generator counter counts */
	NRF_PWM0->COUNTERTOP = (PWM_COUNTER_TOP << PWM_COUNTERTOP_COUNTERTOP_Pos);
	
	/* Amount of playback of a loop */
	NRF_PWM0->LOOP = (PWM_LOOP_CNT_Disabled << PWM_LOOP_CNT_Pos);
	
	/* How a sequence is read from RAM and spread to the compare register */
	NRF_PWM0->DECODER = (PWM_DECODER_LOAD_Individual << PWM_DECODER_LOAD_Pos) | (PWM_DECODER_MODE_RefreshCount << PWM_DECODER_MODE_Pos);
	
	/* Define a sequence of PWM 0 duty cycles */
	nrf_pwm_sequence_set(NRF_PWM0, 0, &seq_0);

	/* Start NRF_PWM0 */ 
	NRF_PWM0->TASKS_SEQSTART[0] = 1;
}

static void pwm_1_init(void)
{	
	/* Assign pins to NRF_PWM1 output channels */
	NRF_PWM1->PSEL.OUT[0] = (out_pins[4] << PWM_PSEL_OUT_PIN_Pos) | (PWM_PSEL_OUT_CONNECT_Connected << PWM_PSEL_OUT_CONNECT_Pos);
	NRF_PWM1->PSEL.OUT[1] = (out_pins[5] << PWM_PSEL_OUT_PIN_Pos) | (PWM_PSEL_OUT_CONNECT_Connected << PWM_PSEL_OUT_CONNECT_Pos);
	NRF_PWM1->PSEL.OUT[2] = (out_pins[6] << PWM_PSEL_OUT_PIN_Pos) | (PWM_PSEL_OUT_CONNECT_Connected << PWM_PSEL_OUT_CONNECT_Pos);
	NRF_PWM1->PSEL.OUT[3] = (out_pins[7] << PWM_PSEL_OUT_PIN_Pos) | (PWM_PSEL_OUT_CONNECT_Connected << PWM_PSEL_OUT_CONNECT_Pos);

	/* Enable NRF_PWM1 */
	NRF_PWM1->ENABLE = (PWM_ENABLE_ENABLE_Enabled << PWM_ENABLE_ENABLE_Pos);

	/* Selects operating mode of the wave counter. */
	NRF_PWM1->MODE = (PWM_MODE_UPDOWN_Up << PWM_MODE_UPDOWN_Pos);

	/* Prescaler of PWM_CLK */
	NRF_PWM1->PRESCALER = (PWM_PRESCALER_PRESCALER_DIV_4 << PWM_PRESCALER_PRESCALER_Pos);
	
	/* Value up to which the pulse generator counter counts */
	NRF_PWM1->COUNTERTOP = (PWM_COUNTER_TOP << PWM_COUNTERTOP_COUNTERTOP_Pos);
	
	/* Amount of playback of a loop */
	NRF_PWM1->LOOP = (PWM_LOOP_CNT_Disabled << PWM_LOOP_CNT_Pos);

	/* How a sequence is read from RAM and spread to the compare register */
	NRF_PWM1->DECODER = (PWM_DECODER_LOAD_Individual << PWM_DECODER_LOAD_Pos) | (PWM_DECODER_MODE_RefreshCount << PWM_DECODER_MODE_Pos);

	/* Define a sequence of PWM 0 duty cycles */
	nrf_pwm_sequence_set(NRF_PWM1, 0, &seq_1);

	/* Start NRF_PWM1 */
	NRF_PWM1->TASKS_SEQSTART[0] = 1;
}

void pwm_init_hw(void)
{
	set_duty_cycle_init();
	pwm_config_gpio();

	pwm_0_init();
	pwm_1_init();
}

static void set_duty_cycle_per_channel(u16_t channel, u16_t duty_circle)
{
	switch(channel)
	{
		case 0:
			m_seq_values.channel_0 = duty_circle;
			break;
		case 1:
			m_seq_values.channel_1 = duty_circle;
			break;
		case 2:
			m_seq_values.channel_2 = duty_circle;
			break;
		case 3:
			m_seq_values.channel_3 = duty_circle;
			break;
		default:
			break;
	}
}

u16_t convert_percent_to_duty_circle_hw(u16_t percent)
{
	if (percent > 100)
    {
        return 0;
    }
    else if (percent < 0)
    {
        return PWM_COUNTER_TOP;
    }
    else
    {
    	return PWM_COUNTER_TOP - (PWM_1_PERCENT * percent);
    }
}

int set_percent_pwm_hw(u32_t channel_pin, u16_t percent)
{
	u32_t select_pwm, select_channel;
	u16_t duty_circle;

	duty_circle = convert_percent_to_duty_circle_hw(percent);

	/* PWM[0..2] */
	select_pwm = channel_pin / 4;

	/* channelPWM[0..3] */
	select_channel = channel_pin % 4;

	switch(select_pwm)
	{
		case 0:
			set_duty_cycle_per_channel(select_channel, duty_circle);
			nrf_pwm_sequence_set(NRF_PWM0, 0, &seq_0);
			NRF_PWM0->TASKS_SEQSTART[0] = 1;
			return 0;
		case 1:
			set_duty_cycle_per_channel(select_channel, duty_circle);
			nrf_pwm_sequence_set(NRF_PWM1, 0, &seq_1);
			NRF_PWM1->TASKS_SEQSTART[0] = 1;
			return 0;
		default:
			break;
	}
	
	return 1;
}

