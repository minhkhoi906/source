# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "ASM"
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_ASM
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/core/cortex_m/nmi_on_reset.S" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/cortex_m/nmi_on_reset.S.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/core/cortex_m/reset.S" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/cortex_m/reset.S.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/core/cortex_m/vector_table.S" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/cortex_m/vector_table.S.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/core/cpu_idle.S" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/cpu_idle.S.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/core/exc_exit.S" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/exc_exit.S.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/core/fault_s.S" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/fault_s.S.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/core/isr_wrapper.S" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/isr_wrapper.S.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/core/swap.S" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/swap.S.obj"
  )
set(CMAKE_ASM_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_ASM
  "KERNEL"
  "NRF52832_XXAA"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_ASM_TARGET_INCLUDE_PATH
  "../../../../kernel/include"
  "../../../../arch/arm/include"
  "../../../../arch/arm/soc/nordic_nrf5/nrf52"
  "../../../../arch/arm/soc/nordic_nrf5/nrf52/include"
  "../../../../arch/arm/soc/nordic_nrf5/include"
  "../../../../boards/arm/nrf52_pca10040"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include-fixed"
  "../../../../lib/libc/minimal/include"
  "../../../../ext/lib/crypto/tinycrypt/include"
  "../../../../ext/hal/cmsis/Include"
  "../../../../ext/hal/nordic/nrfx"
  "../../../../ext/hal/nordic/nrfx/drivers/include"
  "../../../../ext/hal/nordic/nrfx/hal"
  "../../../../ext/hal/nordic/nrfx/mdk"
  "../../../../ext/hal/nordic/."
  "../../../../subsys/bluetooth"
  )
set(CMAKE_DEPENDS_CHECK_C
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/core/cortex_m/exc_manage.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/cortex_m/exc_manage.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/core/cortex_m/mpu/arm_core_mpu.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/cortex_m/mpu/arm_core_mpu.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/core/cortex_m/mpu/arm_mpu.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/cortex_m/mpu/arm_mpu.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/core/cortex_m/nmi.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/cortex_m/nmi.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/core/cortex_m/prep_c.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/cortex_m/prep_c.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/core/cortex_m/scb.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/cortex_m/scb.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/core/fatal.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/fatal.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/core/fault.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/fault.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/core/irq_init.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/irq_init.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/core/irq_manage.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/irq_manage.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/core/sys_fatal_error_handler.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/sys_fatal_error_handler.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/core/thread.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/thread.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/core/thread_abort.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/core/thread_abort.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/soc/nordic_nrf5/nrf52/mpu_regions.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/soc/nordic_nrf5/nrf52/mpu_regions.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/soc/nordic_nrf5/nrf52/power.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/soc/nordic_nrf5/nrf52/power.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/arm/soc/nordic_nrf5/nrf52/soc.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/arm/soc/nordic_nrf5/nrf52/soc.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/arch/common/isr_tables.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/arch/common/isr_tables.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/drivers/clock_control/nrf5_power_clock.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/drivers/clock_control/nrf5_power_clock.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/drivers/gpio/gpio_nrf5.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/drivers/gpio/gpio_nrf5.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/drivers/timer/nrf_rtc_timer.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/drivers/timer/nrf_rtc_timer.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/drivers/timer/sys_clock_init.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/drivers/timer/sys_clock_init.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/lib/crypto/tinycrypt/source/aes_decrypt.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/ext/lib/crypto/tinycrypt/source/aes_decrypt.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/lib/crypto/tinycrypt/source/aes_encrypt.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/ext/lib/crypto/tinycrypt/source/aes_encrypt.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/ext/lib/crypto/tinycrypt/source/utils.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/ext/lib/crypto/tinycrypt/source/utils.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/lib/crc/crc16_sw.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/lib/crc/crc16_sw.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/lib/crc/crc8_sw.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/lib/crc/crc8_sw.c.obj"
  "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/misc/generated/configs.c" "/home/khoinlm/KhoiNLM/Zephyr/zephyr_1.11_do/project/campus_VNG/street_light/build/zephyr/CMakeFiles/zephyr.dir/misc/generated/configs.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "KERNEL"
  "NRF52832_XXAA"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../../kernel/include"
  "../../../../arch/arm/include"
  "../../../../arch/arm/soc/nordic_nrf5/nrf52"
  "../../../../arch/arm/soc/nordic_nrf5/nrf52/include"
  "../../../../arch/arm/soc/nordic_nrf5/include"
  "../../../../boards/arm/nrf52_pca10040"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include-fixed"
  "../../../../lib/libc/minimal/include"
  "../../../../ext/lib/crypto/tinycrypt/include"
  "../../../../ext/hal/cmsis/Include"
  "../../../../ext/hal/nordic/nrfx"
  "../../../../ext/hal/nordic/nrfx/drivers/include"
  "../../../../ext/hal/nordic/nrfx/hal"
  "../../../../ext/hal/nordic/nrfx/mdk"
  "../../../../ext/hal/nordic/."
  "../../../../subsys/bluetooth"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
